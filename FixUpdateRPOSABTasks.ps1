﻿###################################################
# Reinstall Local Running RPOS Auto-Updater Tasks #
###################################################

#Grabbing computer version
$computerver = (Get-WmiObject -class Win32_OperatingSystem).Caption


#Two Digit Store Number
$TwoDigPCName = "$ENV:COMPUTERNAME"

if ($TwoDigPCName -match 'S[0-9][0-9][0-9]00') {
	$TwoDigPCName = $TwoDigPCName.Substring(6, 2) }

if ($TwoDigPCName -match 'S[0-9][0-9][0-9]NPC') {
	$TwoDigPCName = $TwoDigPCName.Substring(7, 2) }

function createwin10taska
{
	$taskSecurity = [System.Security.AccessControl.FileSecurity]::new()
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"NT AUTHORITY\System",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Administrators",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Users",
			"ReadAndExecute",
			"Allow"
		)
	)
	
	$taskParams = @{
		Principal = New-ScheduledTaskPrincipal -UserId "TBC\shprorpos" -RunLevel Highest
		Action    = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-command Get-RPOSUpdateRPOS"
		Trigger   = @(
			(New-JobTrigger -AtStartup -RandomDelay (New-TimeSpan -Minutes 1)),
			(New-ScheduledTaskTrigger -At $PCTime1 -Daily),
			(New-ScheduledTaskTrigger -At $PCTime2 -Daily)
		)
		Settings  = New-ScheduledTaskSettingsSet -Hidden -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit (New-TimeSpan -Hours 2)
	}
	
	$taskObj = New-ScheduledTask @taskParams
	
	New-ScheduledJobOption
	$taskObj.SecurityDescriptor = $taskSecurity.Sddl
	
	$taskObj | Register-ScheduledTask -TaskName "Update RPOS Local A" -User "TBC\shprorpos" -Password "z6BX4564%C2#X9T!" | Out-Null
	
}

function createwin10taskb
{
	$taskSecurity = [System.Security.AccessControl.FileSecurity]::new()
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"NT AUTHORITY\System",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Administrators",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Users",
			"ReadAndExecute",
			"Allow"
		)
	)
	
	$taskParams = @{
		Principal = New-ScheduledTaskPrincipal -UserId "TBC\shprorpos2" -RunLevel Highest
		Action    = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-command Get-RPOSUpdateRPOS"
		Trigger   = @(
			(New-JobTrigger -AtStartup -RandomDelay (New-TimeSpan -Minutes 1)),
			(New-ScheduledTaskTrigger -At $PCTime1 -Daily),
			(New-ScheduledTaskTrigger -At $PCTime2 -Daily)
		)
		Settings  = New-ScheduledTaskSettingsSet -Hidden -Disable -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit (New-TimeSpan -Hours 2)
	}
	
	$taskObj = New-ScheduledTask @taskParams
	
	New-ScheduledJobOption
	$taskObj.SecurityDescriptor = $taskSecurity.Sddl
	
	$taskObj | Register-ScheduledTask -TaskName "Update RPOS Local B" -User "TBC\shprorpos2" -Password "k;SW%gILuhSfZbS/" | Out-Null
}

function createtsmaintasksabxml
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-06-16T18:25:06.7320657"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	####################################################################################################
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("BootTrigger"))
	####################################################################################################
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task7Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task8Root.InnerText = "true"
	[System.Xml.XmlElement]$Task9Root = $Task6Root.AppendChild($Taskone1.CreateElement("Delay"))
	$Task9Root.InnerText = "PT1M"
	
	
	[System.Xml.XmlElement]$Task10Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task11Root = $Task10Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task11Root.InnerText = "$PCTime1"
	[System.Xml.XmlElement]$Task12Root = $Task10Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task12Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task13Root = $Task10Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task13Root.InnerText = "true"
	[System.Xml.XmlElement]$Task14Root = $Task10Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task15Root = $Task14Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task15Root.InnerText = "1"
	
	[System.Xml.XmlElement]$Task16Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task17Root = $Task16Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task17Root.InnerText = "$PCTime2"
	[System.Xml.XmlElement]$Task18Root = $Task16Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task18Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task19Root = $Task16Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task19Root.InnerText = "true"
	[System.Xml.XmlElement]$Task20Root = $Task16Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task21Root = $Task20Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task21Root.InnerText = "1"
	
	
	[System.Xml.XmlElement]$Task22Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task23Root = $Task22Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task23Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task24Root = $Task23Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task24Root.InnerText = "TBC\shprorpos2"
	[System.Xml.XmlElement]$Task25Root = $Task23Root.AppendChild($Taskone1.CreateElement("LogonType"))
	$Task25Root.InnerText = "Password"
	[System.Xml.XmlElement]$Task26Root = $Task23Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task26Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task27Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task28Root = $Task27Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task28Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task29Root = $Task27Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task29Root.InnerText = "false"
	[System.Xml.XmlElement]$Task30Root = $Task27Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task30Root.InnerText = "true"
	[System.Xml.XmlElement]$Task31Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task31Root.InnerText = "true"
	[System.Xml.XmlElement]$Task32Root = $Task27Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task33Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task34Root = $Task27Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task35Root = $Task34Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task35Root.InnerText = "true"
	[System.Xml.XmlElement]$Task36Root = $Task34Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task36Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task37Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task37Root.InnerText = "true"
	[System.Xml.XmlElement]$Task38Root = $Task27Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task38Root.InnerText = "true"
	[System.Xml.XmlElement]$Task39Root = $Task27Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task39Root.InnerText = "true"
	[System.Xml.XmlElement]$Task40Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task40Root.InnerText = "false"
	[System.Xml.XmlElement]$Task41Root = $Task27Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task41Root.InnerText = "false"
	[System.Xml.XmlElement]$Task42Root = $Task27Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task42Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task43Root = $Task27Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task43Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task44Root = $Task27Root.AppendChild($Taskone1.CreateElement("RestartOnFailure"))
	[System.Xml.XmlElement]$Task45Root = $Task44Root.AppendChild($Taskone1.CreateElement("Interval"))
	$Task45Root.InnerText = "PT15M"
	[System.Xml.XmlElement]$Task46Root = $Task44Root.AppendChild($Taskone1.CreateElement("Count"))
	$Task46Root.InnerText = "3"
	
	
	[System.Xml.XmlElement]$Task47Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task47Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task48Root = $Task47Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task49Root = $Task48Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task49Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task50Root = $Task48Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task50Root.InnerText = "-command Get-RPOSUpdateRPOS"
	
	$Taskone1.Save("$TasksLocalRPOSABFile")
	
}

$task1 = schtasks /query /TN "Update RPOS Local A"
$task2 = schtasks /query /TN "Update RPOS Local B"

$TestPathRPOSInstaller = Test-Path "C:\temp\installupdaterpos"
$TestPathTempFolder = Test-Path "C:\temp"

if ($TestPathTempFolder -eq $false)
{
New-Item -Path "C:\temp\" -ItemType Directory -Force
}
 
if ($TestPathRPOSInstaller -eq $false)
{
New-Item -Path "C:\temp\installupdaterpos" -ItemType Directory -Force	
}

if ($task1 -eq $null)
{
		if ($computerver -match "Microsoft Windows 10")
		{
		Get-ScheduledTask -TaskName "Update RPOS Local A" | Unregister-ScheduledTask -Confirm:$false
		
		if ($TwoDigPCName -eq "01")
		{
			$script:PCTime1 = "1:00"
			$script:PCTime2 = "4:00"
			createwin10taska
		}
		if ($TwoDigPCName -eq "02")
		{
			$script:PCTime1 = "2:00"
			$script:PCTime2 = "5:00"
			createwin10taska
			
		}
		if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02"))
		{
			$script:PCTime1 = "3:00"
			$script:PCTime2 = "6:00"
			createwin10taska
		}
	}
	Else
	{
		schtasks /delete /TN "Update RPOS Local A" /F
		
		if ($TwoDigPCName -eq "01")
		{
			$script:PCTime1 = "2016-05-02T01:00:00"
			$script:PCTime2 = "2016-05-02T04:00:00"
			$script:TasksLocalRPOSABFile = "C:\temp\installupdaterpos\Update RPOS Local A.xml"
			createtsmaintasksabxml
		}
		
		if ($TwoDigPCName -eq "02")
		{
			$script:PCTime1 = "2016-05-02T02:00:00"
			$script:PCTime2 = "2016-05-02T05:00:00"
			$script:TasksLocalRPOSABFile = "C:\temp\installupdaterpos\Update RPOS Local A.xml"
			createtsmaintasksabxml
		}
		
		if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02"))
		{
			$script:PCTime1 = "2016-05-02T03:00:00"
			$script:PCTime2 = "2016-05-02T06:00:00"
			$script:TasksLocalRPOSABFile = "C:\temp\installupdaterpos\Update RPOS Local A.xml"
			createtsmaintasksabxml
		}
		
		
		schtasks /create /RU "TBC\shprorpos" /RP "z6BX4564%C2#X9T!" /TN "Update RPOS Local A" /XML "C:\temp\installupdaterpos\Update RPOS Local A.xml"
		icacls "C:\Windows\System32\Tasks\Update RPOS Local A" /grant Everyone:RX
		Remove-Item "C:\Windows\Scripts\RPOS\update\Update RPOS Local A.xml" -Force
	}
}

if ($task2 -eq $null)
	{
	if ($computerver -match "Microsoft Windows 10")
	{
		Get-ScheduledTask -TaskName "Update RPOS Local B" | Unregister-ScheduledTask -Confirm:$false
		
		if ($TwoDigPCName -eq "01")
		{
			$script:PCTime1 = "1:00"
			$script:PCTime2 = "4:00"
			createwin10taskb
		}
		if ($TwoDigPCName -eq "02")
		{
			$script:PCTime1 = "2:00"
			$script:PCTime2 = "5:00"
			createwin10taskb
			
		}
		if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02"))
		{
			$script:PCTime1 = "3:00"
			$script:PCTime2 = "6:00"
			createwin10taskb
		}	
	}
	Else
	{
		schtasks /delete /TN "Update RPOS Local B" /F
		
		if ($TwoDigPCName -eq "01")
		{
			$script:PCTime1 = "2016-05-02T01:00:00"
			$script:PCTime2 = "2016-05-02T04:00:00"
			$script:TasksLocalRPOSABFile = "C:\temp\installupdaterpos\Update RPOS Local B.xml"
			createtsmaintasksabxml
		}
		
		if ($TwoDigPCName -eq "02")
		{
			$script:PCTime1 = "2016-05-02T02:00:00"
			$script:PCTime2 = "2016-05-02T05:00:00"
			$script:TasksLocalRPOSABFile = "C:\temp\installupdaterpos\Update RPOS Local B.xml"
			createtsmaintasksabxml
		}
		
		if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02"))
		{
			$script:PCTime1 = "2016-05-02T03:00:00"
			$script:PCTime2 = "2016-05-02T06:00:00"
			$script:TasksLocalRPOSABFile = "C:\temp\installupdaterpos\Update RPOS Local B.xml"
			createtsmaintasksabxml
		}
		
		schtasks /create /RU "TBC\shprorpos2" /RP "k;SW%gILuhSfZbS/" /TN "Update RPOS Local B" /XML "C:\temp\installupdaterpos\Update RPOS Local B.xml"
		icacls "C:\Windows\System32\Tasks\Update RPOS Local B" /grant Everyone:RX
		Remove-Item "C:\Windows\Scripts\RPOS\update\Update RPOS Local B.xml" -Force
		schtasks /change /disable /TN "Update RPOS Local B"
	}
	
}

Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A.xml" -Force
Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B.xml" -Force