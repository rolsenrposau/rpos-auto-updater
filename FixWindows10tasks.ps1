﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2017 v5.4.141
	 Created on:   	8/6/2019 8:27 AM
	 Created by:   	Admin
	 Organization: 	
	 Filename:     	
	===========================================================================
	.DESCRIPTION
		Fix RPOS Auto-Updater Update Local RPOS A and B Tasks in Windows 10
#>
#Grabbing computer version
$computerver = (Get-WmiObject -class Win32_OperatingSystem).Caption

#Two Digit Store Number
$TwoDigPCName = "$ENV:COMPUTERNAME"

if ($TwoDigPCName -match 'S[0-9][0-9][0-9]00') {
	$TwoDigPCName = $TwoDigPCName.Substring(6, 2) }

if ($TwoDigPCName -match 'S[0-9][0-9][0-9]NPC') {
	$TwoDigPCName = $TwoDigPCName.Substring(7, 2) }


function createwin10taska
{
	$taskSecurity = [System.Security.AccessControl.FileSecurity]::new()
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"NT AUTHORITY\System",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Administrators",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Users",
			"ReadAndExecute",
			"Allow"
		)
	)
	
	$taskParams = @{
		Principal = New-ScheduledTaskPrincipal -UserId "TBC\shprorpos" -RunLevel Highest
		Action    = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-command Get-RPOSUpdateRPOS"
		Trigger   = @(
			(New-JobTrigger -AtStartup -RandomDelay (New-TimeSpan -Minutes 1)),
			(New-ScheduledTaskTrigger -At $PCTime1 -Daily),
			(New-ScheduledTaskTrigger -At $PCTime2 -Daily)
		)
		Settings  = New-ScheduledTaskSettingsSet -Hidden -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit (New-TimeSpan -Hours 2)
	}
	
	$taskObj = New-ScheduledTask @taskParams
	
	New-ScheduledJobOption
	$taskObj.SecurityDescriptor = $taskSecurity.Sddl
	
	$taskObj | Register-ScheduledTask -TaskName "Update RPOS Local A" -User "TBC\shprorpos" -Password "z6BX4564%C2#X9T!" | Out-Null
	
}

function createwin10taskb
{
	$taskSecurity = [System.Security.AccessControl.FileSecurity]::new()
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"NT AUTHORITY\System",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Administrators",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Users",
			"ReadAndExecute",
			"Allow"
		)
	)
	
	$taskParams = @{
		Principal = New-ScheduledTaskPrincipal -UserId "TBC\shprorpos2" -RunLevel Highest
		Action    = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-command Get-RPOSUpdateRPOS"
		Trigger   = @(
			(New-JobTrigger -AtStartup -RandomDelay (New-TimeSpan -Minutes 1)),
			(New-ScheduledTaskTrigger -At $PCTime1 -Daily),
			(New-ScheduledTaskTrigger -At $PCTime2 -Daily)
		)
		Settings  = New-ScheduledTaskSettingsSet -Hidden -Disable -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit (New-TimeSpan -Hours 2)
	}
	
	$taskObj = New-ScheduledTask @taskParams
	
	New-ScheduledJobOption
	$taskObj.SecurityDescriptor = $taskSecurity.Sddl
	
	$taskObj | Register-ScheduledTask -TaskName "Update RPOS Local B" -User "TBC\shprorpos2" -Password "k;SW%gILuhSfZbS/" | Out-Null
}


if ($computerver -match "Microsoft Windows 10")
{
	Get-ScheduledTask -TaskName "Update RPOS Local A" | Unregister-ScheduledTask -Confirm:$false
	
	if ($TwoDigPCName -eq "01")
	{
		$script:PCTime1 = "1:00"
		$script:PCTime2 = "4:00"
		createwin10taska
	}
	if ($TwoDigPCName -eq "02")
	{
		$script:PCTime1 = "2:00"
		$script:PCTime2 = "5:00"
		createwin10taska
		
	}
	if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02"))
	{
		$script:PCTime1 = "3:00"
		$script:PCTime2 = "6:00"
		createwin10taska
	}
	
	Get-ScheduledTask -TaskName "Update RPOS Local B" | Unregister-ScheduledTask -Confirm:$false
	
	if ($TwoDigPCName -eq "01")
	{
		$script:PCTime1 = "1:00"
		$script:PCTime2 = "4:00"
		createwin10taskb
	}
	if ($TwoDigPCName -eq "02")
	{
		$script:PCTime1 = "2:00"
		$script:PCTime2 = "5:00"
		createwin10taskb
		
	}
	if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCname -notmatch "02"))
	{
		$script:PCTime1 = "3:00"
		$script:PCTime2 = "6:00"
		createwin10taskb
	}
	
}


