﻿function createsecondrunxml
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2017-02-15T18:57:14.2530215"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "RPOSAU"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("BootTrigger"))
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task7Root.InnerText = "true"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Delay"))
	$Task8Root.InnerText = "PT2M"
	
	[System.Xml.XmlElement]$Task9Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task10Root = $Task9Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task10Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task11Root = $Task10Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task11Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task12Root = $Task10Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task12Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task13Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task14Root = $Task13Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task14Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task15Root = $Task13Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task15Root.InnerText = "true"
	[System.Xml.XmlElement]$Task16Root = $Task13Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task16Root.InnerText = "true"
	[System.Xml.XmlElement]$Task17Root = $Task13Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task17Root.InnerText = "true"
	[System.Xml.XmlElement]$Task18Root = $Task13Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task18Root.InnerText = "false"
	[System.Xml.XmlElement]$Task19Root = $Task13Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task19Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task20Root = $Task13Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task21Root = $Task20Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task21Root.InnerText = "true"
	[System.Xml.XmlElement]$Task22Root = $Task20Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task22Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task23Root = $Task13Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task23Root.InnerText = "true"
	[System.Xml.XmlElement]$Task24Root = $Task13Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task24Root.InnerText = "true"
	[System.Xml.XmlElement]$Task25Root = $Task13Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task25Root.InnerText = "false"
	[System.Xml.XmlElement]$Task26Root = $Task13Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task26Root.InnerText = "false"
	[System.Xml.XmlElement]$Task27Root = $Task13Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task27Root.InnerText = "false"
	[System.Xml.XmlElement]$Task28Root = $Task13Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task28Root.InnerText = "PT1H"
	[System.Xml.XmlElement]$Task29Root = $Task13Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task29Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task30Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task30Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task31Root = $Task30Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task32Root = $Task31Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task32Root.InnerText = "$installrposaufile"
	
	
	$Taskone1.Save("C:\temp\installupdaterpos\Second Install.xml")
	
}

New-Item -ItemType Directory -Path "C:\Temp\" -Force
New-Item -ItemType Directory -Path "C:\temp\installupdaterpos" -Force

#Checking to see if Powershell is the latest version
$PowershellKeys = "HKLM:\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine"
$PSVersioncheck = (Get-ItemProperty -Path $PowershellKeys -Name "PowerShellVersion")."PowerShellVersion"
$PSVersion = $PSVersioncheck.Substring(0, 1)

#Setting up checking for processes
$processpend = 0

if ($PSversion -ge "5")
{
	Write-Output "Your version of Powershell is $PSVersion have a version that is higher than 5.0" | Out-File -Append -FilePath $Log
}
Else
{
	$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/Win7AndW2KR2Powershellv5.zip"
	$PowershellInstallPath = "C:\temp\installupdaterpos\Win7AndW2KR2Powershellv5.zip"
	$Webclient = New-Object System.Net.WebClient
	$Webclient.DownloadFile($URL, $PowershellInstallPath)
	#Invoke-WebRequest -Uri $URL -OutFile $PowershellInstallPath
	#[System.IO.Compression.ZipFile]::ExtractToDirectory($PowershellInstallPath, "C:\temp\installupdaterpos")
	
	# Old Unzipping Powershell version 2.0
	$shell = New-Object -Com shell.application
	$zip = $shell.NameSpace("$PowershellInstallPath")
	foreach ($item in $zip.items())
	{
		$shell.Namespace("C:\temp\installupdaterpos").copyhere($item)
	}
	
	Remove-Item "$PowershellInstallPath" -Force
	
	wusa.exe "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3191566-x64.msu" /quiet /norestart
	while ($processpend -lt 500)
	{
		$processpend += 1
		$ProcessActive = Get-Process wusa -ErrorAction SilentlyContinue
		if ($ProcessActive -eq $null)
		{
			Write-Output "Windows Update is not running" | Out-File -Append -FilePath $Log
			New-Item -ItemType Directory -Path "C:\temp\installupdaterpos" -Force
            $URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/RPOSUpdaterInstaller.exe"
	        $PowershellInstallPath = "C:\temp\installupdaterpos\RPOSUpdaterInstaller.exe"
	        $Webclient = New-Object System.Net.WebClient
	        $Webclient.DownloadFile($URL, $PowershellInstallPath)
            $script:installrposaufile = Get-ChildItem -Path "C:\" -Include "RPOSUpdaterInstaller.exe" -Recurse -ErrorAction SilentlyContinue -Force | Select-Object -ExpandProperty FullName
            createsecondrunxml
			Start-Sleep -seconds 10
			schtasks /create /TN "After Reboot RPOS AU Install" /XML "C:\temp\installupdaterpos\Second Install.xml"
			Restart-Computer -Force
		}
		Else
		{
			Write-Output "Windows Update is running" | Out-File -Append -FilePath $Log
			Start-Sleep -seconds 10
		}
	}
}

New-Item -ItemType Directory -Path "C:\temp\installupdaterpos" -Force
$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/RPOSUpdaterInstaller.exe"
$PowershellInstallPath = "C:\temp\installupdaterpos\RPOSUpdaterInstaller.exe"
$Webclient = New-Object System.Net.WebClient
$Webclient.DownloadFile($URL, $PowershellInstallPath)

Start-Process -FilePath "C:\temp\installupdaterpos\RPOSUpdaterInstaller.exe" 