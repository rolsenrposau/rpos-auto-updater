﻿<#	
	.NOTES
	===========================================================================
	 Created by:   	 Randy Olsen
	 Organization: 	 TBC Corproation
	 Filename:     	 InstallRPOS from Single Script
	 Version:        v2.2
	 Date Of Update: 3/9/2020
	===========================================================================
	.DESCRIPTION
		This is the single and only installer file for the RPOS Auto-Updater
#>

Add-Type -AssemblyName System.IO.Compression.FileSystem

#Other Variables
$script:localfile = "C:\temp\installupdaterpos\updaterpos.zip"
$downloadtimes = "0"
$script:LANDeskKeys = "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"

#Version check keys
$Installerversion = "2.2"
$NewVersion = "3.10"
$previousversion = "3.9"
$previouspreviousversion = "3.8"

#Computer Name
$name = $env:COMPUTERNAME

#Grabbing Pilot PCs List
$pilotpclist1 = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/pilotpcs.xml"
$pilotpclist2 = [xml](New-Object System.Net.WebClient).downloadstring($pilotpclist1)
$script:ppc1 = $pilotpclist2.pilotpcs.pc1
$script:ppc2 = $pilotpclist2.pilotpcs.pc2
$script:ppc3 = $pilotpclist2.pilotpcs.pc3
$script:ppc4 = $pilotpclist2.pilotpcs.pc4
$script:ppc5 = $pilotpclist2.pilotpcs.pc5
$script:ppc6 = $pilotpclist2.pilotpcs.pc6
$script:ppc7 = $pilotpclist2.pilotpcs.pc7
$script:ppc8 = $pilotpclist2.pilotpcs.pc8
$script:ppc9 = $pilotpclist2.pilotpcs.pc9
$script:ppc10 = $pilotpclist2.pilotpcs.pc10
$script:ppc11 = $pilotpclist2.pilotpcs.pc11
$script:ppc12 = $pilotpclist2.pilotpcs.pc12
$script:ppc13 = $pilotpclist2.pilotpcs.pc13
$script:ppc14 = $pilotpclist2.pilotpcs.pc14
$script:ppc15 = $pilotpclist2.pilotpcs.pc15
$script:ppc16 = $pilotpclist2.pilotpcs.pc16
$script:ppc17 = $pilotpclist2.pilotpcs.pc17
$script:ppc18 = $pilotpclist2.pilotpcs.pc18
$script:ppc19 = $pilotpclist2.pilotpcs.pc19
$script:ppc20 = $pilotpclist2.pilotpcs.pc20

function createtsupdateauxml
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-06-16T18:25:06.7320657"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task7Root.InnerText = "2016-06-16T22:00:00"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task8Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task9Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task9Root.InnerText = "true"
	[System.Xml.XmlElement]$Task10Root = $Task6Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task11Root = $Task10Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task11Root.InnerText = "1"
	
	[System.Xml.XmlElement]$Task12Root = $Task5Root.AppendChild($Taskone1.CreateElement("BootTrigger"))
	[System.Xml.XmlElement]$Task13Root = $Task12Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task13Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task14Root = $Task12Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task14Root.InnerText = "true"
	[System.Xml.XmlElement]$Task15Root = $Task12Root.AppendChild($Taskone1.CreateElement("Delay"))
	$Task15Root.InnerText = "PT30M"
	
	[System.Xml.XmlElement]$Task16Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task17Root = $Task16Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task17Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task18Root = $Task17Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task18Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task19Root = $Task17Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task19Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task19Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task20Root = $Task19Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task20Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task21Root = $Task19Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task21Root.InnerText = "false"
	[System.Xml.XmlElement]$Task22Root = $Task19Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task22Root.InnerText = "true"
	[System.Xml.XmlElement]$Task23Root = $Task19Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task23Root.InnerText = "true"
	[System.Xml.XmlElement]$Task24Root = $Task19Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task24Root.InnerText = "false"
	[System.Xml.XmlElement]$Task25Root = $Task19Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task25Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task26Root = $Task19Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task27Root = $Task26Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task27Root.InnerText = "true"
	[System.Xml.XmlElement]$Task28Root = $Task26Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task28Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task29Root = $Task19Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task29Root.InnerText = "true"
	[System.Xml.XmlElement]$Task30Root = $Task19Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task30Root.InnerText = "true"
	[System.Xml.XmlElement]$Task31Root = $Task19Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task31Root.InnerText = "true"
	[System.Xml.XmlElement]$Task32Root = $Task19Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task19Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task33Root.InnerText = "false"
	[System.Xml.XmlElement]$Task34Root = $Task19Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task34Root.InnerText = "P3D"
	[System.Xml.XmlElement]$Task35Root = $Task19Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task35Root.InnerText = "7"
	[System.Xml.XmlElement]$Task36Root = $Task19Root.AppendChild($Taskone1.CreateElement("RestartOnFailure"))
	[System.Xml.XmlElement]$Task37Root = $Task36Root.AppendChild($Taskone1.CreateElement("Interval"))
	$Task37Root.InnerText = "PT1M"
	[System.Xml.XmlElement]$Task38Root = $Task36Root.AppendChild($Taskone1.CreateElement("Count"))
	$Task38Root.InnerText = "3"
	
	[System.Xml.XmlElement]$Task39Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task39Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task40Root = $Task39Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task41Root = $Task40Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task41Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task42Root = $Task40Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task42Root.InnerText = "-command Get-RPOSUpdateAU"
	
	$Taskone1.Save("C:\temp\installupdaterpos\RPOS AU updater.xml")
	
}

function createtsliveupdatexml
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2018-03-12T10:14:50.4432154"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "TBC EOC Department"
	[System.Xml.XmlElement]$Task38Root = $Task2Root.AppendChild($Taskone1.CreateElement("URI"))
	$Task38Root.InnerText = "\Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task39Root = $Task6Root.AppendChild($Taskone1.CreateElement("Repetition"))
	[System.Xml.XmlElement]$Task40Root = $Task39Root.AppendChild($Taskone1.CreateElement("Interval"))
	$Task40Root.InnerText = "PT6H"
	[System.Xml.XmlElement]$Task41Root = $Task39Root.AppendChild($Taskone1.CreateElement("StopAtDurationEnd"))
	$Task41Root.InnerText = "false"
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task7Root.InnerText = "2018-03-12T06:00:00"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task8Root.InnerText = "true"
	
	[System.Xml.XmlElement]$Task9Root = $Task6Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task10Root = $Task9Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task10Root.InnerText = "1"
	
	[System.Xml.XmlElement]$Task11Root = $Task5Root.AppendChild($Taskone1.CreateElement("BootTrigger"))
	[System.Xml.XmlElement]$Task12Root = $Task11Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task12Root.InnerText = "true"
	[System.Xml.XmlElement]$Task13Root = $Task11Root.AppendChild($Taskone1.CreateElement("Delay"))
	$Task13Root.InnerText = "PT6M"
	
	[System.Xml.XmlElement]$Task42Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task43Root = $Task42Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task43Root.InnerText = "$RPOSAULiveUpdateTime"
	[System.Xml.XmlElement]$Task44Root = $Task42Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task44Root.InnerText = "true"
	
	[System.Xml.XmlElement]$Task45Root = $Task42Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task46Root = $Task45Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task46Root.InnerText = "1"
	
	[System.Xml.XmlElement]$Task14Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task15Root = $Task14Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task15Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task16Root = $Task15Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task16Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task17Root = $Task15Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task17Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task18Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task19Root = $Task18Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task19Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task20Root = $Task18Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task20Root.InnerText = "false"
	[System.Xml.XmlElement]$Task21Root = $Task18Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task21Root.InnerText = "true"
	[System.Xml.XmlElement]$Task22Root = $Task18Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task22Root.InnerText = "true"
	[System.Xml.XmlElement]$Task23Root = $Task18Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task23Root.InnerText = "false"
	[System.Xml.XmlElement]$Task24Root = $Task18Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task24Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task25Root = $Task18Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task26Root = $Task25Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task26Root.InnerText = "true"
	[System.Xml.XmlElement]$Task27Root = $Task25Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task27Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task28Root = $Task18Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task28Root.InnerText = "true"
	[System.Xml.XmlElement]$Task29Root = $Task18Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task29Root.InnerText = "true"
	[System.Xml.XmlElement]$Task30Root = $Task18Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task30Root.InnerText = "false"
	[System.Xml.XmlElement]$Task31Root = $Task18Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task31Root.InnerText = "false"
	[System.Xml.XmlElement]$Task32Root = $Task18Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task18Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task33Root.InnerText = "PT12H"
	[System.Xml.XmlElement]$Task34Root = $Task18Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task34Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task35Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task35Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task36Root = $Task35Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task37Root = $Task36Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task37Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task47Root = $Task36Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task47Root.InnerText = "-command Get-RPOSLiveUpdate"
	
	
	$Taskone1.Save("$RPOSLiveUpdateXML")
	
}

function createtsclearlogsxml
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-05-04T09:15:09.7726706"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task7Root.InnerText = "2016-05-04T00:00:00"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task8Root.InnerText = "true"
	[System.Xml.XmlElement]$Task9Root = $Task6Root.AppendChild($Taskone1.CreateElement("ScheduleByMonth"))
	[System.Xml.XmlElement]$Task10Root = $Task9Root.AppendChild($Taskone1.CreateElement("DaysOfMonth"))
	[System.Xml.XmlElement]$Task11Root = $Task10Root.AppendChild($Taskone1.CreateElement("Day"))
	$Task11Root.InnerText = "1"
	[System.Xml.XmlElement]$Task11Root = $Task9Root.AppendChild($Taskone1.CreateElement("Months"))
	[System.Xml.XmlElement]$Task12Root = $Task11Root.AppendChild($Taskone1.CreateElement("January"))
	[System.Xml.XmlElement]$Task13Root = $Task11Root.AppendChild($Taskone1.CreateElement("February"))
	[System.Xml.XmlElement]$Task14Root = $Task11Root.AppendChild($Taskone1.CreateElement("March"))
	[System.Xml.XmlElement]$Task15Root = $Task11Root.AppendChild($Taskone1.CreateElement("April"))
	[System.Xml.XmlElement]$Task16Root = $Task11Root.AppendChild($Taskone1.CreateElement("May"))
	[System.Xml.XmlElement]$Task17Root = $Task11Root.AppendChild($Taskone1.CreateElement("June"))
	[System.Xml.XmlElement]$Task18Root = $Task11Root.AppendChild($Taskone1.CreateElement("July"))
	[System.Xml.XmlElement]$Task19Root = $Task11Root.AppendChild($Taskone1.CreateElement("August"))
	[System.Xml.XmlElement]$Task20Root = $Task11Root.AppendChild($Taskone1.CreateElement("September"))
	[System.Xml.XmlElement]$Task21Root = $Task11Root.AppendChild($Taskone1.CreateElement("October"))
	[System.Xml.XmlElement]$Task22Root = $Task11Root.AppendChild($Taskone1.CreateElement("November"))
	[System.Xml.XmlElement]$Task23Root = $Task11Root.AppendChild($Taskone1.CreateElement("December"))
	
	[System.Xml.XmlElement]$Task24Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task25Root = $Task24Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task25Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task26Root = $Task25Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task26Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task27Root = $Task25Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task27Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task27Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task28Root = $Task27Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task28Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task29Root = $Task27Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task29Root.InnerText = "false"
	[System.Xml.XmlElement]$Task30Root = $Task27Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task30Root.InnerText = "true"
	[System.Xml.XmlElement]$Task31Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task31Root.InnerText = "true"
	[System.Xml.XmlElement]$Task32Root = $Task27Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task33Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task34Root = $Task27Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task35Root = $Task34Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task35Root.InnerText = "true"
	[System.Xml.XmlElement]$Task36Root = $Task34Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task36Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task37Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task37Root.InnerText = "true"
	[System.Xml.XmlElement]$Task38Root = $Task27Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task38Root.InnerText = "true"
	[System.Xml.XmlElement]$Task39Root = $Task27Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task39Root.InnerText = "false"
	[System.Xml.XmlElement]$Task40Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task40Root.InnerText = "false"
	[System.Xml.XmlElement]$Task41Root = $Task27Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task41Root.InnerText = "false"
	[System.Xml.XmlElement]$Task42Root = $Task27Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task42Root.InnerText = "P3D"
	[System.Xml.XmlElement]$Task43Root = $Task27Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task43Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task44Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task44Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task45Root = $Task44Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task46Root = $Task45Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task46Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task47Root = $Task45Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task47Root.InnerText = "-command Get-RPOSRemoveAULogs"
	
	$Taskone1.Save("C:\temp\installupdaterpos\Update RPOS Clear Logs.xml")
}

function createtsrollbackxml
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-05-05T12:15:36.3215604"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task7Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task8Root = $Task7Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task8Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task9Root = $Task7Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task9Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task10Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task12Root = $Task10Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task12Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task13Root = $Task10Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task13Root.InnerText = "false"
	[System.Xml.XmlElement]$Task14Root = $Task10Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task14Root.InnerText = "true"
	[System.Xml.XmlElement]$Task15Root = $Task10Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task15Root.InnerText = "false"
	[System.Xml.XmlElement]$Task16Root = $Task10Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task16Root.InnerText = "false"
	[System.Xml.XmlElement]$Task17Root = $Task10Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task17Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task18Root = $Task10Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task19Root = $Task18Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task19Root.InnerText = "true"
	[System.Xml.XmlElement]$Task20Root = $Task18Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task20Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task21Root = $Task10Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task21Root.InnerText = "true"
	[System.Xml.XmlElement]$Task22Root = $Task10Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task22Root.InnerText = "true"
	[System.Xml.XmlElement]$Task23Root = $Task10Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task23Root.InnerText = "false"
	[System.Xml.XmlElement]$Task24Root = $Task10Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task24Root.InnerText = "false"
	[System.Xml.XmlElement]$Task25Root = $Task10Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task25Root.InnerText = "false"
	[System.Xml.XmlElement]$Task26Root = $Task10Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task26Root.InnerText = "PT8H"
	[System.Xml.XmlElement]$Task27Root = $Task10Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task27Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task28Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task28Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task29Root = $Task28Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task30Root = $Task29Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task30Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task31Root = $Task29Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task31Root.InnerText = "-command Get-RPOSRollback"
	
	$Taskone1.Save("C:\temp\installupdaterpos\Rollback RPOS Local.xml")
	
}

function createtsmaintasksabxml
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-06-16T18:25:06.7320657"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	####################################################################################################
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("BootTrigger"))
	####################################################################################################
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task7Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task8Root.InnerText = "true"
	[System.Xml.XmlElement]$Task9Root = $Task6Root.AppendChild($Taskone1.CreateElement("Delay"))
	$Task9Root.InnerText = "PT1M"
	
	
	[System.Xml.XmlElement]$Task10Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task11Root = $Task10Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task11Root.InnerText = "$PCTime1"
	[System.Xml.XmlElement]$Task12Root = $Task10Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task12Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task13Root = $Task10Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task13Root.InnerText = "true"
	[System.Xml.XmlElement]$Task14Root = $Task10Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task15Root = $Task14Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task15Root.InnerText = "1"
	
	[System.Xml.XmlElement]$Task16Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task17Root = $Task16Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task17Root.InnerText = "$PCTime2"
	[System.Xml.XmlElement]$Task18Root = $Task16Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task18Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task19Root = $Task16Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task19Root.InnerText = "true"
	[System.Xml.XmlElement]$Task20Root = $Task16Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task21Root = $Task20Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task21Root.InnerText = "1"
	
	
	[System.Xml.XmlElement]$Task22Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task23Root = $Task22Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task23Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task24Root = $Task23Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task24Root.InnerText = "TBC\shprorpos2"
	[System.Xml.XmlElement]$Task25Root = $Task23Root.AppendChild($Taskone1.CreateElement("LogonType"))
	$Task25Root.InnerText = "Password"
	[System.Xml.XmlElement]$Task26Root = $Task23Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task26Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task27Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task28Root = $Task27Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task28Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task29Root = $Task27Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task29Root.InnerText = "false"
	[System.Xml.XmlElement]$Task30Root = $Task27Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task30Root.InnerText = "true"
	[System.Xml.XmlElement]$Task31Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task31Root.InnerText = "true"
	[System.Xml.XmlElement]$Task32Root = $Task27Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task33Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task34Root = $Task27Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task35Root = $Task34Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task35Root.InnerText = "true"
	[System.Xml.XmlElement]$Task36Root = $Task34Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task36Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task37Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task37Root.InnerText = "true"
	[System.Xml.XmlElement]$Task38Root = $Task27Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task38Root.InnerText = "true"
	[System.Xml.XmlElement]$Task39Root = $Task27Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task39Root.InnerText = "true"
	[System.Xml.XmlElement]$Task40Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task40Root.InnerText = "false"
	[System.Xml.XmlElement]$Task41Root = $Task27Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task41Root.InnerText = "false"
	[System.Xml.XmlElement]$Task42Root = $Task27Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task42Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task43Root = $Task27Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task43Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task44Root = $Task27Root.AppendChild($Taskone1.CreateElement("RestartOnFailure"))
	[System.Xml.XmlElement]$Task45Root = $Task44Root.AppendChild($Taskone1.CreateElement("Interval"))
	$Task45Root.InnerText = "PT15M"
	[System.Xml.XmlElement]$Task46Root = $Task44Root.AppendChild($Taskone1.CreateElement("Count"))
	$Task46Root.InnerText = "3"
	
	
	[System.Xml.XmlElement]$Task47Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task47Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task48Root = $Task47Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task49Root = $Task48Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task49Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task50Root = $Task48Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task50Root.InnerText = "-command Get-RPOSUpdateRPOS"
	
	$Taskone1.Save("$TasksLocalRPOSABFile")
	
}

function createsecondrunxml
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2017-02-15T18:57:14.2530215"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "RPOSAU"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("BootTrigger"))
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task7Root.InnerText = "true"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Delay"))
	$Task8Root.InnerText = "PT2M"
	
	[System.Xml.XmlElement]$Task9Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task10Root = $Task9Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task10Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task11Root = $Task10Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task11Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task12Root = $Task10Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task12Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task13Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task14Root = $Task13Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task14Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task15Root = $Task13Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task15Root.InnerText = "true"
	[System.Xml.XmlElement]$Task16Root = $Task13Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task16Root.InnerText = "true"
	[System.Xml.XmlElement]$Task17Root = $Task13Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task17Root.InnerText = "true"
	[System.Xml.XmlElement]$Task18Root = $Task13Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task18Root.InnerText = "false"
	[System.Xml.XmlElement]$Task19Root = $Task13Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task19Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task20Root = $Task13Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task21Root = $Task20Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task21Root.InnerText = "true"
	[System.Xml.XmlElement]$Task22Root = $Task20Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task22Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task23Root = $Task13Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task23Root.InnerText = "true"
	[System.Xml.XmlElement]$Task24Root = $Task13Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task24Root.InnerText = "true"
	[System.Xml.XmlElement]$Task25Root = $Task13Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task25Root.InnerText = "false"
	[System.Xml.XmlElement]$Task26Root = $Task13Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task26Root.InnerText = "false"
	[System.Xml.XmlElement]$Task27Root = $Task13Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task27Root.InnerText = "false"
	[System.Xml.XmlElement]$Task28Root = $Task13Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task28Root.InnerText = "PT1H"
	[System.Xml.XmlElement]$Task29Root = $Task13Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task29Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task30Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task30Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task31Root = $Task30Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task32Root = $Task31Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task32Root.InnerText = "$installrposaufile"
	
	
	$Taskone1.Save("C:\temp\installupdaterpos\Second Install.xml")
	
}
#########################################################################################################################################
# Function Set-RPOSModuleDownloadHashSetup
function Set-RPOSModuleDownloadHashSetup
{
	param (
		[parameter(Mandatory = $true, HelpMessage = "Downloadfile parm is the actual http URL to the file needed to download.")]
		[string]$downloadfile,
		[parameter(Mandatory = $true, HelpMessage = "Localfile parm is the path in which you want the file to save to and the filename.")]
		[string]$localfile,
		[parameter(Mandatory = $true, HelpMessage = "DownloadMD5 is the hash value of the file in which powershell will compare the file to. All values must be filled out in order for the function to run.")]
		[string]$downloadMD5)
	
	Set-ItemProperty -Path "$LANDeskKeys" -Name "DownloadMD5HashModule" -Value "0" -Force
	
	while ($downloadtimes -lt 5)
	{
		$downloadtimes += 1
		Write-Output "Starting to download file $downloadfile" | Out-File -Append -FilePath $Log
		Invoke-WebRequest -Uri $downloadfile -OutFile $localfile
		$localfilehash = Get-FileHash -Path "$localfile" -Algorithm MD5 | Select-Object -ExpandProperty hash
		if ($downloadMD5 -eq $localfilehash)
		{
			Set-ItemProperty -Path "$LANDeskKeys" -Name "DownloadMD5HashModule" -Value "1" -Force
			Write-Output "File has been downloaded $localfilehash and the MD5 hash that was supposed to match is $downloadMD5 file is complete and downloaded." | Out-File -Append -FilePath $Log
		}
		if ($downloadMD5 -eq $localfilehash) { break }
	}
	
}

New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
New-Item "C:\temp\" -ItemType Directory -Force
$date = (get-date).ToString("yyyyMMdd-hhmmss")
$Log = "C:\Windows\Scripts\RPOS\Logs\RPOS_InstallLog_$date.txt"
Write-Output "Removing C:\temp\installupdaterpos directory to ensure installer has clean files" | Out-File -Append -FilePath $Log
Remove-Item "C:\temp\installupdaterpos" -Recurse -Force
New-Item -Path "C:\temp\installupdaterpos" -ItemType Directory -Force

#Checking to see if Powershell is the latest version
$PowershellKeys = "HKLM:\SOFTWARE\Microsoft\PowerShell\3\PowerShellEngine"
$PSVersioncheck = (Get-ItemProperty -Path $PowershellKeys -Name "PowerShellVersion")."PowerShellVersion"
$PSVersion = $PSVersioncheck.Substring(0, 1)

#Setting up checking for processes
$processpend = 0

if ($PSversion -ge "5")
{
	Write-Output "Your version of Powershell is $PSVersion have a version that is higher than 5.0" | Out-File -Append -FilePath $Log
}
Else
{
	$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/Win7AndW2KR2Powershellv5.zip"
	$PowershellInstallPath = "C:\temp\installupdaterpos\Win7AndW2KR2Powershellv5.zip"
	$Webclient = New-Object System.Net.WebClient
	$Webclient.DownloadFile($URL, $PowershellInstallPath)
	#Invoke-WebRequest -Uri $URL -OutFile $PowershellInstallPath
	#[System.IO.Compression.ZipFile]::ExtractToDirectory($PowershellInstallPath, "C:\temp\installupdaterpos")
	
	# Old Unzipping Powershell version 2.0
	$shell = New-Object -Com shell.application
	$zip = $shell.NameSpace("$PowershellInstallPath")
	foreach ($item in $zip.items())
	{
		$shell.Namespace("C:\temp\installupdaterpos").copyhere($item)
	}
	
	Remove-Item "$PowershellInstallPath" -Force
	
	wusa.exe "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3191566-x64.msu" /quiet /norestart
	while ($processpend -lt 500)
	{
		$processpend += 1
		$ProcessActive = Get-Process wusa -ErrorAction SilentlyContinue
		if ($ProcessActive -eq $null)
		{
			Write-Output "Windows Update is not running" | Out-File -Append -FilePath $Log
			$script:installrposaufile = Get-ChildItem -Path "C:\" -Include "RPOSUpdaterInstaller.exe" -Recurse -ErrorAction SilentlyContinue -Force | Select-Object -ExpandProperty FullName
			createsecondrunxml
			Start-Sleep -seconds 10
			schtasks /create /TN "After Reboot RPOS AU Install" /XML "C:\temp\installupdaterpos\Second Install.xml"
			Restart-Computer -Force
		}
		Else
		{
			Write-Output "Windows Update is running" | Out-File -Append -FilePath $Log
			Start-Sleep -seconds 10
		}
	}
}

Write-Output "RPOS Installer v$Installerversion (c) EOC TBC Corporation" | Out-File -Append -FilePath $Log
$PCname = "$env:COMPUTERNAME"
$PCname2 = "$env:COMPUTERNAME"

if ($PCname -match 'S[0-9][0-9][0-9]00')
{
	$PCname = $PCname.Substring(6, 2)
}

if ($PCname -match 'S[0-9][0-9][0-9]NPC')
{
	$PCname = $PCname.Substring(7, 2)
}

Write-Output "Removing C:\temp\installupdaterpos directory to ensure installer has clean files" | Out-File -Append -FilePath $Log
Remove-Item "C:\temp\installupdaterpos" -Recurse -Force
New-Item -Path "C:\temp\installupdaterpos" -ItemType Directory -Force

Write-Output "$PCName2 is the number of this PC" | Out-File -Append -FilePath $Log

$LANDeskKeys = "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"
$updatekey = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Auto-Updater")."RPOS Auto-Updater"
Write-Output "$updatekey is the version of Auto-Updater running on this PC" | Out-File -Append -FilePath $Log
$datereg = (Get-Date).ToString("MM/dd/yyyy")
$computerver = (Get-WmiObject -class Win32_OperatingSystem).Caption

#Starting install for new PC's and/or have older versions
if (($updatekey -match "$NewVersion.1") -or ($updatekey -match "$NewVersion.2") -or ($updatekey -match "$NewVersion.3"))
	{
		Write-Output "Setup has detected that this PC has the most current version reinstalling newer files" | Out-File -Append -FilePath $Log
	
	if (($PCname2 -match 'S[0-9][0-9][0-9]00') -or ($PCname2 -match 'S[0-9][0-9][0-9]NPC')) {
		
		Write-Output "Setup has detected that this PC is a Store PC, proceeding with setup." | Out-File -Append -FilePath $Log
		if ($PCname -eq "01") {
			Write-Output "Installer has started installing files for PC1" | Out-File -Append -FilePath $Log
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.1" -Force }
		if ($PCname -eq "02") {
			Write-Output "Installer has started installing files for PC2" | Out-File -Append -FilePath $Log
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.2" -Force }
		if (($PCname -notmatch "01") -and ($PCname -notmatch "02")) {
			Write-Output "Installer has started installing files for rest of PC's" | Out-File -Append -FilePath $Log
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.3" -Force }
		
	}
	Else
	{
		Write-Output "Installer has started installing files for PC3" | Out-File -Append -FilePath $Log
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.3" -Force
	}
	
	if (($name -eq $ppc1) -or ($name -eq $ppc2) -or ($name -eq $ppc3) -or ($name -eq $ppc4) -or ($name -eq $ppc5) -or ($name -eq $ppc6) -or ($name -eq $ppc7) -or ($name -eq $ppc8) -or ($name -eq $ppc9) -or ($name -eq $ppc10) -or ($name -eq $ppc11) -or ($name -eq $ppc12) -or ($name -eq $ppc13) -or ($name -eq $ppc14) -or ($name -eq $ppc15) -or ($name -eq $ppc16) -or ($name -eq $ppc17) -or ($name -eq $ppc18) -or ($name -eq $ppc19) -or ($name -eq $ppc20))
	{
		$downloadfile = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/LiveUpdatePilot.zip"
		$hasher = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/hasher.xml"
		$hasherlist = [xml](New-Object System.Net.WebClient).downloadstring($hasher)
		$liveupdatehashzip = $hasherlist.liveupdate.pilot.hashvalues.fixrposaufileszip
	}
	Else
	{
		$downloadfile = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/LiveUpdate.zip"
		$hasher = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/hasher.xml"
		$hasherlist = [xml](New-Object System.Net.WebClient).downloadstring($hasher)
		$liveupdatehashzip = $hasherlist.liveupdate.retail.hashvalues.fixrposaufileszip
	}
	
	Set-RPOSModuleDownloadHashSetup -downloadfile $downloadfile -localfile $localfile -downloadMD5 $liveupdatehashzip
	
	
		#Grabbing the most up-to-date RPOS Auto-Updater from RPOS.tbccorp.com
		[System.IO.Compression.ZipFile]::ExtractToDirectory($localfile, "C:\temp\installupdaterpos")
		#Expand-Archive -Path $updaterposinstallzip -DestinationPath "C:\temp\installupdaterpos" -Force
		###############
	
		<# Fixing Windows 10 Scheduled Tasks
		if ($computerver -match "Microsoft Windows 10")
		{
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/FixWindows10tasks.exe"
		$FixWin10TasksEXELocal = "C:\temp\installupdaterpos\FixWindows10tasks.exe"
		$Webclient = New-Object System.Net.WebClient
		$Webclient.DownloadFile($URL, $FixWin10TasksEXELocal)
		
		Start-Process -FilePath "C:\temp\installupdaterpos\FixWindows10tasks.exe" -Wait
		}#>
	
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value "$datereg" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUVer" -Value "$NewVersion" -Force
	
		<#if (($PCname2 -match 'S[0-9][0-9][0-9]00') -or ($PCname2 -match 'S[0-9][0-9][0-9]NPC'))
		{
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EndpointXMLchecksum" -Value "0" -Force
		}#>
	
		New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayVersion" -Value "$NewVersion" -Force
		Set-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "UninstallString" -Value '"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -command "Get-RPOSAutoUpdaterUninstall"' -Force
	
		New-Item "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -ItemType Directory -Force
		New-Item "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -ItemType Directory -Force
		Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
		Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
	
		Start-Sleep -Seconds 120
		schtasks /delete /TN "After Reboot RPOS AU Install" /F
	
		Remove-Item "C:\Windows\Scripts\RPOS\uninstall" -Recurse -Force
	
		Remove-Item "C:\Windows\Scripts\RPOS\UninstallRPOSAU.exe" -Force
		Remove-Item "C:\Windows\System32\rposauaf.exe" -Force
		Remove-Item "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3134760-x64.msu" -Force
		Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.exe" -Force
		Remove-Item "C:\temp\installupdaterpos\quietuninstall.vbs" -Force
		Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.bat" -Force
		Remove-Item "C:\temp\installupdaterpos\Update RPOS.lnk" -Force
		Remove-Item "C:\temp\installupdaterpos\rposauaf.exe" -Force
		Remove-Item "C:\temp\installupdaterpos\auicon.ico" -Force
		Remove-Item "C:\temp\installupdaterpos\RPOS.lnk" -Force
		Remove-Item "C:\temp\installupdaterpos\Update RPOS.lnk" -Force
		Remove-Item "C:\temp\installupdaterpos\UpdateRPOS.ico" -Force
		Remove-Item "C:\temp\installupdaterpos" -Recurse -Force
		Remove-Item "C:\temp\installupdaterpos\FixWindows10tasks.exe" -Recurse -Force
	
		#Running RPOS Auto-Updater After Setup Completes
		schtasks /Run /TN "Update RPOS Local A"
		schtasks /Run /TN "Update RPOS Local B"
	Exit
}
if (($updatekey -match "$previousversion.1") -or ($updatekey -match "$previousversion.2") -or ($updatekey -match "$previousversion.3"))
{
	Write-Output "Setup has detected that this PC has a previous version of Auto-Updater Installed preforming upgrade" | Out-File -Append -FilePath $Log
	schtasks /End /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix"
	Write-Host "RPOS Auto-Updater Installer Is Resting for 120 Seconds, Please Wait."
	Start-Sleep -Seconds 120
	schtasks /change /disable /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix"
	
	if (($PCname2 -match 'S[0-9][0-9][0-9]00') -or ($PCname2 -match 'S[0-9][0-9][0-9]NPC')) {
		Write-Output "Setup has detected that this PC is a Store PC, proceeding with setup." | Out-File -Append -FilePath $Log
		
		if ($PCname -eq "01") {
			Write-Output "Installer has started installing files for PC1" | Out-File -Append -FilePath $Log
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.1" -Force }
		if ($PCname -eq "02") {
			Write-Output "Installer has started installing files for PC2" | Out-File -Append -FilePath $Log
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.2" -Force }
		if (($PCname -notmatch "01") -and ($PCname -notmatch "02")) {
			Write-Output "Installer has started installing files for rest of PC's" | Out-File -Append -FilePath $Log
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.3" -Force }
	}
	Else {
		Write-Output "Installer has started installing files for PC3" | Out-File -Append -FilePath $Log
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.3" -Force
	}
	
	if (($name -eq $ppc1) -or ($name -eq $ppc2) -or ($name -eq $ppc3) -or ($name -eq $ppc4) -or ($name -eq $ppc5) -or ($name -eq $ppc6) -or ($name -eq $ppc7) -or ($name -eq $ppc8) -or ($name -eq $ppc9) -or ($name -eq $ppc10) -or ($name -eq $ppc11) -or ($name -eq $ppc12) -or ($name -eq $ppc13) -or ($name -eq $ppc14) -or ($name -eq $ppc15) -or ($name -eq $ppc16) -or ($name -eq $ppc17) -or ($name -eq $ppc18) -or ($name -eq $ppc19) -or ($name -eq $ppc20))
	{
		$downloadfile = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/LiveUpdatePilot.zip"
		$hasher = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/hasher.xml"
		$hasherlist = [xml](New-Object System.Net.WebClient).downloadstring($hasher)
		$liveupdatehashzip = $hasherlist.liveupdate.pilot.hashvalues.fixrposaufileszip
	}
	Else
	{
		$downloadfile = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/LiveUpdate.zip"
		$hasher = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/hasher.xml"
		$hasherlist = [xml](New-Object System.Net.WebClient).downloadstring($hasher)
		$liveupdatehashzip = $hasherlist.liveupdate.retail.hashvalues.fixrposaufileszip
	}
	
	Set-RPOSModuleDownloadHashSetup -downloadfile $downloadfile -localfile $localfile -downloadMD5 $liveupdatehashzip
	
	[System.IO.Compression.ZipFile]::ExtractToDirectory($localfile, "C:\temp\installupdaterpos")
	#Expand-Archive -Path $updaterposinstallzip -DestinationPath "C:\temp\installupdaterpos" -Force
	###############
	
	<# Fixing Windows 10 Scheduled Tasks
	if ($computerver -match "Microsoft Windows 10")
	{
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/FixWindows10tasks.exe"
		$FixWin10TasksEXELocal = "C:\temp\installupdaterpos\FixWindows10tasks.exe"
		$Webclient = New-Object System.Net.WebClient
		$Webclient.DownloadFile($URL, $FixWin10TasksEXELocal)
		
		Start-Process -FilePath "C:\temp\installupdaterpos\FixWindows10tasks.exe" -Wait
	}#>
	
	Remove-Item $localfile -Force
	
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value "$datereg" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUVer" -Value "$NewVersion" -Force
	
	<#if (($PCname2 -match 'S[0-9][0-9][0-9]00') -or ($PCname2 -match 'S[0-9][0-9][0-9]NPC')) {
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EndpointXMLchecksum" -Value "0" -Force
	}#>
	
	schtasks /delete /TN "After Reboot RPOS AU Install" /F

	#Remove-Item "C:\Windows\Scripts\RPOS\uninstall" -Recurse -Force
	
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayVersion" -Value "$NewVersion" -Force
	Set-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "UninstallString" -Value '"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -command "Get-RPOSAutoUpdaterUninstall"' -Force
	
	New-Item "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -ItemType Directory -Force
	New-Item "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -ItemType Directory -Force
	Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
	Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
	
	schtasks /delete /TN "After Reboot RPOS AU Install" /F
	Remove-Item "C:\Windows\Scripts\RPOS\UninstallRPOSAU.exe" -Force
	Remove-Item "C:\Windows\System32\rposauaf.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3134760-x64.msu" -Force
	Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\quietuninstall.vbs" -Force
	Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.bat" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS.lnk" -Force
	Remove-Item "C:\temp\installupdaterpos\rposauaf.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\auicon.ico" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS.lnk" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS.lnk" -Force
	Remove-Item "C:\temp\installupdaterpos\UpdateRPOS.ico" -Force
	Remove-Item "C:\temp\installupdaterpos" -Recurse -Force
	Remove-Item "C:\temp\installupdaterpos\FixWindows10tasks.exe" -Recurse -Force
	
	#Running RPOS Auto-Updater After Setup Completes
	schtasks /Run /TN "Update RPOS Local A"
	schtasks /Run /TN "Update RPOS Local B"
	
	Exit
	
}

if (($updatekey -notmatch "$NewVersion.[0-9]") -or ($updatekey -notmatch "$previousversion.[0-9]") -or ($updatekey -notmatch "$previouspreviousversion.[0-9]") -or ($updatekey -eq $null))
{
	Write-Output "Setup has detected that this PC an older version and/or new install." | Out-File -Append -FilePath $Log
	[Environment]::SetEnvironmentVariable("RPOSAutoUpdater", "$env:SystemRoot\Scripts\RPOS", "Machine")
	schtasks /End /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix"
	Write-Host "RPOS Auto-Updater Installer Is Resting for 120 Seconds, Please Wait."
	Start-Sleep -Seconds 120
	schtasks /change /disable /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix"
	Write-Output "Setup is removing all previous scheduled tasks if they are installed on the system." | Out-File -Append -FilePath $Log
	schtasks /End /TN "RPOS AU updater"
	schtasks /End /TN "Update RPOS Clear Logs"
	schtasks /End /TN "Rollback RPOS Local"
	schtasks /End /TN "Update RPOS Local A"
	schtasks /End /TN "Update RPOS Local B"
	schtasks /End /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix"
	schtasks /Delete /TN "RPOS AU updater" /F
	schtasks /Delete /TN "Update RPOS Clear Logs" /F
	schtasks /Delete /TN "Rollback RPOS Local" /F
	schtasks /Delete /TN "Update RPOS Local A" /F
	schtasks /Delete /TN "Update RPOS Local B" /F
	schtasks /Delete /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater Autofix" /F
	
	Write-Output "Adding ShopRPOS Users to administrator group on local PC" | Out-File -Append -FilePath $Log
	net localgroup "Administrators" /add TBC\shprorpos
	net localgroup "Administrators" /add TBC\shprorpos2
	
	#Installing RPOS Live Update Task
	if ($PCname -eq "01")
	{
		$script:RPOSAULiveUpdateTime = "2016-05-04T01:00:00"
	}
	
	if ($PCname -eq "02")
	{
		$script:RPOSAULiveUpdateTime = "2016-05-04T02:00:00"
	}
	
	if (($PCname -notmatch "01") -and ($PCname -notmatch "02"))
	{
		$script:RPOSAULiveUpdateTime = "2016-05-04T03:00:00"
	}
	
	$script:RPOSLiveUpdateXML = "C:\temp\installupdaterpos\LiveUpdate.xml"
	createtsliveupdatexml
	schtasks /create /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate" /XML "$RPOSLiveUpdateXML"
	
	Remove-Item "$RPOSLiveUpdateXML" -Force
	
	Write-Output "Creating XML files for Clear Logs and Rollback RPOS" | Out-File -Append -FilePath $Log
	createtsrollbackxml
	createtsclearlogsxml
	
	schtasks /create /TN "Update RPOS Clear Logs" /XML "C:\temp\installupdaterpos\Update RPOS Clear Logs.xml"
	schtasks /create /TN "Rollback RPOS Local" /XML "C:\temp\installupdaterpos\Rollback RPOS Local.xml"
	
	if ($PCname -eq "01")
	{
		Write-Output "Installer has started installing Scheduled Tasks for PC1" | Out-File -Append -FilePath $Log

		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.1" -Force
		
		$URL1 = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/FixUpdateRPOSABTasks.zip"
		$updaterposselectionmodels = "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.zip"
		Invoke-WebRequest -Uri $URL1 -OutFile $updaterposselectionmodels
		Expand-Archive -Path $updaterposselectionmodels -DestinationPath "C:\temp\installupdaterpos" -Force
		
		Start-Process -FilePath "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.exe" -Wait
		
		Remove-Item "$updaterposselectionmodels" -Force
		Remove-Item "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.exe" -Force
		
	}
	
	if ($PCname -eq "02")
	{
		Write-Output "Installer has started installing Scheduled Tasks for PC2" | Out-File -Append -FilePath $Log
		
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.2" -Force
		
		$URL1 = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/FixUpdateRPOSABTasks.zip"
		$updaterposselectionmodels = "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.zip"
		Invoke-WebRequest -Uri $URL1 -OutFile $updaterposselectionmodels
		Expand-Archive -Path $updaterposselectionmodels -DestinationPath "C:\temp\installupdaterpos" -Force
		
		Start-Process -FilePath "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.exe" -Wait
		
		Remove-Item "$updaterposselectionmodels" -Force
		Remove-Item "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.exe" -Force
		
	}
	
	if (($PCname -notmatch "01") -and ($PCname -notmatch "02") -and ($PCname -notmatch 'S[0-9][0-9][0-9]00') -and ($PCname -notmatch 'S[0-9][0-9][0-9]NPC'))
	{
		Write-Output "Installer has started installing Scheduled Tasks for the rest of the PC's" | Out-File -Append -FilePath $Log
		
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Value "$NewVersion.3" -Force
		
		$URL1 = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/FixUpdateRPOSABTasks.zip"
		$updaterposselectionmodels = "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.zip"
		Invoke-WebRequest -Uri $URL1 -OutFile $updaterposselectionmodels
		Expand-Archive -Path $updaterposselectionmodels -DestinationPath "C:\temp\installupdaterpos" -Force
		
		Start-Process -FilePath "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.exe" -Wait
		
		Remove-Item "$updaterposselectionmodels" -Force
		Remove-Item "C:\temp\installupdaterpos\FixUpdateRPOSABTasks.exe" -Force
		

	}
	
	if (($name -eq $ppc1) -or ($name -eq $ppc2) -or ($name -eq $ppc3) -or ($name -eq $ppc4) -or ($name -eq $ppc5) -or ($name -eq $ppc6) -or ($name -eq $ppc7) -or ($name -eq $ppc8) -or ($name -eq $ppc9) -or ($name -eq $ppc10) -or ($name -eq $ppc11) -or ($name -eq $ppc12) -or ($name -eq $ppc13) -or ($name -eq $ppc14) -or ($name -eq $ppc15) -or ($name -eq $ppc16) -or ($name -eq $ppc17) -or ($name -eq $ppc18) -or ($name -eq $ppc19) -or ($name -eq $ppc20))
	{
		$downloadfile = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/LiveUpdatePilot.zip"
		$hasher = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/hasher.xml"
		$hasherlist = [xml](New-Object System.Net.WebClient).downloadstring($hasher)
		$liveupdatehashzip = $hasherlist.liveupdate.pilot.hashvalues.fixrposaufileszip
	}
	Else
	{
		$downloadfile = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/LiveUpdate.zip"
		$hasher = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/hasher.xml"
		$hasherlist = [xml](New-Object System.Net.WebClient).downloadstring($hasher)
		$liveupdatehashzip = $hasherlist.liveupdate.retail.hashvalues.fixrposaufileszip
	}
	
	Set-RPOSModuleDownloadHashSetup -downloadfile $downloadfile -localfile $localfile -downloadMD5 $liveupdatehashzip

	
	[System.IO.Compression.ZipFile]::ExtractToDirectory($localfile, "C:\temp\installupdaterpos")
	###############
	
	Remove-Item $localfile -Force
	Remove-Item "C:\Windows\Scripts\RPOS\uninstall" -Recurse -Force
	Remove-Item "C:\Windows\Scripts\RPOS\UninstallRPOSAU.exe" -Force
	Remove-Item "C:\Windows\System32\rposauaf.exe" -Force
	
	
	icacls "C:\Users\Public\Desktop\RPOS.lnk" /grant Everyone:F
	icacls "C:\Windows\System32\Tasks\Update RPOS Local A" /grant Everyone:RX
	icacls "C:\Windows\System32\Tasks\Update RPOS Local B" /grant Everyone:RX
	Set-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name EnableLUA -Value 0 -Type DWord
	Set-ExecutionPolicy -ExecutionPolicy RemoteSigned -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoXMLchecksum" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Value "1" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value "$datereg" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUVer" -Value "$NewVersion" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Checksum" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "FlowModule Checksum" -Value "0" -Force
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Value "1" -Force
	
	if (($PCname2 -match 'S[0-9][0-9][0-9]00') -or ($PCname2 -match 'S[0-9][0-9][0-9]NPC'))
	{
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Endpoint" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Enable" -Value "0" -Force
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EndpointXMLchecksum" -Value "0" -Force
	}
	New-ItemProperty -path "$LANDeskKeys" -Name "AdobeAIRVer" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AdobeDistro" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRNI" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRInstalling" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckSelfSubDownload" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUFailedLocal" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AULocalReinstall" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "AUGatherReportingData" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "DownloadMD5HashModule" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "EMVGateway" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "EMVIPAddress" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "InstallComplete" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "Tesla DCount" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "TeslaDistro" -Value "0" -Force
	New-ItemProperty -path "$LANDeskKeys" -Name "TeslaVer" -Value "0" -Force
	New-Item "C:\Windows\Scripts" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\Logs" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\Bak" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\build" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item "C:\Windows\Scripts\RPOS\update" -ItemType Directory | %{ $_.Attributes = "hidden" }
	New-Item -ItemType directory "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater"
	net share cs18ebyi3$=C:\Windows\Scripts\RPOS\build "/grant:TBC\shprorpos,READ" "/grant:TBC\shprorpos2,READ"
	Copy-Item "C:\temp\installupdaterpos\rposauaf.exe" "C:\Windows\System32" -Force
	Copy-Item "C:\temp\installupdaterpos\Update RPOS.lnk" "C:\Users\Public\Desktop" -Force
	Copy-Item "C:\temp\installupdaterpos\Update RPOS.lnk" "C:\Windows\Scripts\RPOS\Bak" -Force
	New-Item "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -ItemType Directory -Force
	New-Item "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -ItemType Directory -Force
	Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos"
	Copy-Item "C:\temp\installupdaterpos\updaterpos.psm1" "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos"
	Copy-Item "C:\temp\installupdaterpos\UpdateRPOS.ico" "C:\Windows\Scripts\RPOS" -Force
	Copy-Item "C:\temp\installupdaterpos\auicon.ico" "C:\Windows\Scripts\RPOS" -Force
	
	#Removing EXE from Auto-Updater
	Remove-Item "C:\Windows\Scripts\RPOS\RemoveLog60days.exe" -Force
	Remove-Item "C:\Windows\Scripts\RPOS\rollbackrpos.exe" -Force
	Remove-Item "C:\Windows\Scripts\RPOS\update.exe" -Force
	Remove-Item "C:\Windows\Scripts\RPOS\updaterpos.exe" -Force
	Remove-Item "C:\Windows\Scripts\RPOS\updaterposdesktop.exe" -Force
	
	New-Item -ItemType directory "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater"
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayIcon" -Value "C:\Windows\Scripts\RPOS\auicon.ico" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayName" -Value "RPOS Auto-Updater" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "DisplayVersion" -Value "$NewVersion" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "EstimatedSize" -Value "2000" -PropertyType DWORD -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "InstallLocation" -Value "C:\Windows\Scripts\RPOS" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "NoModify" -Value "1" -PropertyType DWORD -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "NoRepair" -Value "1" -PropertyType DWORD -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "Publisher" -Value "TBC Corporation EOC Department" -Force
	New-ItemProperty -path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Name "UninstallString" -Value '"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -command "Get-RPOSAutoUpdaterUninstall"' -Force
	icacls "C:\Windows\System32\Tasks\Update RPOS Local A" /grant Everyone:RX
	icacls "C:\Windows\System32\Tasks\Update RPOS Local B" /grant Everyone:RX
	icacls "C:\Windows\Scripts\RPOS\uninstall\quietuninstall.vbs" /grant Everyone:RX
	icacls "C:\Users\Public\Desktop\RPOS.lnk" /grant Everyone:F
	icacls "C:\Users\Public\Desktop\Update RPOS.lnk" /grant Everyone:RX
	Remove-Item "C:\temp\installupdaterpos\Builduninstallers.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local A PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Local B PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS Clear Logs.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Rollback RPOS Local.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\Win7AndW2K8R2-KB3134760-x64.msu" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC1.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC2.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS AU updater PC3.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS Auto-Updater Autofix.xml"
	Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\quietuninstall.vbs" -Force
	Remove-Item "C:\temp\installupdaterpos\UninstallRPOSAU.bat" -Force
	Remove-Item "C:\temp\installupdaterpos\Second Install.xml" -Force
	Remove-Item "C:\temp\installupdaterpos\updaterposdesktop.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS.lnk" -Force
	Remove-Item "C:\temp\installupdaterpos\rposauaf.exe" -Force
	Remove-Item "C:\temp\installupdaterpos\auicon.ico" -Force
	Remove-Item "C:\temp\installupdaterpos\RPOS.lnk" -Force
	Remove-Item "C:\temp\installupdaterpos\Update RPOS.lnk" -Force
	Remove-Item "C:\temp\installupdaterpos\UpdateRPOS.ico" -Force
	Remove-Item "C:\temp\installupdaterpos" -Recurse -Force
	schtasks /delete /TN "After Reboot RPOS AU Install" /F
	
	#Running RPOS Auto-Updater After Setup Completes
	schtasks /Run /TN "Update RPOS Local A"
	schtasks /Run /TN "Update RPOS Local B"
	
	& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N
	Exit
	
}
