﻿###################################################
#       Auto-Update RPOS Powershell               #
#       Made By: Randy Olsen                      #
#       Date: 5/6/2021                            #
#       Version: 3.14                             #
###################################################

#ALL VARIABLES THAT ARE IN THIS POWERSHELL SCRIPT CAN BE CHANGED FROM THE FIRST COUPLE ROWS TO REFLECT THE ENTIRE SCRIPT!#
#Setting Up Varibles

#########################################################################################################################################
#                                                         * START OF MODULES *                                                          # 
#########################################################################################################################################
#Function Set-RPOSVar
function Set-RPOSVar
{
	$script:name = $env:COMPUTERNAME
	$script:date = (get-date).ToString("yyyyMMdd-hhmmss")
	
	#Default DIR for Auto-Updater
	$script:RPOSAUDIRHOME = "$env:SystemRoot\Scripts\RPOS"
	$script:RPOSAUDIRBAK = "$env:SystemRoot\Scripts\RPOS\Bak"
	$script:RPOSAUDIRBUILD = "$env:SystemRoot\Scripts\RPOS\Build"
	$script:RPOSAUDIRLOGS = "$env:SystemRoot\Scripts\RPOS\Logs"
	$script:RPOSAUDIRIMAGES = "$env:SystemRoot\Scripts\RPOS\images"
	$script:RPOSAUDIRUNINSTALL = "$env:SystemRoot\Scripts\RPOS\uninstall"
	$script:RPOSAUDIRUPDATE = "$env:SystemRoot\Scripts\RPOS\update"
	$script:RPOSAUDIRIVANTI = "$env:SystemRoot\Scripts\RPOS\ivanti"
	$script:RPOSAUDIRLIVEUPDATEFIX = "$env:SystemRoot\Scripts\RPOS\Liveupdate"
	$script:RPOSAUAUTOFIX = "$env:SystemRoot\System32\rposauaf.exe"
	
	#Log File
	if (!$Log)
	{
		$script:Log = "$RPOSAUDIRLOGS\RPOS_Update_$date.txt"
	}
	
	#RPOS Program Files Dirs
	$script:RPOSHOME = "${env:ProgramFiles(x86)}\RPOS"
	$script:FLOWHOME = "${env:ProgramFiles(x86)}\FlowModule"
	
	$script:windowscaption = (Get-WmiObject -class Win32_OperatingSystem).Caption
	$script:autofixhttpdir = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles"
	$script:ReleaseInfo = "3.14"
	$script:LANDeskKeys = "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"
	$script:keyrpos = "HKLM:\SOFTWARE\WOW6432Node\Microsoft\Windows\CurrentVersion\Uninstall\com.tbccorp.rpos.RPOS"
	$script:keyflow = "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\com.tbccorp.rpos.FlowModule"
	$script:nameprofile = $name.Substring(1, 3)
	$script:profilename = "store00$nameprofile"
	$script:XMLEMVfileRPOS = "C:\Users\$profilename\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
	$script:XMLEMVfileFlow = "C:\Users\$profilename\AppData\Roaming\com.tbccorp.rpos.FlowModule\Local Store\rpos-preferences.xml"
	$script:XMLEMVRPOSbak = "$RPOSAUDIRBAK\rpos-preferencesrpos.xml"
	$script:XMLEMVFLOWbak = "$RPOSAUDIRBAK\rpos-preferencesflow.xml"
	$script:XML = "$RPOSAUDIRUPDATE.xml"
	$script:XMLhash = "$RPOSAUDIRHOME\checksum.xml"
	$script:EndpointXMLURL = "http://rpos.tbccorp.com/rposendpoints.xml"
	$script:RPOSEXE = "$RPOSHOME\RPOS.exe"
	$script:RPOSDesktopIcon = "C:\Users\Public\Desktop\RPOS.lnk"
	$script:arhfile = "$RPOSAUDIRHOME\arh.exe"
	$script:RPOSfileBakair = "$RPOSAUDIRBAK\RPOS.bakair"
	$script:flowfileBakair = "$RPOSAUDIRBAK\FlowModule.bakair"
	$script:RPOSfileBakexe = "$RPOSAUDIRBAK\RPOS.bakexe"
	$script:flowfileBakexe = "$RPOSAUDIRBAK\FlowModule.bakexe"
	$script:flowfileair = "$RPOSAUDIRHOME\FlowModule.air"
	$script:RPOSfileair = "$RPOSAUDIRHOME\RPOS.air"
	$script:flowfileexe = "$RPOSAUDIRHOME\FlowModule.exe"
	$script:RPOSfileexe = "$RPOSAUDIRHOME\RPOS.exe"
	$script:RPOSVersionLabelXML = "$RPOSHOME\META-INF\AIR\application.xml"
	$script:LiveIP = "$RPOSAUDIRHOME\LiveIPs.txt"
	$script:LiveNames = "$RPOSAUDIRHOME\LiveNames.txt"
	$script:datereg = (Get-Date).ToString("MM/dd/yyyy")
	$script:dateofdelayhour = Get-Date -Format "HH"
	$script:adobesumnum = "0"
	$script:sumnum = "0"
	$script:sumnumbginfo = "0"
	$script:results = "$RPOSAUDIRHOME\results.txt"
	$script:AdobeAIRInstallRoot = "$RPOSAUDIRHOME\AdobeAIRInstaller.exe"
	$script:AdobeAIRInstallShare = "$RPOSAUDIRBUILD\AdobeAIRInstaller.exe"
	$script:AdobeInstallerFile = "AdobeAIRInstaller.exe"
	$script:BGInfoLocalFile = "C:\bginfo\TBC.bgi"
	$script:checkupdatedXMLs = "http://rpos.tbccorp.com/RPOSfileupdatechecksum.xml"
	$script:RegBGInfoXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoXMLchecksum).BGInfoXMLchecksum
	$script:RegSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	$script:ReportingMaserXMLhtml = "http://rpos.tbccorp.com/RPOSAutoUpdater/Reporting/MasterData/MasterCollection.xml"
	$script:ReportingMaserXMLlocal = "R:\MasterCollection.xml"
	$script:builddir = "$RPOSAUDIRBUILD"
	$script:flowfileair = "$RPOSAUDIRHOME\FlowModule.air"
	$script:RPOSfileair = "$RPOSAUDIRHOME\RPOS.air"
	$script:flowfileexe = "$RPOSAUDIRHOME\FlowModule.exe"
	$script:RPOSfileexe = "$RPOSAUDIRHOME\RPOS.exe"
	$script:RPOSfileBakair = "$RPOSAUDIRBAK\RPOS.bakair"
	$script:flowfileBakair = "$RPOSAUDIRBAK\FlowModule.bakair"
	$script:RPOSfileBakexe = "$RPOSAUDIRBAK\RPOS.bakexe"
	$script:flowfileBakexe = "$RPOSAUDIRBAK\FlowModule.bakexe"
	$script:teslafileexe = "$RPOSAUDIRHOME\Tesla.exe"
	$script:teslafileBakexe = "$RPOSAUDIRBAK\TeslaBak.exe"
	$script:TeslaInstallerFile = "tesla-electron-latest.exe"
	$script:TeslaShareFile = "$RPOSAUDIRBUILD\Tesla.exe"
	$script:TeslaPublicFold = "C:\Users\Public\AppData\Local\tesla-electron"
	$script:PublicAppData = "C:\Users\Public\AppData"
	$script:PublicAppDataLocalFolder = "C:\Users\Public\AppData\Local"
	$script:TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" -ErrorAction SilentlyContinue | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
	$script:TeslaElectronversion = "$RPOSAUDIRHOME\TESLA-ELECTRON.VERSION"
	$script:TeslaElectronversionstandard = "C:\Users\Public\AppData\Local\TESLA-ELECTRON.VERSION"
	$script:CiscoStatus = get-wmiobject win32_networkadapter | select name, netconnectionstatus | Where-Object { $_.name -match "Cisco" } | Select-Object -ExpandProperty netconnectionstatus
	$script:CiscoExists = get-wmiobject win32_networkadapter | select name | Where-Object { $_.name -match "Cisco" } | Select-Object -ExpandProperty Name
	$script:ivantifolderpath = "C:\Program Files (x86)\LANDesk\LDClient"
	
	#Short Name For Windows name versions
	if ($windowscaption -match "Microsoft Windows 10")
	{
		$script:windowsshortname = "Win10"
	}
	elseif ($windowscaption -match "Microsoft Windows 7")
	{
		$script:windowsshortname = "Win7"
	}
	elseif ($windowscaption -match "Microsoft Windows Server")
	{
		$script:windowsshortname = "WinSRV"
	}
	
	#Two Digit Store Number
	$TwoDigPCName = "$ENV:COMPUTERNAME"
	
	#Adding Ivanti Agent information to see what agent the PC is running
	$RPOSAgentname = Get-ChildItem "${ivantifolderpath}" -Filter "*.msi" | Where-Object { $_.Name -match "Corporate" -or $_.Name -match "Store" -or $_.Name -match "Warehouse" -or $_.Name -match "Server" } | Select-Object -ExpandProperty Name
	
	if ($RPOSAgentname -match "Store")
	{
		$indexnumber1 = $RPOSAgentname.IndexOf("S")
		$RPOStempagentname1 = $RPOSAgentname.Substring($indexnumber1)
		$indexnumber2 = $RPOStempagentname1.IndexOf(" A")
		$script:RPOSAgentname = $RPOStempagentname1.Substring(0, $indexnumber2)
		
		if ($TwoDigPCName -match 'S[0-9][0-9][0-9]00')
		{
			$script:TwoDigPCName = $TwoDigPCName.Substring(6, 2)
		}
		if ($TwoDigPCName -match 'S[A-Z][0-9][0-9]00')
		{
			$script:TwoDigPCName = $TwoDigPCName.Substring(6, 2)
		}
		if ($TwoDigPCName -match 'S[A-Z][0-9][0-9]NPC')
		{
			$script:TwoDigPCName = $TwoDigPCName.Substring(7, 2)
		}
		if ($TwoDigPCName -match 'S[0-9][0-9][0-9]NPC')
		{
			$script:TwoDigPCName = $TwoDigPCName.Substring(7, 2)
		}	
	}
	
	if ($RPOSAgentname -match "Warehouse")
	{
		$indexnumber1 = $RPOSAgentname.IndexOf("W")
		$RPOStempagentname1 = $RPOSAgentname.Substring($indexnumber1)
		$indexnumber2 = $RPOStempagentname1.IndexOf(" A")
		$script:RPOSAgentname = $RPOStempagentname1.Substring(0, $indexnumber2)
	}
	if ($RPOSAgentname -match "Corporate Agent")
	{
		$indexnumber1 = $RPOSAgentname.IndexOf("C")
		$RPOStempagentname1 = $RPOSAgentname.Substring($indexnumber1)
		$indexnumber2 = $RPOStempagentname1.IndexOf(" A")
		$script:RPOSAgentname = $RPOStempagentname1.Substring(0, $indexnumber2)
	}
	if ($RPOSAgentname -match "Server Agent")
	{
		$indexnumber1 = $RPOSAgentname.IndexOf("S")
		$RPOStempagentname1 = $RPOSAgentname.Substring($indexnumber1)
		$indexnumber2 = $RPOStempagentname1.IndexOf(" A")
		$script:RPOSAgentname = $RPOStempagentname1.Substring(0, $indexnumber2)
	}
	
	
	#Setting up downloadtimes 
	$script:downloadtimes = "0"

	if ($RPOSAgentname -eq "Store") {
		
		#If XML EMV for RPOS equals true check the IP address
		if (Test-Path $XMLEMVfileRPOS)
		{
			#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for EMV
			[xml]$xmlread = Get-Content $XMLEMVfileRPOS
			$script:Result = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
			
			if ($Result -eq $null)
			{
				#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for NON-EMV
				[xml]$xmlread = Get-Content $XMLEMVfileRPOS
				$Read = $xmlread.ApplicationPreferences.creditCardTerminalUrl | Select-Object -ExpandProperty "#cdata-section"
				$script:Result = $Read -replace ":9001"
			}
			
			#Adding Verifone XML IP Address
			$script:XMLipa = ([ipaddress] "$Result").GetAddressBytes()[0]
			$script:XMLipb = ([ipaddress] "$Result").GetAddressBytes()[1]
			$script:XMLipc = ([ipaddress] "$Result").GetAddressBytes()[2]
			$script:XMLipd = ([ipaddress] "$Result").GetAddressBytes()[3]
		}
		
	}
	
	if (($CiscoExists -ne $null) -and ($CiscoStatus -eq "2") -or ($windowsshortname -eq "WinSRV"))
	{
	#Write-Output "Cisco AnyConnect is connected bypassing Set-Var Get-IP Address for Subnet Sharing" | Out-File -Append -FilePath $Log
	}
	Else
	{
		#Grabbing pinged IP address and gateway address
		$script:IP = Test-Connection $name -count 1 | select -ExpandProperty Ipv4Address | Select -ExpandProperty IPAddressToString
		$script:ipmain1 = ([ipaddress] "$IP").GetAddressBytes()[0]
		$script:ipmain2 = ([ipaddress] "$IP").GetAddressBytes()[1]
		$script:ipmain3 = ([ipaddress] "$IP").GetAddressBytes()[2]
		$script:ipmain4 = ([ipaddress] "$IP").GetAddressBytes()[3]
		$script:IPZero = "$ipmain1.$ipmain2.$ipmain3.0"
	}
	
	#Looking up endpoint key hash for store locations
	if ($RPOSAgentname -eq "Store") {
		$script:RegEndpointXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name EndpointXMLchecksum).EndpointXMLchecksum
	}
	
	#Testing to see if there is internet
	$script:networkcheck = Test-Connection "rpos.tbccorp.com" -count 4 -Quiet
	$pingrposobject = New-Object System.Net.Networkinformation.ping
	$script:pingrpos = $pingrposobject.Send("rpos.tbccorp.com") | Select-Object -ExpandProperty Status
	
	#Grabbing Current User logged in 
	$script:username = "${env:USERDOMAIN}\${env:USERNAME}"
	
	#Taking the username and finding the SID for the user
	$AdObj = New-Object System.Security.Principal.NTAccount("$username")
	$strSID = $AdObj.Translate([System.Security.Principal.SecurityIdentifier])
	$script:SID = $strSID.Value
	
	#Servers List
	$script:prod = "rpos.tbccorp.com"
	$script:train = "rpos-training.tbccorp.com"
	$script:pilot = "rpos-pilot.tbccorp.com"
	$script:pilot2 = "rpos-pilot2.tbccorp.com"
	$script:pilot3 = "rpos-pilot3.tbccorp.com"
	$script:qa = "rpos-qa.tbccorp.com"
	$script:qapilot = "rpos-qapilot.tbccorp.com"
	$script:qapilot2 = "rpos-qapilot2.tbccorp.com"
	$script:qapilot3 = "rpos-qapilot3.tbccorp.com"
	$script:alpha = "rpos-alpha.tbccorp.com"
	$script:dev = "rpos-dev.tbccorp.com"
	$script:devpilot = "rpos-devpilot.tbccorp.com"
	$script:devpilot2 = "rpos-devpilot2.tbccorp.com"
	$script:devpilot3 = "rpos-devpilot3.tbccorp.com"
	
	#Random Run For Inventory And Starting RPOS Auto-Updater
	$script:StartTimeDelay = Get-Random -Maximum 3600
	$script:InventoryRunTime = Get-Random -Maximum 600
	$script:RandomLetter = -join ((65 .. 90) + (97 .. 122) | Get-Random -Count 7 | % { [char]$_ })
	
	#Name of XML file that will get deposited into rpos.tbccorp.com
	$script:XMLDeposit = "${name}_${RandomLetter}.xml"
	
	#Grabbing Pilot PCs List
	$pilotpclist1 = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/pilotpcs.xml"
	$pilotpclist2 = [xml](New-Object System.Net.WebClient).downloadstring($pilotpclist1)
	$script:ppc1 = $pilotpclist2.pilotpcs.pc1
	$script:ppc2 = $pilotpclist2.pilotpcs.pc2
	$script:ppc3 = $pilotpclist2.pilotpcs.pc3
	$script:ppc4 = $pilotpclist2.pilotpcs.pc4
	$script:ppc5 = $pilotpclist2.pilotpcs.pc5
	$script:ppc6 = $pilotpclist2.pilotpcs.pc6
	$script:ppc7 = $pilotpclist2.pilotpcs.pc7
	$script:ppc8 = $pilotpclist2.pilotpcs.pc8
	$script:ppc9 = $pilotpclist2.pilotpcs.pc9
	$script:ppc10 = $pilotpclist2.pilotpcs.pc10
	$script:ppc11 = $pilotpclist2.pilotpcs.pc11
	$script:ppc12 = $pilotpclist2.pilotpcs.pc12
	$script:ppc13 = $pilotpclist2.pilotpcs.pc13
	$script:ppc14 = $pilotpclist2.pilotpcs.pc14
	$script:ppc15 = $pilotpclist2.pilotpcs.pc15
	$script:ppc16 = $pilotpclist2.pilotpcs.pc16
	$script:ppc17 = $pilotpclist2.pilotpcs.pc17
	$script:ppc18 = $pilotpclist2.pilotpcs.pc18
	$script:ppc19 = $pilotpclist2.pilotpcs.pc19
	$script:ppc20 = $pilotpclist2.pilotpcs.pc20
	
}

function Get-RPOSLocalUserGUI {
	
	Set-RPOSVar
	
	$GUIthatshouldbedisplayed = (Get-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay").RPOSAUGuiDisplay
	
	if ($GUIthatshouldbedisplayed -eq "1")
	{
		
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		$timer = New-Object System.Windows.Forms.Timer
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="RPOS Install Progress" Height="210" Width="610" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="52" Margin="185,119,0,0" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Rectangle HorizontalAlignment="Left" Height="109" Margin="10,10,0,0" Stroke="Black" VerticalAlignment="Top" Width="574" Fill="#FF7E7E7E"/>
        <Rectangle Fill="#FFD1D1D1" HorizontalAlignment="Left" Height="94" Margin="15,17,0,0" Stroke="Black" VerticalAlignment="Top" Width="564"/>
        <Rectangle HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" Stroke="Black" VerticalAlignment="Top" Width="564">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF707070" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" TextWrapping="Wrap" Text="  RPOS INSTALL PROGRESS" VerticalAlignment="Top" Width="245" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="35" Margin="23,47,0,0" TextWrapping="Wrap" Text="RPOS is in the process of updating. During this time, please do not shut down your PC. RPOS will be unavailable. Do not use until the install prompts show a successful installation. " VerticalAlignment="Top" Width="549"/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="463,75,0,0" VerticalAlignment="Top" Width="109" Foreground="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFFB7979"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
        <Image x:Name="service" HorizontalAlignment="Left" Height="42" Margin="21,126,0,0" VerticalAlignment="Top" Width="31" Source="$RPOSAUDIRIMAGES\service.png"/>
        <Image x:Name="wheel" HorizontalAlignment="Left" Height="42" VerticalAlignment="Top" Width="37" Margin="73,125,0,0" Source="$RPOSAUDIRIMAGES\wheels.png"/>
        <Image x:Name="engine" HorizontalAlignment="Left" Height="42" Margin="127,126,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\engine.png"/>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		$timer.Interval = 15000 # 8 seconds
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		#$timer = [Timer]::new()
		#$timer.Interval = 8000 # 8 seconds
		
		# Set up the event handler for the timer.
		$timer.Add_Tick({
				# Close the form.
				# Note: We dispose (stop) the timer later, right after the form closes;
				#       with a very short interval, another tick event could fire before
				#       that happens, but calling .Close() on an already closed form is fine.
				$form.Close()
			})
		
		# Start the timer.
		$timer.Start()
		
		# Show the dialog, which will automatically close
		# when the timer fires.
		$result = $form.ShowDialog()
		
		# Dispose (stop) the timer. Doing this here instead of in the tick
		# event handler ensures that the timer is stopped even when the form 
		# was closed by the user.
		$timer.Dispose()
		
		# Dispose the form and its controls. Skip, if you want to redisplay the form later.
		$form.Close()
	}
	
	if ($GUIthatshouldbedisplayed -eq "2")
	{
		
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		$timer = New-Object System.Windows.Forms.Timer
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="RPOS Install Progress" Height="210" Width="610" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="52" Margin="185,119,0,0" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Rectangle HorizontalAlignment="Left" Height="109" Margin="10,10,0,0" Stroke="Black" VerticalAlignment="Top" Width="574" Fill="#FF7E7E7E"/>
        <Rectangle Fill="#FFD1D1D1" HorizontalAlignment="Left" Height="94" Margin="15,17,0,0" Stroke="Black" VerticalAlignment="Top" Width="564"/>
        <Rectangle HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" Stroke="Black" VerticalAlignment="Top" Width="564">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF707070" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" TextWrapping="Wrap" Text="  RPOS INSTALL PROGRESS" VerticalAlignment="Top" Width="245" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="35" Margin="23,47,0,0" TextWrapping="Wrap" Text="RPOS is installing the newest version of Adobe AIR. Please do not shut down your PC. RPOS will be unavailable during this time." VerticalAlignment="Top" Width="549"/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="463,75,0,0" VerticalAlignment="Top" Width="109" Foreground="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFFB7979"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
        <Image x:Name="service" HorizontalAlignment="Left" Height="42" Margin="21,126,0,0" VerticalAlignment="Top" Width="31" Source="$RPOSAUDIRIMAGES\service.png"/>
        <Image x:Name="wheel" HorizontalAlignment="Left" Height="42" VerticalAlignment="Top" Width="37" Margin="73,125,0,0" Source="$RPOSAUDIRIMAGES\wheels.png"/>
        <Image x:Name="engine" HorizontalAlignment="Left" Height="42" Margin="127,126,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\engine.png"/>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		$timer.Interval = 15000 # 8 seconds
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		#$timer = [Timer]::new()
		#$timer.Interval = 8000 # 8 seconds
		
		# Set up the event handler for the timer.
		$timer.Add_Tick({
				# Close the form.
				# Note: We dispose (stop) the timer later, right after the form closes;
				#       with a very short interval, another tick event could fire before
				#       that happens, but calling .Close() on an already closed form is fine.
				$form.Close()
			})
		
		# Start the timer.
		$timer.Start()
		
		# Show the dialog, which will automatically close
		# when the timer fires.
		$result = $form.ShowDialog()
		
		# Dispose (stop) the timer. Doing this here instead of in the tick
		# event handler ensures that the timer is stopped even when the form 
		# was closed by the user.
		$timer.Dispose()
		
		# Dispose the form and its controls. Skip, if you want to redisplay the form later.
		$form.Close()
	}
	
	if ($GUIthatshouldbedisplayed -eq "3")
	{
		
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		$timer = New-Object System.Windows.Forms.Timer
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="RPOS Install Progress" Height="210" Width="610" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="52" Margin="185,119,0,0" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Rectangle HorizontalAlignment="Left" Height="109" Margin="10,10,0,0" Stroke="Black" VerticalAlignment="Top" Width="574" Fill="#FF7E7E7E"/>
        <Rectangle Fill="#FFD1D1D1" HorizontalAlignment="Left" Height="94" Margin="15,17,0,0" Stroke="Black" VerticalAlignment="Top" Width="564"/>
        <Rectangle HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" Stroke="Black" VerticalAlignment="Top" Width="564">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF707070" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" TextWrapping="Wrap" Text="  RPOS INSTALL PROGRESS" VerticalAlignment="Top" Width="245" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="35" Margin="23,47,0,0" TextWrapping="Wrap" Text="RPOS has completed installing the latest version of Adobe AIR." VerticalAlignment="Top" Width="549"/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="463,75,0,0" VerticalAlignment="Top" Width="109" Foreground="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFFB7979"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
        <Image x:Name="service" HorizontalAlignment="Left" Height="42" Margin="21,126,0,0" VerticalAlignment="Top" Width="31" Source="$RPOSAUDIRIMAGES\service.png"/>
        <Image x:Name="wheel" HorizontalAlignment="Left" Height="42" VerticalAlignment="Top" Width="37" Margin="73,125,0,0" Source="$RPOSAUDIRIMAGES\wheels.png"/>
        <Image x:Name="engine" HorizontalAlignment="Left" Height="42" Margin="127,126,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\engine.png"/>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		$timer.Interval = 15000 # 8 seconds
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		#$timer = [Timer]::new()
		#$timer.Interval = 8000 # 8 seconds
		
		# Set up the event handler for the timer.
		$timer.Add_Tick({
				# Close the form.
				# Note: We dispose (stop) the timer later, right after the form closes;
				#       with a very short interval, another tick event could fire before
				#       that happens, but calling .Close() on an already closed form is fine.
				$form.Close()
			})
		
		# Start the timer.
		$timer.Start()
		
		# Show the dialog, which will automatically close
		# when the timer fires.
		$result = $form.ShowDialog()
		
		# Dispose (stop) the timer. Doing this here instead of in the tick
		# event handler ensures that the timer is stopped even when the form 
		# was closed by the user.
		$timer.Dispose()
		
		# Dispose the form and its controls. Skip, if you want to redisplay the form later.
		$form.Close()
	}
	
	#If RPOS is successfully installed
	if ($GUIthatshouldbedisplayed -eq "4")
	{
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		$timer = New-Object System.Windows.Forms.Timer
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="RPOS Install Progress" Height="210" Width="610" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="52" Margin="185,119,0,0" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Rectangle HorizontalAlignment="Left" Height="109" Margin="10,10,0,0" Stroke="Black" VerticalAlignment="Top" Width="574" Fill="#FF7E7E7E"/>
        <Rectangle Fill="#FFD1D1D1" HorizontalAlignment="Left" Height="94" Margin="15,17,0,0" Stroke="Black" VerticalAlignment="Top" Width="564"/>
        <Rectangle HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" Stroke="Black" VerticalAlignment="Top" Width="564">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF707070" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" TextWrapping="Wrap" Text="  RPOS INSTALL SUCCESSFUL" VerticalAlignment="Top" Width="245" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="35" Margin="23,47,0,0" TextWrapping="Wrap" Text="RPOS successfully installed the latest version. You may use RPOS now." VerticalAlignment="Top" Width="549"/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="463,75,0,0" VerticalAlignment="Top" Width="109" Foreground="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFFB7979"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
        <Image x:Name="service" HorizontalAlignment="Left" Height="42" Margin="21,126,0,0" VerticalAlignment="Top" Width="31" Source="$RPOSAUDIRIMAGES\service.png"/>
        <Image x:Name="wheel" HorizontalAlignment="Left" Height="42" VerticalAlignment="Top" Width="37" Margin="73,125,0,0" Source="$RPOSAUDIRIMAGES\wheels.png"/>
        <Image x:Name="engine" HorizontalAlignment="Left" Height="42" Margin="127,126,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\engine.png"/>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		$timer.Interval = 15000 # 8 seconds
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		#$timer = [Timer]::new()
		#$timer.Interval = 8000 # 8 seconds
		
		# Set up the event handler for the timer.
		$timer.Add_Tick({
				# Close the form.
				# Note: We dispose (stop) the timer later, right after the form closes;
				#       with a very short interval, another tick event could fire before
				#       that happens, but calling .Close() on an already closed form is fine.
				$form.Close()
			})
		
		# Start the timer.
		$timer.Start()
		
		# Show the dialog, which will automatically close
		# when the timer fires.
		$result = $form.ShowDialog()
		
		# Dispose (stop) the timer. Doing this here instead of in the tick
		# event handler ensures that the timer is stopped even when the form 
		# was closed by the user.
		$timer.Dispose()
		
		# Dispose the form and its controls. Skip, if you want to redisplay the form later.
		$form.Close()
	}
	
	#If RPOS is unsuccessfully installed
	if ($GUIthatshouldbedisplayed -eq "5")
	{
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		$timer = New-Object System.Windows.Forms.Timer
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="RPOS Install Progress" Height="210" Width="610" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="52" Margin="185,119,0,0" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Rectangle HorizontalAlignment="Left" Height="109" Margin="10,10,0,0" Stroke="Black" VerticalAlignment="Top" Width="574" Fill="#FF7E7E7E"/>
        <Rectangle Fill="#FFD1D1D1" HorizontalAlignment="Left" Height="94" Margin="15,17,0,0" Stroke="Black" VerticalAlignment="Top" Width="564"/>
        <Rectangle HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" Stroke="Black" VerticalAlignment="Top" Width="564">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF707070" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="22" Margin="15,18,0,0" TextWrapping="Wrap" Text="  RPOS INSTALL UNSUCCESSFUL" VerticalAlignment="Top" Width="245" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="35" Margin="23,47,0,0" TextWrapping="Wrap" Text="RPOS was unsuccessful installing the latest version. It will try to install again at a later time." VerticalAlignment="Top" Width="549"/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="463,75,0,0" VerticalAlignment="Top" Width="109" Foreground="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFFB7979"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
        <Image x:Name="service" HorizontalAlignment="Left" Height="42" Margin="21,126,0,0" VerticalAlignment="Top" Width="31" Source="$RPOSAUDIRIMAGES\service.png"/>
        <Image x:Name="wheel" HorizontalAlignment="Left" Height="42" VerticalAlignment="Top" Width="37" Margin="73,125,0,0" Source="$RPOSAUDIRIMAGES\wheels.png"/>
        <Image x:Name="engine" HorizontalAlignment="Left" Height="42" Margin="127,126,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\engine.png"/>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		$timer.Interval = 15000 # 8 seconds
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		#$timer = [Timer]::new()
		#$timer.Interval = 8000 # 8 seconds
		
		# Set up the event handler for the timer.
		$timer.Add_Tick({
				# Close the form.
				# Note: We dispose (stop) the timer later, right after the form closes;
				#       with a very short interval, another tick event could fire before
				#       that happens, but calling .Close() on an already closed form is fine.
				$form.Close()
			})
		
		# Start the timer.
		$timer.Start()
		
		# Show the dialog, which will automatically close
		# when the timer fires.
		$result = $form.ShowDialog()
		
		# Dispose (stop) the timer. Doing this here instead of in the tick
		# event handler ensures that the timer is stopped even when the form 
		# was closed by the user.
		$timer.Dispose()
		
		# Dispose the form and its controls. Skip, if you want to redisplay the form later.
		$form.Close()
	}
	
	#If RPOS is successfully installed DETAILED
	if ($GUIthatshouldbedisplayed -eq "6")
	{
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		$timer = New-Object System.Windows.Forms.Timer
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS SUCCESSFUL INSTALL" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="RPOS successfully installed the latest version. You may use RPOS now."/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
	<Image x:Name="successfulimage" HorizontalAlignment="Left" Height="49" Margin="164,284,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\successful.png"/>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		$timer.Interval = 15000 # 8 seconds
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		#$timer = [Timer]::new()
		#$timer.Interval = 8000 # 8 seconds
		
		# Set up the event handler for the timer.
		$timer.Add_Tick({
				# Close the form.
				# Note: We dispose (stop) the timer later, right after the form closes;
				#       with a very short interval, another tick event could fire before
				#       that happens, but calling .Close() on an already closed form is fine.
				$form.Close()
			})
		
		# Start the timer.
		$timer.Start()
		
		# Show the dialog, which will automatically close
		# when the timer fires.
		$result = $form.ShowDialog()
		
		# Dispose (stop) the timer. Doing this here instead of in the tick
		# event handler ensures that the timer is stopped even when the form 
		# was closed by the user.
		$timer.Dispose()
		
		# Dispose the form and its controls. Skip, if you want to redisplay the form later.
		$form.Close()
	}
	
	#If RPOS is UNsuccessfully installed DEAILED
	if ($GUIthatshouldbedisplayed -eq "7")
	{
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		$timer = New-Object System.Windows.Forms.Timer
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS UNSUCCESSFUL INSTALL" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="RPOS was unsuccessful installing the latest version. It will try to install again at a later time."/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
	<Image x:Name="successfulimage" HorizontalAlignment="Left" Height="49" Margin="164,284,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\failure.png"/>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		$timer.Interval = 15000 # 8 seconds
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		#$timer = [Timer]::new()
		#$timer.Interval = 8000 # 8 seconds
		
		# Set up the event handler for the timer.
		$timer.Add_Tick({
				# Close the form.
				# Note: We dispose (stop) the timer later, right after the form closes;
				#       with a very short interval, another tick event could fire before
				#       that happens, but calling .Close() on an already closed form is fine.
				$form.Close()
			})
		
		# Start the timer.
		$timer.Start()
		
		# Show the dialog, which will automatically close
		# when the timer fires.
		$result = $form.ShowDialog()
		
		# Dispose (stop) the timer. Doing this here instead of in the tick
		# event handler ensures that the timer is stopped even when the form 
		# was closed by the user.
		$timer.Dispose()
		
		# Dispose the form and its controls. Skip, if you want to redisplay the form later.
		$form.Close()
	}
}

#######################################################################################################################
# Function Set-RPOSModuleClearReg
function Set-RPOSModuleClearReg
{
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRNI" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRInstalling" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckSelfSubDownload" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUFailedLocal" -Value "0" -Force
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AULocalReinstall" -Value "0" -Force
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "0" -Force
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingTesla" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUReinstall3rdTime" -Value "0" -Force
}

#######################################################################################################################
#  Function Check-RPOSEndpoints                                                                                       #
#######################################################################################################################
function Check-RPOSEndpoints
{
	Set-RPOSVar
	Write-Output "Starting Daily Nightly check of Endpoints for RPOS" | Out-File -Append -Filepath $Log
	
	if ($RPOSAgentname -eq "Store") {
		
		Write-Output "PC has been validated as a store PC, proceeding with looking at endpoints" | Out-File -Append -Filepath $Log
		
		#Grabbing users on PC
		$localstoreuesrs = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
		
		# Grabbing Registry Endpoint Settings
		$script:regsecurityendpoint = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Endpoint")."RPOS Security Endpoint"
		$script:regservicesendpoint = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Endpoint")."RPOS Services Endpoint"
		$script:regkatanaendpoint = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Endpoint")."RPOS Katana Endpoint"
		$script:regenablesecurity = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Enable")."RPOS Security Enable"
		$script:regenableservices = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Enable")."RPOS Services Enable"
		$script:regenablekatana = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Enable")."RPOS Katana Enable"
		
		foreach ($localstoreuser in $localstoreuesrs)
		{
			
			#Checking to see if there is a configuration file in local user profile
			$testrposuserconfigpath = Test-Path "C:\Users\${localstoreuser}\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
			
			if ($testrposuserconfigpath -eq "True")
			{
				
				# Grabbing configuration file contents now
				[xml]$xmlreadlocal = Get-Content "C:\Users\${localstoreuser}\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
				$script:localsecurityendpoint = $xmlreadlocal.ApplicationPreferences.securityEndpoint.'#cdata-section'
				$script:localservicesendpoint = $xmlreadlocal.ApplicationPreferences.servicesEndpoint.'#cdata-section'
				$script:localkatanaendpoint = $xmlreadlocal.ApplicationPreferences.katanaServicesEndpoint.'#cdata-section'
				$script:localenablesecurity = $xmlreadlocal.ApplicationPreferences.useCustomSecurityEndpoint.'#text'
				$script:localenableservices = $xmlreadlocal.ApplicationPreferences.useCustomServicesEndpoint.'#text'
				$script:localenablekatana = $xmlreadlocal.ApplicationPreferences.useCustomKatanaServicesEndpoint.'#text'
				
				if (($localsecurityendpoint -ne $regsecurityendpoint) -or ($localenablesecurity -ne $regenablesecurity) -and ($localsecurityendpoint) -and ($localenablesecurity))
				{
					
					Write-Output "There was a change in security endpoints from the xml configuration changing back from $localsecurityendpoint to $regsecurityendpoint and enabling it in XML" | Out-File -Append -Filepath $Log
					# Using Set-RPOSEndpoints Module to set security endpoints and enable endpoints
					Set-RPOSEndpoints -securityendpoint $regsecurityendpoint -enablesecurity $regenablesecurity
				}
				
				if (($localservicesendpoint -ne $regservicesendpoint) -or ($localenableservices -ne $regenableservices) -and ($localservicesendpoint) -and ($localenableservices))
				{
					Write-Output "There was a change in services endpoints from the xml configuration changing back from $localservicesendpoint to $regservicesendpoint and enabling it in XML" | Out-File -Append -Filepath $Log
					# Using Set-RPOSEndpoints Module to set services endpoints and enable endpoints
					Set-RPOSEndpoints -servicesendpoint $regservicesendpoint -enableservices $regenableservices
				}
				
				if (($localkatanaendpoint -ne $regkatanaendpoint) -or ($localenablekatana -ne $regenablekatana) -and ($localkatanaendpoint) -and ($localenablekatana))
				{
					Write-Output "There was a change in services endpoints from the xml configuration changing back from $localkatanaendpoint to $regkatanaendpoint and enabling it in XML" | Out-File -Append -Filepath $Log
					# Using Set-RPOSEndpoints Module to set katana endpoints and enable endpoints
					Set-RPOSEndpoints -katanaendpoint $regkatanaendpoint -enablekatana $regenablekatana
				}
			}
			
		}
	}
	Else
	{
		Write-Output "Check-RPOSEndpoints Corporate PC's are not supported for custom endpoints in $ReleaseInfo. BYPASSING" | Out-File -Append -Filepath $Log
	}
	
}
#######################################################################################################
# Function Set-RPOSEndpointCheckXML                                                                   #
#######################################################################################################
function Set-RPOSEndpointsCheckXML {
	
	Write-Output "Starting to check Endpoints for RPOS" | Out-File -Append -Filepath $Log
	Set-RPOSVar
	Set-RPOSModuleWhatPCAmI
	
	if ($RPOSAgentname -eq "Store")
	{
		
		# Adding new MD5 Hash to the registry for check points
		Set-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EndpointXMLchecksum" -Value $checkendpointxml -Type String -Force
		
		# Grabbing users on PC
		$localstoreuesrs = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
		
		# Checking to see if there is a change to the RPOS Endpoints
		$EndpointXMLURL = "http://rpos.tbccorp.com/rposendpoints.xml"
		
		# Getting endpoint information from RPOS.tbccorp.com from rposendpoints xml
		Write-Output "Grabbing contents of Endpoint XML for store locations on RPOS.tbccorp.com" | Out-File -Append -Filepath $Log
		$xmlread = [xml](New-Object System.Net.WebClient).DownloadString($EndpointXMLURL)
		$script:onlinexmlsecurityendpoint = $xmlread.customendpoint.$switchvalue.security
		$script:onlinexmlservicesendpoint = $xmlread.customendpoint.$switchvalue.services
		$script:onlinexmlkatanaendpoint = $xmlread.customendpoint.$switchvalue.katana
		$script:onlinexmlenablesecurity = $xmlread.customendpoint.$switchvalue.enablesecurity
		$script:onlinexmlenableservices = $xmlread.customendpoint.$switchvalue.enableservices
		$script:onlinexmlenablekatana = $xmlread.customendpoint.$switchvalue.enablekatana
		
		# Taking Enable Values and switching them to zeros and ones
		# Editing varible for security
		if ($onlinexmlenablesecurity -eq "true") {
			$script:onlinexmlenablesecurity = "1" }
		if ($onlinexmlenablesecurity -eq "false") {
			$script:onlinexmlenablesecurity = "0" }
		
		# Editing varible for services
		if ($onlinexmlenableservices -eq "true") {
			$script:onlinexmlenableservices = "1" }
		if ($onlinexmlenableservices -eq "false") {
			$script:onlinexmlenableservices = "0" }
		
		#Editing variable for Katana
		if ($onlinexmlenablekatana -eq "true") {
			$script:onlinexmlenablekatana = "1" }
		if ($onlinexmlenablekatana -eq "false") {
			$script:onlinexmlenablekatana = "0" }
		
		# Grabbing Registry Endpoint Settings
		$script:regsecurityendpoint = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Endpoint")."RPOS Security Endpoint"
		$script:regservicesendpoint = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Endpoint")."RPOS Services Endpoint"
		$script:regkatanaendpoint = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Endpoint")."RPOS Katana Endpoint"
		$script:regenablesecurity = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Enable")."RPOS Security Enable"
		$script:regenableservices = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Enable")."RPOS Services Enable"
		$script:regenablekatana = (Get-ItemProperty -Path "HKLM:\SOFTWARE\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Enable")."RPOS Katana Enable"
		
		
		if (($regsecurityendpoint -ne $onlinexmlsecurityendpoint) -or ($regenablesecurity -ne $onlinexmlenablesecurity))
		{
			Write-Output "There was a change in security endpoints changing from $localsecurityendpoint to $onlinexmlsecurityendpoint and enabling it in XML" | Out-File -Append -Filepath $Log
			
			#Setting Registry Keys for Security Endpoints
			Set-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Endpoint" -Value $onlinexmlsecurityendpoint -Type String -Force
			Set-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Enable" -Value $onlinexmlenablesecurity -Type String -Force
			
			# Using Set-RPOSEndpoints Module to set security endpoints and enable endpoints
			Set-RPOSEndpoints -securityendpoint $onlinexmlsecurityendpoint -enablesecurity $onlinexmlenablesecurity
		}
		
		if (($regservicesendpoint -ne $onlinexmlservicesendpoint) -or ($regenableservices -ne $onlinexmlenableservices))
		{
			Write-Output "There was a change in service endpoints changing from $localservicesendpoint to $onlinexmlservicesendpoint and enabling it in XML" | Out-File -Append -Filepath $Log
			
			#Setting Registry Keys for Services Endpoints
			Set-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Endpoint" -Value $onlinexmlservicesendpoint -Type String -Force
			Set-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Enable" -Value $onlinexmlenableservices -Type String -Force
			
			# Using Set-RPOSEndpoints Module to set services endpoints and enable endpoints
			Set-RPOSEndpoints -servicesendpoint $onlinexmlservicesendpoint -enableservices $onlinexmlenableservices
		}
		
		if (($regkatanaendpoint -ne $onlinexmlkatanaendpoint) -or ($regenablekatana -ne $onlinexmlenablekatana))
		{
			Write-Output "There was a change in Katana endpoints changing from $localkatanaendpoint to $onlinexmlkatanaendpoint and enabling it in XML" | Out-File -Append -Filepath $Log
			
			#Setting Registry Keys for Katana Endpoints
			Set-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Endpoint" -Value $onlinexmlkatanaendpoint -Type String -Force
			Set-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Enable" -Value $onlinexmlenablekatana -Type String -Force
			
			# Using Set-RPOSEndpoints Module to set katana endpoints and enable endpoints
			Set-RPOSEndpoints -katanaendpoint $onlinexmlkatanaendpoint -enablekatana $onlinexmlenablekatana
		}
	}
	Else
	{
		Write-Output "This PC is a corporate PC, Set-RPOSEndpointsCheckXML is not supported in $ReleaseInfo" | Out-File -Append -Filepath $Log
	}
}


#######################################################################################################
# Function Set-RPOSEndpoints                                                                          #
#######################################################################################################
function Set-RPOSEndpoints
{
	param (
		[parameter(Mandatory = $false, HelpMessage = "This is the Security endpoint for RPOS, please use the full URL when entering.")]
		$securityendpoint,
		[parameter(Mandatory = $false, HelpMessage = "This is the Services endpoint for RPOS, please use the full URL when entering.")]
		$servicesendpoint,
		[parameter(Mandatory = $false, HelpMessage = "This is the Katana endpoiint for RPOS, please use the full URL when entering.")]
		$katanaendpoint,
		[parameter(Mandatory = $false, HelpMessage = "This is telling wether to enable or disable security endpoint for RPOS. 0 for off and 1 for on.")]
		[ValidateRange(0, 1)]
		$enablesecurity,
		[parameter(Mandatory = $false, HelpMessage = "This is telling wether to enable or disable services endpoint for RPOS. 0 for off and 1 for on.")]
		[ValidateRange(0, 1)]
		$enableservices,
		[parameter(Mandatory = $false, HelpMessage = "This is telling wether to enable or disable Katana endpoint for RPOS. 0 for off and 1 for on.")]
		[ValidateRange(0, 1)]
		$enablekatana)
	
	#Getting Current Users on PC
	$usersendpoint = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
	
	
	if (($securityendpoint) -and ($enablesecurity))
	{
		#Getting Current Users on PC
		$usersendpoint = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
		
		#Killing RPOS and FlowModule
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
		Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
		
		#Foreach User in Users
		foreach ($userendpoint in $usersendpoint)
		{
			
			$preferencesfile = Test-Path "C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
			
			if ($preferencesfile -eq $true)
			{
				#Grabbing RPOS Preferences File
				[xml]$xmlrpospreferences = Get-Content "C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
				
				#Setting Preferences Data for security
				$xmlrpospreferences.ApplicationPreferences.securityEndpoint.'#cdata-section' = "$securityendpoint"
				$xmlrpospreferences.ApplicationPreferences.useCustomSecurityEndpoint.'#text' = "$enablesecurity"
				$xmlrpospreferences.Save("C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml")
				
			}
		}
		
	}
	
	if (($servicesendpoint) -and ($enableservices))
	{
		#Getting Current Users on PC
		$usersendpoint = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
		
		#Killing RPOS and FlowModule
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
		Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
		
		#Foreach User in Users
		foreach ($userendpoint in $usersendpoint)
		{
			
			$preferencesfile = Test-Path "C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
			
			if ($preferencesfile -eq $true)
			{
				#Grabbing RPOS Preferences File
				[xml]$xmlrpospreferences = Get-Content "C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
				
				#Setting Preferences Data for services
				$xmlrpospreferences.ApplicationPreferences.servicesEndpoint.'#cdata-section' = "$servicesendpoint"
				$xmlrpospreferences.ApplicationPreferences.useCustomServicesEndpoint.'#text' = "$enableservices"
				$xmlrpospreferences.Save("C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml")
				
			}
		}
		
	}
	
	if (($katanaendpoint) -and ($enablekatana))
	{
		#Getting Current Users on PC
		$usersendpoint = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
		
		#Killing RPOS and FlowModule
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
		Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
		
		#Foreach User in Users
		foreach ($userendpoint in $usersendpoint)
		{
			
			$preferencesfile = Test-Path "C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
			
			if ($preferencesfile -eq $true)
			{
				#Grabbing RPOS Prefences File
				[xml]$xmlrpospreferences = Get-Content "C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml"
				
				#Setting Preferences Data for Katana
				$xmlrpospreferences.ApplicationPreferences.katanaServicesEndpoint.'#cdata-section' = "$katanaendpoint"
				$xmlrpospreferences.ApplicationPreferences.useCustomKatanaServicesEndpoint.'#text' = "$enablekatana"
				$xmlrpospreferences.Save("C:\Users\$userendpoint\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\rpos-preferences.xml")
				
			}
			
		}
		
	}
	
}
#########################################################################################################################################
# Function Set-RPOSCreateUserScheduledTask
function Set-RPOSCreateUserScheduledTask
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-05-04T09:15:09.7726706"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	
	[System.Xml.XmlElement]$Task24Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task25Root = $Task24Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task25Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task26Root = $Task25Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task26Root.InnerText = "$username"
	[System.Xml.XmlElement]$Task27Root = $Task25Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task27Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task27Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task28Root = $Task27Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task28Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task29Root = $Task27Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task29Root.InnerText = "false"
	[System.Xml.XmlElement]$Task30Root = $Task27Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task30Root.InnerText = "true"
	[System.Xml.XmlElement]$Task31Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task31Root.InnerText = "true"
	[System.Xml.XmlElement]$Task32Root = $Task27Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task33Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task34Root = $Task27Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task35Root = $Task34Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task35Root.InnerText = "true"
	[System.Xml.XmlElement]$Task36Root = $Task34Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task36Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task37Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task37Root.InnerText = "true"
	[System.Xml.XmlElement]$Task38Root = $Task27Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task38Root.InnerText = "true"
	[System.Xml.XmlElement]$Task39Root = $Task27Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task39Root.InnerText = "false"
	[System.Xml.XmlElement]$Task40Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task40Root.InnerText = "false"
	[System.Xml.XmlElement]$Task41Root = $Task27Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task41Root.InnerText = "false"
	[System.Xml.XmlElement]$Task42Root = $Task27Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task42Root.InnerText = "P3D"
	[System.Xml.XmlElement]$Task43Root = $Task27Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task43Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task44Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task44Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task45Root = $Task44Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task46Root = $Task45Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task46Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task47Root = $Task45Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task47Root.InnerText = "-ExecutionPolicy Bypass -Windowstyle Hidden -command Get-RPOSLocalUserGUI"
	
	$Taskone1.Save("$RPOSCreateScheduledTaskGUIXML")
}

#########################################################################################################################################
# Function Set-RPOSCreateUserScheduledTaskWin10
function Set-RPOSCreateUserScheduledTaskWin10
{
	$taskSecurity = [System.Security.AccessControl.FileSecurity]::new()
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"NT AUTHORITY\System",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Administrators",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Users",
			"ReadAndExecute",
			"Allow"
		)
	)
	
	$taskParams = @{
		Principal = New-ScheduledTaskPrincipal -UserId "SYSTEM" -RunLevel Highest
		Action    = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-ExecutionPolicy Bypass -WindowStyle hidden -Command Get-RPOSLocalUserGui"
		Settings  = New-ScheduledTaskSettingsSet -Hidden -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit (New-TimeSpan -Minutes 60)
	}
	
	$taskObj = New-ScheduledTask @taskParams
	
	New-ScheduledJobOption
	$taskObj.SecurityDescriptor = $taskSecurity.Sddl
	
	$taskObj | Register-ScheduledTask -TaskName "\RPOS AU Local User GUI $useridsub" -User "$username" -Force | Out-Null
	
}

#########################################################################################################################################
# Function Set-RPOSCreateClearLogsXML
function Set-RPOSCreateClearLogsXML
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-05-04T09:15:09.7726706"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task7Root.InnerText = "2016-05-04T00:00:00"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task8Root.InnerText = "true"
	[System.Xml.XmlElement]$Task9Root = $Task6Root.AppendChild($Taskone1.CreateElement("ScheduleByMonth"))
	[System.Xml.XmlElement]$Task10Root = $Task9Root.AppendChild($Taskone1.CreateElement("DaysOfMonth"))
	[System.Xml.XmlElement]$Task11Root = $Task10Root.AppendChild($Taskone1.CreateElement("Day"))
	$Task11Root.InnerText = "1"
	[System.Xml.XmlElement]$Task11Root = $Task9Root.AppendChild($Taskone1.CreateElement("Months"))
	[System.Xml.XmlElement]$Task12Root = $Task11Root.AppendChild($Taskone1.CreateElement("January"))
	[System.Xml.XmlElement]$Task13Root = $Task11Root.AppendChild($Taskone1.CreateElement("February"))
	[System.Xml.XmlElement]$Task14Root = $Task11Root.AppendChild($Taskone1.CreateElement("March"))
	[System.Xml.XmlElement]$Task15Root = $Task11Root.AppendChild($Taskone1.CreateElement("April"))
	[System.Xml.XmlElement]$Task16Root = $Task11Root.AppendChild($Taskone1.CreateElement("May"))
	[System.Xml.XmlElement]$Task17Root = $Task11Root.AppendChild($Taskone1.CreateElement("June"))
	[System.Xml.XmlElement]$Task18Root = $Task11Root.AppendChild($Taskone1.CreateElement("July"))
	[System.Xml.XmlElement]$Task19Root = $Task11Root.AppendChild($Taskone1.CreateElement("August"))
	[System.Xml.XmlElement]$Task20Root = $Task11Root.AppendChild($Taskone1.CreateElement("September"))
	[System.Xml.XmlElement]$Task21Root = $Task11Root.AppendChild($Taskone1.CreateElement("October"))
	[System.Xml.XmlElement]$Task22Root = $Task11Root.AppendChild($Taskone1.CreateElement("November"))
	[System.Xml.XmlElement]$Task23Root = $Task11Root.AppendChild($Taskone1.CreateElement("December"))
	
	[System.Xml.XmlElement]$Task24Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task25Root = $Task24Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task25Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task26Root = $Task25Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task26Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task27Root = $Task25Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task27Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task27Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task28Root = $Task27Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task28Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task29Root = $Task27Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task29Root.InnerText = "false"
	[System.Xml.XmlElement]$Task30Root = $Task27Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task30Root.InnerText = "true"
	[System.Xml.XmlElement]$Task31Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task31Root.InnerText = "true"
	[System.Xml.XmlElement]$Task32Root = $Task27Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task33Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task34Root = $Task27Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task35Root = $Task34Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task35Root.InnerText = "true"
	[System.Xml.XmlElement]$Task36Root = $Task34Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task36Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task37Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task37Root.InnerText = "true"
	[System.Xml.XmlElement]$Task38Root = $Task27Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task38Root.InnerText = "true"
	[System.Xml.XmlElement]$Task39Root = $Task27Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task39Root.InnerText = "false"
	[System.Xml.XmlElement]$Task40Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task40Root.InnerText = "false"
	[System.Xml.XmlElement]$Task41Root = $Task27Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task41Root.InnerText = "false"
	[System.Xml.XmlElement]$Task42Root = $Task27Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task42Root.InnerText = "P3D"
	[System.Xml.XmlElement]$Task43Root = $Task27Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task43Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task44Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task44Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task45Root = $Task44Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task46Root = $Task45Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task46Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task47Root = $Task45Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task47Root.InnerText = "-ExecutionPolicy Bypass -command Get-RPOSRemoveAULogs"
	
	$Taskone1.Save("$RPOSClearLogsScheduleXML")
}
#########################################################################################################################################
# Function Set-RPOSCreateClearLogsXML
function Set-RPOSCreateClearLogsXMLWin10
{
	$taskSecurity = [System.Security.AccessControl.FileSecurity]::new()
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"NT AUTHORITY\System",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Administrators",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Users",
			"ReadAndExecute",
			"Allow"
		)
	)
	
	$taskParams = @{
		Principal = New-ScheduledTaskPrincipal -UserId "SYSTEM" -RunLevel Highest
		Action    = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-ExecutionPolicy Bypass -command Get-RPOSRemoveAULogs"
		Trigger   = @(
			(New-ScheduledTaskTrigger -At "0:00:00" -Weekly -DaysOfWeek Monday)
		)
		Settings  = New-ScheduledTaskSettingsSet -Hidden -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit (New-TimeSpan -Minutes 60)
	}
	
	$taskObj = New-ScheduledTask @taskParams
	
	New-ScheduledJobOption
	$taskObj.SecurityDescriptor = $taskSecurity.Sddl
	
	$taskObj | Register-ScheduledTask -TaskName "Update RPOS Clear Logs" -User "SYSTEM" -Force | Out-Null

}
#########################################################################################################################################
# Function Set-RPOSCreateRollbackRPOSXML
function Set-RPOSCreateRollbackRPOSXML
{	
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-05-05T12:15:36.3215604"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task7Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task8Root = $Task7Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task8Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task9Root = $Task7Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task9Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task10Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task12Root = $Task10Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task12Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task13Root = $Task10Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task13Root.InnerText = "false"
	[System.Xml.XmlElement]$Task14Root = $Task10Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task14Root.InnerText = "true"
	[System.Xml.XmlElement]$Task15Root = $Task10Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task15Root.InnerText = "false"
	[System.Xml.XmlElement]$Task16Root = $Task10Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task16Root.InnerText = "false"
	[System.Xml.XmlElement]$Task17Root = $Task10Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task17Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task18Root = $Task10Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task19Root = $Task18Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task19Root.InnerText = "true"
	[System.Xml.XmlElement]$Task20Root = $Task18Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task20Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task21Root = $Task10Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task21Root.InnerText = "true"
	[System.Xml.XmlElement]$Task22Root = $Task10Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task22Root.InnerText = "true"
	[System.Xml.XmlElement]$Task23Root = $Task10Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task23Root.InnerText = "false"
	[System.Xml.XmlElement]$Task24Root = $Task10Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task24Root.InnerText = "false"
	[System.Xml.XmlElement]$Task25Root = $Task10Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task25Root.InnerText = "false"
	[System.Xml.XmlElement]$Task26Root = $Task10Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task26Root.InnerText = "PT8H"
	[System.Xml.XmlElement]$Task27Root = $Task10Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task27Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task28Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task28Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task29Root = $Task28Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task30Root = $Task29Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task30Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task31Root = $Task29Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task31Root.InnerText = "-ExecutionPolicy Bypass -command Get-RPOSRollback"
	
	$Taskone1.Save("$RPOSRollbackScheduledXML")
	
}

#########################################################################################################################################
# Function Set-RPOSCreateRollbackRPOSXMLWIn10
function Set-RPOSCreateRollbackRPOSXMLWin10
{
	$taskSecurity = [System.Security.AccessControl.FileSecurity]::new()
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"NT AUTHORITY\System",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Administrators",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Users",
			"ReadAndExecute",
			"Allow"
		)
	)
	
	$taskParams = @{
		Principal = New-ScheduledTaskPrincipal -UserId "SYSTEM" -RunLevel Highest
		Action    = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-ExecutionPolicy Bypass -command Get-RPOSRollback"
		Settings  = New-ScheduledTaskSettingsSet -Hidden -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit (New-TimeSpan -Minutes 60)
	}
	
	$taskObj = New-ScheduledTask @taskParams
	
	New-ScheduledJobOption
	$taskObj.SecurityDescriptor = $taskSecurity.Sddl
	
	$taskObj | Register-ScheduledTask -TaskName "Rollback RPOS Local" -User "SYSTEM" -Force | Out-Null
}

#########################################################################################################################################
#  Function Set-RPOSCreateABTaskXML
function Set-RPOSCreateABTaskXML
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2016-06-16T18:25:06.7320657"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "EOC"
	
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	####################################################################################################
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("BootTrigger"))
	####################################################################################################
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task7Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task8Root.InnerText = "true"
	[System.Xml.XmlElement]$Task9Root = $Task6Root.AppendChild($Taskone1.CreateElement("Delay"))
	$Task9Root.InnerText = "PT1M"
	
	
	[System.Xml.XmlElement]$Task10Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task11Root = $Task10Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task11Root.InnerText = "$PCTime1"
	[System.Xml.XmlElement]$Task12Root = $Task10Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task12Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task13Root = $Task10Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task13Root.InnerText = "true"
	[System.Xml.XmlElement]$Task14Root = $Task10Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task15Root = $Task14Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task15Root.InnerText = "1"
	
	[System.Xml.XmlElement]$Task16Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task17Root = $Task16Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task17Root.InnerText = "$PCTime2"
	[System.Xml.XmlElement]$Task18Root = $Task16Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task18Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task19Root = $Task16Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task19Root.InnerText = "true"
	[System.Xml.XmlElement]$Task20Root = $Task16Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task21Root = $Task20Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task21Root.InnerText = "1"
	
	
	[System.Xml.XmlElement]$Task22Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task23Root = $Task22Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task23Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task24Root = $Task23Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task24Root.InnerText = "TBC\shprorpos2"
	[System.Xml.XmlElement]$Task25Root = $Task23Root.AppendChild($Taskone1.CreateElement("LogonType"))
	$Task25Root.InnerText = "Password"
	[System.Xml.XmlElement]$Task26Root = $Task23Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task26Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task27Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task28Root = $Task27Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task28Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task29Root = $Task27Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task29Root.InnerText = "false"
	[System.Xml.XmlElement]$Task30Root = $Task27Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task30Root.InnerText = "true"
	[System.Xml.XmlElement]$Task31Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task31Root.InnerText = "true"
	[System.Xml.XmlElement]$Task32Root = $Task27Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task33Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task34Root = $Task27Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task35Root = $Task34Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task35Root.InnerText = "true"
	[System.Xml.XmlElement]$Task36Root = $Task34Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task36Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task37Root = $Task27Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task37Root.InnerText = "true"
	[System.Xml.XmlElement]$Task38Root = $Task27Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task38Root.InnerText = "true"
	[System.Xml.XmlElement]$Task39Root = $Task27Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task39Root.InnerText = "true"
	[System.Xml.XmlElement]$Task40Root = $Task27Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task40Root.InnerText = "false"
	[System.Xml.XmlElement]$Task41Root = $Task27Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task41Root.InnerText = "false"
	[System.Xml.XmlElement]$Task42Root = $Task27Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task42Root.InnerText = "PT2H"
	[System.Xml.XmlElement]$Task43Root = $Task27Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task43Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task44Root = $Task27Root.AppendChild($Taskone1.CreateElement("RestartOnFailure"))
	[System.Xml.XmlElement]$Task45Root = $Task44Root.AppendChild($Taskone1.CreateElement("Interval"))
	$Task45Root.InnerText = "PT15M"
	[System.Xml.XmlElement]$Task46Root = $Task44Root.AppendChild($Taskone1.CreateElement("Count"))
	$Task46Root.InnerText = "3"
	
	
	[System.Xml.XmlElement]$Task47Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task47Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task48Root = $Task47Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task49Root = $Task48Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task49Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task50Root = $Task48Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task50Root.InnerText = "-ExecutionPolicy Bypass -command Get-RPOSUpdateRPOS"
	
	$Taskone1.Save("$TasksLocalRPOSABFile")
	
}
#########################################################################################################################################
# Fuction Set-RPOSCreateLiveUpdateTask
function Set-RPOSCreateLiveUpdateTask
{
	[System.Xml.XmlDocument]$Taskone1 = New-Object System.Xml.XmlDocument
	
	[System.XML.XMLElement]$Task1Root = $Taskone1.CreateElement("Task")
	$Task1Root.SetAttribute("version", "1.2")
	$Task1Root.SetAttribute("xmlns", "http://schemas.microsoft.com/windows/2004/02/mit/task")
	$Taskone1.AppendChild($Task1Root)
	
	[System.Xml.XmlElement]$Task2Root = $Task1Root.AppendChild($Taskone1.CreateElement("RegistrationInfo"))
	[System.Xml.XmlElement]$Task3Root = $Task2Root.AppendChild($Taskone1.CreateElement("Date"))
	$Task3Root.InnerText = "2018-03-12T10:14:50.4432154"
	[System.Xml.XmlElement]$Task4Root = $Task2Root.AppendChild($Taskone1.CreateElement("Author"))
	$Task4Root.InnerText = "TBC EOC Department"
	[System.Xml.XmlElement]$Task38Root = $Task2Root.AppendChild($Taskone1.CreateElement("URI"))
	$Task38Root.InnerText = "\Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate"
	
	[System.Xml.XmlElement]$Task5Root = $Task1Root.AppendChild($Taskone1.CreateElement("Triggers"))
	[System.Xml.XmlElement]$Task6Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task39Root = $Task6Root.AppendChild($Taskone1.CreateElement("Repetition"))
	[System.Xml.XmlElement]$Task40Root = $Task39Root.AppendChild($Taskone1.CreateElement("Interval"))
	$Task40Root.InnerText = "PT6H"
	[System.Xml.XmlElement]$Task41Root = $Task39Root.AppendChild($Taskone1.CreateElement("StopAtDurationEnd"))
	$Task41Root.InnerText = "false"
	[System.Xml.XmlElement]$Task7Root = $Task6Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task7Root.InnerText = "2018-03-12T06:00:00"
	[System.Xml.XmlElement]$Task8Root = $Task6Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task8Root.InnerText = "true"
	
	[System.Xml.XmlElement]$Task9Root = $Task6Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task10Root = $Task9Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task10Root.InnerText = "1"
	
	[System.Xml.XmlElement]$Task11Root = $Task5Root.AppendChild($Taskone1.CreateElement("BootTrigger"))
	[System.Xml.XmlElement]$Task12Root = $Task11Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task12Root.InnerText = "true"
	[System.Xml.XmlElement]$Task13Root = $Task11Root.AppendChild($Taskone1.CreateElement("Delay"))
	$Task13Root.InnerText = "PT6M"
	
	[System.Xml.XmlElement]$Task42Root = $Task5Root.AppendChild($Taskone1.CreateElement("CalendarTrigger"))
	[System.Xml.XmlElement]$Task43Root = $Task42Root.AppendChild($Taskone1.CreateElement("StartBoundary"))
	$Task43Root.InnerText = "$RPOSAULiveUpdateTime"
	[System.Xml.XmlElement]$Task44Root = $Task42Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task44Root.InnerText = "true"
	
	[System.Xml.XmlElement]$Task45Root = $Task42Root.AppendChild($Taskone1.CreateElement("ScheduleByDay"))
	[System.Xml.XmlElement]$Task46Root = $Task45Root.AppendChild($Taskone1.CreateElement("DaysInterval"))
	$Task46Root.InnerText = "1"
	
	[System.Xml.XmlElement]$Task14Root = $Task1Root.AppendChild($Taskone1.CreateElement("Principals"))
	[System.Xml.XmlElement]$Task15Root = $Task14Root.AppendChild($Taskone1.CreateElement("Principal"))
	$Task15Root.SetAttribute("id", "Author")
	[System.Xml.XmlElement]$Task16Root = $Task15Root.AppendChild($Taskone1.CreateElement("UserId"))
	$Task16Root.InnerText = "S-1-5-18"
	[System.Xml.XmlElement]$Task17Root = $Task15Root.AppendChild($Taskone1.CreateElement("RunLevel"))
	$Task17Root.InnerText = "HighestAvailable"
	
	[System.Xml.XmlElement]$Task18Root = $Task1Root.AppendChild($Taskone1.CreateElement("Settings"))
	[System.Xml.XmlElement]$Task19Root = $Task18Root.AppendChild($Taskone1.CreateElement("MultipleInstancesPolicy"))
	$Task19Root.InnerText = "IgnoreNew"
	[System.Xml.XmlElement]$Task20Root = $Task18Root.AppendChild($Taskone1.CreateElement("DisallowStartIfOnBatteries"))
	$Task20Root.InnerText = "false"
	[System.Xml.XmlElement]$Task21Root = $Task18Root.AppendChild($Taskone1.CreateElement("StopIfGoingOnBatteries"))
	$Task21Root.InnerText = "true"
	[System.Xml.XmlElement]$Task22Root = $Task18Root.AppendChild($Taskone1.CreateElement("AllowHardTerminate"))
	$Task22Root.InnerText = "true"
	[System.Xml.XmlElement]$Task23Root = $Task18Root.AppendChild($Taskone1.CreateElement("StartWhenAvailable"))
	$Task23Root.InnerText = "false"
	[System.Xml.XmlElement]$Task24Root = $Task18Root.AppendChild($Taskone1.CreateElement("RunOnlyIfNetworkAvailable"))
	$Task24Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task25Root = $Task18Root.AppendChild($Taskone1.CreateElement("IdleSettings"))
	[System.Xml.XmlElement]$Task26Root = $Task25Root.AppendChild($Taskone1.CreateElement("StopOnIdleEnd"))
	$Task26Root.InnerText = "true"
	[System.Xml.XmlElement]$Task27Root = $Task25Root.AppendChild($Taskone1.CreateElement("RestartOnIdle"))
	$Task27Root.InnerText = "false"
	
	[System.Xml.XmlElement]$Task28Root = $Task18Root.AppendChild($Taskone1.CreateElement("AllowStartOnDemand"))
	$Task28Root.InnerText = "true"
	[System.Xml.XmlElement]$Task29Root = $Task18Root.AppendChild($Taskone1.CreateElement("Enabled"))
	$Task29Root.InnerText = "true"
	[System.Xml.XmlElement]$Task30Root = $Task18Root.AppendChild($Taskone1.CreateElement("Hidden"))
	$Task30Root.InnerText = "false"
	[System.Xml.XmlElement]$Task31Root = $Task18Root.AppendChild($Taskone1.CreateElement("RunOnlyIfIdle"))
	$Task31Root.InnerText = "false"
	[System.Xml.XmlElement]$Task32Root = $Task18Root.AppendChild($Taskone1.CreateElement("WakeToRun"))
	$Task32Root.InnerText = "false"
	[System.Xml.XmlElement]$Task33Root = $Task18Root.AppendChild($Taskone1.CreateElement("ExecutionTimeLimit"))
	$Task33Root.InnerText = "PT12H"
	[System.Xml.XmlElement]$Task34Root = $Task18Root.AppendChild($Taskone1.CreateElement("Priority"))
	$Task34Root.InnerText = "7"
	
	[System.Xml.XmlElement]$Task35Root = $Task1Root.AppendChild($Taskone1.CreateElement("Actions"))
	$Task35Root.SetAttribute("Context", "Author")
	[System.Xml.XmlElement]$Task36Root = $Task35Root.AppendChild($Taskone1.CreateElement("Exec"))
	[System.Xml.XmlElement]$Task37Root = $Task36Root.AppendChild($Taskone1.CreateElement("Command"))
	$Task37Root.InnerText = "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe"
	[System.Xml.XmlElement]$Task47Root = $Task36Root.AppendChild($Taskone1.CreateElement("Arguments"))
	$Task47Root.InnerText = "-ExecutionPolicy Bypass -command Get-RPOSLiveUpdate"
	
	
	$Taskone1.Save("$RPOSLiveUpdateXML")
}
#########################################################################################################################################
# Fuction Set-RPOSCreateLiveUpdateTask
function Set-RPOSCreateLiveUpdateTaskWin10
{
	$taskSecurity = [System.Security.AccessControl.FileSecurity]::new()
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"NT AUTHORITY\System",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Administrators",
			"FullControl",
			"Allow"
		)
	)
	$taskSecurity.AddAccessRule(
		[System.Security.AccessControl.FileSystemAccessRule]::new(
			[System.Security.Principal.NTAccount]"BUILTIN\Users",
			"ReadAndExecute",
			"Allow"
		)
	)
	
	$taskParams = @{
		Principal = New-ScheduledTaskPrincipal -UserId "SYSTEM" -RunLevel Highest
		Action    = New-ScheduledTaskAction -Execute "C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -Argument "-ExecutionPolicy Bypass -command Get-RPOSLiveUpdate"
		Trigger   = @(
			(New-JobTrigger -AtStartup -RandomDelay (New-TimeSpan -Minutes 1)),
			(New-ScheduledTaskTrigger -At "$RPOSAULiveUpdateTime" -Daily)
			(New-ScheduledTaskTrigger -Once -At "6:00:00" -RepetitionInterval (New-TimeSpan -Hours 6))
		)
		Settings  = New-ScheduledTaskSettingsSet -Hidden -AllowStartIfOnBatteries -DontStopIfGoingOnBatteries -ExecutionTimeLimit (New-TimeSpan -Minutes 10)
	}
	
	$taskObj = New-ScheduledTask @taskParams
	
	New-ScheduledJobOption
	$taskObj.SecurityDescriptor = $taskSecurity.Sddl
	
	$taskObj | Register-ScheduledTask -TaskName "\Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate" -User "SYSTEM" -Force | Out-Null
}

#########################################################################################################################################
# Function Set-RPOSModuleDownloadHashSetup
function Set-RPOSModuleDownloadHashSetup {
	param (
		[parameter(Mandatory=$true,HelpMessage="Downloadfile parm is the actual http URL to the file needed to download.")]
		[string]$downloadfile,
		[parameter(Mandatory=$true,HelpMessage="Localfile parm is the path in which you want the file to save to and the filename.")]
		[string]$localfile,
		[parameter(Mandatory=$true,HelpMessage="DownloadMD5 is the hash value of the file in which powershell will compare the file to. All values must be filled out in order for the function to run.")]
		[string]$downloadMD5)
	
	Set-ItemProperty -Path "$LANDeskKeys" -Name "DownloadMD5HashModule" -Value "0" -Force
	
	while ($downloadtimes -lt 5)
	{
		$downloadtimes += 1
		Write-Output "Starting to download file $downloadfile" | Out-File -Append -FilePath $Log
		Invoke-WebRequest -Uri $downloadfile -OutFile $localfile
		$localfilehash = Get-FileHash -Path "$localfile" -Algorithm MD5 | Select-Object -ExpandProperty hash
		if ($downloadMD5 -eq $localfilehash)
		{
			Set-ItemProperty -Path "$LANDeskKeys" -Name "DownloadMD5HashModule" -Value "1" -Force
			Write-Output "File has been downloaded $localfilehash and the MD5 hash that was supposed to match is $downloadMD5 file is complete and downloaded." | Out-File -Append -FilePath $Log
		}
		if ($downloadMD5 -eq $localfilehash) {break}
	}
	
}
#########################################################################################################################################
# Function Set-RPOSModuleTeslaCheckSymbolicLink
function Set-RPOSModuleTeslaCheckSymbolicLink
{
	
	$SymbolicLinkUsers = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
	
	foreach ($SymbolicLinkUser in $SymbolicLinkUsers)
	{
		#Checking to see if there is a Symbolic Link installer or not on the profiles
		$file = Get-Item "C:\Users\$SymbolicLinkUser\AppData\Local\tesla-electron" -Force -ea SilentlyContinue
		$TestUserSymbolicLink  = [bool]($file.Attributes -band [IO.FileAttributes]::ReparsePoint)
		
		if (($SymbolicLinkUser -eq "shprorpos") -or ($SymbolicLinkUser -eq "shprorpos2") -or ($SymbolicLinkUser -eq "Public"))
		{
			Write-Output "$SymbolicLinkUser is a system profile NO Link Needed" | Out-File -Append -FilePath $Log
			
		}
		Else
		{
			if (($TestUserSymbolicLink -ne $true) -or (!$TestUserSymbolicLink))
			{
				Write-Output "$SymbolicLinkUser had no link installed, installing symbolic link on profile" | Out-File -Append -FilePath $Log
				Remove-Item "C:\Users\$SymbolicLinkUser\AppData\Local\tesla-electron" -Recurse -Force
				(Get-Item "C:\Users\$SymbolicLinkUser\AppData\Local\tesla-electron\").Delete()
				New-Item -ItemType SymbolicLink "C:\Users\$SymbolicLinkUser\AppData\Local" -Name tesla-electron -Value "$TeslaPublicFold" -Force
			}
			Else 
			{
				Write-Output "$SymbolicLinkUser has link already installed proceeding to next profile" | Out-File -Append -FilePath $Log
			}
		}
		
	}
	
	icacls "C:\Users\Public\AppData\Local\tesla-electron" /grant Everyone:F
}

#########################################################################################################################################
# Function Set-RPOSModuleTeslaDeleteProfile
function Set-RPOSModuleTeslaDeleteProfile
{
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	
	Start-Sleep -Seconds 5
	
	#Start uninstalling and installing tesla
	Remove-Item "C:\Users\Public\AppData\Local\tesla-electron" -Recurse -Force
	Write-Output "Tesla has been removed from Public AppData folder" | Out-File -Append -Filepath $Log
	Remove-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Recurse -Force
	
	$TeslaProfiles = Get-ChildItem "C:\Users\" | Select-Object -ExpandProperty Name
	
	foreach ($TeslaProfile in $TeslaProfiles)
	{
		Remove-Item "C:\Users\$TeslaProfile\AppData\Local\tesla-electron" -Recurse -Force
		(Get-Item "C:\Users\$TeslaProfile\AppData\Local\tesla-electron").Delete()
		Write-Output "Tesla-electron folder has been deleted from $TeslaProfile" | Out-File -Append -Filepath $Log
	}
	
}


#########################################################################################################################################
#Function Set-RPOSModuleTeslaClearDatabase
function Set-RPOSModuleTeslaClearDatabase
{	
	#Shutting down Tesla, FlowModule, RPOS before clearing the Database
	Write-Output "Shutting down RPOS, FlowModule, RPOS before clearing the database for the morning" | Out-File -Append -FilePath $Log
	Write-Output "Closing Tesla.." | Out-File -Append -FilePath $Log
	Write-Output "Was Electron running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was Proton Running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	Write-Output "Closing FlowModule.." | Out-File -Append -FilePath $Log
	Write-Output "Was FlowModule running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Closing RPOS.." | Out-File -Append -FilePath $Log
	Write-Output "Was RPOS Running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName rpos* | Out-File -Append -Filepath $Log
	
	Start-Sleep -Seconds 15
	
	#Clearing out the database files everynight to prevent any issues the next business day.
	Write-Output "Clearing out Tesla Database at midnight to improve preformace in the morning." | Out-File -Append -FilePath $Log
	
	Remove-Item "C:\Users\Public\AppData\Local\tesla-electron\resources\db" -Recurse -Force
	Write-Output "Finished clearing out database files." | Out-File -Append -FilePath $Log
	
	#Creating Counter int to see if the Tesla DB folder is successfully deleted
	[int]$counter = "0"
	
	#If Tesla DB folder is still active and the counter is set less than 5 tries, try to stop processes again and delete the folder
	while ((Test-Path "C:\Users\Public\AppData\Local\tesla-electron\resources\db") -and ($counter -lt "5"))
	{
		$counter += 1
		Write-Output "Auto-Updater has discovered that the DB folder was never deleted properly, starting to loop to delete counter is at $counter"
		
		#Shutting down Tesla, FlowModule, RPOS before clearing the Database
		Write-Output "Shutting down RPOS, FlowModule, RPOS before clearing the database for the morning" | Out-File -Append -FilePath $Log
		Write-Output "Closing Tesla.." | Out-File -Append -FilePath $Log
		Write-Output "Was Electron running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
		Write-Output "Was Proton Running?" | Out-File -Append -FilePath $Log
		Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
		Write-Output "Closing FlowModule.." | Out-File -Append -FilePath $Log
		Write-Output "Was FlowModule running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		Write-Output "Closing RPOS.." | Out-File -Append -FilePath $Log
		Write-Output "Was RPOS Running?" | Out-File -Append -FilePath $Log
		Stop-Process -Force -PassThru -ProcessName rpos* | Out-File -Append -Filepath $Log
		
		Start-Sleep -Seconds 15
		
		#Clearing out the database files everynight to prevent any issues the next business day.
		Write-Output "Clearing out Tesla Database at midnight to improve preformace in the morning." | Out-File -Append -FilePath $Log
		
		Remove-Item "C:\Users\Public\AppData\Local\tesla-electron\resources\db" -Recurse -Force
		Write-Output "Finished clearing out database files." | Out-File -Append -FilePath $Log
		
	}
	
}

#########################################################################################################################################
#Function Set-RPOSModuleTeslaInstall
function Set-RPOSModuleTeslaInstall
{
	#Setup server address names and http addresses
	$rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
	if ($rposname -eq "RPOS - PRODUCTION"){
		$script:server = "http://$prod"} 
	if ($rposname -eq "RPOS - TRAINING") {
		$script:server = "http://$train"}
	if ($rposname -eq "RPOS - PILOT") {
		$script:server = "http://$pilot" }
	if ($rposname -eq "RPOS - PILOT2") {
		$script:server = "http://$pilot2" }
	if ($rposname -eq "RPOS - PILOT3") {
		$script:server = "http://$pilot3" }
	if ($rposname -eq "RPOS - QA") {
		$script:server = "http://$qa" }
	if ($rposname -eq $null) {
		$script:server = "http://$prod" }
	if ($rposname -eq "RPOS - QAPILOT") {
		$script:server = "http://$qapilot" }
	if ($rposname -eq "RPOS - QAPILOT2") {
		$script:server = "http://$qapilot2" }
	if ($rposname -eq "RPOS - QAPILOT3") {
		$script:server = "http://$qapilot3" }
	if ($rposname -eq "RPOS - ALPHA") {
		$script:server = "http://$alpha" }
	if ($rposname -eq "RPOS - DEV") {
		$script:server = "http://$dev" }
	if ($rposname -eq "RPOS - DEVPILOT") {
		$script:server = "http://$devpilot" }
	if ($rposname -eq "RPOS - DEVPILOT2") {
		$script:server = "http://$devpilot2" }
	if ($rposname -eq "RPOS - DEVPILOT3") {
		$script:server = "http://$devpilot3" }
	
	#Setting small tesla file values
	$tfile = "Tesla.exe"
	
	#Starting to Install RPOS Tesla MiddleLayer Module
	Write-Output "########################################" | Out-File -Append -FilePath $Log
	Write-Output "#           Starting TESLA             #" | Out-File -Append -FilePath $Log
	Write-Output "########################################" | Out-File -Append -FilePath $Log
	Write-Output "Starting the installtion of RPOS Tesla Middleware Application" | Out-File -Append -FilePath $Log
	Write-Output "Copying over the older version of Tesla, just in case needing to rollback" | Out-File -Append -FilePath $Log
	Copy-Item -Path $teslafileexe -Destination $teslafileBakexe -PassThru -Force | Out-File -Append -FilePath $Log
	Write-Output "Removing older Tesla files from the root directory of RPOS Folder" | Out-File -Append -FilePath $Log
	Remove-Item $teslafileexe -Force
	
	#Grabbing current tesla installer hash from the server
	$TeslaHashURL = "$server/checksum.xml"
	$Teslaxmlread = [xml](New-Object System.Net.WebClient).downloadstring($TeslaHashURL)
	$teslahash = $Teslaxmlread.checksum.Tesla
	
	#Removing old Tesla Files from RPOS Installer Root
	Remove-Item "$teslafileexe" -Force
	Remove-Item "$teslaShareFile" -Force
	
	if (($CiscoExists -ne $null) -and ($CiscoStatus -eq "2") -or ($windowsshortname -eq "WinSRV"))
	{
		Write-Output "Cisco AnyConnect is connected bypassing Self-election Process and Subnet Sharing" | Out-File -Append -FilePath $Log
	}
	Else
	#If Cisco not connected
	{
	#Starting Self Electing function
	#Set-RPOSModuleStartSelfElect
	$selected = Get-Content $LiveIP
	
	#Starting Self Electing Process

		foreach ($electedPC in $selected)
		{
			$path = "\\$electedPC\cs18ebyi3$"
			$pathresults = Test-Path -path $path
			
			if ($pathresults -eq $false)
			{
				Write-Output "Powershell was unable to find network share on $electedPC trying next PC" | Out-File -Append -FilePath $log
			}
			Else
			{
				New-PSDrive -Name M FileSystem "\\$electedPC\cs18ebyi3$"
				$teslashare01 = Test-Path "M:\$tfile"
				$teslasharehash01 = Get-Item -Path "M:\$tfile" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
				
				if (($teslashare01 -eq $true) -and ($teslasharehash01 -eq $teslahash))
				{
					Write-Output "Powershell has found correct Tesla Installer on local PC $electedPC starting transfer" | Out-File -Append -FilePath $Log
					Copy-Item "M:\$tfile" -Destination $teslafileexe -PassThru -Force | Out-File -Append -FilePath $Log
					Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaDistro" -Value "$electedPC" -Force
					Write-Output "Powershell succssfully copied Tesla Installer on local PC $electedPC" | Out-File -Append -FilePath $Log
					Remove-PSDrive -Name M; break
				}
				Else
				{
					Write-Output "Powershell was unable to find correct matching version of Tesla on $electedPC trying different PC" | Out-File -Append -FilePath $log
					Remove-PSDrive -Name M
				}
			}
		}
	}

	#Running MD5 checksum to ensure files are NOT! corrupt
	$teslalocalhash = Get-FileHash -Path $teslafileexe -Algorithm MD5 | select -ExpandProperty hash
	
	if ($teslalocalhash -ne $teslahash)
	{
		while ($teslasumnum -lt 5)
		{
			$teslasumnum += 1
			Write-Output "$teslasumnum is the current count of the amount of times script has tried redownloading max 5 times" | Out-File -Append -FilePath $Log
			#Setting download counter due to download count		
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Tesla DCount" -Value $teslasumnum -Force
			Write-Output "Starting re-download of Tesla for the $teslasumnum time" | Out-File -Append -FilePath $Log
			
			#Removing Local copy of Adobe AIR
			Write-Output "Removing current local copy of Tesla if it exists" | Out-File -Append -FilePath $Log
			Remove-Item $teslafileexe -Force
			
			#Downloading RPOS and Flow Files
			Write-Output "Starting to download never version of Tesla"
			$downloadTesla = "$server/$TeslaInstallerFile"
			Invoke-WebRequest -Uri $downloadTesla -OutFile $teslafileexe
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaDistro" -Value "DistroServer" -Force
			Write-Output "$TeslaInstallerFile has been downloaded" | Out-File -Append -FilePath $Log
			
			#Running MD5 checksum to ensure files are NOT! corrupt
			$teslalocalhash = Get-FileHash -Path $teslafileexe -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$teslalocalhash is the MD5 hash of Tesla file of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$teslahash is the MD5 has of Tesla file that server has" | Out-File -Append -FilePath $Log
			if ($teslahash -eq $teslalocalhash) { Break }
		}
	}
	if (($teslasumnum -ne "5") -and ($teslahash -eq $teslalocalhash))
		{
			#Starting the install of Tesla
			Write-Output "Tesla is the correct hash continuing" | Out-File -Append -FilePath $Log
			Write-Output "$teslalocalhash is the MD5 hash of Tesla of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$teslahash is the MD5 has of Tesla that server has" | Out-File -Append -FilePath $Log
			
			#Copying over Tesla for subnet sharing to rest of the PCs
			Remove-Item $TeslaShareFile -Force
			Write-Output "Starting to copy over for subnet sharing" | Out-File -Append -FilePath $Log
			Copy-Item $teslafileexe -Destination $TeslaShareFile -PassThru -Force | Out-File -Append -FilePath $Log
			Write-Output "$TeslaShareFile has been copied for sharing on $TeslaShareFile" | Out-File -Append -FilePath $Log
			
			#Killing RPOS and Flow if running
			Write-Output "Closing RPOS and FlowModule to start the install" | Out-File -Append -FilePath $Log
			Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
			Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
			
			Start-Sleep -Seconds 5
			
			#Killing Tesla if running in the backgroud
			Write-Output "Closing Tesla to start the install" | Out-File -Append -FilePath $Log
			Write-Output "Was Electron running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
			Write-Output "Was Proton Running?" | Out-File -Append -FilePath $Log
			Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
			
			Start-Sleep -Seconds 10
			Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
			Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
			
			#Start uninstalling and installing tesla
			Remove-Item "C:\Users\Public\AppData\Local\tesla-electron" -Recurse -Force
			Write-Output "Tesla has been removed from Public AppData folder" | Out-File -Append -Filepath $Log
			Remove-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Recurse -Force
		
			$TeslaProfiles = Get-ChildItem "C:\Users\" | Select-Object -ExpandProperty Name
		
			foreach ($TeslaProfile in $TeslaProfiles)
			{
				Remove-Item "C:\Users\$TeslaProfile\AppData\Local\tesla-electron" -Recurse -Force
				(Get-Item "C:\Users\$TeslaProfile\AppData\Local\tesla-electron").Delete()
				Write-Output "Tesla-electron folder has been deleted from $TeslaProfile" | Out-File -Append -Filepath $Log
			}
			
			#Starting to install tesla
			Start-Process $teslafileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru | Out-File -Append -FilePath $Log
			Write-Output "Tesla has completed installing" | Out-File -Append -Filepath $Log
			
			Start-Sleep -Seconds 10
			Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
			Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
			
			If (Test-Path $PublicAppData) {
			Write-Output "The Public AppData Folder Exists, No need to make one" | Out-File -Append -FilePath $Log	
			}
			Else
			{
				New-Item "$PublicAppData" -ItemType Directory -Force
				icacls "C:\Users\Public\AppData" /grant Everyone:F
				Write-Output "Public AppData Folder wasn't on this PC made one" | Out-File -Append -FilePath $Log
			}
		
			if (Test-Path $PublicAppDataLocalFolder)
			{
			Write-Output "The Public AppData Local Folder Exists, No need to make one" | Out-File -Append -FilePath $Log
			}
			Else
			{
				New-Item "$PublicAppDataLocalFolder" -ItemType Directory -Force
				icacls "C:\Users\Public\AppData\Local" /grant Everyone:F
				Write-Output "Public AppData Local Folder wasn't on the PC made one" | Out-File -Append -FilePath $Log
			}
		
			
			Copy-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Destination $PublicAppDataLocalFolder -Recurse -Force | Out-File -Append -FilePath $Log
			Remove-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Recurse -Force | Out-File -Append -FilePath $Log
		
			Write-Output "Moving Tesla install over to Public AppData folder for sharing" | Out-File -Append -FilePath $Log
		
			$teslausers = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
			
			foreach ($teslauser in $teslausers) {
			
				#Checking to see if there is a Symbolic Link installer or not on the profiles
				$file = Get-Item "C:\Users\$teslauser\AppData\Local\tesla-electron" -Force -ea SilentlyContinue
				$TestUserSymbolicLink = [bool]($file.Attributes -band [IO.FileAttributes]::ReparsePoint)
			
				if ($TestUserSymbolicLink -eq $true)
				{
				Write-Output "There is symbolic link created for Tesla to the public AppData folder for user $teslauser not needed" | Out-File -Append -FilePath $Log
				}
				Else
				{
					if (Test-Path "C:\Users\$teslauser\AppData\Local")
					{
					Write-Output "There was a AppData Local folder in $teslauser account" | Out-File -Append -FilePath $Log
					}
					Else {
					New-Item "C:\Users\$teslauser\AppData\Local" -ItemType Directory -Force
					Write-Output "There was no AppDataLocal folder in $teslauser account creating folder" | Out-File -Append -FilePath $Log
					}
				
				
				if (($teslauser -eq "shprorpos") -or ($teslauser -eq "shprorpos2") -or ($teslauser -eq "Public"))
				{
					Write-Output "These are RPOS Auto-Updater system profiles, do not create a symoblic link for these profiles" | Out-File -Append -FilePath $Log
				}
				Else
				{
					New-Item -ItemType SymbolicLink "C:\Users\$teslauser\AppData\Local" -Name tesla-electron -Value "$TeslaPublicFold" -Force
					Write-Output "Tesla Electrong AppData Symbolic Link has been created for $teslauser" | Out-File -Append -FilePath $Log
				}
			}
			
			
			
		}
		
			icacls "C:\Users\Public\AppData\Local\tesla-electron" /grant Everyone:F
		
	}
		$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
		$versionnumberTesla = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
		New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaVer" -Value "$versionnumberTesla" -Force
	
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingTesla" -Value "1" -Force
		
		Write-Output "Tesla Install is complete for this newer build" | Out-File -Append -FilePath $Log
		Write-Output "########################################" | Out-File -Append -FilePath $Log
		Write-Output "#           End Of TESLA               #" | Out-File -Append -FilePath $Log
		Write-Output "########################################" | Out-File -Append -FilePath $Log
	}



#########################################################################################################################################
#Function CheckRPOSIcon
function Set-RPOSModuleCheckRPOSIcon
{
	#Checking to see if RPOS Icon exists in $RPOSAUDIRHOME
	If (Test-Path -Path "$RPOSAUDIRBAK\RPOS.lnk") {
		Write-Output "RPOS.lnk is in the RPOS Auto-Updater folder, no action needed" | Out-File -Append -FilePath $Log }
	else {
		Write-Output "RPOS.lnk not found in $RPOSAUDIRBAK coping over from Desktop" | Out-File -Append -FilePath $Log
		Copy-Item "C:\Users\Public\Desktop\RPOS.lnk" -Destination "$RPOSAUDIRBAK" -Force }
	
	#Checking to see if RPOS Icon exists in C:\Users\Public\Desktop
	If (Test-Path -Path "C:\Users\Public\Desktop\RPOS.lnk") {
		Write-Output "RPOS.lnk is in the Public Desktop folder, no action needed" | Out-File -Append -FilePath $Log }
	
	Else {
		Write-Output "RPOS.lnk not found in Public Desktop coping it over to Public Desktop" | Out-File -Append -FilePath $Log
		Copy-Item "$RPOSAUDIRBAK\RPOS.lnk" -Destination "C:\Users\Public\Desktop" -Force }
	
	#Checking Permissions of Desktop Icon
	$RPOSPer = icacls "C:\Users\Public\Desktop\RPOS.lnk" | Select-String "Everyone"
	if ($RPOSPer -eq $null) {
		Write-Output "RPOS.lnk on the public desktop does not have everyone permissions, granting permissions" | Out-File -Append -FilePath $Log
		icacls "C:\Users\Public\Desktop\RPOS.lnk" /grant Everyone:F }
	Else {
		Write-Output "RPOS.lnk has everyone permissions not doing anything at this time." | Out-File -Append -FilePath $Log }
	
	#checking to see if Update RPOS is on all users desktop
	If (Test-Path -Path "C:\Users\Public\Desktop\Update RPOS.lnk") {
		Write-Output "Update RPOS.lnk is showing on the public desktop no action is needed" | Out-File -Append -FilePath $Log
	}
	Else {
		Write-Output "Update RPOS.lnk is showing missing at the moment copying it over from backup folder" | Out-File -Append -FilePath $Log
		Copy-Item "$RPOSAUDIRBAK\Update RPOS.lnk" -Destination "C:\Users\Public\Desktop" -Force
		icacls "C:\Users\Public\Desktop\Update RPOS.lnk" /grant Everyone:RX
	}
	#Checking to see if FlowModule Desktop Icon Exists
	If (Test-Path -Path "C:\Users\Public\Desktop\FlowModule.lnk") {
		Write-Output "FlowModule Icon was found on the public desktop removing it, to aviod confusion" | Out-File -Append -FilePath $Log
		Remove-Item "C:\Users\Public\Desktop\FlowModule.lnk" -Force }
	Else {
		Write-Output "There was no FlowModule Icon found on the public desktop" | Out-File -Append -FilePath $Log
	}
}

#########################################################################################################################################
#Function FindVersion
function Set-RPOSModuleFindVersion
{
	#Grabbing actual version of RPOS and FlowModule
	[xml]$xmlreadRPOSversion = Get-Content $RPOSVersionLabelXML
	$RPOSxmlversion = $xmlreadRPOSversion.application.versionLabel
	
	#Setting values for RPOS Versions in Registry
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version Label" -Value $RPOSxmlversion -Force
	$rposname2 = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version" -Value $rposname2 -Force
}

#########################################################################################################################################
#Function ThreeDigitIPfromXML
function Set-RPOSModuleThreeDigitIPfromXML
{
	
	$XMLipa1 = $XMLipa | Measure-Object -Character | Select-Object -ExpandProperty Characters
	$XMLipb2 = $XMLipb | Measure-Object -Character | Select-Object -ExpandProperty Characters
	$XMLipc3 = $XMLipc | Measure-Object -Character | Select-Object -ExpandProperty Characters
	$XMLipd4 = $XMLipd | Measure-Object -Character | Select-Object -ExpandProperty Characters
	
	if ($XMLipa1 -eq "1") {
		$script:XMLip1 = "00${XMLipa}." }
	
	if ($XMLipa1 -eq "2") {
		$script:XMLip1 = "0${XMLipa}." }
	
	if ($XMLipa1 -eq "3") {
		$script:XMLip1 = "${XMLipa}." }
	
	if ($XMLipb2 -eq "1") {
		$script:XMLip2 = "00${XMLipb}." }
	
	if ($XMLipb2 -eq "2") {
		$script:XMLip2 = "0${XMLipb}." }
	
	if ($XMLipb2 -eq "3") {
		$script:XMLip2 = "${XMLipb}." }
	
	if ($XMLipc3 -eq "1") {
		$script:XMLip3 = "00${XMLipc}." }
	
	if ($XMLipc3 -eq "2") {
		$script:XMLip3 = "0${XMLipc}." }
	
	if ($XMLipc3 -eq "3") {
		$script:XMLip3 = "${XMLipc}." }
	
	if ($XMLipd4 -eq "1") {
		$script:XMLip4 = "00${XMLipd}" }
	
	if ($XMLipd4 -eq "2") {
		$script:XMLip4 = "0${XMLipd}" }
	
	if ($XMLipd4 -eq "3") {
		$script:XMLip4 = "${XMLipd}" }
	
}

#####################################################################################################################################
# Function CheckReportXML
function Set-RPOSModuleCheckReportXML
{
	
	#Get Date for last inventory scan
	$InventoryTimeScan = Get-Date -Format "MM/dd/yyyy HH:mm:ss tt"
	
	#Getting Host IP address
	$HostIPAddress = "$ipmain1.$ipmain2.$ipmain3.$ipmain4"
	
	#Setting Up Network Share on rpos.tbccorp.com
	New-PSDrive -Name O -PSProvider FileSystem -Root "\\rpos.tbccorp.com\MbPnTRr7DR$\Bucket"
	
	Write-Output "Grabbing Registry Value Data to send data back to XML Database on rpos.tbccorp.com for processing" | Out-File -Append -FilePath $Log
	
	#Grabbing Registry Values	
	$XMLName = $Env:COMPUTERNAME
	$RegAdobeAIRVer = (Get-ItemProperty -Path $LANDeskKeys -Name AdobeAIRVer).AdobeAIRVer
	$RegAdobeAIRDistro = (Get-ItemProperty -Path $LANDeskKeys -Name AdobeDistro).AdobeDistro
	$RegAuVerUpdateDate = (Get-ItemProperty -Path $LANDeskKeys -Name "AuVer UpdateDate")."AuVer UpdateDate"
	$RegBGInfoVersion = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoVersion).BGInfoVersion
	$RegBGInfoXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name BGInfoXMLchecksum).BGInfoXMLchecksum
	$RegEMVIPaddress = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVIPaddress")."EMVIPaddress"
	$RegEMVGateway = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVGateway")."EMVGateway"
	$RegFlowModulechecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "FlowModule Checksum")."FlowModule Checksum"
	$RegRPOSAutoUpdater = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Auto-Updater")."RPOS Auto-Updater"
	$RegRPOSChecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Checksum")."RPOS Checksum"
	$RegRPOSDCount = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Dcount")."RPOS Dcount"
	$RegRPOSDLSource = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS DL Source")."RPOS DL Source"
	$RegRPOSUpdatedate = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Update Date")."RPOS Update Date"
	$RegRPOSVersion = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version")."RPOS Version"
	$RegRPOSVersionLabel = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version Label")."RPOS Version Label"
	$RegRPOSAUVer = (Get-ItemProperty -Path $LANDeskKeys -Name RPOSAUVer).RPOSAUVer
	$RegRPOSSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	$RegAdobeDownloadCount = (Get-ItemProperty -Path $LANDeskKeys -Name "Adobe DCount")."Adobe DCount"
	$RegEMVInstalled = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVVeriFoneInstalled")."EMVVeriFoneInstalled"
	$RegTeslaVer = (Get-ItemProperty -Path $LANDeskKeys -Name "TeslaVer")."TeslaVer"
	$RegTeslaDistro = (Get-ItemProperty -Path $LANDeskKeys -Name "TeslaDistro")."TeslaDistro"
	$RegTeslaDCount = (Get-ItemProperty -Path $LANDeskKeys -Name "Tesla DCount")."Tesla DCount"
	
	#Adding Endpoint Connection To Reporting Tool for store locations
	if ($RPOSAgentname -eq "Store")
	{
		$RegSecurityEndpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Security Endpoint")."RPOS Security Endpoint"
		$RegServiceEndpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Services Endpoint")."RPOS Services Endpoint"
		$RegKatanaEndpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Katana Endpoint")."RPOS Katana Endpoint"
		$RegEnableSecurityEndpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Security Enable")."RPOS Security Enable"
		$RegEnableServiceEndpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Services Enable")."RPOS Services Enable"
		$RegEnableKatanaEndpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Katana Enable")."RPOS Katana Enable"
		$RegEndpointChecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "EndpointXMLchecksum")."EndpointXMLchecksum"
	}
	
	Write-Output "Starting to build XML file to send over to rpos.tbccorp.com" | Out-File -Append -FilePath $Log
	
	#Creating New XML for Data Deposits
	[System.XML.XMLDocument]$RPOSInventory = New-Object System.XML.XMLDocument
	
	#Creating first Root Element	
	[System.XML.XMLElement]$RPOSRoot = $RPOSInventory.CreateElement("RPOS")
	$RPOSInventory.appendChild($RPOSRoot)
	
	# Append as child to an existing node
	[System.XML.XMLElement]$RPOSAttributes1 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("name"))
	$RPOSAttributes1.InnerText = "$XMLName"
	[System.XML.XMLElement]$RPOSAttributes2 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("address"))
	$RPOSAttributes2.InnerText = "$HostIPAddress"
	[System.XML.XMLElement]$RPOSAttributes3 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSVersion"))
	$RPOSAttributes3.InnerText = "$RegRPOSVersion"
	[System.XML.XMLElement]$RPOSAttributes4 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AutoUpdateVersion"))
	$RPOSAttributes4.InnerText = "$RegRPOSAUVer"
	[System.XML.XMLElement]$RPOSAttributes5 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSUpdateDate"))
	$RPOSAttributes5.InnerText = "$RegRPOSUpdatedate"
	[System.XML.XMLElement]$RPOSAttributes6 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSDownloadSource"))
	$RPOSAttributes6.InnerText = "$RegRPOSDLSource"
	[System.XML.XMLElement]$RPOSAttributes7 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSCheckSum"))
	$RPOSAttributes7.InnerText = "$RegRPOSChecksum"
	[System.XML.XMLElement]$RPOSAttributes8 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("FlowModuleCheckSum"))
	$RPOSAttributes8.InnerText = "$RegFlowModulechecksum"
	[System.XML.XMLElement]$RPOSAttributes9 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSVersionLabel"))
	$RPOSAttributes9.InnerText = "$RegRPOSVersionLabel"
	[System.XML.XMLElement]$RPOSAttributes10 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeAIRVersion"))
	$RPOSAttributes10.InnerText = "$RegAdobeAIRVer"
	[System.XML.XMLElement]$RPOSAttributes11 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeDownloadDistro"));
	$RPOSAttributes11.InnerText = "$RegAdobeAIRDistro"
	[System.XML.XMLElement]$RPOSAttributes12 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AdobeDownloadCount"));
	$RPOSAttributes12.InnerText = "$RegAdobeDownloadCount"
	[System.XML.XMLElement]$RPOSAttributes13 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVInstalled"));
	$RPOSAttributes13.InnerText = "$RegEMVInstalled"
	[System.XML.XMLElement]$RPOSAttributes14 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVIP"));
	$RPOSAttributes14.InnerText = "$RegEMVIPaddress"
	[System.XML.XMLElement]$RPOSAttributes15 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EMVGatewayIP"));
	$RPOSAttributes15.InnerText = "$RegEMVGateway"
	[System.XML.XMLElement]$RPOSAttributes16 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("RPOSDownloadCt"));
	$RPOSAttributes16.InnerText = "$RegRPOSDCount"
	[System.XML.XMLElement]$RPOSAttributes17 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("BGInfoVersion"));
	$RPOSAttributes17.InnerText = "$RegBGInfoVersion"
	[System.XML.XMLElement]$RPOSAttributes18 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("BGInfoXMLChecksum"));
	$RPOSAttributes18.InnerText = "$RegBGInfoXMLchecksum"
	[System.XML.XMLElement]$RPOSAttributes19 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("SwitchXMLChecksum"));
	$RPOSAttributes19.InnerText = "$RegRPOSSwitchXMLchecksum"
	[System.XML.XMLElement]$RPOSAttributes20 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("AuUpdateDate"));
	$RPOSAttributes20.InnerText = "$RegAuVerUpdateDate"
	[System.XML.XMLElement]$RPOSAttributes21 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("InventoryDate"));
	$RPOSAttributes21.InnerText = "$InventoryTimeScan"
	[System.XML.XMLElement]$RPOSAttributes22 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("TeslaVer"));
	$RPOSAttributes22.InnerText = "$RegTeslaVer"
	[System.XML.XMLElement]$RPOSAttributes23 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("TeslaDistro"));
	$RPOSAttributes23.InnerText = "$RegTeslaDistro"
	[System.XML.XMLElement]$RPOSAttributes24 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("TeslaDCount"));
	$RPOSAttributes24.InnerText = "$RegTeslaDCount"
	
	if ($RPOSAgentname -eq "Store")
	{
		[System.XML.XMLElement]$RPOSAttributes25 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("SecurityEndpoint"));
		$RPOSAttributes25.InnerText = "$RegSecurityEndpoint"
		[System.XML.XMLElement]$RPOSAttributes26 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("ServicesEndpoint"));
		$RPOSAttributes26.InnerText = "$RegServiceEndpoint"
		[System.XML.XMLElement]$RPOSAttributes27 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("KatanaEndpoint"));
		$RPOSAttributes27.InnerText = "$RegKatanaEndpoint"
		[System.XML.XMLElement]$RPOSAttributes28 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EnableSecurity"));
		$RPOSAttributes28.InnerText = "$RegEnableSecurityEndpoint"
		[System.XML.XMLElement]$RPOSAttributes29 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EnableServices"));
		$RPOSAttributes29.InnerText = "$RegEnableServiceEndpoint"
		[System.XML.XMLElement]$RPOSAttributes30 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EnableKatana"));
		$RPOSAttributes30.InnerText = "$RegEnableKatanaEndpoint"
		[System.XML.XMLElement]$RPOSAttributes31 = $RPOSRoot.appendChild($RPOSInventory.CreateElement("EndpointChecksum"));
		$RPOSAttributes31.InnerText = "$RegEndpointChecksum"
	}
	
	
	#Save File
	$RPOSInventory.Save("$RPOSAUDIRHOME\$XMLDeposit")
	
	#Copying Data file To rpos.tbccorp.com
	Copy-Item "$RPOSAUDIRHOME\$XMLDeposit" -Destination "\\rpos.tbccorp.com\MbPnTRr7DR$\Bucket" -Force
	Write-Output "File $RPOSAUDIRHOME\$XMLDeposit has been processed and moved to rpos.tbccorp.com" | Out-File -Append -FilePath $Log
	
	#Removing File from Local PC.
	Remove-Item "$RPOSAUDIRHOME\$XMLDeposit" -Force
	Write-Output "Removed $RPOSAUDIRHOME\$XMLDeposit from Local PC" | Out-File -Append -FilePath $Log
	
	Remove-PSDrive -Name O -Force
}

#####################################################################################################################################
# Function CheckBGInfoUpdatewithEMV
function Set-RPOSModuleCheckBGInfoUpdateandEMV
{
	
	#Reading to see if EMV is currently installed on the PC, if it is nothing will happen, otherwise it will start recording the proper reg keys
	[xml]$xmlread = Get-Content $XMLEMVfileRPOS
	$script:Reademv = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
	
	#Checking to see if EMV has been installed already on PC
	$script:ValidateEMV = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVVeriFoneInstalled")."EMVVeriFoneInstalled"
	Write-Output "Does the PC have the EMV files installed which is $ValidateEMV" | Out-File -Append -FilePath $Log
	Write-Output "Auto-Updater is checking to see if EMV XML files and BGInfo needs to be updated" | Out-File -Append -FilePath $Log
	
	#Checking to see if the site is complete and has the correct information in RPOS XML
	if (($ValidateEMV -eq $null) -and ($Result -ne $IPZero) -and ($Reademv -eq $null)) {
		Write-Output "Setup has determained the RPOS needs to switch over to EMV for chip and pin processing." | Out-File -Append -FilePath $Log
		Write-Output "Starting setupEMVxml fuction" | Out-File -Append -FilePath $Log
		Set-RPOSModulesetupEMVxml }
	Else {
		Write-Output "RPOS has already installed EMV on this PC" | Out-File -Append -FilePath $Log }
	
	#Checking to see if the checksum of UpdatedXML's is updated or not.
	if ($checkBGInfoxml -ne $RegBGInfoXMLchecksum)
	{
		
		#adding newer update hash in registry
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoXMLchecksum" -Value $checkBGInfoxml -Force
		
		Write-Output "Checking to see if BGInfo is at the most current verion" | Out-File -Append -FilePath $Log
		Write-Output "Auto-Updater is proceeding with check an newer version of BGInfo EMV Complete" | Out-File -Append -FilePath $Log
		Write-Output "RPOS Auto-Updater is starting to check to see if there is an update for BGInfo" | Out-File -Append -FilePath $Log
		$BGInfoUpdateKey = (Get-ItemProperty -Path $LANDeskKeys -Name "BGInfoVersion")."BGInfoVersion"
		Write-Output "The current version of BGInfo on this PC is $BGInfoUpdateKey" | Out-File -Append -FilePath $Log
		
		$URL = "http://rpos.tbccorp.com/RPOSAutoUpdater/BGInfo/bginfoupdater.xml"
		
		$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
		$m = $xmlread.BGInfoUpdate.$switchvalue.version
		$n = $xmlread.BGInfoUpdate.$switchvalue.hash
		$o = $xmlread.BGInfoUpdate.$switchvalue.URL
		
		Write-Output "Current version of BGinfo on rpos.tbccorp.com is $m" | Out-File -Append -FilePath $Log
		
		if ($BGInfoUpdateKey -ne $m)
		{
			Write-Output "Powershell has identified that PC is not running the current version of BGinfo from TBC starting backing up and downloading" | Out-File -Append -FilePath $Log
			Remove-Item "$RPOSAUDIRBAK\TBC.bgi.bak" -Force
			Write-Output "Removed old backup of BGInfo in backup folder" | Out-File -Append -FilePath $Log
			Copy-Item -Path "C:\bginfo\TBC.bgi" -Destination "$RPOSAUDIRBAK\TBC.bgi.bak" -PassThru -Force | Out-File -Append -FilePath $Log
			Write-Output "Backed up previous version of BGInfo to $RPOSAUDIRBAK"
			Remove-Item $BGInfoLocalFile -Force
			
			Write-Output "Starting download of New BGInfo file" | Out-File -Append -FilePath $Log
			
			#Downloading hash checksum for swutch value
			Copy-Item -Path "$o" -Destination "$BGInfoLocalFile" -Force
			Write-Output "TBC.bgi has been downloaded" | Out-File -Append -FilePath $Log
			
			Write-Output "Checking file hash of the new bginfo file" | Out-File -Append -FilePath $Log
			$LocalBGInfoHash = Get-FileHash -Path $BGInfoLocalFile -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$LocalBGInfoHash is the hash of the BGInfo file that was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$n is the hash of the BGInfo file on the server" | Out-File -Append -FilePath $Log
			
			if ($LocalBGInfoHash -ne $n)
			{
				while ($sumnumbginfo -lt 5)
				{
					$sumnumbginfo += 1
					Write-Output "The BGInfo file that was download is not the correct hash trying to re-download count equals $sumnumbginfo" | Out-File -Append -FilePath $Log
					Write-Output "Starting download of New BGInfo file" | Out-File -Append -FilePath $Log
					
					#Downloading hash checksum for swutch value
					Copy-Item -Path "$o" -Destination "$BGInfoLocalFile" -Force
					Write-Output "TBC.bgi has been downloaded" | Out-File -Append -FilePath $Log
					$LocalBGInfoHash = Get-FileHash -Path $BGInfoLocalFile -Algorithm MD5 | select -ExpandProperty hash
					if ($LocalBGInfoHash -eq $n) { break }
				}
			}
			
			
			
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoVersion" -Value $m -Force
			
			#Restarting PC to ensure that the newer BGInfo installs correctly
			Write-Output "Restarting PC to ensure settings on BGInfo display on store PC desktop Be Right Back!!!" | Out-File -Append -FilePath $Log
			Restart-Computer -Force
			Start-Sleep -Seconds 60
		}
		Else
		{
			#Value for Progress Bar
			Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "1" -Force
			
			Write-Output "Powershell has identified this PC is running the current version of BGinfo which is $BGInfoUpdateKey" | Out-File -Append -FilePath $Log
		}
		
	}
	Else
	{
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "1" -Force
		
		Write-Output "Auto-Updater doesn't see any changes to BGInfo.xml files bypassing BGInfo check" | Out-File -Append -FilePath $Log
	}
	
	if ($Result -ne $IPZero)
	{
		Write-Output "Starting to check to see if any IP changes were made for the RPOS-Preferences file to BGInfo" | Out-File -Append -FilePath $Log
		$BGEMVIPAddress = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVIPaddress")."EMVIPaddress"
		$EMVXMLVeriFone3digitIP = "$XMLip1$XMLip2$XMLip3$XMLip4"
		Write-Output "IP address of the RPOS-Preferences XML is $EMVXMLVeriFone3digitIP and current registry IP is $BGEMVIPAddress" | Out-File -Append -FilePath $Log
		
		#Checking to see if the IP address in registry for BGInfo EMV needs to be changed from the XML
		if ($BGEMVIPAddress -ne $EMVXMLVeriFone3digitIP)
		{
			Write-Output "BGInfo Registry key for IP address desktop was not matching updating with newer IP address" | Out-File -Append -FilePath $Log
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVIPaddress" -Value $EMVXMLVeriFone3digitIP -Force
			$RPOSEMVxmlgateway = "$XMLip1$XMLip2${XMLip3}001"
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVGateway" -Value $RPOSEMVxmlgateway -Force
		}
		Else
		{
			Write-Output "BGInfo Reigstry key for IP address desktop was matching the current RPOSXML" | Out-File -Append -FilePath $Log
		}
		
		#Checking to see if XML ETIM IP address is current with EMV Ip address in RPOS-preferences.xml
		#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for EMV
		[xml]$xmlread = Get-Content $XMLEMVfileRPOS
		$script:ResultEMV = $xmlread.ApplicationPreferences.pointSCAPaymentDeviceIPAddress | Select-Object -ExpandProperty "#cdata-section"
		
		#Grabbing IP address from C:\Users\*storename*\AppData\Roaming\com.tbccorp.rpos.RPOS\Local Store\RPOS-preferences.xml for NON-EMV
		[xml]$xmlread = Get-Content $XMLEMVfileRPOS
		$Read = $xmlread.ApplicationPreferences.creditCardTerminalUrl | Select-Object -ExpandProperty "#cdata-section"
		$script:ResultNonEMV = $Read -replace ":9001"
		
		#If the IP address for the verifone device has changed change all the rest of the xml files and values.
		if (($ResultEMV -ne $ResultNonEMV) -and ($ResultEMV -ne $null))
		{
			Write-Output "Setup has found there has been a change in the RPOS-Perferences Verifone EMV IP address" | Out-File -Append -FilePath $Log
			Write-Output "Changing IP addresses from $ResultNonEMV to $ResultEMV" | Out-File -Append -FilePath $Log
			
			$ResultEMVtoNonEMV = "${ResultEMV}:9001"
			
			#putting current EMV IP address in Flow RPOS-Preferences.xml
			[xml]$Flowxml = get-content "$XMLEMVfileFlow"
			$Flowxml.applicationpreferences.pointSCAPaymentDeviceIPAddress.'#cdata-section' = "$ResultEMV"
			$Flowxml.Save("$XMLEMVfileFlow")
			
			#putting current EMV IP address with :9001 in RPOS RPOS-Preferences.xml
			[xml]$RPOSxml = get-content "$XMLEMVfileRPOS"
			$RPOSxml.applicationpreferences.creditcardterminalurl.'#cdata-section' = "$ResultEMVtoNonEMV"
			$RPOSxml.Save("$XMLEMVfileRPOS")
			
			#putting current EMV IP address with :9001 in FLOW RPOS-Preferences.xml
			[xml]$Flowxml = get-content "$XMLEMVfileFlow"
			$Flowxml.applicationpreferences.creditcardterminalurl.'#cdata-section' = "$ResultEMVtoNonEMV"
			$Flowxml.Save("$XMLEMVfileFlow")
			
		}
	}
	
	Write-Output "BGInfo IP address for RPOS Preferences and New BGInfo is complete exiting this section of the Auto-Updater" | Out-File -Append -FilePath $Log
}

#####################################################################################################################################
#Function setupEMVxml
function Set-RPOSModulesetupEMVxml
{
	
	Write-Output "Starting RPOS EMV preferences XML Encoding" | Out-File -Append -FilePath $Log
	Write-Output "Name of the user profile, in which PowerShell will be working with is $profilename" | Out-File -Append -FilePath $Log
	if ($Result -eq $IPZero)
	{
		Write-Output "The RPOS XML has an IP address of $Result not changing the xml's on the PC. Exiting the portion of RPOS Auto-Updater EMV XML Edit" | Out-File -Append -FilePath $Log
	}
	Else
	{
		Write-Output "RPOS preferences file is $XMLEMVfileRPOS" | Out-File -Append -FilePath $Log
		#Starting To Backup Preferences Files
		Write-Output "PowerShell is now backing up Store User Profile RPOS Preferences files" | Out-File -Append -FilePath $Log
		Copy-Item -Path $XMLEMVfileRPOS -Destination $XMLEMVRPOSbak -PassThru -Force | Out-File -Append -FilePath $Log
		
		if (Test-Path $XMLEMVfileFlow)
		{
			Write-Output "Flow RPOS preferences file was found" | Out-File -Append -FilePath $Log
			Write-Output "Flow RPOS preferences file is $XMLEMVfileFlow" | Out-File -Append -FilePath $Log
			#Starting To Backup Preferences Files
			Write-Output "PowerShell is now backing up Store User Profile RPOS Flow Preferences files" | Out-File -Append -FilePath $Log
			Copy-Item -Path $XMLEMVfileFlow -Destination $XMLEMVFLOWbak -PassThru -Force | Out-File -Append -FilePath $Log
		}
		Else
		{
			Write-Output "Flow RPOS preferences file was not found no backup needed" | Out-File -Append -FilePath $Log
		}
		
		#Starting To pull XML's into PowerShell to start editing
		Write-Output "$script:Result is the IP address that will be inserted into pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
		Write-Output "Starting to edit RPOS Preferences file with $Result IP address in Append Child pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
		
		#Starting to edit RPOS Preferences File
		$xmlrpos = [xml](Get-Content $XMLEMVfileRPOS)
		$child1 = $xmlrpos.CreateElement("pointSCAPaymentDeviceIPAddress")
		$child1.InnerXml = "<![CDATA[$Result]]>"
		$xmlrpos.ApplicationPreferences.AppendChild($child1)
		$xmlrpos.Save("$XMLEMVfileRPOS")
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVVeriFoneInstalled" -Value "true" -Force
		
		if (Test-Path $XMLEMVfileFlow)
		{
			Write-Output "Starting to edit RPOS Flow Preferences file with $Result IP address in Append Child pointSCAPaymentDeviceIPAddress" | Out-File -Append -FilePath $Log
			#Starting to edit RPOS Flow Preferences File
			$xmlflow = [xml](Get-Content $XMLEMVfileFlow)
			$child2 = $xmlflow.CreateElement("pointSCAPaymentDeviceIPAddress")
			$child2.InnerXml = "<![CDATA[$Result]]>"
			$xmlflow.ApplicationPreferences.AppendChild($child2)
			$xmlflow.Save("$XMLEMVfileFlow")
			
			
		}
		Else
		{
			
			Write-Output "Flow RPOS preferences file was not found no editing needed" | Out-File -Append -FilePath $Log
		}
		
		Write-Output "Script is done editing preferences files, it is complete" | Out-File -Append -FilePath $Log
		
		#Grabbing the IP address from perviously used value above
		$RPOSEMVxmlIPaddress = "$XMLip1$XMLip2$XMLip3$XMLip4"
		$RPOSEMVxmlgateway = "$XMLip1$XMLip2${XMLip3}001"
		
		#Exporting IP address to BGInfo text file
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVIPaddress" -Value $RPOSEMVxmlIPaddress -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVGateway" -Value $RPOSEMVxmlgateway -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVVeriFoneInstalled" -Value "true" -Force
	}
}

#########################################################################################################################################
#Function InstallAIR
function Set-RPOSModuleInstallAIR
{
	#Checking to see what version of Adobe AIR is installed on PC
	$AdobeAIRverLocal = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -like "*Adobe AIR*" } | Select-Object -ExpandProperty Version
	Write-Output "The current version of Adobe AIR is $AdobeAIRverLocal" | Out-File -Append -FilePath $Log
	
	#Getting most current version of Adobe AIR from rpos.tbccorp.com
	Write-Output "Checking $prod to see what version of Adobe AIR is the current" | Out-File -Append -FilePath $Log
	$AdobeURL = "http://$prod/AdobeAirCheck.xml"
	$adobexmlread = [xml](New-Object System.Net.WebClient).downloadstring($AdobeURL)
	$adobever = $adobexmlread.AdobeAir.AdobeAirVer
	$adobehash = $adobexmlread.AdobeAir.AdobeHashCheck
	Write-Output "The most current version of Adobe AIR on server is $adobever" | Out-File -Append -FilePath $Log
	
	if (($AdobeAIRverLocal -lt "$adobever") -or ($AdobeAIRverLocal -eq $null))
	{
		#Removing local copy of Adobe AIR on RPOS Script Local Folder
		Remove-Item $AdobeAIRInstallRoot -Force
		Write-Output "The current version of Adobe AIR is the incorrect and out of date version proceeding with upgrade" | Out-File -Append -FilePath $Log
		Write-Output "Proceeding to check to see most current version of Adobe AIR is on Self elected subnet" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRNI" -Value "1" -Force
		
		if (($CiscoExists -ne $null) -and ($CiscoStatus -eq "2") -or ($windowsshortname -eq "WinSRV"))
		{
			Write-Output "Cisco AnyConnect is connected bypassing Self-election Process and Subnet Sharing" | Out-File -Append -FilePath $Log
		}
		Else
		{
			New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "2" -PropertyType String -Force
			
			$RPOSMasterGUITasks = schtasks /query /FO list | findstr "RPOS AU Local User GUI" | Select-String -Pattern "RPOS AU Local User GUI"
			
			foreach ($RPOSMasterGUITask in $RPOSMasterGUITasks)
			{
				#Finding task name
				[string]$RPOSMasterGUITask = $RPOSMasterGUITask
				$indexmastertask = $RPOSMasterGUITask.IndexOf("RPOS AU Local")
				$RPOSMasterGUISIDUserTask = $RPOSMasterGUITask.Substring($indexmastertask)
				schtasks /Run /TN "$RPOSMasterGUISIDUserTask"
			}
			
			$counter = 0
			
			do
			{
				$counter += 1
				[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSLocalUserGUI" } | Select-Object -ExpandProperty ProcessId
				#$processtemp = Get-Process -Id $processtempsub
				Start-Sleep -Seconds 1
				
			}
			Until ((!$processtempsub) -or ($counter -eq "20"))
			
			
			New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "0" -PropertyType String -Force
			
			#Notifying user or adobe air being installed.
			#Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME 300 * RPOS is installing the newest version of Adobe AIR. Please do not shut down your PC. RPOS will be unavailable during this time."
			
			#Starting Self Electing function
			Set-RPOSModuleStartSelfElect
			$selected = Get-Content $LiveIP
			
			foreach ($electedPC in $selected)
			{
				$path = "\\$electedPC\cs18ebyi3$"
				$pathresults = Test-Path -path $path
				if ($pathresults -eq $false)
				{
					Write-Output "Powershell was unable to find network share on $electedPC trying next PC" | Out-File -Append -FilePath $log
				}
				Else
				{
					New-PSDrive -Name M FileSystem "\\$electedPC\cs18ebyi3$"
					$adobeshare01 = Test-Path "M:\$AdobeInstallerFile"
					$adobesharehash01 = Get-Item -Path "M:\$AdobeInstallerFile" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
					if (($adobeshare01 -eq $true) -and ($adobesharehash01 -eq $adobehash))
					{
						Write-Output "Powershell has found correct Adobe AIR Installer on local PC $electedPC starting transfer" | Out-File -Append -FilePath $Log
						Copy-Item "M:\$AdobeInstallerFile" -Destination $AdobeAIRInstallRoot -PassThru -Force | Out-File -Append -FilePath $Log
						Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeDistro" -Value "$electedPC" -Force
						Write-Output "Powershell succssfully copied Adobe Air Installer on local PC $electedPC" | Out-File -Append -FilePath $Log
						Remove-PSDrive -Name M; break
					}
					Else
					{
						Write-Output "Powershell was unable to find correct matching version of Adobe AIR on $electedPC trying different PC" | Out-File -Append -FilePath $log
						Remove-PSDrive -Name M
					}
				}
			}
		}
		#Running MD5 checksum to ensure files are NOT! corrupt
		$adobelocalhash = Get-FileHash -Path $AdobeAIRInstallRoot -Algorithm MD5 | select -ExpandProperty hash
		
		#If Hash files are not matching trying to re-download
		if ($adobelocalhash -ne $adobehash)
		{
			while ($adobesumnum -lt 5)
			{
				$adobesumnum += 1
				Write-Output "$adobesumnum is the current count of the amount of times script has tried redownloading max 5 times" | Out-File -Append -FilePath $Log
				#Setting download counter due to download count		
				Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Adobe DCount" -Value $adobesumnum -Force
				Write-Output "Starting re-download of Adobe AIR for the $adobesumnum time" | Out-File -Append -FilePath $Log
				
				#Removing Local copy of Adobe AIR
				Write-Output "Removing current local copy of Adobe AIR if it exists" | Out-File -Append -FilePath $Log
				Remove-Item $AdobeAIRInstallRoot -Force
				
				#Downloading RPOS and Flow Files
				Write-Output "Starting to download never version of Adobe AIR"
				$downloadAIR = "http://$prod/$AdobeInstallerFile"
				Invoke-WebRequest -Uri $downloadAIR -OutFile $AdobeAIRInstallRoot
				Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeDistro" -Value "DistroServer" -Force
				Write-Output "$AdobeInstallerFile has been downloaded" | Out-File -Append -FilePath $Log
				
				#Running MD5 checksum to ensure files are NOT! corrupt
				$adobelocalhash = Get-FileHash -Path $AdobeAIRInstallRoot -Algorithm MD5 | select -ExpandProperty hash
				Write-Output "$adobelocalhash is the MD5 hash of Adobe AIR of what was downloaded" | Out-File -Append -FilePath $Log
				Write-Output "$adobehash is the MD5 has of RPOS that server has" | Out-File -Append -FilePath $Log
				if ($adobehash -eq $adobelocalhash) { Break }
			}
		}
		
		if (($adobesumnum -ne "5") -and ($adobehash -eq $adobelocalhash))
		{
			#Starting install of adobe air
			Write-Output "Adobe AIR is the correct hash continuing" | Out-File -Append -FilePath $Log
			Write-Output "$adobelocalhash is the MD5 hash of Adobe AIR of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$adobehash is the MD5 has of Adobe AIR that server has" | Out-File -Append -FilePath $Log
			
			#Coping file over to subnet file share
			Remove-Item $AdobeAIRInstallShare -Force
			Write-Output "Starting to copy over for subnet sharing" | Out-File -Append -FilePath $Log
			Copy-Item $AdobeAIRInstallRoot -Destination $AdobeAIRInstallShare -PassThru -Force | Out-File -Append -FilePath $Log
			Write-Output "$AdobeAIRInstallRoot has been copied for sharing on $AdobeAIRInstallShare" | Out-File -Append -FilePath $Log
			
			#Killing RPOS and Flow if running
			Write-Output "Closing RPOS and FlowModule to start the install"
			Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
			Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
			Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
			
			#Uninstalling Adobe AIR
			$app = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -match "Adobe AIR" }
			$app.Uninstall()
			Write-Output "Adobe AIR has been uninstalled" | Out-File -Append -FilePath $Log
			
			#Installing Adobe AIR with Newer Version
			Start-Process $AdobeAIRInstallRoot -ArgumentList "-silent" -wait -NoNewWindow -PassThru | Out-File -Append -FilePath $Log
			Write-Output "Adobe AIR has been installed for brand new install or upgrade" | Out-File -Append -Filepath $Log
			Write-Output "Adobe Installer Portion of RPOS Auto-Updater is exiting" | Out-File -Append -Filepath $Log
			
			New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "3" -PropertyType String -Force
			
			$RPOSMasterGUITasks = schtasks /query /FO list | findstr "RPOS AU Local User GUI" | Select-String -Pattern "RPOS AU Local User GUI"
			
			foreach ($RPOSMasterGUITask in $RPOSMasterGUITasks)
			{
				#Finding task name
				[string]$RPOSMasterGUITask = $RPOSMasterGUITask
				$indexmastertask = $RPOSMasterGUITask.IndexOf("RPOS AU Local")
				$RPOSMasterGUISIDUserTask = $RPOSMasterGUITask.Substring($indexmastertask)
				schtasks /Run /TN "$RPOSMasterGUISIDUserTask"
			}
			
			$counter = 0
			do
			{
				$counter += 1
				[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSLocalUserGUI" } | Select-Object -ExpandProperty ProcessId
				#$processtemp = Get-Process -Id $processtempsub
				Start-Sleep -Seconds 1
				
			}
			Until ((!$processtempsub) -or ($counter -eq "20"))
			
			New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "0" -PropertyType String -Force
			
			#Notifying user that Adobe AIR install is complete
			#Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:300 * RPOS has completed installing the latest version of Adobe AIR."
			
			#Value for Progress Bar
			Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "1" -Force
		}
	}
	Else {
		Write-Output "Adobe AIR on this PC is $AdobeAIRverLocal which is the most current verison exiting and processing with RPOS Auto-Updater" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "1" -Force
		}
	
	
	#Value for Progress Bar
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRInstalling" -Value "1" -Force
	
	#Scanning PC for the most current version of Adobe AIR
	$AdobeAIRFinalverLocal = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -like "*Adobe AIR*" } | Select-Object -ExpandProperty Version
	
	#Putting the most current version of Adobe AIR in LANDesk Registry Key
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeAIRVer" -Value "$AdobeAIRFinalverLocal" -Force
	
	#Resetting Status Update GUI installer
	New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "0" -PropertyType String -Force
}

##########################################################################################################################################
#Function What PC Am I?
function Set-RPOSModuleWhatPCAmI
{
	
	If ($RPOSAgentname -eq "Store") {
		Write-Output "Powershell has found out that this PC is Store PC" | Out-File -Append -FilePath $Log
		$script:pname = $name
		$script:pname = $pname.Substring(1, 3)
		$script:switchvalue = "store${pname}"
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchstore.xml"
		$script:XMLswitchchecksum = "switchstore"
		$script:XMLbginfochecksum = "switchbginfo" }
	
	Elseif ($RPOSAgentname -eq "Warehouse") {
		Write-Output "Powershell has found out that this PC is a Warehouse PC" | Out-File -Append -FilePath $Log
		$script:ctpc = $env:COMPUTERNAME
		$script:ctpc = $ctpc.Substring(0, 4)
		$script:switchvalue = $ctpc
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchct.xml"
		$script:XMLswitchchecksum = "switchct"
		$script:XMLbginfochecksum = "switchbginfo"
	}
	
	Elseif (($RPOSAgentname -eq "Corporate") -or ($RPOSAgentname -eq "Server") -or (!$RPOSAgentname)) {
		Write-Output "Powershell has found out that this PC is Corporate, Personal and/or out of the normal PC naming scheme." | Out-File -Append -FilePath $Log
		$script:switchvalue = $name
		$script:XMLSwitchURL = "http://rpos.tbccorp.com/switchcorp.xml"
		$script:XMLswitchchecksum = "switchcorp"
		$script:XMLbginfochecksum = "switchbginfo" }
}

##########################################################################################################################################
#Function Start Self Elect Process
function Set-RPOSModuleStartSelfElect
{
	#Checking to see if LiveNames.txt is already there
	
	If (Test-Path $LiveIP)
	{
		Write-Output "LiveIP text file was found in the auto-updater grabbing PC names from file" | Out-File -Append -FilePath $Log
		$selected = Get-Content $LiveIP
		Write-Output "$selected PC discovered" | Out-File -Append -FilePath $Log
	}
	Else
	{
		#Checking local PC IP address, then scan network, then convert IP Addresses to PC Names
		$finalIP = "$ipmain1.$ipmain2.$ipmain3."
		Write-Output "$finalIP is the subnet that this PC will be scanning" | Out-File -Append -FilePath $Log
		$ping = New-Object System.Net.Networkinformation.ping
		
		Write-Output "Starting scan of PC's on the subnet" | Out-File -Append -FilePath $Log
		1 .. 254 | % { $ping.Send("${finalIP}$_", 500) } | select-object -ExpandProperty Address | Select -ExpandProperty IPAddressToString | Out-File -Append -FilePath $LiveIP
		#Getting getting rid of all the 10.x.x.1 addresses
		$getrid = Get-Content $LiveIP
		$getrid | Where-Object { $_ -notcontains "${finalIP}1" } | Set-Content $LiveIP
		#########################################################
		#Changed on 2-21-2018 NetBIOS Names have been disabled  #
		#########################################################
		#End of getting rid of all the 10.x.x.1 addresses
		#$IPs = Get-Content $LiveIP
		#Write-Output "Starting to get PC names from IP list" | Out-File -Append -FilePath $Log
		#$IPs | ForEach-Object { ([system.net.dns]::GetHostByAddress($_)).hostname } | Out-File -Append -FilePath $LiveNames
		#########################################################
		$script:selected = Get-Content $LiveIP
		Write-Output "$script:selected PC discovered" | Out-File -Append -FilePath $Log
	}
}

##########################################################################################################################################
#Function Install POS - Default Installer Process for RPOS
function Set-RPOSModuleInstallPOS
{
	
	#Switching registry key to zero that tells the Auto-Updater desktop if it was a successful install or not
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "0" -Force
	
	#Adding updated date of RPOS update in registry for LANDesk Inventory
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Value $datereg -Force
	Set-ItemProperty -Path "$LANDeskKeys" -Name "AUInstallingConfirm" -Value "1" -Force
	
	#Throwing up display message on users screen
	#Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:300 * RPOS is in the process of updating. During this time, please do not shut down your PC. RPOS will be unavailable. Do not use until the install prompts show a successful installation."
	
	New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "1" -PropertyType String -Force
	
	$RPOSMasterGUITasks = schtasks /query /FO list | findstr "RPOS AU Local User GUI" | Select-String -Pattern "RPOS AU Local User GUI"
	
	foreach ($RPOSMasterGUITask in $RPOSMasterGUITasks)
	{
		#Finding task name
		[string]$RPOSMasterGUITask = $RPOSMasterGUITask
		$indexmastertask = $RPOSMasterGUITask.IndexOf("RPOS AU Local")
		$RPOSMasterGUISIDUserTask = $RPOSMasterGUITask.Substring($indexmastertask)
		schtasks /Run /TN "$RPOSMasterGUISIDUserTask"
	}
	
	$counter = 0
	
	do
	{
		$counter += 1
		[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSLocalUserGUI" } | Select-Object -ExpandProperty ProcessId
		#$processtemp = Get-Process -Id $processtempsub
		Start-Sleep -Seconds 1
		
	} Until ((!$processtempsub) -or ($counter -eq "20"))
	
	#schtasks /Run /TN "RPOS AU Local User GUI $SID"
	#Start-Job -ScriptBlock { & C:\Temp\psexec.exe \\$env:COMPUTERNAME -s powershell.exe C:\temp\ServiceUI.exe C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe -WindowStyle hidden -executionpolicy Bypass -file C:\temp\WindowTest.ps1 }
	#Start-Sleep -Seconds 8
	New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "0" -PropertyType String -Force
	
	#Downloading Adobe Arh file if needed
	if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
	Else
	{
		$arh = "http://rpos.tbccorp.com/arh.exe"
		Invoke-WebRequest -Uri $arh -OutFile $arhfile
		Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
	}
	
	#Backing Up Current RPOS Build determaining if it is EXE or AIR file.
	Write-Output "Removing old bak files if there are in the BAK directory" | Out-File -Append -FilePath $Log
	Remove-Item "$RPOSfileBakair" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$flowfileBakair" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$RPOSfileBakexe" -Force | Out-File -Append -FilePath $Log
	Remove-Item "$flowfileBakexe" -Force | Out-File -Append -FilePath $Log
	
	if ($drfile -eq "RPOS.air")
	{
		Write-Output "Powershell has found an older version of RPOS AIR on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$drfile" -Destination $RPOSfileBakair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$drfile has been copied to $RPOSfileBakair Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq "FlowModule.air")
	{
		Write-Output "Powershell has found an older version of FlowModule AIR on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$dffile" -Destination $flowfileBakair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$dffile has been copied to $flowfileBakair Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($drfile -eq "RPOS.exe")
	{
		Write-Output "Powershell has found an older version of RPOS EXE on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$drfile" -Destination $RPOSfileBakexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$drfile has been copied to $RPOSfileBakexe Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq "FlowModule.exe")
	{
		Write-Output "Powershell has found an older version of FlowModule EXE on this PC backing up file" | Out-File -Append -FilePath $Log
		Copy-Item -Path "$RPOSAUDIRHOME\$dffile" -Destination $flowfileBakexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSAUDIRHOME\$dffile has been copied to $flowfileBakexe Successfully" | Out-File -Append -FilePath $Log
	}
	
	if ($drfile -eq $null)
	{
		Write-Output "Powershell did not find an older version of RPOS on this PC" | Out-File -Append -FilePath $Log
	}
	
	if ($dffile -eq $null)
	{
		Write-Output "Powershell did not find an older version of FlowModule on this PC" | Out-File -Append -FilePath $Log
	}
	
	#Removing Previous build files
	#If there is an EXE file delete AIR file and if there is an AIR file delete EXE files	
	if (($rfile -ne $drfile) -and ($drfile -eq "RPOS.air"))
	{
		Write-Output "Script found it is downloading EXE files deleting all AIR Files" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\rpos.air" -Force
		Remove-Item "$RPOSAUDIRHOME\FlowModule.air" -Force
		Remove-Item "$RPOSAUDIRBUILD\rpos.air" -Force
		Remove-Item "$RPOSAUDIRBUILD\FlowModule.air" -Force
	}
	if (($rfile -ne $drfile) -and ($drfile -eq "RPOS.exe"))
	{
		Write-Output "Script found it is downloading AIR files deleting all EXE Files" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\rpos.exe" -Force
		Remove-Item "$RPOSAUDIRHOME\FlowModule.exe" -Force
		Remove-Item "$RPOSAUDIRBUILD\rpos.exe" -Force
		Remove-Item "$RPOSAUDIRBUILD\FlowModule.exe" -Force
	}
	if (($rfile -eq $drfile) -and ($ffile -eq $dffile))
	{
		Write-Output "Script found the same AIR or EXE files deleting the same file" | Out-File -Append -FilePath $Log
		Remove-Item "$RPOSAUDIRHOME\$drfile" -Force
		Remove-Item "$RPOSAUDIRHOME\$dffile" -Force
	}
	if (($drfile -eq $null) -and ($dffile -eq $null))
	{
		Write-Output "Powershell didn't find any files FlowModule or RPOS no need to delete" | Out-File -Append -FilePath $Log
	}
	
	#Testing to see if there was already a subnet scan completed, if so it will use the current LiveNames file and run it against it
	if (Test-Path $LiveIP)
	{
		Write-Output "LiveIP.txt has been found on the PC at this time proceeding grabbing content from text file" | Out-File -Append -FilePath $Log
		$selected = Get-Content $LiveIP
	}
	Else
	{
		Write-Output "There was no subnet scanning for Adobe AIR previously starting subnet scan" | Out-File -Append -FilePath $Log
		
		if (($CiscoExists -ne $null) -and ($CiscoStatus -eq "2") -or ($windowsshortname -eq "WinSRV"))
		{
			Write-Output "Cisco AnyConnect is connected bypassing Self-election Process" | Out-File -Append -FilePath $Log
		}
		Else 
		{
			#Starting Self-Electing Process
			Set-RPOSModuleStartSelfElect
			$selected = Get-Content $LiveIP
		}

	}
	
	if (($CiscoExists -ne $null) -and ($CiscoStatus -eq "2") -or ($windowsshortname -eq "WinSRV"))
	{
		Write-Output "Cisco AnyConnect is connected bypassing Subnet Sharing" | Out-File -Append -FilePath $Log
	}
	Else
	{
		#If Cisco not connected
		
		#For each PC in the list of Livenames.txt start checking for network share and local files
		foreach ($electedPC in $selected)
		{
			$path = "\\$electedPC\cs18ebyi3$"
			$pathresults = Test-Path -path $path
			if ($pathresults -eq $false)
			{
				Write-Output "Powershell was unable to find network share on $electedPC trying next PC" | Out-File -Append -FilePath $log
			}
			Else
			{
				New-PSDrive -Name M FileSystem "\\$electedPC\cs18ebyi3$"
				$rshare01 = Test-Path "M:\$rfile"
				$fshare01 = Test-Path "M:\$ffile"
				$rsharehash01 = Get-Item -Path "M:\$rfile" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
				$fsharehash01 = Get-Item -Path "M:\$ffile" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
				if (($rshare01 -eq $true) -and ($fshare01 -eq $true) -and ($rsharehash01 -eq $xr) -and ($fsharehash01 -eq $xf))
				{
					Write-Output "Powershell has found correct RPOS and FLOW on local PC $electedPC starting transfer" | Out-File -Append -FilePath $Log
					Copy-Item "M:\$rfile" -Destination $RPOSfile -PassThru -Force | Out-File -Append -FilePath $Log
					Copy-Item "M:\$ffile" -Destination $flowfile -PassThru -Force | Out-File -Append -FilePath $Log
					Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Value "$electedPC" -Force
					Write-Output "Powershell succssfully copied RPOS and FLOW on local PC $electedPC" | Out-File -Append -FilePath $Log
					Remove-PSDrive -Name M; break
				}
				Else
				{
					Write-Output "Powershell was unable to find correct matching version of RPOS on $electedPC trying different PC" | Out-File -Append -FilePath $log
					Remove-PSDrive -Name M
				}
			}
		}
		
	}
	#Running MD5 checksum to ensure files are NOT! corrupt
	$nhashRPOS = Get-FileHash -Path $RPOSfile -Algorithm MD5 | select -ExpandProperty hash
	$nhashFlow = Get-FileHash -Path $flowfile -Algorithm MD5 | select -ExpandProperty hash
	
	#If Hash files are not matching trying to re-download
	if (($nhashRPOS -ne $xr) -or ($nhashFlow -ne $xf))
	{
		while ($sumnum -lt 5)
		{
			$sumnum += 1
			Write-Output "$sumnum is the current count of the amount of times script has tried redownloading max 5 times" | Out-File -Append -FilePath $Log
			#Setting download counter due to download count		
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Value $sumnum -Force
			Write-Output "Starting re-download of RPOS and FlowModule for the $sumnum time" | Out-File -Append -FilePath $Log
			Set-RPOSModuledownloadRPOS
			#Running MD5 checksum to ensure files are NOT! corrupt
			$nhashRPOS = Get-FileHash -Path $RPOSfile -Algorithm MD5 | select -ExpandProperty hash
			$nhashFlow = Get-FileHash -Path $flowfile -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$nhashRPOS is the MD5 hash of RPOS of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$nhashFlow is the MD5 hash of FlowModule of what was downloaded" | Out-File -Append -FilePath $Log
			Write-Output "$xr is the MD5 has of RPOS that server has" | Out-File -Append -FilePath $Log
			Write-Output "$xf is the MD5 has of FlowModule that server has" | Out-File -Append -FilePath $Log
			if (($xr -eq $nhashRPOS) -and ($xf -eq $nhashFlow)) { Break }
		}
	}
	
	if (($sumnum -ne "5") -and ($xr -eq $nhashRPOS) -and ($xf -eq $nhashFlow))
	{
		Write-Output "RPOS and FlowModule are matching the correct hash continuing" | Out-File -Append -FilePath $Log
		Write-Output "$nhashRPOS is the MD5 hash of RPOS of what was downloaded" | Out-File -Append -FilePath $Log
		Write-Output "$nhashFlow is the MD5 hash of FlowModule of what was downloaded" | Out-File -Append -FilePath $Log
		Write-Output "$xr is the MD5 has of RPOS that server has" | Out-File -Append -FilePath $Log
		Write-Output "$xf is the MD5 has of FlowModule that server has" | Out-File -Append -FilePath $Log
		
		#Coping file over to subnet file share
		Remove-Item $ShareFlow -Force
		Remove-Item $ShareRPOS -Force
		Write-Output "Starting to copy over for subnet sharing" | Out-File -Append -FilePath $Log
		Copy-Item $RPOSfile -Destination $ShareRPOS -PassThru -Force | Out-File -Append -FilePath $Log
		Write-Output "$RPOSfile has been copied for sharing on $ShareRPOS" | Out-File -Append -FilePath $Log
		Copy-Item $flowfile -Destination $ShareFlow -PassThru -Force | Out-File -Append -FilePath $Log
		Write-Output "$flowfile has been copied for sharing on $ShareFlow" | Out-File -Append -FilePath $Log
		
		#Value For Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckSelfSubDownload" -Value "1" -Force
		
		#Killing RPOS and Flow if running
		Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		
		#Value For Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "1" -Force
		
		#Uninstalling RPOS
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sur = $p.ExitCode
		Remove-Item "$RPOSHome" -Recurse -Force
		Write-Output "Arh silent RPOS uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		#Uninstalling Flow
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:suf = $p.ExitCode
		Write-Output "Arh silent FlowModule uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
		
		#Uninstalling Flow
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "1" -Force
		
		Start-Sleep -s 15
		
		if ($rfile -eq "RPOS.air")
		{
			#Installing RPOS AIR
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfile" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		if ($rfile -eq "RPOS.exe")
		{
			#Installing RPOS EXE
			$p = Start-Process "$RPOSAUDIRHOME\RPOS.exe" -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
			
		}
		
		Start-Sleep -s 15
		
		if ($ffile -eq "FlowModule.air")
		{
			#Installing FlowModule AIR
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfile" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		if ($ffile -eq "FlowModule.exe")
		{
			#Installing FlowModule EXE
			$p = Start-Process "$RPOSAUDIRHOME\FlowModule.exe" -ArgumentList "-silent" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
		}

		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "1" -Force
		
		#Logging amount of times download failed and file hashes to LANDesk Inventory scan and version of RPOS running on PC
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Value $sumnum -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Checksum" -Value $nhashRPOS -Force
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "FlowModule Checksum" -Value $nhashFlow -Force

		#Starting Removal Of Tesla
		Set-RPOSModuleTeslaDeleteProfile
			
		#Starting to install Tesla
		Set-RPOSModuleTeslaInstall
		
		#Setting RPOS Version Labels in Registry
		Set-RPOSModuleFindVersion
				
		#Removing IP address text files and DNS names of PC's
		#Remove-Item $LiveIP -Force
		Remove-Item $LiveNames -Force
	}
	Else
	{
		Write-Output "RPOS was not successful on downloading files correctly - no install or action will be taken" | Out-File -Append -Filepath $Log
	}
}

##########################################################################################################################################
#Fuction Install POS Without backing up previous build - Default Installer Process for RPOS
function Set-RPOSModuleInstallPOSnobackup
{	
	#Adding updated date of RPOS update in registry for LANDesk Inventory
	#Set-ItemProperty -path "$LANDeskKeys" -Name "RPOS Update Date" -Value $datereg -Force
	
	#Value for Progress Bar
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AULocalReinstall" -Value "1" -Force
	
	#Value for Progress Bar
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckSelfSubDownload" -Value "1" -Force
	
	#Downloading Adobe Arh file if needed
	if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
	Else
	{
		$arh = "http://rpos.tbccorp.com/arh.exe"
		Invoke-WebRequest -Uri $arh -OutFile $arhfile
		Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
	}
	
	#Killing RPOS and Flow if running
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	
	#Value for Progress Bar
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "1" -Force
	
	#Uninstalling RPOS
	$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
	$p.HasExited
	$script:sur = $p.ExitCode
	Remove-Item "$RPOSHome" -Recurse -Force
	Write-Output "Arh silent RPOS uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
	
	Start-Sleep -s 15
	
	#Uninstalling Flow
	$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
	$p.HasExited
	$script:suf = $p.ExitCode
	Write-Output "Arh silent FlowModule uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
	
	Start-Sleep -s 15
	
	if ($rfile -eq "RPOS.air")
	{
		#Installing RPOS AIR
		$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfile" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sir = $p.ExitCode
		Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
	}
	
	if ($rfile -eq "RPOS.exe")
	{
		#Installing RPOS EXE
		$p = Start-Process "$RPOSAUDIRHOME\RPOS.exe" -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sir = $p.ExitCode
		Write-Output "Arh silent RPOS install Exit Code $sir" | Out-File -Append -Filepath $Log
	}
	
	#Value for Progress Bar
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "1" -Force
	
	Start-Sleep -s 15
	
	if ($ffile -eq "FlowModule.air")
	{
		#Installing FlowModule AIR
		$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfile" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sif = $p.ExitCode
		Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
	}
	
	if ($ffile -eq "FlowModule.exe")
	{
		#Installing Flow
		$p = Start-Process "$RPOSAUDIRHOME\FlowModule.exe" -ArgumentList "-silent" -wait -NoNewWindow -PassThru
		$p.HasExited
		$script:sif = $p.ExitCode
		Write-Output "Arh silent FlowModule install Exit Code $sif" | Out-File -Append -Filepath $Log
	}
	
	#Value for Progress Bar
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "1" -Force
	
	
	#############################################################################
	#      Tesla Install no POS Backup Added on 5-10-2018                       #
	#############################################################################
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	
	#Value for Progress Bar
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingTesla" -Value "1" -Force
	
	Start-Process $teslafileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru | Out-File -Append -FilePath $Log
	
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	
	Copy-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Destination $PublicAppDataLocalFolder -Recurse -Force | Out-File -Append -FilePath $Log
	Remove-Item "$env:USERPROFILE\AppData\Local\tesla-electron" -Recurse -Force | Out-File -Append -FilePath $Log
	
	$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
	$versionnumberTesla = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
	$teslausers = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
	
	icacls "C:\Users\Public\AppData\Local\tesla-electron" /grant Everyone:F
	
	New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaVer" -Value "$versionnumberTesla" -Force
	
	foreach ($teslauser in $teslausers) {
		
			if (Test-Path "C:\Users\$teslauser\AppData\Local") {
				Write-Output "There was a AppData Local folder in $teslauser account" | Out-File -Append -FilePath $Log
			}
			Else {
				New-Item "C:\Users\$teslauser\AppData\Local" -ItemType Directory -Force
				Write-Output "There was no AppDataLocal folder in $teslauser account creating folder" | Out-File -Append -FilePath $Log
			}
			
			if (($teslauser -eq "shprorpos") -or ($teslauser -eq "shprorpos2") -or ($teslauser -eq "Public")) {
				Write-Output "These are RPOS Auto-Updater system profiles, do not create a symoblic link for these profiles" | Out-File -Append -FilePath $Log
			}
			Else {
				New-Item -ItemType SymbolicLink "C:\Users\$teslauser\AppData\Local" -Name tesla-electron -Value "$TeslaPublicFold" -Force
				Write-Output "Tesla Electrong AppData Symbolic Link has been created for $teslauser" | Out-File -Append -FilePath $Log
			}
		
		
	}
	
	
	#Value for Progress Bar
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "1" -Force
	#Value for Progress Bar
	#Set-ItemProperty -path "$LANDeskKeys" -Name "AULocalReinstall" -Value "1" -Force
	
	#Grabbing Values of versions for registry
	Set-RPOSModuleFindVersion

}

##########################################################################################################################################
#Fuction Remove RPOS Manually - This will remove all Registry Keys and files/folders if installer will not work
function Set-RPOSModuleManualRemove
{
	$RegRPOS64 = @("HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegRPOS64App = Get-ItemProperty $RegRPOS64 -EA 0
	$WantedRPOS64 = $RegRPOS64App | Where { $_.DisplayName -like "*RPOS*" }
	$WantedRPOS64 = $WantedRPOS64.PSChildName
	
	$RegRPOS32 = @("HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegRPOS32App = Get-ItemProperty $RegRPOS32 -EA 0
	$WantedRPOS32 = $RegRPOS32App | Where { $_.DisplayName -like "*RPOS*" }
	$wantedRPOS32 = $WantedRPOS32.PSChildName
	
	$RegRPOSInstall = @("HKLM:\Software\Classes\Installer\Products\*")
	$RegRPOSInstallApp = Get-ItemProperty $RegRPOSInstall -EA 0
	$WantedRPOSInstall = $RegRPOSInstallApp | Where { $_.ProductName -like "*RPOS*" }
	$WantedRPOSInstall = $WantedRPOSInstall.PSChildName
	
	$RegFLOW64 = @("HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegFLOW64App = Get-ItemProperty $RegFLOW64 -EA 0
	$WantedFlow64 = $RegFLOW64App | Where { $_.DisplayName -like "*FlowModule*" }
	$WantedFlow64 = $WamtedFlow64.PSChildName
	
	$RegFLOW32 = @("HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\*")
	$RegFLOW32App = Get-ItemProperty $RegFLOW32 -EA 0
	$WantedFlow32 = $RegFLOW32App | Where { $_.DisplayName -like "*FlowModule*" }
	$wantedFlow32 = $WantedFlow32.PSChildName
	
	foreach ($key in $WantedRPOS64) {
		if ($key -eq "RPOSAutoUpdater") {
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else {
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	foreach ($key in $WantedRPOS32) {
		if ($key -eq "RPOSAutoUpdater") {
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else {
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	foreach ($key in $WantedRPOSInstall) {
		
		Remove-Item "HKLM:\Software\classes\Installer\Products\$key" -Recurse -ErrorAction SilentlyContinue -Force
		Write-Output "Registry Key HKLM:\Software\classes\Installer\Products\$key was deleted" | Out-File -Append -FilePath $Log
	}
	
	foreach ($key in $WantedFlow64) {
		if ($key -eq "RPOSAutoUpdater") {
			Write-Output "Found HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else {
			Remove-Item "HKLM:\Software\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\classes\Installer\Products\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	foreach ($key in $WantedFlow32) {
		if ($key -eq "RPOSAutoUpdater")
		{
			Write-Output "Found HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key is part of the Auto-Updater bypassing" | Out-File -Append -FilePath $Log
		}
		Else {
			Remove-Item "HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key" -Recurse -ErrorAction SilentlyContinue -Force
			Write-Output "Registry Key HKLM:\Software\Microsoft\Windows\CurrentVersion\Uninstall\$key was deleted" | Out-File -Append -FilePath $Log
		}
	}
	
	Remove-Item -Path "$RPOSHOME" -Recurse -Force
	Write-OutPut "Folder C:\Program Files (x86\RPOS was deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "$FLOWHOME" -Recurse -Force
	Write-OutPut "Folder C:\Program Files (x86\FlowModule was deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "C:\Users\Public\Desktop\RPOS.lnk" -Force
	Write-Output "C:\Users\Public\Desktop\RPOS.lnk has been deleted" | Out-File -Append -FilePath $Log
	Remove-Item -Path "C:\Users\Public\Desktop\FlowModule.lnk" -Force
	Write-Output "C:\Users\Public\Desktop\FlowModule.lnk has been deleted" | Out-File -Append -FilePath $Log
	
	##################################################################################
	#           Adding Tesla Manual Removal Section                                  #
	##################################################################################
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	
	Write-Output "Starting the process of removing tesla proton and electron manually" | Out-File -Append -FilePath $Log
	
	Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
	Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
	Write-Output "Was tesla-electron Running?" | Out-File -Append -FilePath $Log
	Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
	Write-Output "Was proton running?" | Out-File -Append -FilePath $Log
	Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
	Write-Output "Force closing any Tesla open installers running" | Out-File -Append -Filepath $Log
	Stop-Process -Force -PassThru -ProcessName Tesla* | Out-File -Append -FilePath $Log
	
	Remove-Item "C:\Users\Public\AppData\Local\tesla-electron" -Force -Recurse
	Write-Output "Tesla Electron Public AppData folder has been deleted from PC" | Out-File -Append -FilePath $Log
	
	$teslausersdels = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
	
	foreach ($teslauserdel in $teslausersdels) {
		if (Test-Path "C:\Users\$teslauserdel\AppData\Local\tesla-electron") {
			Remove-Item "C:\Users\$teslauserdel\AppData\Local\tesla-electron" -Recurse -Force
			(Get-Item "C:\Users\$teslauserdel\AppData\Local\tesla-electron\").Delete()
			Write-Output "Removed C:\Users\$teslauserdel\AppData\Local\tesla-electron Symbolic Link" | Out-File -Append -FilePath $Log
		}
		Else {
			Write-Output "$teslauserdel had no symbolic link attached to the profile, no need to remove anything" | Out-File -Append -FilePath $Log	
		}
	}
	
}

##########################################################################################################################################
#Fuction check install - Checking to make sure no bad installs are there
function Set-RPOSModulecheckinstall
{
	Write-Output "Checking to see if Install was successful" | Out-File -Append -FilePath $Log
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	################################################################################################
	#   Checking Tesla Installer Added on 5/10/2018                                                #
	################################################################################################
	
	#Grabbing current tesla installer name to verify if there is the correct version of Tesla Installed and directories are setup.
	$TeslaHashURL = "$server/hashes.xml"
	$Teslaxmlread = [xml](New-Object System.Net.WebClient).downloadstring($TeslaHashURL)
	$teslafolderver = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron" | Where-Object { $_.Name -like "app*" } | Select-Object -ExpandProperty Name
	
	#$teslaveronline = $Teslaxmlread.checksum.file | ? { $_.filename -like "tesla-electron-[0-9].[0-9].[0-9] *" } | Select-Object -ExpandProperty "filename"
	#$teslaveronline = ($teslaveronline).Substring(15, 5)
	
	#$teslaverlocal = (Get-Item "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\tesla-electron.exe").VersionInfo | Select-Object -ExpandProperty FileVersion
	
	Write-Output "Checking Tesla Install....." | Out-File -Append -Filepath $Log
	
	$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
	$script:TeslaElectronversionWeb = (Get-Content "$TeslaElectronversion").Substring(8)
	
	$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
	
	#End of Tesla Installer check
	
	#Checking Symoblic Links
	Set-RPOSModuleTeslaCheckSymbolicLink
	
	#If RPOS or FlowModule are blank reinstall them if not blank do nothing
	If (($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($verRPOS -ne $x) -or ($verFLOW -ne $x) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal)) 
	{
		Write-Output "Found Install was not successful" | Out-File -Append -FilePath $Log
		Write-Output "After install RPOS and Flow Status" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is what the check pulled for RPOS" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is what the check pulled for FlowModule" | Out-File -Append -FilePath $Log
		Write-Output "Running installer again for Flow and RPOS" | Out-File -Append -FilePath $Log
		#Write-Output "Tesla version downloaded from server is $teslaveronline and version found on PC hash $teslaverlocal" | Out-File -Append -FilePath $Log
		Write-Output "Tesla install directory found on PC? $CheckingTeslaDirectory" | Out-File -Append -FilePath $Log
		Write-Output "Tesla electron exe found in tesla install directory? $CheckingTeslaElectronexe" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUFailedLocal" -Value "1" -Force
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "1" -Force
		
		Set-RPOSModuleInstallPOS
	}
	Else
	{
		Write-Output "RPOS and FlowModule Installed checkinstall is complete and passed" | Out-File -Append -FilePath $Log
		New-ItemProperty -Path "$LANDeskKeys" -Name "InstallComplete" -Value "1" -Force
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "1" -Force
		
	}
}

##########################################################################################################################################
#Fuction check install - Checking to make sure no bad installs are there
function Set-RPOSModulecheckinstallnoreinstall
{
	Write-Output "Checking to see if Install was successful" | Out-File -Append -FilePath $Log
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	################################################################################################
	#   Checking Tesla Installer Added on 5/10/2018                                                #
	################################################################################################
	
	#Grabbing current tesla installer name to verify if there is the correct version of Tesla Installed and directories are setup.
	$TeslaHashURL = "$server/hashes.xml"
	$Teslaxmlread = [xml](New-Object System.Net.WebClient).downloadstring($TeslaHashURL)
	$teslafolderver = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron" | Where-Object { $_.Name -like "app*" } | Select-Object -ExpandProperty Name
	
	Write-Output "Checking Tesla Install....." | Out-File -Append -Filepath $Log
	
	$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
	$script:TeslaElectronversionWeb = (Get-Content "$TeslaElectronversion").Substring(8)
	
	$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
	
	#End of Tesla Installer check
	
	#Checking Symoblic Links
	Set-RPOSModuleTeslaCheckSymbolicLink
	
	#If RPOS or FlowModule are blank reinstall them if not blank do nothing
	If (($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($verRPOS -ne $x) -or ($verFLOW -ne $x) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal))
	{
		Write-Output "Found Install was not successful" | Out-File -Append -FilePath $Log
		Write-Output "After install RPOS and Flow Status" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is what the check pulled for RPOS" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is what the check pulled for FlowModule" | Out-File -Append -FilePath $Log
		Write-Output "Running installer again for Flow and RPOS" | Out-File -Append -FilePath $Log
		#Write-Output "Tesla version downloaded from server is $teslaveronline and version found on PC hash $teslaverlocal" | Out-File -Append -FilePath $Log
		Write-Output "Tesla install directory found on PC? $CheckingTeslaDirectory" | Out-File -Append -FilePath $Log
		Write-Output "Tesla electron exe found in tesla install directory? $CheckingTeslaElectronexe" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUFailedLocal" -Value "1" -Force
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "1" -Force
		
	}
	Else
	{
		Write-Output "RPOS and FlowModule Installed checkinstall is complete and passed" | Out-File -Append -FilePath $Log
		New-ItemProperty -Path "$LANDeskKeys" -Name "InstallComplete" -Value "1" -Force
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "1" -Force
		
	}
}

##########################################################################################################################################
#Fuction download RPOS - actual download process for RPOS and FlowModule
function Set-RPOSModuledownloadRPOS
{
	
	#Removing Previous build files
	Remove-Item $flowfile -Force
	Remove-Item $RPOSfile -Force
	
	#Removing IP scan and DNS names text files	
	Remove-Item $LiveIP -Force
	Remove-Item $LiveNames -Force
	
	#Downloading RPOS and Flow Files
	$RPOS = "$server/$rfile"
	Invoke-WebRequest -Uri $RPOS -OutFile $RPOSfile
	Write-Output "$rfile has been downloaded" | Out-File -Append -FilePath $Log
	$Flow = "$server/$ffile"
	Invoke-WebRequest -Uri $Flow -OutFile $flowfile
	Write-Output "$ffile has been downloaded" | Out-File -Append -FilePath $Log
	Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Value "DistroServer" -Force
}

##########################################################################################################################################
#Fuction switchbuild - This controls the switching of RPOS builds
function Set-RPOSModuleswitchbuild
{
	#Checking to see if there is a switch in the builds
	$URL = $XMLSwitchURL
	
	#Getting switch build response
	$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
	$script:w = $xmlread.serverswitch.$switchvalue.name
	$script:x = $xmlread.serverswitch.$switchvalue.statement
	$script:y = $xmlread.serverswitch.$switchvalue.server
	Write-Output "Switch build revision equals $x" | Out-File -Append -FilePath $Log
	
	#If switch build equals true start downloading distrubtion package on specified server if false continue with version check and install on same server.
	if (($x -eq "true") -and ($rposname -ne $w))
	{
		Write-Output "Switching RPOS Distros was detected switching to $w on $y on $switchvalue" | Out-File -Append -FilePath $Log
		#Setting switch server from XML file
		$server = "http://$y"
		
		#Downloading hash checksum for swutch value
		$URL = "$server/checksum.xml"
		Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash
		
		#Getting HD5 hash numbers
		[xml]$xmlread = Get-Content $XMLhash
		$xr = $xmlread.checksum.RPOS
		[xml]$xmlread = Get-Content $XMLhash
		$xf = $xmlread.checksum.Flow
		Write-Output "$xr is the checksum of RPOS version on $server" | Out-File -Append -FilePath $Log
		Write-Output "$xf is the checksum of FlowModule version on $server" | Out-File -Append -FilePath $Log
		
		#Downloading update XML for switch build
		$URL = "$server/update.xml"
		Invoke-WebRequest -Uri $URL -OutFile $XML
		[xml]$xmlread = Get-Content $xml
		$script:x = $xmlread.update.versionNumber
		Write-Output "$x is the version pulled on $server" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is the version of RPOS pulled on PC" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is the version of Flow pulled on PC" | Out-File -Append -FilePath $Log
			
			
		####################################################################################################
		#Adding New Tesla Module 5-22-2018                                                                 #
		####################################################################################################
		$URLTeslaElectron = "$server/TESLA-ELECTRON.VERSION"
		Invoke-WebRequest -Uri "$URLTeslaElectron" -OutFile $TeslaElectronversion
		$script:TeslaElectronversionWeb = (Get-Content "$TeslaElectronversion").Substring(8)
			
		$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
		$CheckingTeslaDirectory = Test-path "C:\Users\Public\AppData\Local\tesla-electron"
		$CheckingTeslaElectronexe = Test-Path "C:\Users\Public\AppData\Local\tesla-electron\$teslafolderver\tesla-electron.exe"
			
		$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
		Write-Output "$TeslaElectronversionWeb is the version of Tesla Electron on the server" | Out-File -Append -FilePath $Log
		Write-Output "$TeslaElectronversionLocal is the version of Tesla Electron on the PC now" | Out-File -Append -FilePath $Log
		
		#Finding out if RPOS and FlowModule are AIR or EXE
		Invoke-Expression "Invoke-webrequest $server/RPOS.exe -DisableKeepAlive -UseBasicParsing -Method head" -ErrorVariable resultexeair
		$resultexeair | Out-File -FilePath $results
		
		#If statement for EXE or AIR files	
		If ((Get-Content $results) -eq $null)
		{
			$rfile = "RPOS.exe"
			$ffile = "FlowModule.exe"
			Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log
		}
		If ((Get-Content $results) -ne $null)
		{
			$rfile = "RPOS.air"
			$ffile = "FlowModule.air"
			Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log
		}
		
		#Creating values for the default RPOS folder and Default share folder	
		$RPOSfile = "$RPOSAUDIRHOME\$rfile"
		$flowfile = "$RPOSAUDIRHOME\$ffile"
		$ShareRPOS = "$RPOSAUDIRBUILD\$rfile"
		$ShareFlow = "$RPOSAUDIRBUILD\$ffile"
		
		#Starting to install RPOS
		Set-RPOSModuleInstallPOS
		
		$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
		$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
		if (($sir -gt "0") -or ($sif -gt "0") -or ($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal))
		{
			Write-Output "Where was an error detected with the automatic installer - switching to manual mode and reinstalling" | Out-File -Append -FilePath $Log
			Set-RPOSModuleManualRemove
			Set-RPOSModuleInstallPOSnobackup
			Set-RPOSModulecheckinstall
			#Setting Checksum registry Key if passed
			{ Break }
		}
		Else
		{
			Write-Output "Version passed validation of successful install" | Out-File -Append -FilePath $Log
			Set-RPOSModulecheckinstall
			#Setting Checksum registry Key if passed
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Value $checkswitchxml -Force
			{ Break }
		}
	}
	Else
	{
		Write-Output "There was a newer version of switchxml, however the site was not matching what was changed" | Out-File -Append -FilePath $Log
		#Setting Checksum registry Key if passed
		Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Value $checkswitchxml -Force
		$script:RegSwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name SwitchXMLchecksum).SwitchXMLchecksum
	}
	
	#CheckRPOSIcon Checking Desktop Icon
	Set-RPOSModuleCheckRPOSIcon
}

#########################################################################################################################################
#                                                         * END OF MODULES *                                                            # 
#########################################################################################################################################


###############################################################################################################
# Function RPOS-UpdatePOS
function Get-RPOSUpdateRPOS
{
#Setting up varibles from above
Set-RPOSVar

##########################################################################################################################################
	
	#Setting All Registry Keys for Status Window Back To Zero
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRNI" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUAdobeAIRInstalling" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckSelfSubDownload" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUClosingWindows" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInstall" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUFailedLocal" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AULocalReinstall" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUGatherReportingData" -Value "0" -Force
	Set-ItemProperty -path "$LANDeskKeys" -Name "AUInstallingTesla" -Value "0" -Force
	Set-ItemProperty -Path "$LANDeskKeys" -Name "AUInstallingConfirm" -Value "0" -Force
	
	#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d" | Out-File -Append -FilePath $Log
	Write-Output "RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
	
	Write-Output "Checking internet connection, please wait......" | Out-File -Append -FilePath $Log
	
	If (($networkcheck -eq $null) -or ($IPZero -eq "127.0.0.0") -or ($pingrpos -ne "Success")) {
		Write-Output "There is currnetly no internet connection RPOS Auto-Updater will check again for an update another time" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "2" -Force
		Set-ItemProperty -Path "$LANDeskKeys" -Name "InstallComplete" -Value "0" -Force
		#Starting LANDesk Inventory Scan
		#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N /W=$InventoryRunTime
		Write-Output "Waiting $InventoryRunTime until starting the LANDesk Inventory Scan" | Out-File -Append -FilePath $Log
		
		#Getting time of end of script for log file
		$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
		Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log }
	Else {
		
		Write-Output "There is currently an active and running internet connection on this PC proceeding with setup" | Out-File -Append -FilePath $Log
		
		if ($dateofdelayhour -ge "00" -and $dateofdelayhour -le "06") {
			#Starting Random Delay Up To One Hour
			Write-Output "Starting delay of $StartTimeDelay seconds before running RPOS Auto-Updater due to after hours" | Out-File -Append -FilePath $Log
			Start-Sleep -Seconds $StartTimeDelay }
		
		Write-Output "Starting Auto-Updater during normal business hours without the delay" | Out-File -Append -FilePath $Log
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUCheckInternet" -Value "1" -Force
		
		#Grabbing three digit IP address for RPOS-Preferences XML for later use
		Set-RPOSModuleThreeDigitIPfromXML
		
		#Starting to check for Adobe AIR
		Write-Output "Starting to check for Adobe AIR Install" | Out-File -Append -FilePath $Log
		Set-RPOSModuleInstallAIR
		
		#setting up registry search
		New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
		
		#Grabbing version from RPOS and FLOW Registry
		$script:rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
		$script:verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$script:verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		
		#Grabbing files names in RPOS directory
		$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
		$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
		
		#Checking to see what PC name is assoicated with a Store, TAC, Carroll Tire, Or Corprate Owned
		Set-RPOSModuleWhatPCAmI
		
		#Checking XML update files checksums
		$URL = $checkupdatedXMLs
		$script:xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
		$checkswitchxml = $xmlread.filechecksum.switchxml.$XMLswitchchecksum
		$checkBGInfoxml = $xmlread.filechecksum.BGInfoxml.$XMLbginfochecksum
		
		#If the PC is a store PC please pull the newer endpoint xml hash
		if ($RPOSAgentname -eq "Store") {
			Write-Output "Store PC is checking to see if there, is newer endpoint information on RPOS.tbccorp.com" | Out-File -Append -FilePath $Log
			$script:checkendpointxml = $xmlread.filechecksum.RPOSEndpointxml.endpointhash
		}		
		
		#Preventing install of BGInfo and EMV on Carroll Tire, TAC, Personal PC - Checking to see if EMV is installed and BGinfo is up to date.
		if ($RPOSAgentname -eq "Store")
		{
			
			Write-Output "Checking for EMV install and current BGInfo" | Out-File -Append -FilePath $Log
			Set-RPOSModuleCheckBGInfoUpdateandEMV
		}
		
		# Check Endpoints for RPOS Store PCs Only
		if (($RPOSAgentname -eq "Store") -and ($RegEndpointXMLchecksum -ne $checkendpointxml))
		{
			Write-Output "There is an update for the endpoints for store locations, moving to check endpoints for further changes" | Out-File -Append -FilePath $Log
			Write-Output "Older Endpoint Checkxml was $RegEndpointXMLchecksum and the newer one is $checkendpointxml" | Out-File -Append -FilePath $Log
			Set-RPOSEndpointsCheckXML
		}
		
		Write-Output "$switchvalue is the switch value." | Out-File -Append -FilePath $Log
		Write-Output "$rposname is the version of RPOS running on this PC" | Out-File -Append -FilePath $Log
		
		#checking to see if switch file has changed and if it has to switch the RPOS version
		if ($checkswitchxml -ne $RegSwitchXMLchecksum)
		{
			
			Write-Output "There is a different switchxml on RPOS.tbccorp.com checking xml and seeing if there is a new distro change" | Out-File -Append -FilePath $Log
			Set-RPOSModuleswitchbuild
		}
		
		if ($checkswitchxml -eq $RegSwitchXMLchecksum)
		{
			Write-Output "There is no new switch.xml on RPOS.tbccorp.com, bypassing switching distro to checking for newer version of RPOS without switching." | Out-File -Append -FilePath $Log
			Write-Output "Powershell has found no distro change needed. Version upgrade check starting" | Out-File -Append -FilePath $Log
			
			#Pulling update xml file from server to compare local and server update.xml
			if ($rposname -eq "RPOS - PRODUCTION") {
				$URL = "http://$prod/update.xml"
				$server = "http://$prod"
				Invoke-WebRequest -Uri "$URL" -OutFile $XML }
			if ($rposname -eq "RPOS - TRAINING") {
				$URL = "http://$train/update.xml"
				$server = "http://$train"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - PILOT") {
				$URL = "http://$pilot/update.xml"
				$server = "http://$pilot"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - PILOT2") {
				$URL = "http://$pilot2/update.xml"
				$server = "http://$pilot2"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - PILOT3") {
				$URL = "http://$pilot3/update.xml"
				$server = "http://$pilot3"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - QA") {
				$URL = "http://$qa/update.xml"
				$server = "http://$qa"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq $null) {
				$URL = "http://$prod/update.xml"
				$server = "http://$prod"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - QAPILOT") {
				$URL = "http://$qapilot/update.xml"
				$server = "http://$qapilot"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - QAPILOT2") {
				$URL = "http://$qapilot2/update.xml"
				$server = "http://$qapilot2"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - QAPILOT3") {
				$URL = "http://$qapilot3/update.xml"
				$server = "http://$qapilot3"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - ALPHA") {
				$URL = "http://$alpha/update.xml"
				$server = "http://$alpha"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - DEV") {
				$URL = "http://$dev/update.xml"
				$server = "http://$dev"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - DEVPILOT") {
				$URL = "http://$devpilot/update.xml"
				$server = "http://$devpilot"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - DEVPILOT2") {
				$URL = "http://$devpilot2/update.xml"
				$server = "http://$devpilot2"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			if ($rposname -eq "RPOS - DEVPILOT3") {
				$URL = "http://$devpilot3/update.xml"
				$server = "http://$devpilot3"
				Invoke-WebRequest -Uri $URL -OutFile $XML }
			
			#Getting version number 
			[xml]$xmlread = Get-Content $xml
			$x = $xmlread.update.versionNumber
			Write-Output "$x is the version pulled on $server" | Out-File -Append -FilePath $Log
			Write-Output "$verRPOS is the version of RPOS pulled on PC" | Out-File -Append -FilePath $Log
			Write-Output "$verFLOW is the version of Flow pulled on PC" | Out-File -Append -FilePath $Log
			
			#Finding out if RPOS and FlowModule are AIR or EXE
			Invoke-Expression "Invoke-webrequest $server/RPOS.exe -DisableKeepAlive -UseBasicParsing -Method head" -ErrorVariable resultexeair
			$resultexeair | Out-File -FilePath $results
			
			#If statement for EXE or AIR files	
			If ((Get-Content $results) -eq $null) {
				$rfile = "RPOS.exe"
				$ffile = "FlowModule.exe"
				Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log }
			If ((Get-Content $results) -ne $null) {
				$rfile = "RPOS.air"
				$ffile = "FlowModule.air"
				Write-Output "Powershell found that the execute of RPOS and FlowModule are $rfile and $ffile" | Out-File -Append -FilePath $Log }
			
			#Creating values for the default RPOS folder and Default share folder	
			$RPOSfile = "$RPOSAUDIRHOME\$rfile"
			$flowfile = "$RPOSAUDIRHOME\$ffile"
			$ShareRPOS = "$RPOSAUDIRBUILD\$rfile"
			$ShareFlow = "$RPOSAUDIRBUILD\$ffile"
			
			#Pulling checksum xml file from server to compare the downloaded checksum to ensure download was downloaded correctly
			if ($rposname -eq "RPOS - PRODUCTION") {
				$URL = "http://$prod/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - TRAINING") {
				$URL = "http://$train/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - PILOT") {
				$URL = "http://$pilot/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - PILOT2") {
				$URL = "http://$pilot2/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - PILOT3") {
				$URL = "http://$pilot3/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - QA") {
				$URL = "http://$qa/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq $null) {
				$URL = "http://$prod/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - QAPILOT") {
				$URL = "http://$qapilot/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - QAPILOT2") {
				$URL = "http://$qapilot2/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - QAPILOT3") {
				$URL = "http://$qapilot3/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - ALPHA") {
				$URL = "http://$alpha/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - DEV") {
				$URL = "http://$dev/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - DEVPILOT") {
				$URL = "http://$devpilot/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - DEVPILOT2") {
				$URL = "http://$devpilot2/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			if ($rposname -eq "RPOS - DEVPILOT3") {
				$URL = "http://$devpilot3/checksum.xml"
				Invoke-WebRequest -Uri "$URL" -OutFile $XMLhash }
			
			#Getting HD5 hash numbers
			[xml]$xmlread = Get-Content $XMLhash
			$script:xr = $xmlread.checksum.RPOS
			[xml]$xmlread = Get-Content $XMLhash
			$script:xf = $xmlread.checksum.Flow
			Write-Output "$xr is the checksum of RPOS version on $server" | Out-File -Append -FilePath $Log
			Write-Output "$xf is the checksum of FlowModule version on $server" | Out-File -Append -FilePath $Log
			
			####################################################################
			# Checking Tesla Version Added 5/11/2018                           #
			####################################################################
			
			$URLTeslaElectron = "$server/TESLA-ELECTRON.VERSION"
			Invoke-WebRequest -Uri "$URLTeslaElectron" -OutFile $TeslaElectronversion
			$script:TeslaElectronversionWeb = (Get-Content "$TeslaElectronversion").Substring(8)
			
			$TeslaAppDirectory  = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
			
			$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
			
			Write-Output "$TeslaElectronversionWeb is the version of Tesla Electron on the server" | Out-File -Append -FilePath $Log
			Write-Output "$TeslaElectronversionLocal is the version of Tesla Electron on the PC now" | Out-File -Append -FilePath $Log
			
			#Downloading Adobe Arh file if needed
			if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
			Else
			{
				$arh = "http://rpos.tbccorp.com/arh.exe"
				Invoke-WebRequest -Uri $arh -OutFile $arhfile
				Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
			}
			
			#Checking if RPOS and Flow Equal or Not Equal to server
			if (($x -eq $verRPOS) -and ($x -eq $verFLOW) -and ($TeslaElectronversionWeb -eq $TeslaElectronversionLocal))
			{
				Write-Output "Nothing to download versions of RPOS and flow and Tesla are current" | Out-File -Append -FilePath $Log
				Set-RPOSModulecheckinstall
				Set-RPOSModuleFindVersion
			}
			Else
			{
				#Starting RPOS and Flow Module Install
				Write-Output "RPOS and Flow and Tesla are different versions - Starting New Install" | Out-File -Append -FilePath $Log
				Set-RPOSModuleInstallPOS
				
				$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
				$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
				$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
				$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
				
				if (($sir -gt "0") -or ($sif -gt "0") -or ($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal))
				{
					Write-Output "There was an error detected with the automatic installer - switching to manual mode and reinstalling" | Out-File -Append -FilePath $Log
					
					#Adding registry key for GUI responce to reinstall
					New-ItemProperty -Path "$LANDeskKeys" -Name "AULocalReinstall" -Value "1" -Type String -Force
					
					#First Uninstall process if the first install failed
					Set-RPOSModulecheckinstall
					
					#Adding registry key for GUI responce to reinstall					
					Set-RPOSModulecheckinstallnoreinstall
					
					$successful = (Get-ItemProperty -Path $LANDeskKeys -Name "InstallComplete")."InstallComplete"
					
					if ($successful -eq "1")
					{
						New-ItemProperty -Path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "1" -Type String -Force
					} 
					
					Elseif (($successful -eq "0") -or (!$successful))
					{
						Set-RPOSModuleManualRemove
						Set-RPOSModuleInstallPOSnobackup
						
						
						New-ItemProperty -Path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "1" -Type String -Force
						Set-RPOSModulecheckinstall
						
					}
				}
				Else
				{
					Write-Output "Version passed validation of successful install" | Out-File -Append -FilePath $Log
					Set-RPOSModulecheckinstall
					Set-RPOSModuleFindVersion
				}
			}
		}
		
		#Checking Install One last time
		$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
		$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
		$testpospath = (Test-Path "$RPOSHOME\RPOS.exe")
		$testflowpath = (Test-Path "$FLOWHOME\FlowModule.exe")
		$TeslaAppDirectory = Get-ChildItem "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
		$CheckingTeslaDirectory = Test-path "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron"
		$CheckingTeslaElectronexe = Test-Path "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron\$teslafolderver\tesla-electron.exe"
		$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
		
		if (($verRPOS -ne $x) -or ($verFLOW -ne $x) -or ($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($testflowpath -eq $false) -or ($testpospath -eq $false) -or ($CheckingTeslaDirectory -eq $false) -or ($CheckingTeslaElectronexe -eq $false) -or ($TeslaElectronversionWeb -ne $TeslaElectronversionLocal))
		{
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "0" -Force
		}
		Else
		{
			New-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Value "1" -Force
		}
		
		#Value for Progress Bar
		Set-ItemProperty -path "$LANDeskKeys" -Name "AUGatherReportingData" -Value "1" -Force
		
		#Checking Reporting XML
		Set-RPOSModuleCheckReportXML
		
		#CheckRPOSIcon Checking Desktop Icon
		Set-RPOSModuleCheckRPOSIcon
		
		#Checking to see if there was a 3rd reinstall.
		$3rdReinstallCars = (Get-ItemProperty -Path $LANDeskKeys -Name AUReinstall3rdTime).AUReinstall3rdTime
		
		if ($3rdReinstallCars -eq "1")
		{
			$counter = 0
			
			do
			{
				$counter += 1
				[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSStatusInstall" } | Select-Object -ExpandProperty ProcessId
				Start-Sleep -Seconds 1
			}
			Until ((!$processtempsub) -or ($counter -eq "200"))
		}
		
		#Adding a stop for Windows Server applications 
		if ($windowsshortname -eq "WinSRV")
		{
			Start-Sleep -seconds 20
		}

		#Setting All Registry Keys for Status Window Back To Zero
		Set-RPOSModuleClearReg
		
		#Starting LANDesk Inventory Scan
		#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N /W=$InventoryRunTime
		#Write-Output "Waiting $InventoryRunTime until starting the LANDesk Inventory Scan" | Out-File -Append -FilePath $Log
		
		$completeinstall = (Get-ItemProperty -Path $LANDeskKeys -Name "InstallComplete")."InstallComplete"
		$AUInstallingConfirm = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingConfirm")."AUInstallingConfirm"
		
		if (($completeinstall -eq "0") -and ($AUInstallingConfirm -eq "1"))
		{
			#Displaying message to notify user that install is incomplete
			#Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:300 * RPOS was unsuccessful installing the latest version. It will try to install again at a later time."
			
			New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "5" -PropertyType String -Force
			
			$RPOSMasterGUITasks = schtasks /query /FO list | findstr "RPOS AU Local User GUI" | Select-String -Pattern "RPOS AU Local User GUI"
			
			foreach ($RPOSMasterGUITask in $RPOSMasterGUITasks)
			{
				#Finding task name
				[string]$RPOSMasterGUITask = $RPOSMasterGUITask
				$indexmastertask = $RPOSMasterGUITask.IndexOf("RPOS AU Local")
				$RPOSMasterGUISIDUserTask = $RPOSMasterGUITask.Substring($indexmastertask)
				schtasks /Run /TN "$RPOSMasterGUISIDUserTask"
			}
			
			$counter = 0
			
			do
			{
				$counter += 1
				[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSLocalUserGUI" } | Select-Object -ExpandProperty ProcessId
				#$processtemp = Get-Process -Id $processtempsub
				Start-Sleep -Seconds 1
				
			}
			Until ((!$processtempsub) -or ($counter -eq "20"))
			
			New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "0" -PropertyType String -Force
			
			#Checking to see if desktop run is running so there are not multiple validation prompts
			[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSDesktopUpdate" } | Select-Object -ExpandProperty ProcessId
			
			if (!$processtempsub)
			{
				#Showing Detailed UnSuccessful Install
				New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "7" -PropertyType String -Force
				
				$RPOSMasterGUITasks = schtasks /query /FO list | findstr "RPOS AU Local User GUI" | Select-String -Pattern "RPOS AU Local User GUI"
				
				foreach ($RPOSMasterGUITask in $RPOSMasterGUITasks)
				{
					#Finding task name
					[string]$RPOSMasterGUITask = $RPOSMasterGUITask
					$indexmastertask = $RPOSMasterGUITask.IndexOf("RPOS AU Local")
					$RPOSMasterGUISIDUserTask = $RPOSMasterGUITask.Substring($indexmastertask)
					schtasks /Run /TN "$RPOSMasterGUISIDUserTask"
				}
				
				$counter = 0
				
				do
				{
					$counter += 1
					[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSLocalUserGUI" } | Select-Object -ExpandProperty ProcessId
					#$processtemp = Get-Process -Id $processtempsub
					Start-Sleep -Seconds 1
					
				}
				Until ((!$processtempsub) -or ($counter -eq "20"))
				
				New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "0" -PropertyType String -Force
			}
		}
		if (($completeinstall -eq "1") -and ($AUInstallingConfirm -eq "1"))
		{
			#Displaying message to notify user that install is complete
			#Invoke-WmiMethod -Class Win32_Process -ComputerName $env:COMPUTERNAME -Name Create -ArgumentList "C:\Windows\System32\msg.exe /TIME:300 * RPOS successfully installed the latest version. You may use RPOS now."
			
			New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "4" -PropertyType String -Force
			
			$RPOSMasterGUITasks = schtasks /query /FO list | findstr "RPOS AU Local User GUI" | Select-String -Pattern "RPOS AU Local User GUI"
			
			foreach ($RPOSMasterGUITask in $RPOSMasterGUITasks)
			{
				#Finding task name
				[string]$RPOSMasterGUITask = $RPOSMasterGUITask
				$indexmastertask = $RPOSMasterGUITask.IndexOf("RPOS AU Local")
				$RPOSMasterGUISIDUserTask = $RPOSMasterGUITask.Substring($indexmastertask)
				schtasks /Run /TN "$RPOSMasterGUISIDUserTask"
			}
			
			$counter = 0
			
			do
			{
				$counter += 1
				[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSLocalUserGUI" } | Select-Object -ExpandProperty ProcessId
				#$processtemp = Get-Process -Id $processtempsub
				Start-Sleep -Seconds 1
				
			}
			Until ((!$processtempsub) -or ($counter -eq "20"))
			
			New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "0" -PropertyType String -Force
			
			#Checking to see if desktop RPOS is running at the moment so we don't have multiple results window
			[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSDesktopUpdate" } | Select-Object -ExpandProperty ProcessId
			
			if (!$processtempsub)
			{
				
				#Showing Detailed Successful Install
				New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "6" -PropertyType String -Force
				
				$RPOSMasterGUITasks = schtasks /query /FO list | findstr "RPOS AU Local User GUI" | Select-String -Pattern "RPOS AU Local User GUI"
				
				foreach ($RPOSMasterGUITask in $RPOSMasterGUITasks)
				{
					#Finding task name
					[string]$RPOSMasterGUITask = $RPOSMasterGUITask
					$indexmastertask = $RPOSMasterGUITask.IndexOf("RPOS AU Local")
					$RPOSMasterGUISIDUserTask = $RPOSMasterGUITask.Substring($indexmastertask)
					schtasks /Run /TN "$RPOSMasterGUISIDUserTask"
				}
				
				$counter = 0
				
				do
				{
					$counter += 1
					[string]$processtempsub = Get-WmiObject -Class Win32_Process -ErrorAction SilentlyContinue | Where-Object { $_.CommandLine -match "Get-RPOSLocalUserGUI" } | Select-Object -ExpandProperty ProcessId
					#$processtemp = Get-Process -Id $processtempsub
					Start-Sleep -Seconds 1
					
				}
				Until ((!$processtempsub) -or ($counter -eq "20"))
				
				New-ItemProperty -Path "HKLM:\SOFTWARE\WOW6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUGuiDisplay" -Value "0" -PropertyType String -Force
				
			}
		}
		
		#Getting time of end of script for log file
		$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
		Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log
		
	}
	#Starting LANDesk Inventory Scan
	#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /N /W=$InventoryRunTime
	
	#Removing IP address text files and DNS names of PC's
	Remove-Item $LiveIP -Force
	Remove-Item $LiveNames -Force
	#cleaning house
	Remove-PSDrive -Name HKLM -Force
	Remove-Item $XMLhash -Force
	Remove-Item $results -Force
	Remove-Item $TeslaElectronversion -Force
	
	##############################################################
	#  New As of 4/25/2021 Adding protection LiveUpdate Task     #
	##############################################################
	Write-Output "New LiveUpdate Check Added 4/25/2021 - Checking LiveUpdate Task Now" | Out-File -Append -FilePath $Log
	
	if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
	{
		$currenttaskdate = (Get-Date).ToString("M/dd/yyyy")
		$previoustaskdate = ((Get-Date).AddDays(-1).ToString("M/dd/yyyy"))
		$3daypreviousstaskdate = ((Get-Date).AddDays(-2).ToString("M/dd/yyyy"))
		$4daypreviousstaskdate = ((Get-Date).AddDays(-3).ToString("M/dd/yyyy"))
		$5daypreviousstaskdate = ((Get-Date).AddDays(-4).ToString("M/dd/yyyy"))
		$6daypreviousstaskdate = ((Get-Date).AddDays(-5).ToString("M/dd/yyyy"))
		$7daypreviousstaskdate = ((Get-Date).AddDays(-6).ToString("M/dd/yyyy"))
	}
	Else
	{
		$currenttaskdate = (Get-Date).ToString("M/d/yyyy")
		$previoustaskdate = ((Get-Date).AddDays(-1).ToString("M/d/yyyy"))
		$3daypreviousstaskdate = ((Get-Date).AddDays(-2).ToString("M/d/yyyy"))
		$4daypreviousstaskdate = ((Get-Date).AddDays(-3).ToString("M/d/yyyy"))
		$5daypreviousstaskdate = ((Get-Date).AddDays(-4).ToString("M/d/yyyy"))
		$6daypreviousstaskdate = ((Get-Date).AddDays(-5).ToString("M/d/yyyy"))
		$7daypreviousstaskdate = ((Get-Date).AddDays(-6).ToString("M/d/yyyy"))
	}
	
	if ($windowsshortname -eq "Win7")
	{
		$sched = New-Object -Com "Schedule.Service"
		$sched.Connect()
		$out = @()
		$sched.GetFolder("\Microsoft\Windows\RPOS Auto-Updater").GetTasks(1) | % {
			$xml = [xml]$_.xml
			$out += New-Object psobject -Property @{
				"Name" = $_.Name
				"Status" = switch ($_.State) { 0 { "Unknown" } 1 { "Disabled" } 2 { "Queued" } 3 { "Ready" } 4 { "Running" } }
				"LastRunTime" = $_.LastRunTime
				"LastRunResult" = $_.LastTaskResult
			}
		}
		
		$out | fl Name, LastRunTime, LastRunResult | Out-File "$RPOSAUDIRHOME\LiveUpdateRunStatus.txt"
		[string]$IRanToday = (Get-Content "$RPOSAUDIRHOME\LiveUpdateRunStatus.txt" | findstr "LastRunTime")

	}
	if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
	{
		[string]$IRanToday = (Get-ScheduledTaskInfo -TaskName "\Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate" | Select-Object -ExpandProperty LastRunTime).ToString("M/dd/yyyy")
	}
	
	#Grabbing the previous count value from the LANDesk Registry Folder
	[int]$count = (Get-ItemProperty -Path $LANDeskKeys -Name "AULiveUpdateTaskFailure" -Erroraction SilentlyContinue).AULiveUpdateTaskFailure
	
	#Setting counter to see if the tasks have been running over the past few days
	[int]$datevailidation = "0"
	
	#Checking to see if the tasks have been running in the past 7 days
	if ($IRanToday -match $currenttaskdate)
	{
		$datevailidation += 1
	}
	if (($IRanToday -match $previoustaskdate) -and ($datevailidation -ne "1"))
	{
		$datevailidation += 1
	}
	if (($IRanToday -match $3daypreviousstaskdate) -and ($datevailidation -ne "1"))
	{
		$datevailidation += 1
	}
	if (($IRanToday -match $4daypreviousstaskdate) -and ($datevailidation -ne "1"))
	{
		$datevailidation += 1
	}
	if (($IRanToday -match $5daypreviousstaskdate) -and ($datevailidation -ne "1"))
	{
		$datevailidation += 1
	}
	if (($IRanToday -match $6daypreviousstaskdate) -and ($datevailidation -ne "1"))
	{
		$datevailidation += 1
	}
	if (($IRanToday -match $7daypreviousstaskdate) -and ($datevailidation -ne "1"))
	{
		$datevailidation += 1
	}
	
	if (($datevailidation -eq "0") -or (!$IRanToday))
	{
		Write-Output "LiveUpdate has not ran today adding 1 to $count" | Out-File -Append -FilePath $Log
		
		[int]$count = (Get-ItemProperty -Path $LANDeskKeys -Name "AULiveUpdateTaskFailure").AULiveUpdateTaskFailure
		$count += 1
		New-ItemProperty -Path $LANDeskKeys -Name "AULiveUpdateTaskFailure" -Value "$count" -Type String -Force

	}
	if ($count -ge "8")
	{
		Write-Output "There have been no LiveUpdate Logs in the past 2 days, Deleting old task and creating newer ones." | Out-File -Append -FilePath $Log
		
		schtasks /End /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate"
		schtasks /delete /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate" /F
		if ($TwoDigPCName -eq "01")
		{
			if ($windowsshortname -eq "Win7")
			{
				$script:RPOSAULiveUpdateTime = "2016-05-04T01:00:00"
			}
			if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
			{
				$script:RPOSAULiveUpdateTime = "01:00:00"
			}
		}
		
		if ($TwoDigPCName -eq "02")
		{
			if ($windowsshortname -eq "Win7")
			{
				$script:RPOSAULiveUpdateTime = "2016-05-04T02:00:00"
			}
			if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
			{
				$script:RPOSAULiveUpdateTime = "02:00:00"
			}
		}
		
		if (($TwoDigPCName -ne "01") -and ($TwoDigPCname -ne "02"))
		{
			if ($windowsshortname -eq "Win7")
			{
				$script:RPOSAULiveUpdateTime = "2016-05-04T03:00:00"
			}
			if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
			{
				$script:RPOSAULiveUpdateTime = "03:00:00"
			}
		}
		
		$script:RPOSLiveUpdateXML = "$RPOSAUDIRUPDATE\LiveUpdate.xml"
		
		if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
		{
			Set-RPOSCreateLiveUpdateTaskWin10
		}
		Elseif ($windowsshortname -eq "Win7")
		{
			Set-RPOSCreateLiveUpdateTask
			schtasks /create /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate" /XML "$RPOSLiveUpdateXML"
			Remove-Item "$RPOSLiveUpdateXML" -Force
		}
		
		New-ItemProperty -Path $LANDeskKeys -Name "AULiveUpdateTaskFailure" -Value "0" -Type String -Force
	}
	
	if ($IRanToday -match $currenttaskdate)
	{
		Write-Output "LiveUpdate Task is running as it should be." | Out-File -Append -FilePath $Log
	}
	
	if ($windowsshortname -eq "Win7")
	{
		Remove-Item "$RPOSAUDIRHOME\LiveUpdateRunStatus.txt" -Force
	}	
	
}

#######################################################################################################
#Function RPOS-RemoveAULogs
function Get-RPOSRemoveAULogs {
	Set-RPOSVar
	$Path = "$RPOSAUDIRLOGS"
	$Daysback = "-60"
	
	$CurrentDate = Get-Date
	$DatetoDelete = $CurrentDate.AddDays($Daysback)
	Get-ChildItem $Path | Where-Object { $_.LastWriteTime -lt $DatetoDelete } | Remove-Item
}

#########################################################################################################
#Function RPOS-Rollback
function Get-RPOSRollback
{
	#Setting up varibles from above
	Set-RPOSVar
	
	#ALL VARIABLES THAT ARE IN THIS POWERSHELL SCRIPT CAN BE CHANGED FROM THE FIRST COUPLE ROWS TO REFLECT THE ENTIRE SCRIPT!#
	#Setting Up Varibles
	$date = (get-date).ToString("yyyyMMdd-hhmmss")
	$Log = "$RPOSAUDIRLOGS\RPOS_Rollback_$date.txt"
	$builddir = "$RPOSAUDIRBUILD"
	
	#Fuction Install POS - Default Installer Process for RPOS
	$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
	$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
	$script:drfilebak = Get-ChildItem -path "$RPOSAUDIRBAK" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
	$script:dffilebak = Get-ChildItem -path "$RPOSAUDIRBAK" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
	
	function InstallPOS
	{
		#Removing old RPOS and FlowModule files
		if (($drfile -eq $null) -and ($dffile -eq $null))
		{
			Write-Output "There are no files found in the root directory of $RPOSAUDIRHOME not deleting anything" | Out-File -Append -FilePath $Log
		}
		if (($drfile -eq "RPOS.exe") -or ($dffile -eq "FlowModule.exe") -or ($drfile -eq "RPOS.air") -or ($dffile -eq "FlowModule.air"))
		{
			Write-Output "Deleting RPOS and FlowModule from $RPOSAUDIRHOME" | Out-File -Append -FilePath $Log
			Remove-Item "$RPOSAUDIRHOME\$drfile" -Force
			Remove-Item "$RPOSAUDIRHOME\$dffile" -Force
			Remove-Item "$RPOSAUDIRBUILD\*" -Recurse -Force
		}
		
		#Copying old RPOS Build to install directory and back to the Build directory
		if ($drfilebak -eq "RPOS.bakair")
		{
			Copy-Item -Path $RPOSfileBakair -Destination $RPOSfileair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "RPOS.bakair has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $flowfileBakair -Destination $flowfileair -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "FlowModule.bakair has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $RPOSfileair -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
			Copy-Item -Path $flowfileair -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
		}
		
		if ($drfilebak -eq "RPOS.bakexe")
		{
			Copy-Item -Path $RPOSfileBakexe -Destination $RPOSfileexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "RPOS.bakexe has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $flowfileBakexe -Destination $flowfileexe -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -FilePath $Log
			Write-Output "FlowModule.bakexe has been copied over to install directory" | Out-File -Append -FilePath $Log
			Copy-Item -Path $RPOSfileexe -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
			Copy-Item -Path $flowfileexe -Destination $builddir -PassThru -ErrorAction SilentlyContinue -Force | Out-File -Append -Filepath $Log
		}
		
		#Downloading Adobe Arh file if needed
		if (Test-Path $arhfile) { Write-Output "Adobe AIR Arh file is in directory no download needed" | Out-File -Append -FilePath $Log }
		Else
		{
			$arh = "http://rpos.tbccorp.com/arh.exe"
			Invoke-WebRequest -Uri $arh -OutFile $arhfile
			Write-Output "Arh.exe has been downloaded" | Out-File -Append -FilePath $Log
		}
		
		#Killing RPOS and Flow if running
		Write-Output "Was RPOS running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname rpos* | Out-File -Append -Filepath $Log
		Write-Output "Was Flow running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname FlowModule* | Out-File -Append -Filepath $Log
		
		#Uninstalling RPOS
		Write-Output "Starting Uninstall of RPOS" | Out-File -Append -FilePath $Log
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.RPOS" -wait -NoNewWindow -PassThru
		$p.HasExited
		$sur = $p.ExitCode
		Write-Output "RPOS silent uninstall Exit Code $sur" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		#Uninstalling Flow
		Write-Output "Starting Uninstall of FlowModule" | Out-File -Append -FilePath $Log
		$p = Start-Process $arhfile -ArgumentList "-uninstallAppSilent com.tbccorp.rpos.FlowModule" -wait -NoNewWindow -PassThru
		$p.HasExited
		$suf = $p.ExitCode
		Write-Output "FlowModule silent uninstall Exit Code $suf" | Out-File -Append -Filepath $Log
		
		Start-Sleep -s 15
		
		if ($drfilebak -eq "RPOS.bakair")
		{
			#Installing RPOS AIR
			Write-Output "Starting install of RPOS.air" | Out-File -Append -FilePath $Log
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent -desktopShortcut $RPOSfileair" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "RPOS.air silent install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		if ($drfilebak -eq "RPOS.bakexe")
		{
			#Installing RPOS EXE
			Write-Output "Starting install of RPOS.exe" | Out-File -Append -FilePath $Log
			$p = Start-Process $RPOSfileexe -ArgumentList "-silent -desktopShortcut" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sir = $p.ExitCode
			Write-Output "RPOS.exe silent install Exit Code $sir" | Out-File -Append -Filepath $Log
		}
		
		Start-Sleep -s 15
		
		if ($dffilebak -eq "FlowModule.bakair")
		{
			#Installing Flow AIR
			Write-Output "Starting install of FlowModule.air" | Out-File -Append -FilePath $Log
			$p = Start-Process $arhfile -ArgumentList "-installAppSilent $flowfileair" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "FlowModule.air silent install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		if ($dffilebak -eq "FlowModule.bakexe")
		{
			#Installing Flow EXE
			Write-Output "Starting install of FlowModule.exe" | Out-File -Append -FilePath $Log
			$p = Start-Process $flowfileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru
			$p.HasExited
			$script:sif = $p.ExitCode
			Write-Output "FlowModule.exe silent install Exit Code $sif" | Out-File -Append -Filepath $Log
		}
		
		#Starting LANDesk Inventory Scan
		#& "C:\Program Files (x86)\LANDesk\LDClient\LDISCN32.EXE" /V /F
	}
	
	function InstallTesla
	{
		Write-Output "Taking the orginal Tesla file and removing it out of the default directory" | Out-File -Append -FilePath $Log
		Remove-Item $teslafileexe -Force
		Write-Output "Tesla EXE has been removed from C:\Windows\Scripts\RPOS\" | Out-File -Append -FilePath $Log
		Copy-Item $teslafileBakexe -Destination $teslafileexe -Force
		Write-Output "Tesla Backup file has been moved to the default install directory" | Out-File -Append -FilePath $Log
		
		Write-Output "Closing Tesla to start the removing old Tesla Installer" | Out-File -Append -FilePath $Log
		Write-Output "Was Electron running?" | Out-File -Append -FilePath $Log
		Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
		Write-Output "Was Proton Running?" | Out-File -Append -FilePath $Log
		Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
		
		#Start uninstalling and installing tesla
		Remove-Item "$env:HOMEDRIVE\Users\Public\AppData\Local\tesla-electron" -Recurse -Force
		Write-Output "Tesla has been removed from Public AppData folder" | Out-File -Append -Filepath $Log
		Remove-Item "$env:HOMEDRIVE\Users\$env:USERNAME\AppData\Local\tesla-electron" -Recurse -Force
		
		#Starting to install tesla
		Start-Process $teslafileexe -ArgumentList "-silent" -wait -NoNewWindow -PassThru | Out-File -Append -FilePath $Log
		Write-Output "Tesla has completed installing" | Out-File -Append -Filepath $Log
		
		Stop-Process -force -passthru -processname tesla-electron* | Out-File -Append -Filepath $Log
		Stop-Process -Force -PassThru -ProcessName proton* | Out-File -Append -Filepath $Log
		
		Move-Item "$env:HOMEDRIVE\Users\$env:USERNAME\AppData\Local\tesla-electron" -Destination $PublicAppDataLocalFolder -PassThru -Force | Out-File -Append -FilePath $Log
		Write-Output "Tesla has been moved over to the user public folder for everyone to use" | Out-File -Append -FilePath $Log
		
	}
	
	#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d" | Out-File -Append -FilePath $Log
	
	#setting up registry search
	Set-Location Registry::\HKEY_LOCAL_MACHINE
	New-PSDrive HKLM Registry HKEY_LOCAL_MACHINE
	Set-Location HKLM:
	
	#Grabbing version from RPOS and FLOW Registry
	$rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	
	#Starting RPOS and Flow Module Install
	Write-Output "Starting Roll Back UI Install" | Out-File -Append -FilePath $Log
	InstallPOS
	
	#Starting Tesla Rollback Install
	Write-Output "Starting Roll Back Of Tesla Install" | Out-File -Append -FilePath $Log
	InstallTesla
	
	#Checking for successful install
	Write-Output "Checking to see if Install was successful" | Out-File -Append -FilePath $Log
	$verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
	$verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
	$CheckingTeslaDirectory = Test-path "C:\Users\Public\AppData\Local\tesla-electron"
	$CheckingTeslaElectronexe = Test-Path "C:\Users\Public\AppData\Local\tesla-electron\$teslafolderver\tesla-electron.exe"
	
	
	If (($verRPOS -eq $null) -or ($verFLOW -eq $null) -or ($sif -ne "0") -or ($sir -ne "0") -or ($CheckingTeslaDirectory -eq $false) -or ($CheckingTeslaElectronexe -eq $false))
	{
		Write-Output "Found Install was not successful" | Out-File -Append -FilePath $Log
		Write-Output "After install RPOS and Flow Status" | Out-File -Append -FilePath $Log
		Write-Output "$verRPOS is what the check pulled for RPOS" | Out-File -Append -FilePath $Log
		Write-Output "$verFLOW is what the check pulled for FlowModule" | Out-File -Append -FilePath $Log
		Write-Output "Running installer again for Flow and RPOS" | Out-File -Append -FilePath $Log
		Remove-Item $flowfileair -ErrorAction SilentlyContinue -Force
		Remove-Item $flowfileexe -ErrorAction SilentlyContinue -Force
		Remove-Item $RPOSfileair -ErrorAction SilentlyContinue -Force
		Remove-Item $RPOSfileexe -ErrorAction SilentlyContinue -Force
		InstallPOS
		InstallTesla
	}
	Else
	{
		Write-Output "RPOS and FlowModule Installed correctly exiting script" | Out-File -Append -FilePath $Log
	}
	
	#Checking the version of RPOS on PC and importing them into the registry
	Set-RPOSModuleFindVersion
	
	#Checking Reporting XML
	Set-RPOSModuleCheckReportXML
	
	#Checking the RPOS Icons on the desktop
	Set-RPOSModuleCheckRPOSIcon
	
	#Getting time of end of script for log file
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Finished on $d" | Out-File -Append -FilePath $Log
	
	#cleaning house
	Remove-PSDrive -Name HKLM -Force
	cd C:
	
}

#########################################################################################################
<#function RPOS-UpdateAU
# MIGHT NEED TO REMOVE CODE OLDER RPOS AUTO-UPDATER V3.6
function Get-RPOSUpdateAU
{
	#Setting up varibles from above
	Set-RPOSVar
	
	$pcname = $env:COMPUTERNAME
	$pcname = $pcname -replace 'S[0-9][0-9][0-9]'
	$date = (get-date).ToString("yyyyMMdd-hhmmss")
	$Log = "$RPOSAUDIRLOGS\UpdateCheck_$date.txt"
	$install = "$RPOSAUDIRUPDATE\setup.exe"
	$regpath = "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields"
	New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE
	$ver = (Get-ItemProperty -Path $regpath -Name RPOSAUVer).RPOSAUVer
	$dateinstall = (Get-Date).ToString("MM/dd/yyyy")
	
	#Function download update
	function redownload
	{
		Write-Output "Downloading update for RPOS AU" | Out-File -Append -FilePath $Log
		Write-Output "Downloading File $setup from updater server" | Out-File -Append -FilePath $Log
		Invoke-WebRequest -Uri "$setup" -OutFile $install
	}
	
	if (($env:COMPUTERNAME -eq $ppc1) -or ($env:COMPUTERNAME -eq $ppc2) -or ($env:COMPUTERNAME -eq $ppc3) -or ($env:COMPUTERNAME -eq $ppc4) -or ($env:COMPUTERNAME -eq $ppc5) -or ($env:COMPUTERNAME -eq $ppc6) -or ($env:COMPUTERNAME -eq $ppc7) -or ($env:COMPUTERNAME -eq $ppc8) -or ($env:COMPUTERNAME -eq $ppc9) -or ($env:COMPUTERNAME -eq $ppc10) -or ($env:COMPUTERNAME -eq $ppc11) -or ($env:COMPUTERNAME -eq $ppc12) -or ($env:COMPUTERNAME -eq $ppc13) -or ($env:COMPUTERNAME -eq $ppc14) -or ($env:COMPUTERNAME -eq $ppc15) -or ($env:COMPUTERNAME -eq $ppc16) -or ($env:COMPUTERNAME -eq $ppc17) -or ($env:COMPUTERNAME -eq $ppc18) -or ($env:COMPUTERNAME -eq $ppc19) -or ($env:COMPUTERNAME -eq $ppc20))
	{
		#Getting Update.xml from rpos.tbccorp.com to check status
		Write-Output "AutoUpdater for RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
		Write-Output "Downloading Update.xml to check for new install or not" | Out-File -Append -FilePath $Log
		$xml = "$RPOSAUDIRUPDATE\updater.xml"
		$URL = "http://rpos.tbccorp.com/rposautoupdater/updatefiles/updater.xml"
		Invoke-WebRequest -Uri "$URL" -OutFile $xml
		
		#Getting update status TRUE or FALSE
		Write-Output "Starting to read xml files to see if there is an update" | Out-File -Append -FilePath $Log
		[xml]$xmlread = Get-Content $xml
		$x = $xmlread.update.pilot.statement
		$y = $xmlread.update.pilot.version
		$setup = $xmlread.update.pilot.setup
		$hash = $xmlread.update.pilot.hash
		
		Write-Output "$x is the statment of what was pulled from updater.exe" | Out-File -Append -FilePath $Log
		Write-Output "$y is the update version that it should be running" | Out-File -Append -FilePath $Log
		Write-Output "$ver is the update verison that is running on the PC" | Out-File -Append -FilePath $Log
		Write-Output "$hash is the hash that has to match the server for PCs to download" | Out-File -Append -FilePath $Log
	}
	Else
	{
		
		#Getting Update.xml from rpos.tbccorp.com to check status
		Write-Output "AutoUpdater for RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
		Write-Output "Downloading Update.xml to check for new install or not" | Out-File -Append -FilePath $Log
		$xml = "$RPOSAUDIRUPDATE\updater.xml"
		$URL = "http://rpos.tbccorp.com/rposautoupdater/updatefiles/updater.xml"
		Invoke-WebRequest -Uri "$URL" -OutFile $xml
		
		#Getting update status TRUE or FALSE
		Write-Output "Starting to read xml files to see if there is an update" | Out-File -Append -FilePath $Log
		[xml]$xmlread = Get-Content $xml
		$x = $xmlread.update.retail.statement
		$y = $xmlread.update.retail.version
		$setup = $xmlread.update.retail.setup
		$hash = $xmlread.update.retail.hash
		
		Write-Output "$x is the statment of what was pulled from updater.exe" | Out-File -Append -FilePath $Log
		Write-Output "$y is the update version that it should be running" | Out-File -Append -FilePath $Log
		Write-Output "$ver is the update verison that is running on the PC" | Out-File -Append -FilePath $Log
		Write-Output "$hash is the hash that has to match the server for PCs to download" | Out-File -Append -FilePath $Log
	}
	
	if (($x -eq "true") -and ($y -ne $ver))
	{
		Write-Output "Updater has found that a new version needs to be updated starting download" | Out-File -Append -FilePath $Log
		#Starting download and hash checks for PC1
		
		Write-Output "Starting to download newer copy of RPOS Auto" | Out-File -Append -FilePath $Log
		redownload
		$pchash = Get-FileHash -Path $install -Algorithm MD5 | select -ExpandProperty hash
		Write-Output "$pchash is the file hash that was just downloaded from update server" | Out-File -Append -FilePath $Log
		if ($pchash -eq $hash)
		{
			Write-Output "PC hash equals download has download was successful!" | Out-File -Append -FilePath $Log
			Write-Output "Starting install from update file........." | Out-File -Append -FilePath $Log
			Start-Process $install
			Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value $dateinstall -Force
		}
		Else
		{
			Write-Output "Hash of the download file was not correct attempting to re-download" | Out-File -Append -FilePath $Log
			Start-Sleep -Seconds 60
			redownload
			$pchash = Get-FileHash -Path $install -Algorithm MD5 | select -ExpandProperty hash
			Write-Output "$pchash is the file hash that was just re-downloaded from update server" | Out-File -Append -FilePath $Log
			if ($pchash -eq $hash)
			{
				Write-Output "PC hash equals download has download was successful!" | Out-File -Append -FilePath $Log
				Write-Output "Starting install from update file........." | Out-File -Append -FilePath $Log
				Start-Process $install
				Set-ItemProperty -path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Value $dateinstall -Force
			}
			Else
			{
				Write-Output "Redownload was not successful twice, updater will wait until tomorrow" | Out-File -Append -FilePath $Log
			}
		}
	}
	Else
	{
		Write-Output "Updater has not found an update at this time exiting updater" | Out-File -Append -FilePath $Log
	}
	
	#################################################################
	# Added 5/22/2018 for Tesla Supported Install                   #
	#################################################################
	# Clearing Tesla Database during check of RPOS AU Update
	Set-RPOSModuleTeslaClearDatabase
	
	##################################################################
	# Added 8/2/2019 for Endpoint Customization                      #
	##################################################################
	# Checking to make sure endpoints are correct         
	Check-RPOSEndpoints
	
	#Checking RPOS LiveUpdate to make sure that the install is broken
	Start-Process "Powershell.exe" -ArgumentList "Get-RPOSLiveUpdate"
	
	Remove-PSDrive -Name HKLM -Force

}#>

################################################################################################################
#function Get-RPOSStatusInstall
function Get-RPOSStatusInstall
{
	Set-RPOSVar
	
	function BuildStatusBar
	{
		#Starting the base template for the newer user gui status.
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		$timer = New-Object System.Windows.Forms.Timer
		
		# Create the form. START
		[xml]$script:XAML = @"
    <Window WindowStartupLocation="CenterScreen"
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"

        Title="RPOS Install Progress" Height="250" Width="709" Topmost="False" ResizeMode="NoResize" Name="WindowMain" Icon="$RPOSAUDIRIMAGES\icon32.png">
    <Grid Background="Black">
        <Rectangle HorizontalAlignment="Left" Height="176" Margin="10,21,0,0" Stroke="Black" VerticalAlignment="Top" Width="674" Fill="#FFCFC6C6"/>
        <Image x:Name="image1" HorizontalAlignment="Left" Height="91" Margin="17,101,0,0" VerticalAlignment="Top" Width="661" Source="$RPOSAUDIRIMAGES\Road.jpg" />
        <Image x:Name="image" HorizontalAlignment="Left" Height="55" Margin="20,137,0,0" VerticalAlignment="Top" Width="50" RenderTransformOrigin="0.74,0.783" OpacityMask="White" Source="$RPOSAUDIRIMAGES\SportCar.png"/>
        <Rectangle HorizontalAlignment="Left" Height="31" Margin="11,13,0,0" Stroke="White" VerticalAlignment="Top" Width="672">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF817979" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="22" Margin="19,19,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="426" Foreground="White" FontSize="11"><Run FontWeight="Bold" Text="RPOS Install Progress"/></TextBlock>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="26" Margin="19,73,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="652" FontFamily="Open Sans" FontSize="14"><Run Text="Please Wait For The Installer To Start"/></TextBlock>
        <TextBlock x:Name="textBlock2" HorizontalAlignment="Left" Height="21" Margin="21,49,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="90" FontSize="16"><Run FontWeight="Bold" Text="0%"/></TextBlock>
    </Grid>
</Window>
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$script:Form = [Windows.Markup.XamlReader]::Load($reader)
	}
	
	#################################################START FUNCTION INSTALLCHECK###########################################
	

	#Checking to see if Scheduled Task Has started yet
	while ($checkstart -lt "10000000")
	{
		BuildStatusBar
		
		$checkstart += 1
		$checkinternetreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet")."AUCheckInternet"
		if ($checkinternetreg -eq "0")
		{
			
			if ($StartStatusGUIPart1a -ne "1")
			{
				$StartStatusGUIPart1a = 1
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				
				$image.Margin = "20,137,0,0"
				$textBlock2.Text = "0%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Please Wait For The Installer To Start"
				$Form.Show()
				
				do
				{
					$checkinternetreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet")."AUCheckInternet"
				}
				Until (($checkinternetreg -eq "1") -or ($checkinternetreg -eq "2"))
				
				$Form.Close()
			}
			
			$checkinternetreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet")."AUCheckInternet"
			#Start-Sleep -Seconds 3
		}
		if ($checkinternetreg -eq "1")
		{
			Break
			#Start-Sleep -Seconds 3
		}
		if ($checkinternetreg -eq "2")
		{
			
			#AutoFind all controls
			$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
				New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
			}
			
			if ($StartStatusGUIPart1b -ne "1")
			{
				$StartStatusGUIPart1b = 1
				
				$image.Margin = "625,137,0,0"
				$textBlock2.Text = "100%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "WARNING!!! There is no internet connection acquired. Please check cable/adapter and try again!"
				$Form.Show()
				
				Start-Sleep -Seconds 20
				$Form.Close()
			}
			
			$checkinternetreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet")."AUCheckInternet"
		}
		
	}
	
	
	function InstallCheck
	{
		$StartStatusGUIPart5a1 = $null
		$StartStatusGUIPart5a = $null
		$StartStatusGUIPart5b = $null
		$StartStatusGUIPart6a = $null
		$StartStatusGUIPart6b = $null
		$StartStatusGUIPart6c = $null
		$StartStatusGUIPart7a = $null
		$StartStatusGUIPart7b = $null
		$StartStatusGUIPart8a = $null
		$StartStatusGUIPart8b = $null
		$StartStatusGUIPart9a = $null
		$StartStatusGUIPart9b = $null
		
		#Checking for Copying Files over from Local PC or Internet and self elected subnet sharing
		while ($checkselfelectdownload -lt "10000000")
		{
			$checkselfelectdownload += 1
			$checkselfelectdownloadreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckSelfSubDownload")."AUCheckSelfSubDownload"
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Copying Setup Files" -PercentComplete 40
			
			if ($StartStatusGUIPart5a1 -ne "1")
			{
				$StartStatusGUIPart5a1 = 1
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				
				$image.Margin = "250,137,0,0"
				$textBlock2.Text = "40%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Copying Setup Files"
				$Form.Show()
				
				Start-Sleep -Seconds 2
				
				$Form.Close()
			}
			
			if ($checkselfelectdownloadreg -eq "0")
			{	
				if ($StartStatusGUIPart5a -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart5a = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "281,137,0,0"
					$textBlock2.Text = "45%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Scanning Local PCs And/Or Download RPOS Setup Files."
					$Form.Show()
					
					do
					{
						$checkselfelectdownloadreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckSelfSubDownload")."AUCheckSelfSubDownload"
						Start-Sleep -Seconds 1
					}
					Until ($checkselfelectdownloadreg -eq "1")
					
					$Form.Close()
				}
				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Scanning Local PCs And/Or Download RPOS Setup Files." -PercentComplete 45
				Start-Sleep -Seconds 1
			}
			if ($checkselfelectdownloadreg -eq "1")
			{
				if ($StartStatusGUIPart5b -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart5b = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "312,137,0,0"
					$textBlock2.Text = "50%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Setup Files Have Been Copied Over And/Or Download Ready For Install."
					$Form.Show()
					
					do
					{
						$checkselfelectdownloadreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckSelfSubDownload")."AUCheckSelfSubDownload"
						Start-Sleep -Seconds 1
					}
					Until ($checkselfelectdownloadreg -eq "1")
					
					$Form.Close()
				}

				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Setup Files Have Been Copied Over And/Or Download Ready For Install." -PercentComplete 50
				#Start-Sleep -Seconds 1
				break
			}
			
		}
		
		#Checking any open RPOS and FlowModule Windows that may be open
		while ($checkopenwindows -lt "10000000")
		{
			$checkopenwindows += 1
			$checkopenwindowsreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUClosingWindows")."AUClosingWindows"
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking Open Sessions" -PercentComplete 52
			
			if ($StartStatusGUIPart6a -ne "1")
			{
				$StartStatusGUIPart6a = 1
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				
				$image.Margin = "325,137,0,0"
				$textBlock2.Text = "52%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Copying Setup Files"
				$Form.Show()
				
				Start-Sleep -Seconds 2
				
				$Form.Close()
			}
			
			if ($checkopenwindowsreg -eq "0")
			{
				if ($StartStatusGUIPart6b -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart6b = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "343,137,0,0"
					$textBlock2.Text = "55%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Waiting To Close Active Sessions."
					$Form.Show()
					
					do
					{
						$checkopenwindowsreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUClosingWindows")."AUClosingWindows"
						Start-Sleep -Seconds 1
					}
					Until ($checkopenwindowsreg -eq "1")
					
					$Form.Close()
				}
				
				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Waiting To Close Active Sessions." -PercentComplete 55

			}
			if ($checkopenwindowsreg -eq "1")
			{
				if ($StartStatusGUIPart6c -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart6c = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "375,137,0,0"
					$textBlock2.Text = "60%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "RPOS And FlowModule Have Been Closed."
					$Form.Show()
					
					do
					{
						$checkopenwindowsreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUClosingWindows")."AUClosingWindows"
						Start-Sleep -Seconds 1
					}
					Until ($checkopenwindowsreg -eq "1")
					
					$Form.Close()
				}

				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "RPOS And FlowModule Have Been Closed." -PercentComplete 60
				#Start-Sleep -Seconds 1
				break
			}
			
		}
		
		#Showing Uninstall Process of RPOS and FlowModule
		while ($checkuninstallrpos -lt "10000000")
		{
			$checkuninstallrpos += 1
			$checkuninstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUUninstallRPOS")."AUUninstallRPOS"
			
			if ($checkuninstallrposreg -eq "0")
			{
				if ($StartStatusGUIPart7a -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart7a = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "406,137,0,0"
					$textBlock2.Text = "65%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Uninstalling RPOS"
					$Form.Show()
					
					do
					{
						$checkuninstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUUninstallRPOS")."AUUninstallRPOS"
						Start-Sleep -Seconds 1
					}
					Until ($checkuninstallrposreg -eq "1")
					
					$Form.Close()
				}
				

				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Uninstalling RPOS" -PercentComplete 65
				#Start-Sleep -Seconds 1
			}
			if ($checkuninstallrposreg -eq "1")
			{
				if ($StartStatusGUIPart7b -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart7b = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "406,137,0,0"
					$textBlock2.Text = "65%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "RPOS Uninstall Complete"
					$Form.Show()
					
					do
					{
						$checkuninstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUUninstallRPOS")."AUUninstallRPOS"
						Start-Sleep -Seconds 1
					}
					Until ($checkuninstallrposreg -eq "1")
					
					$Form.Close()
				}

				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "RPOS Uninstall Complete" -PercentComplete 70
				#Start-Sleep -Seconds 1
				break
			}
		}
		
		#Showing Install Process of RPOS and FlowModule
		while ($checkinstallrpos -lt "10000000")
		{
			$checkinstallrpos += 1
			$checkinstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingRPOS")."AUInstallingRPOS"
			
			if ($checkinstallrposreg -eq "0")
			{
				if ($StartStatusGUIPart8a -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart8a = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "450,137,0,0"
					$textBlock2.Text = "72%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Installing RPOS"
					$Form.Show()
					
					do
					{
						$checkinstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingRPOS")."AUInstallingRPOS"
						Start-Sleep -Seconds 1
					}
					Until ($checkinstallrposreg -eq "1")
					
					$Form.Close()
				}

				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Installing RPOS" -PercentComplete 72
				#Start-Sleep -Seconds 1
			}
			if ($checkinstallrposreg -eq "1")
			{
				if ($StartStatusGUIPart8b -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart8b = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "450,137,0,0"
					$textBlock2.Text = "75%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "RPOS Install Complete"
					$Form.Show()
					
					do
					{
						$checkinstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingRPOS")."AUInstallingRPOS"
						Start-Sleep -Seconds 1
					}
					Until ($checkinstallrposreg -eq "1")
					
					$Form.Close()
				}
				
				
				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "RPOS Install Complete" -PercentComplete 75
				#Start-Sleep -Seconds 1
				break
			}
		}
		
		#Showing Install Process of Tesla
		while ($checkinstalltesla -lt "10000000")
		{
			$checkinstalltesla += 1
			$checkinstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingTesla")."AUInstallingTesla"
			
			if ($checkinstallrposreg -eq "0")
			{
				if ($StartStatusGUIPart9a -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart9a = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "481,137,0,0"
					$textBlock2.Text = "77%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Installing Tesla Middleware"
					$Form.Show()
					
					do
					{
						$checkinstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingTesla")."AUInstallingTesla"
						Start-Sleep -Seconds 1
					}
					Until ($checkinstallrposreg -eq "1")
					
					$Form.Close()
				}
				
				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Installing Tesla Middleware" -PercentComplete 77
				#Start-Sleep -Seconds 1
			}
			if ($checkinstallrposreg -eq "1")
			{
				if ($StartStatusGUIPart9b -ne "1")
				{
					BuildStatusBar
					$StartStatusGUIPart9b = 1
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "500,137,0,0"
					$textBlock2.Text = "80%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Tesla Middleware Install Complete"
					$Form.Show()
					
					do
					{
						$checkinstallrposreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingTesla")."AUInstallingTesla"
						Start-Sleep -Seconds 1
					}
					Until ($checkinstallrposreg -eq "1")
					
					$Form.Close()
				}
				
				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Tesla Middleware Install Complete" -PercentComplete 80
				#Start-Sleep -Seconds 1
				break
			}
		}
		
	}
	
	####################################################END OF INSTALLCHECK#################################################
	
	#Checking to see if the internet connection is found on the PC and showing status
	while ($checkinternet -lt "10000000")
	{
		BuildStatusBar
		
		$checkinternet += 1
		$checkinternetreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet")."AUCheckInternet"
		if ($checkinternetreg -eq "0")
		{
			$StartStatusGUIPart2a
			
			if ($StartStatusGUIPart2a -ne "1")
			{
				$StartStatusGUIPart2a = 1
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				$image.Margin = "31,137,0,0"
				$textBlock2.Text = "5%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Checking Internet Connection"
				$Form.Show()
			}
			$Form.Close()
			
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking Internet Connection" -PercentComplete 5
			Start-Sleep -Seconds 3
		}
		if ($checkinternetreg -eq "1")
		{
			
			
			if ($StartStatusGUIPart2b -ne "1")
			{
				$StartStatusGUIPart2b = 1
				
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				$image.Margin = "62,137,0,0"
				$textBlock2.Text = "10%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Internet Connection Was Found"
				$Form.Show()
			}
			
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Internet Connection Was Found" -PercentComplete 10
			Start-Sleep -Seconds 3
			$Form.Close()
			
			break
		}
		
		if ($checkinternetreg -eq "2")
		{
			
				if ($StartStatusGUIPart2c -ne "1")
				{
					$StartStatusGUIPart2c = 1
					BuildStatusBar
				
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "625,137,0,0"
					$textBlock2.Text = "100%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "WARNING!!! There is no internet connection acquired. Please check cable/adapter and try again!"
					$Form.Show()
				}
				
			
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Internet Connection No Internet Install Exiting" -PercentComplete 1
			Start-Sleep -Seconds 3
			
			$Form.Close()
			
			exit
		}
		
	}
	
	#Checking to see if Adobe AIR is installed and current - Showing Status on PowerShell
	while ($checkadobeair -lt "10000000")
	{
		BuildStatusBar
		
		$checkadobeair += 1
		$checkadobeairreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckAdobeAIR")."AUCheckAdobeAIR"
		$checkadobeairinstallreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUAdobeAIRNI")."AUAdobeAIRNI"

		if ($checkadobeairreg -eq "0")
		{
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking For Current Adobe AIR" -PercentComplete 15
			
			if ($StartStatusGUIPart3a -ne "1")
			{
				$StartStatusGUIPart3a = 1
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				
				$image.Margin = "93,137,0,0"
				$textBlock2.Text = "15%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Checking For Current Adobe AIR"
				$Form.Show()
				
				do
				{
					Start-Sleep -Seconds 1
					$checkadobeairreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckAdobeAIR")."AUCheckAdobeAIR"
					$checkadobeairinstallreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUAdobeAIRNI")."AUAdobeAIRNI"
					
				}
				Until (($checkadobeairreg -eq "1") -and ($checkadobeairinstallreg -eq "0") -or ($checkadobeairinstallreg -eq "1")) 
				
				$Form.Close()	
			}
			

			if (($checkadobeairreg -eq "0") -and ($checkadobeairinstallreg -eq "1"))
			{
				$StartStatusGUIPart3a = 2
				
				#Starting Adobe AIR Install Checking to See Progress
				while ($checkadobeairinstallstatus -lt "10000000")
				{
					
					BuildStatusBar
					$checkadobeairinstallstatus += 1
					$checkadobeairreginstalling = (Get-ItemProperty -Path $LANDeskKeys -Name "AUAdobeAIRInstalling")."AUAdobeAIRInstalling"
					$checkadobeairinstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AUAdobeAIRNI")."AUAdobeAIRNI"
					
					if ($StartStatusGUIPart3b -ne "1")
					{
						$StartStatusGUIPart3b = 1
						
						#AutoFind all controls
						$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
							New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
						}
						
						
						$image.Margin = "106,137,0,0"
						$textBlock2.Text = "17%"
						$textBlock2.FontWeight = "Bold"
						$textBlock1.Text = "Upgrading/Installing Adobe AIR"
						$Form.Show()
						
						do
						{
							$checkadobeairreginstalling = (Get-ItemProperty -Path $LANDeskKeys -Name "AUAdobeAIRInstalling")."AUAdobeAIRInstalling"
							$checkadobeairinstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AUAdobeAIRNI")."AUAdobeAIRNI"
							Start-Sleep -Seconds 1
						}
						Until (($checkadobeairinstall -eq "1") -and ($checkadobeairreginstalling -eq "1"))
						
						$Form.Close()
					}
					
					#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Upgrading/Installing Adobe AIR" -PercentComplete 17
					
					if (($checkadobeairinstall -eq "1") -and ($checkadobeairreginstalling -eq "1"))
					{
						$StartStatusGUIPart3b = 2
						
						
						if ($StartStatusGUIPart3c -ne "1")
						{
							$StartStatusGUIPart3c = 1
							BuildStatusBar
							
							#AutoFind all controls
							$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
								New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
							}
							
							
							$image.Margin = "118,137,0,0"
							$textBlock2.Text = "19%"
							$textBlock2.FontWeight = "Bold"
							$textBlock1.Text = "Upgrade/Install Adobe AIR Complete"
							$Form.Show()
						}

						#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Upgrade/Install Adobe AIR Complete" -PercentComplete 19
						
						Start-Sleep -Seconds 1
						$Form.Close()
						Break
					}
				}
			}
			
		}
		if ($checkadobeairreg -eq "1")
		{
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Adobe AIR Is Installed And Current" -PercentComplete 20
			
			if ($StartStatusGUIPart3d -ne "1")
			{
				$StartStatusGUIPart3d = 1
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				
				$image.Margin = "125,137,0,0"
				$textBlock2.Text = "20%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Adobe AIR Is Installed And Current"
				$Form.Show()
			}
			
			
			Start-Sleep -Seconds 1
			$Form.Close()
			Break
		}
	}
	
	
	#If PC is a corporate user or Laptop user please bypass added 6-01-2018 change 19967
	if ($RPOSAgentname -eq "Store")
	{ 
	
	#Checking to see all settings varibles are going into the Update
	while ($checkbginfoemv -lt "10000000")
	{
		$checkbginfoemv += 1
		$checkbginfoemvreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckBGInfoEMV")."AUCheckBGInfoEMV"
		#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Gathering Important Data Before Install" -PercentComplete 22
		
		if ($checkbginfoemvreg -eq "0")
		{
				if ($StartStatusGUIPart4a -ne "1")
				{
					$StartStatusGUIPart4a = 1
					BuildStatusBar
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "156,137,0,0"
					$textBlock2.Text = "25%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Checking For Information Before Installing"
					$Form.Show()
					
					do
					{
						$checkbginfoemvreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckBGInfoEMV")."AUCheckBGInfoEMV"
						Start-Sleep -Seconds 1
					}
					Until ($checkbginfoemvreg -eq "1")
				}
				
				
				Start-Sleep -Seconds 1
				$Form.Close()

			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking For Information Before Installing" -PercentComplete 25
			#Start-Sleep -Seconds 1
		}
		if ($checkbginfoemvreg -eq "1")
		{
				if ($StartStatusGUIPart4b -ne "1")
				{
					$StartStatusGUIPart4b = 1
					BuildStatusBar
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					
					$image.Margin = "187,137,0,0"
					$textBlock2.Text = "30%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Information Has Been Gathered Moving To Install."
					$Form.Show()
					
					do
					{
						$checkbginfoemvreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckBGInfoEMV")."AUCheckBGInfoEMV"
						Start-Sleep -Seconds 1
					}
					Until ($checkbginfoemvreg -eq "1")
				}
				
				
				Start-Sleep -Seconds 1
				$Form.Close()
				
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Information Has Been Gathered Moving To Install." -PercentComplete 30
			#Start-Sleep -Seconds 2
			break
		}
		
	}
}

	#Calling Install Check Progress Bar
	InstallCheck
	
	#Validating that install was successful or not
	while ($checkinstall -lt "1000")
	{
		$checkinstall += 1
		$script:checkinstallreg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInstall")."AUCheckInstall"
		$script:aufailedlocal = (Get-ItemProperty -Path $LANDeskKeys -Name "AUFailedLocal")."AUFailedLocal"
		
		if ($StartStatusGUIPart9a -ne "1")
		{
			$StartStatusGUIPart9a = 1
			BuildStatusBar
			
			#AutoFind all controls
			$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
				New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
			}
				
			$image.Margin = "512,137,0,0"
			$textBlock2.Text = "82%"
			$textBlock2.FontWeight = "Bold"
			$textBlock1.Text = "Checking For Successful Install"
			$Form.Show()
			
			Start-Sleep -Milliseconds 1500
			$Form.Close()
		}
		
		
		
		#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Checking For Successful Install" -PercentComplete 82
		
		if ($checkinstallreg -eq "0")
		{
			if ($StartStatusGUIPart10a -ne "1")
			{
				$StartStatusGUIPart10a = 1
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				$image.Margin = "531,137,0,0"
				$textBlock2.Text = "85%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Validating Install"
				$Form.Show()
				
				Start-Sleep -Seconds 1
				$Form.Close()
			}

			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Validating Install" -PercentComplete 85
			#Start-Sleep -Seconds 1
		}
		
		
		#If Validation is not SUCCESSFUL 	
		if ($aufailedlocal -eq "1")
		{
			while ($aufailedcount -lt "10000000")
			{
				$aufailedcount += 1
				$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
				
				if ($StartStatusGUIPart10b -ne "1")
				{
					$StartStatusGUIPart10b = 1
					BuildStatusBar
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
					}
					
					$image.Margin = "387,137,0,0"
					$textBlock2.Text = "62%"
					$textBlock2.FontWeight = "Bold"
					$textBlock1.Text = "Validation Failed - Removing Install And Reinstalling."
					$Form.Show()
					
					do
					{
						$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
						Start-Sleep -Seconds 1
						
					} Until ($LocalRPOSReInstall -eq "1") 
					
					Start-Sleep -Seconds 1
					$Form.Close()
				}
				
				#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Validation Failed - Removing Install And Reinstalling." -PercentComplete 62
				
				if ($LocalRPOSReInstall -eq "1")
				{
					$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
					$AUCompleteReinstalReg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall")."AUCompleteReinstall"
					
					if ($StartStatusGUIPart10c -ne "1")
					{
						$StartStatusGUIPart10c = 1
						BuildStatusBar
						
						#AutoFind all controls
						$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
							New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
						}
						
						$image.Margin = "406,137,0,0"
						$textBlock2.Text = "65%"
						$textBlock2.FontWeight = "Bold"
						$textBlock1.Text = "Removing Install Files And Reinstalling."
						$Form.Show()
						
						do
						{
							$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
							$AUCompleteReinstalReg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall")."AUCompleteReinstall"
							Start-Sleep -Milliseconds 500
							
						} Until (($LocalRPOSReInstall -eq "1") -and ($AUCompleteReinstalReg -eq "1"))
						
						#Start-Sleep -Seconds 1
						$Form.Close()
					}
					
					$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
					$AUCompleteReinstalReg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall")."AUCompleteReinstall"
					
					#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Removing Install Files And Reinstalling." -PercentComplete 65
					#Start-Sleep -Seconds 1
					
					#If Local Install Failed 
					<#if (($LocalRPOSReInstall -eq "1") -and ($AUCompleteReinstalReg -eq "1"))
					{
						if ($StartStatusGUIPart10d -ne "1")
						{
							$StartStatusGUIPart10d = 1
							BuildStatusBar
							
							#AutoFind all controls
							$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
								New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
							}
							
							$image.Margin = "218,137,0,0"
							$textBlock2.Text = "35%"
							$textBlock2.FontWeight = "Bold"
							$textBlock1.Text = "Removing Install Files And Reinstalling."
							$Form.Show()
							
							do
							{
								$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
								$AUCompleteReinstalReg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall")."AUCompleteReinstall"
								Start-Sleep -Seconds 1
								
							} Until (($LocalRPOSReInstall -eq "1") -and ($AUCompleteReinstalReg -eq "1"))
							
							Start-Sleep -Seconds 1
							$Form.Close()
						}
												
						<#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Removing Install Files And Reinstalling." -PercentComplete 35
						#Start-Sleep -Seconds 1
						Set-ItemProperty -path $LANDeskKeys -Name "AUCheckSelfSubDownload" -Value "0" -Force
						Set-ItemProperty -path $LANDeskKeys -Name "AUClosingWindows" -Value "0" -Force
						Set-ItemProperty -path $LANDeskKeys -Name "AUUninstallRPOS" -Value "0" -Force
						Set-ItemProperty -path $LANDeskKeys -Name "AUInstallingRPOS" -Value "0" -Force
						Set-ItemProperty -path $LANDeskKeys -Name "AUCheckInstall" -Value "0" -Force
						$StartStatusGUIPart5a1 = $null
						$StartStatusGUIPart5a = $null
						$StartStatusGUIPart5b = $null
						$StartStatusGUIPart6a = $null
						$StartStatusGUIPart6b = $null
						$StartStatusGUIPart6c = $null
						$StartStatusGUIPart7a = $null
						$StartStatusGUIPart7b = $null
						$StartStatusGUIPart8a = $null
						$StartStatusGUIPart8b = $null
						$StartStatusGUIPart9a = $null
						$StartStatusGUIPart9b = $null
						
						#Running Install Check to Run the Installers again.
						#InstallCheck
						
					}#>
					
					if (($LocalRPOSReInstall -eq "1") -and ($AUCompleteReinstalReg -eq "1"))
					{
						$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
						$AUCompleteReinstalReg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall")."AUCompleteReinstall"
						
						if ($StartStatusGUIPart10e -ne "1")
						{
							$StartStatusGUIPart10e = 1
							BuildStatusBar
							
							#AutoFind all controls
							$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
								New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
							}
							
							$image.Margin = "500,137,0,0"
							$textBlock2.Text = "80%"
							$textBlock2.FontWeight = "Bold"
							$textBlock1.Text = "Reinstall Complete"
							$Form.Show()
							
							do
							{
								$LocalRPOSReInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall")."AULocalReinstall"
								$AUCompleteReinstalReg = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall")."AUCompleteReinstall"
								Start-Sleep -Milliseconds 500
								
							} Until (($LocalRPOSReInstall -eq "1") -and ($AUCompleteReinstalReg -eq "1"))
							 
							#Start-Sleep -Seconds 1
							$Form.Close()
						}
						
						#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Reinstall Complete" -PercentComplete 80
						#Start-Sleep -Seconds 1
						Break
					}
					
				}
				
				Break
				
			}
			Break
		}
		
		if ($checkinstallreg -eq "1")
		{
			if ($StartStatusGUIPart10g -ne "1")
			{
				$StartStatusGUIPart10g = 1
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				$image.Margin = "562,137,0,0"
				$textBlock2.Text = "90%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Install Completed"
				$Form.Show()
				

				Start-Sleep -Seconds 1
				$Form.Close()
			}
			

			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Install Completed" -PercentComplete 90
			#Start-Sleep -Seconds 1
			
			
			Break
		}
	}
	
	
	#Gathering Install Data and Sending it off to RPOS.TBCCORP.COM
	while ($checkreporting -lt "10000000")
	{
		$checkreporting += 1
		$checkreportingdatareq = (Get-ItemProperty -Path $LANDeskKeys -Name "AUGatherReportingData")."AUGatherReportingData"
		
		if ($checkreportingdatareq -eq "0")
		{
			if ($StartStatusGUIPart11a -ne "1")
			{
				$StartStatusGUIPart11a = 1
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				$image.Margin = "581,137,0,0"
				$textBlock2.Text = "93%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Gathering Report Data Information"
				$Form.Show()
				
				do
				{
					$checkreportingdatareq = (Get-ItemProperty -Path $LANDeskKeys -Name "AUGatherReportingData")."AUGatherReportingData"
					Start-Sleep -Seconds 1
				} Until (($checkreportingdatareq -eq "0") -or ($checkreportingdatareq -eq "1"))
				
				
				Start-Sleep -Seconds 1
				$Form.Close()
			}
			
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Gathering Report Data Information" -PercentComplete 93
			#Start-Sleep -Seconds 1
			
		}
		if ($checkreportingdatareq -eq "1")
		{
			if ($StartStatusGUIPart11b -ne "1")
			{
				$StartStatusGUIPart11b = 1
				BuildStatusBar
				
				#AutoFind all controls
				$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
					New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
				}
				
				$image.Margin = "593,137,0,0"
				$textBlock2.Text = "95%"
				$textBlock2.FontWeight = "Bold"
				$textBlock1.Text = "Reporting Complete"
				$Form.Show()
				
				do
				{
					$checkreportingdatareq = (Get-ItemProperty -Path $LANDeskKeys -Name "AUGatherReportingData")."AUGatherReportingData"
					Start-Sleep -Seconds 1
				} Until ($checkreportingdatareq -eq "1")
				
				
				Start-Sleep -Seconds 1
				$Form.Close()
			}
			
			#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Reporting Complete" -PercentComplete 95
			#Start-Sleep -Seconds 1
			break
		}
	}
	
	if ($StartStatusGUIPart11c -ne "1")
	{
		$StartStatusGUIPart11c = 1
		BuildStatusBar
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $script:Form.FindName($_.Name) -Force
		}
		
		$image.Margin = "625,137,0,0"
		$textBlock2.Text = "100%"
		$textBlock2.FontWeight = "Bold"
		$textBlock1.Text = "Software Installed"
		$Form.Show()
		
		
		Start-Sleep -Seconds 20
		$Form.Close()
	}
	

	#Write-Progress -Activity "Installing RPOS " -CurrentOperation "Software Installed" -PercentComplete 100
	#Start-Sleep -Seconds 20
	Exit
}

################################################################################################################
#function RPOS-DesktopUpdate
function Get-RPOSDesktopUpdate
{
	#Setting up varibles from above
	Set-RPOSVar
	
	#Setting up scheduled tasks for the user gui
	#Grabbing Current User logged in 
	$username = "${env:USERDOMAIN}\${env:USERNAME}"
	
	#Taking the username and finding the SID for the user
	$AdObj = New-Object System.Security.Principal.NTAccount("$username")
	$strSID = $AdObj.Translate([System.Security.Principal.SecurityIdentifier])
	$script:SID = $strSID.Value
	
	if ($windowsshortname -eq "Win7")
	{
		$userquery = schtasks /query /TN "\RPOS AU Local User GUI $SID"
	}
	elseif (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
	{
		$userquery = Get-ScheduledTask -TaskName "RPOS AU Local User GUI $SID" | Select-Object -ExpandProperty Taskname
	}
	
	if (!$userquery)
	{
		if ($windowsshortname -eq "Win7")
		{
			$script:RPOSCreateScheduledTaskGUIXML = "C:\Temp\CreateRPOSUserScheduledTask.xml"
			Set-RPOSCreateUserScheduledTask
			schtasks /create /TN "\RPOS AU Local User GUI $SID" /XML "$RPOSCreateScheduledTaskGUIXML"
			Remove-Item "$RPOSCreateScheduledTaskGUIXML" -Force
		}
		elseif (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
		{
			#Creating the user scheduled task for Windows 10
			Set-RPOSCreateUserScheduledTaskWin10
		}
	}

	
	[System.Reflection.Assembly]::LoadWithPartialName("System.Windows.Forms")
	
	##########################################################################################################################################
	#Start of Script has begun
	$d = Get-Date -Format "yyyy-MM-dd-hh-mm-ss"
	Write-Output "Script Started at $d"
	Write-Output "RPOS Auto-Updater $ReleaseInfo (c) EOC Department TBC Corp" | Out-File -Append -FilePath $Log
	
	#Finding out if task is enabled or disabled in scheduled task
	$showtaska = schtasks /Query /TN "Update RPOS Local A" | Select-String -Pattern "Disabled"
	$showtaskb = schtasks /Query /TN "Update RPOS Local B" | Select-String -Pattern "Disabled"
	
	if ($showtaska -eq $null) {
	   $task = "A"}
	
	if ($showtaskb -eq $null) {
		$task = "B" }
	
	#setting up registry search
	New-PSDrive -PSProvider Registry -Name HKLM -Root HKEY_LOCAL_MACHINE

	#Checking to see if the desktop installer is running already to not create a duplicate install
	$processcheck = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
	
	if ($processcheck -ne $null)
	{
		#[System.Windows.Forms.MessageBox]::Show("RPOS Auto-Updater is already running!", "Update RPOS")
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="UPDATE RPOS ALREADY RUNNING" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="RPOS Auto-Updater is already running!"/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		$Form.ShowDialog()
	}
	#Checking to see if there is an active internet connection
	If (($networkcheck -eq $null) -or ($IPZero -eq "127.0.0.0") -or ($pingrpos -ne "Success"))
	{
		Write-Output "There is currnetly no internet connection RPOS Auto-Updater will check again for an update another time" | Out-File -Append -FilePath $Log
		[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
		[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
		Add-Type -AssemblyName System.Windows.Forms
		
		# Create the form.
		[xml]$XAML =
		@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="UPDATE RPOS NO INTERNET" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="RPOS Updater has no internet connection. Please check your cables and network adapter and try again!"/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
	<Image x:Name="NoInternet" HorizontalAlignment="Left" Height="60" Margin="29,290,0,0" VerticalAlignment="Top" Width="73" Source="$RPOSAUDIRIMAGES\forbidden.png"/>
    </Grid>
</Window> 
"@
		
		
		#Read the form
		$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
		$Form = [Windows.Markup.XamlReader]::Load($reader)
		
		#AutoFind all controls
		$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
			New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
		}
		
		$button.add_Click({
				
				$Form.Close()
				
			})
		
		$Form.ShowDialog()
	}
	Else
	{
		if ($processcheck -eq $null)
		{
			#Grabbing version from RPOS and FLOW Registry
			$script:rposname = (Get-ItemProperty -Path $keyrpos -Name DisplayName).DisplayName
			$script:verRPOS = (Get-ItemProperty -Path $keyrpos -Name DisplayVersion).DisplayVersion
			$script:verFLOW = (Get-ItemProperty -Path $keyflow -Name DisplayVersion).DisplayVersion
			
			#Grabbing files names in RPOS directory
			$script:drfile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "RPOS" } | Select -ExpandProperty Name
			$script:dffile = Get-ChildItem -path "$RPOSAUDIRHOME" | Where-Object { $_.BaseName -eq "FlowModule" } | Select -ExpandProperty Name
			
			#Checking to see what PC name is assoicated with a Store, TAC, Carroll Tire, Or Corprate Owned
			Set-RPOSModuleWhatPCAmI
			
			#Checking XML update files checksums
			$URL = $checkupdatedXMLs
			$script:xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
			$checkswitchxml = $xmlread.filechecksum.switchxml.$XMLswitchchecksum
			$checkBGInfoxml = $xmlread.filechecksum.BGInfoxml.$XMLbginfochecksum
			
			
			Write-Output "$switchvalue is the switch value."
			Write-Output "$rposname is the version of RPOS running on this PC"
			
			#checking to see if switch file has changed and if it has to switch the RPOS version
			if ($checkswitchxml -ne $RegSwitchXMLchecksum)
			{
				
				Write-Output "There is a different switchxml on RPOS.tbccorp.com checking xml and seeing if there is a new distro change"
				
				#Checking to see if there is a switch in the builds
				$URL = $XMLSwitchURL
				
				#Getting switch build response
				$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				$w = $xmlread.serverswitch.$switchvalue.name
				$x = $xmlread.serverswitch.$switchvalue.statement
				$y = $xmlread.serverswitch.$switchvalue.server
				Write-Output "Switch build revision equals $x" | Out-File -Append -FilePath $Log
				
				#Getting Tesla version information from server
				$URLTeslaElectron = "http://$y/TESLA-ELECTRON.VERSION"
				Invoke-WebRequest -Uri "$URLTeslaElectron" -OutFile $TeslaElectronversionstandard
				$script:TeslaElectronversionWeb = (Get-Content "$TeslaElectronversionstandard").Substring(8)
				
				$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
				
				$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
				
				if ($w -ne $rposname)
				{
					#[System.Windows.Forms.MessageBox]::Show("WARNING!!!`n `nRPOS needs to be updated to $w and Tesla needs to be updated to $TeslaElectronversionWeb. `n `nComplete all work before starting the update process. `n `nRPOS will be closed and unavailable during this update. `n `nAllow 5-10 minutes for the process to complete. `nA message will be displayed when the update is completed. `nClick OK to begin the update process.", "Update RPOS")
					[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
					Add-Type -AssemblyName System.Windows.Forms
					
					# Create the form.
					[xml]$XAML =
					@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="image1" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="image" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg" Margin="0,0,-10,0"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS UPDATE NEEDED" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11"><Run Text="RPOS needs to be updated to the newest version. Please complete all work before starting this update, as RPOS will be unavailable while updating."/><LineBreak/><Run/><LineBreak/><Run Text="A message will appear once the update is complete, please click OK to proceed."/></TextBlock>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
    </Grid>
</Window> 
"@
					
					
					#Read the form
					$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
					$Form = [Windows.Markup.XamlReader]::Load($reader)
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
					}
					
					$button.add_Click({
							
							$Form.Close()
							
							$usersprofile1 = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
							foreach ($userprofile1 in $usersprofile1)
							{
								Remove-Item "C:\Users\$userprofile1\Desktop\RPOS.lnk"
								Remove-Item "C:\Users\$userprofile1\Desktop\FlowModule.lnk"
								
							}
							
							schtasks /Run /TN "Update RPOS Local $task"
							Start-Process "Powershell.exe" -ArgumentList "-ExecutionPolicy Bypass -WindowStyle Hidden -Command Get-RPOSStatusInstall"
							Start-Sleep -Seconds 120
							$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
							
							#$process = Get-Process updaterpos | Select-Object CPU, Name, Starttime
							while ($true)
							{
								if ($process -eq $null)
								{
									$installsuccess1 = (Get-ItemProperty -Path $LANDeskKeys -Name InstallComplete).InstallComplete
									
									if ($installsuccess1 -eq "1")
									{
										#[System.Windows.Forms.MessageBox]::Show("RPOS was successfully updated to the latest version. ", "Update RPOS")
										[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
										Add-Type -AssemblyName System.Windows.Forms
										
										# Create the form.
										[xml]$XAML =
										@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS SUCCESSFUL INSTALL" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="RPOS successfully installed the latest version. You may use RPOS now."/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
	<Image x:Name="successfulimage" HorizontalAlignment="Left" Height="49" Margin="164,284,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\successful.png"/>
    </Grid>
</Window> 
"@
										
										
										#Read the form
										$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
										$Form = [Windows.Markup.XamlReader]::Load($reader)
										
										#AutoFind all controls
										$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
											New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
										}
										
										$button.add_Click({
												
												$Form.Close()
												
											})
										
										$Form.ShowDialog()
										Break
									}
									if ($installsuccess1 -eq "0")
									{
										#[System.Windows.Forms.MessageBox]::Show("RPOS was NOT successfully updated to the latest version. Please try again in a few minutes.", "Update RPOS")
										[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
										Add-Type -AssemblyName System.Windows.Forms
										
										# Create the form.
										[xml]$XAML =
										@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS UNSUCCESSFUL INSTALL" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="RPOS was unsuccessful installing the latest version. It will try to install again at a later time."/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
	<Image x:Name="successfulimage" HorizontalAlignment="Left" Height="49" Margin="164,284,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\failure.png"/>
    </Grid>
</Window> 
"@
										
										
										#Read the form
										$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
										$Form = [Windows.Markup.XamlReader]::Load($reader)
										
										#AutoFind all controls
										$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
											New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
										}
										
										$button.add_Click({
												
												$Form.Close()
												
											})
										
										$Form.ShowDialog()
										Break
									}
								}
								Else
								{
									#Starting sleepto check the scheduled task again and if RPOS and/or FlowModule are running to close it.
									Start-Sleep -Seconds 30
									$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
								}
							}
						})
					
					$Form.ShowDialog()
					
				}

			}
			
			if ($checkswitchxml -eq $RegSwitchXMLchecksum)
			{
				Write-Output "There is no new switch.xml on RPOS.tbccorp.com, bypassing switching distro to checking for newer version of RPOS without switching."
				Write-Output "Powershell has found no distro change needed. Version upgrade check starting"
				
				#Pulling update xml file from server to compare local and server update.xml
				if ($rposname -eq "RPOS - PRODUCTION")
				{
					$URL = "http://$prod/update.xml"
					$server = "http://$prod"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - TRAINING")
				{
					$URL = "http://$train/update.xml"
					$server = "http://$train"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - PILOT")
				{
					$URL = "http://$pilot/update.xml"
					$server = "http://$pilot"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - PILOT2")
				{
					$URL = "http://$pilot2/update.xml"
					$server = "http://$pilot2"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - PILOT3")
				{
					$URL = "http://$pilot3/update.xml"
					$server = "http://$pilot3"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - QA")
				{
					$URL = "http://$qa/update.xml"
					$server = "http://$qa"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq $null)
				{
					$URL = "http://$prod/update.xml"
					$server = "http://$prod"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - QAPILOT")
				{
					$URL = "http://$qapilot/update.xml"
					$server = "http://$qapilot"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - QAPILOT2")
				{
					$URL = "http://$qapilot2/update.xml"
					$server = "http://$qapilot2"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - QAPILOT3")
				{
					$URL = "http://$qapilot3/update.xml"
					$server = "http://$qapilot3"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - ALPHA")
				{
					$URL = "http://$alpha/update.xml"
					$server = "http://$alpha"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - DEV")
				{
					$URL = "http://$dev/update.xml"
					$server = "http://$dev"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - DEVPILOT")
				{
					$URL = "http://$devpilot/update.xml"
					$server = "http://$devpilot"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - DEVPILOT2")
				{
					$URL = "http://$devpilot2/update.xml"
					$server = "http://$devpilot2"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				if ($rposname -eq "RPOS - DEVPILOT3")
				{
					$URL = "http://$devpilot3/update.xml"
					$server = "http://$devpilot3"
					$xmlread = [xml](New-Object System.Net.WebClient).downloadstring($URL)
				}
				
				#Getting version number 
				$x = $xmlread.update.versionNumber
				$y = $xmlread.update.versionLabel
				Write-Output "$x is the version pulled on $server"
				Write-Output "$verRPOS is the version of RPOS pulled on PC"
				Write-Output "$verFLOW is the version of Flow pulled on PC"
				
				#Getting Tesla version information from server
				$URLTeslaElectron = "$server/TESLA-ELECTRON.VERSION"
				Invoke-WebRequest -Uri "$URLTeslaElectron" -OutFile $TeslaElectronversionstandard
				$script:TeslaElectronversionWeb = (Get-Content "$TeslaElectronversionstandard").Substring(8)
				
				$TeslaAppDirectory = Get-ChildItem "C:\Users\Public\AppData\Local\tesla-electron\" | Where-Object { $_.Name -like "app-*" } | Select-Object -ExpandProperty Name
				
				$TeslaElectronversionLocal = (Get-Content "C:\Users\Public\AppData\Local\tesla-electron\$TeslaAppDirectory\resources\TESLA-ELECTRON.VERSION").Substring(8)
				
				#Checking if RPOS and Flow Equal or Not Equal to server
				if (($x -eq $verRPOS) -and ($x -eq $verFLOW) -and ($TeslaElectronversionWeb -eq $TeslaElectronversionLocal))
				{
					[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
					Add-Type -AssemblyName System.Windows.Forms
					
					# Create the form.
					[xml]$XAML =
					@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS UPDATE NOT NEEDED" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="No new version available."/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
    </Grid>
</Window> 
"@
					
					
					#Read the form
					$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
					$Form = [Windows.Markup.XamlReader]::Load($reader)
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
					}
					

					$button.add_Click({
							
						$Form.Close()	
							
						})
					
					$Form.ShowDialog()
				}
				Else
				{
					
					#[System.Windows.Forms.MessageBox]::Show("WARNING!!!`n `nRPOS needs to be updated to the latest version $y and Tesla needs to be updated to $TeslaElectronversionWeb. `n `nComplete all work before starting the update process. `n `nRPOS will be closed and unavailable during this update. `n `nAllow 5-10 minutes for the process to complete. `nA message will be displayed when the update is completed. `nClick OK to begin the update process.", "Update RPOS")
					
					[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
					[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
					Add-Type -AssemblyName System.Windows.Forms
					
					# Create the form.
					[xml]$XAML =
					@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS UPDATE NEEDED" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11"><Run Text="RPOS needs to be updated to the newest version. Please complete all work before starting this update, as RPOS will be unavailable while updating."/><LineBreak/><Run/><LineBreak/><Run Text="A message will appear once the update is complete, please click OK to proceed."/></TextBlock>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
    </Grid>
</Window> 
"@
					
					
					#Read the form
					$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
					$Form = [Windows.Markup.XamlReader]::Load($reader)
					
					#AutoFind all controls
					$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
						New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
					}
					
					
					$button.add_Click({
							
							$Form.Close()
							
							Stop-Process -force -passthru -processname rpos*
							Stop-Process -force -passthru -processname FlowModule*
							
							$usersprofile1 = Get-ChildItem "C:\Users" | Select-Object -ExpandProperty Name
							foreach ($userprofile1 in $usersprofile1)
							{
								Remove-Item "C:\Users\$userprofile1\Desktop\RPOS.lnk"
								Remove-Item "C:\Users\$userprofile1\Desktop\FlowModule.lnk"
								
							}
							
							schtasks /Run /TN "Update RPOS Local $task"
							Start-Process "Powershell.exe" -ArgumentList "-ExecutionPolicy Bypass -WindowStyle Hidden -Command Get-RPOSStatusInstall"
							Start-Sleep -Seconds 120
							$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
							
							#$process = Get-Process updaterpos | Select-Object CPU, Name, Starttime
							while ($true)
							{
								if ($process -eq $null)
								{
									$installsuccess1 = (Get-ItemProperty -Path $LANDeskKeys -Name InstallComplete).InstallComplete
									
									if ($installsuccess1 -eq "1")
									{
										#[System.Windows.Forms.MessageBox]::Show("RPOS was successfully updated to the latest version. ", "Update RPOS")
										[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
										Add-Type -AssemblyName System.Windows.Forms
										
										# Create the form.
										[xml]$XAML =
										@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS SUCCESSFUL INSTALL" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="RPOS successfully installed the latest version. You may use RPOS now."/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
	<Image x:Name="successfulimage" HorizontalAlignment="Left" Height="49" Margin="164,284,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\successful.png"/>
    </Grid>
</Window> 
"@
										
										
										#Read the form
										$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
										$Form = [Windows.Markup.XamlReader]::Load($reader)
										
										#AutoFind all controls
										$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
											New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
										}
										
										$button.add_Click({
												
												$Form.Close()
												
											})
										
										$Form.ShowDialog()
										Break
									}
									if ($installsuccess1 -eq "0")
									{
										#[System.Windows.Forms.MessageBox]::Show("RPOS was NOT successfully updated to the latest version. Please try again in a few minutes.", "Update RPOS")
										[System.Reflection.Assembly]::LoadWithPartialName('System.Windows.Forms') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('presentationframework') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('System.Drawing') | out-null
										[System.Reflection.Assembly]::LoadWithPartialName('WindowsFormsIntegration') | out-null
										Add-Type -AssemblyName System.Windows.Forms
										
										# Create the form.
										[xml]$XAML =
										@"
<Window 
        xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
        xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
        xmlns:d="http://schemas.microsoft.com/expression/blend/2008"
        xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006"
        xmlns:local="clr-namespace:TBCInstantSupport"
        
        Title="Update RPOS" Height="460" Width="400" Topmost="True" ResizeMode="NoResize" Icon="$RPOSAUDIRIMAGES\icon32.png">
    
    <Grid Background="Black">
        <Image x:Name="BottomLogo" HorizontalAlignment="Left" Height="330" Margin="0,382,0,-281" VerticalAlignment="Top" Width="394" Source="$RPOSAUDIRIMAGES\BottomGUI.jpg"/>
        <Image x:Name="TopLogo" HorizontalAlignment="Left" Height="215" VerticalAlignment="Top" Width="404" Margin="0,0,-10,0" Source="$RPOSAUDIRIMAGES\TopLogoGUI.jpg"/>
        <Rectangle Fill="#FF6C6C6C" HorizontalAlignment="Left" Height="167" Margin="10,215,0,0" Stroke="Black" VerticalAlignment="Top" Width="364"/>
        <Rectangle HorizontalAlignment="Left" Height="24" Margin="18,221,0,0" Stroke="Black" VerticalAlignment="Top" Width="346">
            <Rectangle.Fill>
                <LinearGradientBrush EndPoint="0.5,1" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="0"/>
                    <GradientStop Color="#FF7A6464" Offset="1"/>
                </LinearGradientBrush>
            </Rectangle.Fill>
        </Rectangle>
        <TextBlock x:Name="textBlock" HorizontalAlignment="Left" Height="15" Margin="29,224,0,0" TextWrapping="Wrap" Text="RPOS UNSUCCESSFUL INSTALL" VerticalAlignment="Top" Width="331" Foreground="White" FontWeight="Bold" FontSize="11"/>
        <Rectangle Fill="#FFEAD4D4" HorizontalAlignment="Left" Height="131" Margin="18,244,0,0" Stroke="Black" VerticalAlignment="Top" Width="346"/>
        <TextBlock x:Name="textBlock1" HorizontalAlignment="Left" Height="98" Margin="29,252,0,0" TextWrapping="Wrap" VerticalAlignment="Top" Width="331" FontSize="11" Text="RPOS was unsuccessful installing the latest version. It will try to install again at a later time."/>
        <Button x:Name="button" Content="OK" HorizontalAlignment="Left" Height="29" Margin="135,338,0,0" VerticalAlignment="Top" Width="107" Foreground="White" BorderBrush="White">
            <Button.Background>
                <LinearGradientBrush EndPoint="0.5,1" MappingMode="RelativeToBoundingBox" StartPoint="0.5,0">
                    <GradientStop Color="Black" Offset="1"/>
                    <GradientStop Color="#FFE04E4E" Offset="0.503"/>
                    <GradientStop Color="#FFC98F8F" Offset="0.197"/>
                </LinearGradientBrush>
            </Button.Background>
        </Button>
	<Image x:Name="successfulimage" HorizontalAlignment="Left" Height="49" Margin="164,284,0,0" VerticalAlignment="Top" Width="41" Source="$RPOSAUDIRIMAGES\failure.png"/>
    </Grid>
</Window> 
"@
										
										
										#Read the form
										$Reader = (New-Object System.Xml.XmlNodeReader $xaml)
										$Form = [Windows.Markup.XamlReader]::Load($reader)
										
										#AutoFind all controls
										$xaml.SelectNodes("//*[@*[contains(translate(name(.),'n','N'),'Name')]]") | ForEach-Object {
											New-Variable -Name $_.Name -Value $Form.FindName($_.Name) -Force
										}
										
										$button.add_Click({
												
												$Form.Close()
												
											})
										
										$Form.ShowDialog()
										Break
									}
								}
								Else
								{
									#Starting sleepto check the scheduled task again and if RPOS and/or FlowModule are running to close it.
									Start-Sleep -Seconds 30
									$process = schtasks /Query /TN "Update RPOS Local $task" | Select-String -Pattern "Running"
									
								}
								
							}
							
						})
						
					$Form.ShowDialog()

				}
			}
			
			
			#Removing IP address text files and DNS names of PC's
			Remove-Item $LiveIP -Force
			Remove-Item $LiveNames -Force
			#cleaning house
			Remove-PSDrive -Name HKLM -Force
			Remove-Item $XMLhash -Force
			Remove-Item $results -Force
			Remove-Item $TeslaElectronversion -Force
			Remove-Item $TeslaElectronversionstandard -Force
		}
	}
}

#########################################################################################################################################
#Function Get-RPOSAutoUpdaterUninstall
function Get-RPOSAutoUpdaterUninstall
{
	
	function Wait-KeyPress($prompt = 'Press any key to exit!')
	{
		Write-Host $prompt
		
		do
		{
			Start-Sleep -milliseconds 100
		}
		until ($Host.UI.RawUI.KeyAvailable)
		
		$Host.UI.RawUI.FlushInputBuffer()
	}
	
	$CPU = $env:COMPUTERNAME
	
	$title = "Uninstall RPOS Auto-Updater on $CPU"
	$message = "Are you sure that you want uninstall The RPOS Auto-Updater from $CPU"
	
	$yes = New-Object System.Management.Automation.Host.ChoiceDescription "&Yes", `
					  "Uninstalls RPOS Auto-Updater from $CPU."
	
	$no = New-Object System.Management.Automation.Host.ChoiceDescription "&No", `
					 "Will Exit the Uninstall Process of the Auto-Updater!"
	
	$options = [System.Management.Automation.Host.ChoiceDescription[]]($yes, $no)
	
	$result = $host.ui.PromptForChoice($title, $message, $options, 0)
	
	switch ($result)
	{
		0 {
			Write-Host "You have selected to have RPOS Auto-Updater uninstalled from the PC" -ForegroundColor Yellow
			Write-Host "Please wait for script to complete.!" -ForegroundColor Yellow
			Write-Host "Please, ignore any and all errors that come up on the screen!" -ForegroundColor Yellow
			Start-Sleep -Seconds 10
			
			#Setting Up System Variables before Uninstall.
			Set-RPOSVar
			
			net localgroup administrators TBC\shprorpos /delete
			net localgroup administrators TBC\shprorpos2 /delete
			schtasks /delete /TN "Rollback RPOS Local" /F
			schtasks /delete /TN "RPOS AU updater" /F
			schtasks /delete /TN "Update RPOS Clear Logs" /F
			schtasks /delete /TN "Update RPOS Local A" /F
			schtasks /delete /TN "Update RPOS Local B" /F
			schtasks /delete /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate" /F
			
			#Removing Local GUI User Tasks
			$RPOSMasterGUITasks = schtasks /query /FO list | findstr "RPOS AU Local User GUI" | Select-String -Pattern "RPOS AU Local User GUI"
			
			foreach ($RPOSMasterGUITask in $RPOSMasterGUITasks)
			{
				#Finding task name
				[string]$RPOSMasterGUITask = $RPOSMasterGUITask
				$indexmastertask = $RPOSMasterGUITask.IndexOf("RPOS AU Local")
				$RPOSMasterGUISIDUserTask = $RPOSMasterGUITask.Substring($indexmastertask)
				schtasks /End /TN "$RPOSMasterGUISIDUserTask"
				Start-Sleep -Seconds 2
				schtasks /Delete /TN "$RPOSMasterGUISIDUserTask" /F
			}
			
			net share cs18ebyi3$ /delete
			Remove-Item "C:\Windows\System32\Tasks\Microsoft\Windows\RPOS Auto-Updater" -Recurse -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Auto-Updater" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Adobe DCount" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeDistro" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AuVer UpdateDate" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "FlowModule Checksum" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Checksum" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DCount" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS DL Source" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Update Date" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Version Label" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOSAUVer" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeAIRVer" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AdobeDistro" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Adobe DCount" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoVersion" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVGateway" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVIPaddress" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EMVVeriFoneInstalled" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "BGInfoXMLchecksum" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "SwitchXMLchecksum" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "InstallComplete" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckInternet" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckAdobeAIR" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUAdobeAIRNI" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUAdobeAIRInstalling" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckBGInfoEMV" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckSelfSubDownload" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUClosingWindows" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUUninstallRPOS" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUInstallingRPOS" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCheckInstall" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUFailedLocal" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AULocalReinstall" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUInstallingConfirm" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUInstallingTesla" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "DownloadMD5HashModule" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUCompleteReinstall" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AUGatherReportingData" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "Tesla DCount" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaDistro" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "TeslaVer" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Endpoint" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Endpoint" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Endpoint" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Security Enable" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Services Enable" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "RPOS Katana Enable" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "EndpointXMLchecksum" -Force
			Remove-ItemProperty -Path "HKLM:\Software\Wow6432Node\Intel\LANDesk\Inventory\Custom Fields" -Name "AULiveUpdateTaskFailure" -Force
			
			Stop-Process -force -passthru -processname tesla-electron*
			Stop-Process -Force -PassThru -ProcessName proton*
			Stop-Process -force -passthru -processname tesla-electron*
			Stop-Process -Force -PassThru -ProcessName proton*
			
			$users = Get-ChildItem "C:\Users\" | Select-Object -ExpandProperty Name
			
			foreach ($user in $users)
			{
				Remove-Item "$env:HOMEDRIVE\Users\$user\AppData\Local\tesla-electron" -Recurse -Force
				(Get-Item "$env:HOMEDRIVE\Users\$user\AppData\Local\tesla-electron\").Delete()
			}
			
			Remove-Item "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Recurse -Force
			Remove-Item "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -Recurse -Force
			Remove-Item "C:\Windows\Scripts\RPOS\Bak" -Recurse -Force
			Remove-Item "C:\Windows\Scripts\RPOS\build" -Recurse -Force
			Remove-Item "C:\Windows\Scripts\RPOS\Logs" -Recurse -Force
			Remove-Item "C:\Windows\Scripts\RPOS\update" -Recurse -Force
			Remove-Item "C:\Windows\Scripts\RPOS\uninstall" -Recurse -Force
			Remove-Item "C:\Windows\Scripts\RPOS\images" -Recurse -Force
			Remove-Item "C:\Windows\Scripts\RPOS\ivanti" -Recurse -Force
			Remove-Item "C:\Users\Public\Desktop\Update RPOS.lnk" -Force
			Remove-Item "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Recurse -Force
			Remove-Item "HKLM:\SOFTWARE\Wow6432Node\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater" -Recurse -Force
			[Environment]::SetEnvironmentVariable("RPOSAutoUpdater", $null, "Machine")
			
			Remove-Item "C:\Windows\Scripts\RPOS" -Recurse -Force
			Remove-Item "C:\Windows\Scripts\" -Recurse -Force
		}
		1 {
			Write-Host "You have selected NO removing the RPOS Auto-Updater from this PC. Thank you!"
		}
	}
	
}

#########################################################################################################################################
#Fuction Get-RPOSLiveUpdate
function Get-RPOSLiveUpdate
{
	Set-RPOSVar
	Set-RPOSModuleWhatPCAmI
	
	$liveupdaterandomsleep1 = Get-Random -Maximum 60 -Minimum 5
	Start-Sleep -Seconds "$liveupdaterandomsleep1"
	$liveupdatetimehours = (Get-Date).ToString("hh")
	$liveupdatetimeminutes = (Get-Date).ToString("mm")
	$liveupdatetimesec = (Get-Date).ToString("ss")
	$liveupdatetimeAMPM = (Get-Date).ToString("tt")
	$autofixnetworkcheck = Test-Connection "rpos.tbccorp.com" -count 4
	$AutofixDate = (Get-Date).ToString("yyyyMMdd-hhmmss")
	$script:Log = "$RPOSAUDIRLOGS\RPOSLiveUpdate_$AutofixDate.txt"
	
	#Creating LiveUpdate Folder
	New-Item "$RPOSAUDIRLIVEUPDATEFIX" -ItemType Directory -Force
	
	if ($autofixnetworkcheck -ne $null) 
	{
		
		$AutofixDate = (Get-Date).ToString("yyyyMMdd-hhmmss")
		$script:Log = "$RPOSAUDIRLOGS\RPOSLiveUpdate_$AutofixDate.txt"
		$autofixhttpdir = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles"
		#Hasher XML file checker
		$hasher = "$autofixhttpdir/hasher.xml"
		$hasherlocal = "$RPOSAUDIRUPDATE\hasher.xml"
		
		$httpauicon = "$autofixhttpdir/auicon.ico"
		$httpupdaterposdtlnk = "$autofixhttpdir/Update RPOS.lnk"
		$httpupdaterposico = "$autofixhttpdir/UpdateRPOS.ico"
		$httpupdaterpospsm1 = "$autofixhttpdir/updaterpos.psm1"
		$httprposdesktoplnk = "$autofixhttpdir/RPOS.lnk"
		$httpfixabtaskszip = "$autofixhttpdir/FixUpdateRPOSABTasks.zip"
		
		$localauicon = "$RPOSAUDIRHOME\auicon.ico"
		$localupdaterposicon = "$RPOSAUDIRUPDATERPOS.ico"
		$localrposlnkbak = "$RPOSAUDIRBAK\RPOS.lnk"
		$localupdaterposlnkbak = "$RPOSAUDIRBAK\Update RPOS.lnk"
		$localbuilduninstall = "$RPOSAUDIRUNINSTALL\BuildUninstallers.exe"
		$localquietuninstall = "$RPOSAUDIRUNINSTALL\quietuninstall.vbs"
		$localupdaterpospubdesk = "C:\Users\Public\Desktop\Update RPOS.lnk"
		$localpowershell32 = "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1"
		$localpowershell64 = "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1"
		$localfixabtaskszip = "$RPOSAUDIRUPDATE\FixUpdateRPOSABTasks.zip"
		$localfixabtasksexe = "$RPOSAUDIRUPDATE\FixUpdateRPOSABTasks.exe"
		$localautofixzip = "$RPOSAUDIRUPDATE\LiveUpdate.zip"
		
		#Keys
		$uninstallRegFolder = "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater"
		
		#Uninstaller Registry Values
		$DisplayIcon = "C:\Windows\Scripts\RPOS\auicon.ico"
		$DisplayName = "RPOS Auto-Updater"
		$DisplayVersion = "$ReleaseInfo"
		$EstimatedSize = "2000"
		$InstallLocation = "C:\Windows\Scripts\RPOS"
		$NoModify = "1"
		$NoRepair = "1"
		$Publisher = "TBC Corporation EOC Department"
		$UninstallString = '"C:\Windows\System32\WindowsPowerShell\v1.0\powershell.exe" -command "Get-RPOSAutoUpdaterUninstall"'
		
		#LANDesk Registry Values
		#Checking Registry Keys
		$ADcount = (Get-ItemProperty -Path $LANDeskKeys -Name "Adobe DCount" -ErrorAction SilentlyContinue)."Adobe DCount"
		$AdobeAirVer = (Get-ItemProperty -Path $LANDeskKeys -Name "AdobeAIRVer" -ErrorAction SilentlyContinue)."AdobeAIRVer"
		$AdobeDistro = (Get-ItemProperty -Path $LANDeskKeys -Name "AdobeDistro" -ErrorAction SilentlyContinue)."AdobeDistro"
		$AuVerUpdateDate = (Get-ItemProperty -Path $LANDeskKeys -Name "AuVer UpdateDate" -ErrorAction SilentlyContinue)."AuVer UpdateDate"
		$BGInfoVer = (Get-ItemProperty -Path $LANDeskKeys -Name "BGInfoVersion" -ErrorAction SilentlyContinue)."BGInfoVersion"
		$BGInfoXMLCS = (Get-ItemProperty -Path $LANDeskKeys -Name "BGInfoXMLchecksum" -ErrorAction SilentlyContinue)."BGInfoXMLchecksum"
		$EMVGateway = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVGateway" -ErrorAction SilentlyContinue)."EMVGateway"
		$EMVIPAddress = (Get-ItemProperty -Path $LANDeskKeys -Name "EMVIPaddress" -ErrorAction SilentlyContinue)."EMVIPaddress"
		$FMChecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "FlowModule Checksum" -ErrorAction SilentlyContinue)."FlowModule Checksum"
		$InstallComplete = (Get-ItemProperty -Path $LANDeskKeys -Name "InstallComplete" -ErrorAction SilentlyContinue)."InstallComplete"
		$RPOSAUVerPC = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Auto-Updater" -ErrorAction SilentlyContinue)."RPOS Auto-Updater"
		$RPOSChecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Checksum" -ErrorAction SilentlyContinue)."RPOS Checksum"
		$RPOSDCount = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS DCount" -ErrorAction SilentlyContinue)."RPOS DCount"
		$RPOSDLS = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS DL Source" -ErrorAction SilentlyContinue)."RPOS DL Source"
		$RPOSUDate = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Update Date" -ErrorAction SilentlyContinue)."RPOS Update Date"
		$RPOSVersion = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version" -ErrorAction SilentlyContinue)."RPOS Version"
		$RPOSVersionLabel = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Version Label" -ErrorAction SilentlyContinue)."RPOS Version Label"
		$RPOSAUVer = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOSAUVer" -ErrorAction SilentlyContinue)."RPOSAUVer"
		$TeslaDcountKey = (Get-ItemProperty -Path $LANDeskKeys -Name "Tesla DCount" -ErrorAction SilentlyContinue)."Tesla DCount"
		$TeslaDistroKey = (Get-ItemProperty -Path $LANDeskKeys -Name "TeslaDistro" -ErrorAction SilentlyContinue)."TeslaDistro"
		$TeslaVerKey = (Get-ItemProperty -Path $LANDeskKeys -Name "TeslaVer" -ErrorAction SilentlyContinue)."TeslaVer"
		$SwitchXMLchecksum = (Get-ItemProperty -Path $LANDeskKeys -Name "SwitchXMLchecksum" -ErrorAction SilentlyContinue)."SwitchXMLchecksum"
		$desktopAUCheckInternet = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInternet" -ErrorAction SilentlyContinue)."AUCheckInternet"
		$desktopAUCheckAdobeAIR = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckAdobeAIR" -ErrorAction SilentlyContinue)."AUCheckAdobeAIR"
		$desktopAUAdobeAIRNI = (Get-ItemProperty -Path $LANDeskKeys -Name "AUAdobeAIRNI" -ErrorAction SilentlyContinue)."AUAdobeAIRNI"
		$desktopAUAdobeAIRInstalling = (Get-ItemProperty -Path $LANDeskKeys -Name "AUAdobeAIRInstalling" -ErrorAction SilentlyContinue)."AUAdobeAIRInstalling"
		$desktopAUCheckBGInfoEMV = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckBGInfoEMV" -ErrorAction SilentlyContinue)."AUCheckBGInfoEMV"
		$desktopAUCheckSelfSubDownload = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckSelfSubDownload" -ErrorAction SilentlyContinue)."AUCheckSelfSubDownload"
		$desktopAUClosingWindows = (Get-ItemProperty -Path $LANDeskKeys -Name "AUClosingWindows" -ErrorAction SilentlyContinue)."AUClosingWindows"
		$desktopAUUninstallRPOS = (Get-ItemProperty -Path $LANDeskKeys -Name "AUUninstallRPOS" -ErrorAction SilentlyContinue)."AUUninstallRPOS"
		$desktopAUInstallingRPOS = (Get-ItemProperty -Path $LANDeskKeys -Name "AUInstallingRPOS" -ErrorAction SilentlyContinue)."AUInstallingRPOS"
		$desktopAUCheckInstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCheckInstall" -ErrorAction SilentlyContinue)."AUCheckInstall"
		$desktopAUFailedLocal = (Get-ItemProperty -Path $LANDeskKeys -Name "AUFailedLocal" -ErrorAction SilentlyContinue)."AUFailedLocal"
		$desktopAULocalReinstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AULocalReinstall" -ErrorAction SilentlyContinue)."AULocalReinstall"
		$desktopAUCompleteReinstall = (Get-ItemProperty -Path $LANDeskKeys -Name "AUCompleteReinstall" -ErrorAction SilentlyContinue)."AUCompleteReinstall"
		$desktopAUGatherReportingData = (Get-ItemProperty -Path $LANDeskKeys -Name "AUGatherReportingData" -ErrorAction SilentlyContinue)."AUGatherReportingData"
		$endpointsecurityendpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Security Endpoint" -ErrorAction SilentlyContinue)."RPOS Security Endpoint"
		$endpointservicesendpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Services Endpoint" -ErrorAction SilentlyContinue)."RPOS Services Endpoint"
		$endpointkatanaendpoint = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Katana Endpoint" -ErrorAction SilentlyContinue)."RPOS Katana Endpoint"
		$endpointenablesecurity = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Security Enable" -ErrorAction SilentlyContinue)."RPOS Security Enable"
		$endpointenableservices = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Services Enable" -ErrorAction SilentlyContinue)."RPOS Services Enable"
		$endpointenablekatana = (Get-ItemProperty -Path $LANDeskKeys -Name "RPOS Katana Enable" -ErrorAction SilentlyContinue)."RPOS Katana Enable"
		
		#Adding Customer endpoint key for stores
		if ($RPOSAgentname -eq "Store")
		{
			$endpointchecksumnum = (Get-ItemProperty -Path $LANDeskKeys -Name "EndpointXMLchecksum")."EndpointXMLchecksum"
		}
		
		#Testing Powershell Module Folder exists
		$Powershell32 = Test-Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos"
		$Powershell64 = Test-Path "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos"
		
		#Getting Folder in RPOS AU Directory Properties
		$scriptfltest = Test-Path "C:\Windows\Scripts\"
		$scriptflhid = Get-ItemProperty "C:\Windows\Scripts" | Select-Object -ExpandProperty Attributes
		
		$RPOSfltest = Test-Path "$RPOSAUDIRHOME"
		$RPOSflhid = Get-ItemProperty "$RPOSAUDIRHOME" | Select-Object -ExpandProperty Attributes
		
		$bakfltest = Test-Path "$RPOSAUDIRBAK"
		$bakflhid = Get-ItemProperty "$RPOSAUDIRBAK" | Select-Object -ExpandProperty Attributes
		
		$buildfltest = Test-Path "$RPOSAUDIRBUILD"
		$buildflhid = Get-ItemProperty "$RPOSAUDIRBUILD" | Select-Object -ExpandProperty Attributes
		
		$logfltest = Test-Path "$RPOSAUDIRLOGS"
		$logflhid = Get-ItemProperty "$RPOSAUDIRLOGS" | Select-Object -ExpandProperty Attributes
		
		$updatefltest = Test-Path "$RPOSAUDIRUPDATE"
		$updateflhid = Get-ItemProperty "$RPOSAUDIRUPDATE" | Select-Object -ExpandProperty Attributes
		
		$imagesfltest = Test-Path "$RPOSAUDIRIMAGES"
		$imagesflhid = Get-ItemProperty "$RPOSAUDIRIMAGES" | Select-Object -ExpandProperty Attributes
		
		$ivantifltest = Test-Path "$RPOSAUDIRIVANTI"
		$ivantiflhid = Get-ItemProperty "$RPOSAUDIRIVANTI" | Select-Object -ExpandProperty Attributes
		
		#Checking for share folder
		$checkshare = get-WmiObject -class Win32_Share -computer $Env:COMPUTERNAME | Where-Object { $_.Name -eq "cs18ebyi3$" } | Select-Object -ExpandProperty Path
		
		#Checking to see files in C:\Windows\Scripts\RPOS\ are there or not
		$auicon = Test-Path "$RPOSAUDIRHOME\auicon.ico"
		$updaterposicon = Test-Path "$RPOSAUDIRHOME\UpdateRPOS.ico"
		$RPOSlnk = Test-Path "$RPOSAUDIRBAK\RPOS.lnk"
		$updaterposlnk = Test-Path "$RPOSAUDIRBAK\Update RPOS.lnk"
		$publicdesktopicon = Test-Path "C:\Users\Public\Desktop\Update RPOS.lnk"
		$updaterposico = Test-Path "$RPOSAUDIRHOME\UpdateRPOS.ico"
		$autoupdateautofix = Test-Path "$RPOSAUAUTOFIX"
		
		#Checking MD5 Hash information for the files in C:\Windows\Scripts\RPOS
		$updaterposlnkhash = Get-Item -Path "$RPOSAUDIRBAK\Update RPOS.lnk" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$publicdesktopiconhash = Get-Item -Path "C:\Users\Public\Desktop\Update RPOS.lnk" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$auiconhash = Get-Item -Path "$RPOSAUDIRHOME\auicon.ico" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$rposbaklnkhash = Get-Item -Path "$RPOSAUDIRBAK\RPOS.lnk" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$updaterpospsm32 = Get-Item -Path "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$updaterpospsm64 = Get-Item -Path "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$updaterposico = Get-Item -Path "$RPOSAUDIRHOME\UpdateRPOS.ico" | Get-FileHash -Algorithm MD5 | select -ExpandProperty hash
		$bottomguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\BottomGUI.jpg" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$engineguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\engine.png" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$failureguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\Failure.png" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$forbiddenguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\forbidden.png" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$rposiconguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\icon32.png" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$roadguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\Road.jpg" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$serviceguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\service.png" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$sportcarguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\SportCar.png" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$successfulguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\successful.png" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$toplogoguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\TopLogoGUI.jpg" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$wheelsguiimagehash = Get-FileHash -Path "$RPOSAUDIRIMAGES\wheels.png" -Algorithm MD5 | Select-Object -ExpandProperty hash
		$ivantipsexecutionpolicyhash = Get-FileHash -Path "$RPOSAUDIRIVANTI\CP.3219._jRXSjnlcT6HVob&#47pE9pCU3AYvmI=.xml" -Algorithm MD5 | Select-Object -ExpandProperty hash
		
		#Checking Scheduled Tasks
		if ($windowsshortname -eq "Win7")
		{
			$task1 = schtasks /query /TN "Rollback RPOS Local"
			$task2 = schtasks /query /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate"
			$task3 = schtasks /query /TN "Update RPOS Clear Logs"
			$task4 = schtasks /query /TN "Update RPOS Local A"
			$task5 = schtasks /query /TN "Update RPOS Local B"
		}
		elseif (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
		{
			[string]$task1 = Get-ScheduledTaskInfo -TaskName "Rollback RPOS Local"
			[string]$task2 = Get-ScheduledTaskInfo -TaskName "\Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate"
			[string]$task3 = Get-ScheduledTaskInfo -TaskName "Update RPOS Clear Logs"
			[string]$task4 = Get-ScheduledTaskInfo -TaskName "Update RPOS Local A"
			[string]$task5 = Get-ScheduledTaskInfo -TaskName "Update RPOS Local B"
		}
		
		
		#Uninstall registry keys
		$uninstall = Test-Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Uninstall\RPOSAutoUpdater"
		$undisplayicon = (Get-ItemProperty -Path $uninstallRegFolder -Name "DisplayIcon")."DisplayIcon"
		$undisplayname = (Get-ItemProperty -Path $uninstallRegFolder -Name "DisplayName")."DisplayName"
		$undisplayver = (Get-ItemProperty -Path $uninstallRegFolder -Name "DisplayVersion")."DisplayVersion"
		$uninstallloc = (Get-ItemProperty -Path $uninstallRegFolder -Name "InstallLocation")."InstallLocation"
		$unestimatedsize = (Get-ItemProperty -Path $uninstallRegFolder -Name "EstimatedSize")."EstimatedSize"
		$unnomodify = (Get-ItemProperty -Path $uninstallRegFolder -Name "NoModify")."NoModify"
		$unnorepair = (Get-ItemProperty -Path $uninstallRegFolder -Name "NoRepair")."NoRepair"
		$unpublisher = (Get-ItemProperty -Path $uninstallRegFolder -Name "Publisher")."Publisher"
		$ununinstallstring = (Get-ItemProperty -Path $uninstallRegFolder -Name "UninstallString")."UninstallString"
		
		#Check ExecutionPolicy
		[string]$ExecutionDay = (Get-Date).DayOfWeek

		#Checking System Variable
		$envrposautoupdater = "$env:SystemRoot\Scripts\RPOS"
		
		if (($name -eq $ppc1) -or ($name -eq $ppc2) -or ($name -eq $ppc3) -or ($name -eq $ppc4) -or ($name -eq $ppc5) -or ($name -eq $ppc6) -or ($name -eq $ppc7) -or ($name -eq $ppc8) -or ($name -eq $ppc9) -or ($name -eq $ppc10) -or ($name -eq $ppc11) -or ($name -eq $ppc12) -or ($name -eq $ppc13) -or ($name -eq $ppc14) -or ($name -eq $ppc15) -or ($name -eq $ppc16) -or ($name -eq $ppc17) -or ($name -eq $ppc18) -or ($name -eq $ppc19) -or ($name -eq $ppc20))
		{
			
			$LiveUpdateFixList1 = "$hasher"
			$LiveUpdateFixList2 = [xml](New-Object System.Net.WebClient).downloadstring($LiveUpdateFixList1)
			$a = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.bakupdaterpos
			$b = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.publicupdaterpos
			$c = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.auicon
			$d = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.bakrposlnk
			$e = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.updaterpossys
			$f = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.updaterpossyswow
			$g = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.updaterposico
			$h = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.fixrposaufileszip
			$i = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.fixabtaskszip
			$j = $LiveUpdateFixList2.liveupdate.pilot.update.version
			$k = $LiveUpdateFixList2.liveupdate.pilot.update.statement
			$l = $LiveUpdateFixList2.liveupdate.pilot.update.setup
			$m = $LiveUpdateFixList2.liveupdate.pilot.update.hash
			$n = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.bottomguiimage
			$o = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.engineguiimage
			$p = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.failureguiimage
			$q = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.forbiddenguiimage
			$r = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.rposiconguiimage
			$s = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.roadguiimage
			$t = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.serviceguiimage
			$u = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.sportcarguiimage
			$v = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.successfulguiimage
			$w = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.toplogoguiimage
			$x = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.wheelsguiimage
			$y = $LiveUpdateFixList2.liveupdate.pilot.hashvalues.ivantiexecutionpolicy
			$script:downloadfile = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/LiveUpdatePilot.zip"	
		}
		
		Else
		{
			
			$LiveUpdateFixList1 = "$hasher"
			$LiveUpdateFixList2 = [xml](New-Object System.Net.WebClient).downloadstring($LiveUpdateFixList1)
			$a = $LiveUpdateFixList2.liveupdate.retail.hashvalues.bakupdaterpos
			$b = $LiveUpdateFixList2.liveupdate.retail.hashvalues.publicupdaterpos
			$c = $LiveUpdateFixList2.liveupdate.retail.hashvalues.auicon
			$d = $LiveUpdateFixList2.liveupdate.retail.hashvalues.bakrposlnk
			$e = $LiveUpdateFixList2.liveupdate.retail.hashvalues.updaterpossys
			$f = $LiveUpdateFixList2.liveupdate.retail.hashvalues.updaterpossyswow
			$g = $LiveUpdateFixList2.liveupdate.retail.hashvalues.updaterposico
			$h = $LiveUpdateFixList2.liveupdate.retail.hashvalues.fixrposaufileszip
			$i = $LiveUpdateFixList2.liveupdate.retail.hashvalues.fixabtaskszip
			$j = $LiveUpdateFixList2.liveupdate.retail.update.version
			$k = $LiveUpdateFixList2.liveupdate.retail.update.statement
			$l = $LiveUpdateFixList2.liveupdate.retail.update.setup
			$m = $LiveUpdateFixList2.liveupdate.retail.update.hash
			$n = $LiveUpdateFixList2.liveupdate.retail.hashvalues.bottomguiimage
			$o = $LiveUpdateFixList2.liveupdate.retail.hashvalues.engineguiimage
			$p = $LiveUpdateFixList2.liveupdate.retail.hashvalues.failureguiimage
			$q = $LiveUpdateFixList2.liveupdate.retail.hashvalues.forbiddenguiimage
			$r = $LiveUpdateFixList2.liveupdate.retail.hashvalues.rposiconguiimage
			$s = $LiveUpdateFixList2.liveupdate.retail.hashvalues.roadguiimage
			$t = $LiveUpdateFixList2.liveupdate.retail.hashvalues.serviceguiimage
			$u = $LiveUpdateFixList2.liveupdate.retail.hashvalues.sportcarguiimage
			$v = $LiveUpdateFixList2.liveupdate.retail.hashvalues.successfulguiimage
			$w = $LiveUpdateFixList2.liveupdate.retail.hashvalues.toplogoguiimage
			$x = $LiveUpdateFixList2.liveupdate.retail.hashvalues.wheelsguiimage
			$y = $LiveUpdateFixList2.liveupdate.retail.hashvalues.ivantiexecutionpolicy
			$script:downloadfile = "http://rpos.tbccorp.com/RPOSAutoUpdater/UpdateFiles/LiveUpdate.zip"
				
		}
		
		########################################################################################################
		#Checking Varibles and fixing if needed
		#Checking to see if RPOS directories exist on PC.
		Write-Output "RPOS Auto-Updater $ReleaseInfo Live-Update Module Starting" | Out-File -Append -FilePath $Log
		
		if (($scriptfltest -ne $true) -or ($scriptflhid -ne "Hidden, Directory") -or ($RPOSfltest -ne $true) -or ($RPOSflhid -ne "Hidden, Directory") -or ($bakfltest -ne $true) -or ($bakflhid -ne "Hidden, Directory") -or ($buildfltest -ne $true) -or ($buildflhid -ne "Hidden, Directory") -or ($logfltest -ne $true) -or ($logflhid -ne "Hidden, Directory") -or ($updatefltest -ne $true) -or ($updateflhid -ne "Hidden, Directory") -or ($Powershell32 -ne $true) -or ($Powershell64 -ne $true) -or ($imagesfltest -ne $true) -or ($imagesflhid -ne "Hidden, Directory") -or ($ivantifltest -ne $true) -or ($ivantiflhid -ne "Hidden, Directory"))
		{
			
			if (($scriptfltest -ne $true) -or ($scriptflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged C:\Windows\Scripts" | Out-File -Append -FilePath $Log
				Remove-Item "C:\Windows\Scripts" -Recurse -Force
				New-Item "C:\Windows\Scripts" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRHOME" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRLOGS" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRBAK" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRBUILD" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRUPDATE" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRIMAGES" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRIVANTI" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($RPOSfltest -ne $true) -or ($RPOSflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRHOME" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRHOME" -Recurse -Force
				New-Item "$RPOSAUDIRHOME" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRLOGS" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRBAK" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRBUILD" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRUPDATE" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRIMAGES" -ItemType Directory | %{ $_.Attributes = "hidden" }
				New-Item "$RPOSAUDIRIVANTI" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($bakfltest -ne $true) -or ($bakflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRBAK" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRBAK" -Recurse -Force
				New-Item "$RPOSAUDIRBAK" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($buildfltest -ne $true) -or ($buildflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRBUILD" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRBUILD" -Recurse -Force
				New-Item "$RPOSAUDIRBUILD" -ItemType Directory | %{ $_.Attributes = "hidden" }
				net share cs18ebyi3$=C:\Windows\Scripts\RPOS\build "/grant:TBC\shprorpos,READ" "/grant:TBC\shprorpos2,READ"
			}
			
			if (($logfltest -ne $true) -or ($logflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRLOGS" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRLOGS" -Recurse -Force
				New-Item "$RPOSAUDIRLOGS" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($updatefltest -ne $true) -or ($updateflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRUPDATE" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRUPDATE" -Recurse -Force
				New-Item "$RPOSAUDIRUPDATE" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($imagesfltest -ne $true) -or ($imagesflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged $RPOSAUDIRIMAGES" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRIMAGES" -Recurse -Force
				New-Item "$RPOSAUDIRIMAGES" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if (($ivantifltest -ne $true) -or ($ivantiflhid -ne "Hidden, Directory"))
			{
				Write-Output "Following folder was missing and/or damaged RPOSAUDIRIVANTI" | Out-File -Append -FilePath $Log
				Remove-Item "$RPOSAUDIRIVANTI" -Recurse -Force
				New-Item "$RPOSAUDIRIVANTI" -ItemType Directory | %{ $_.Attributes = "hidden" }
			}
			
			if ($Powershell32 -ne $true)
			{
				Write-Output "Following folder was missing and/or damaged C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" | Out-File -Append -FilePath $Log
				New-Item -ItemType Directory "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
			}
			
			if ($Powershell64 -ne $true)
			{
				Write-Output "Following folder was missing and/or damaged C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" | Out-File -Append -FilePath $Log
				New-Item -ItemType Directory "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos" -Force
			}
		}
		Else
		{
			Write-Output "CHECKING FOLDER AUTO UPDATE FOLDERS                                      : PASSED" | Out-File -Append -FilePath $Log
		}
		
		
		if (($a -ne $updaterposlnkhash) -or ($b -ne $publicdesktopiconhash) -or ($c -ne $auiconhash) -or ($d -ne $rposbaklnkhash) -or ($e -ne $updaterpospsm32) -or ($f -ne $updaterpospsm64) -or ($g -ne $updaterposico) -or ($n -ne $bottomguiimagehash) -or ($o -ne $engineguiimagehash) -or ($p -ne $failureguiimagehash) -or ($q -ne $forbiddenguiimagehash) -or ($r -ne $rposiconguiimagehash) -or ($s -ne $roadguiimagehash) -or ($t -ne $serviceguiimagehash) -or ($u -ne $sportcarguiimagehash) -or ($v -ne $successfulguiimagehash) -or ($w -ne $toplogoguiimagehash) -or ($x -ne $wheelsguiimagehash) -or ($y -ne $ivantipsexecutionpolicyhash))
		{
			#Downloading AutoFix Zip file and extracting it
			Write-Output "Downloading Live Updater Files so Auto-Updater can be fixed" | Out-File -Append -FilePath $Log

			#Script Param variables from script to add to Set-RPOSModuleDownloadHashSetup
			$script:downloadMD5 = $h
			$script:localfilezip = $localautofixzip
			$script:downloadtimes = "0"
			
			Set-RPOSModuleDownloadHashSetup -downloadfile $downloadfile -localfile $localfilezip -downloadMD5 $downloadMD5
			
			Expand-Archive -Path $localautofixzip -DestinationPath $RPOSAUDIRLIVEUPDATEFIX -Force
			Remove-Item $localautofixzip -Recurse -Force
			
			#Checking C:\Windows\Scripts\RPOS\Bak\Update RPOS.lnk file
			if ($a -ne $updaterposlnkhash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRBAK\Update RPOS.lnk" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\Update RPOS.lnk" -Destination "$RPOSAUDIRBAK\Update RPOS.lnk" -Force
				icacls $localupdaterposlnkbak /grant Everyone:RX
			}
			
			#Checking C:\Users\Public\Desktop\Update RPOS.lnk file
			if ($b -ne $publicdesktopiconhash)
			{
				Write-Output "Following file was missing and/or damaged C:\Users\Public\Desktop\Update RPOS.lnk" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\Update RPOS.lnk" -Destination "C:\Users\Public\Desktop" -Force
				icacls $localupdaterpospubdesk /grant Everyone:RX
			}
			
			#Checking C:\Windows\Scripts\RPOS\auicon.ico file
			if ($c -ne $auiconhash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRHOME\auicon.ico" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\auicon.ico" -Destination "$RPOSAUDIRHOME\auicon.ico" -Force
			}
			
			#Checking C:\Windows\Scripts\RPOS\Bak\RPOS.lnk file
			if ($d -ne $rposbaklnkhash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRBAK\RPOS.lnk" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\RPOS.lnk" -Destination "$RPOSAUDIRBAK\RPOS.lnk" -Force
				icacls $localrposlnkbak /grant Everyone:RX
			}
			
			
			#Checking C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1 file
			if ($e -ne $updaterpospsm32)
			{
				Write-Output "Following file was missing and/or damaged C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\updaterpos.psm1" -Destination "C:\Windows\System32\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" -Force
			}
			
			#Checking C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1 file
			if ($f -ne $updaterpospsm64)
			{
				Write-Output "Following file was missing and/or damaged C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\updaterpos.psm1" -Destination "C:\Windows\SysWOW64\WindowsPowerShell\v1.0\Modules\updaterpos\updaterpos.psm1" -Force
			}
			
			#Checking C:\Windows\Scripts\RPOS\UpdateRPOS.ico file
			if ($g -ne $updaterposico)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRHOME\UpdateRPOS.ico" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\UpdateRPOS.ico" -Destination "$RPOSAUDIRHOME\UpdateRPOS.ico" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\BottomGUI.jpg file
			if ($n -ne $bottomguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\BottomGUI.jpg" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\BottomGUI.jpg" -Destination "$RPOSAUDIRIMAGEs\BottomGUI.jpg" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\engine.png file
			if ($o -ne $engineguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\engine.png" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\engine.png" -Destination "$RPOSAUDIRIMAGEs\engine.png" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\Failure.png file
			if ($p -ne $failureguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\Failure.png" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\Failure.png" -Destination "$RPOSAUDIRIMAGEs\Failure.png" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\forbidden.png file
			if ($q -ne $forbiddenguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\forbidden.png" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\forbidden.png" -Destination "$RPOSAUDIRIMAGEs\forbidden.png" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\icon32.png file
			if ($r -ne $rposiconguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\icon32.png" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\icon32.png" -Destination "$RPOSAUDIRIMAGEs\icon32.png" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\Road.jpg file
			if ($s -ne $roadguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\Road.jpg" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\Road.jpg" -Destination "$RPOSAUDIRIMAGEs\Road.jpg" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\service.png file
			if ($t -ne $serviceguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\service.png" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\service.png" -Destination "$RPOSAUDIRIMAGEs\service.png" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\SportCar.png file
			if ($u -ne $sportcarguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\SportCar.png" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\SportCar.png" -Destination "$RPOSAUDIRIMAGEs\SportCar.png" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\successful.png file
			if ($v -ne $successfulguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\successful.png" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\successful.png" -Destination "$RPOSAUDIRIMAGEs\successful.png" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\TopLogoGUI.jpg file
			if ($w -ne $toplogoguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\TopLogoGUI.jpg" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\TopLogoGUI.jpg" -Destination "$RPOSAUDIRIMAGEs\TopLogoGUI.jpg" -Force
			}
			
			#Checking C:\Windows\Scripts\Images\wheels.png file
			if ($x -ne $wheelsguiimagehash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIMAGES\wheels.png" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\wheels.png" -Destination "$RPOSAUDIRIMAGEs\wheels.png" -Force
			}
			
			#Checking C:\Windows\Scripts\ivanti\CP.3219._jRXSjnlcT6HVob&#47pE9pCU3AYvmI=.xml file
			if ($y -ne $ivantipsexecutionpolicyhash)
			{
				Write-Output "Following file was missing and/or damaged $RPOSAUDIRIVANTI\CP.3219._jRXSjnlcT6HVob&#47pE9pCU3AYvmI=.xml" | Out-File -Append -FilePath $Log
				Copy-Item "$RPOSAUDIRLIVEUPDATEFIX\CP.3219._jRXSjnlcT6HVob&#47pE9pCU3AYvmI=.xml" -Destination "$RPOSAUDIRIVANTI\CP.3219._jRXSjnlcT6HVob&#47pE9pCU3AYvmI=.xml" -Force
			}
			
		}
		Else
		{
			Write-Output "CHECKING FOR MISSING FILES IN AUTO UPDATER                               : PASSED" | Out-File -Append -FilePath $Log
		}
		
		
		#Checking ExecutionPolicy
		if (($ExecutionDay -eq "Monday") -and ($liveupdatetimehours -eq "12") -and ($liveupdatetimeminutes -lt "05") -and ($liveupdatetimesec -ge "00") -and ($liveupdatetimeAMPM -eq "AM"))
		{
			$Seconds = Get-Random -Minimum 1 -Maximum 300
			Write-Output "I am sleeping for $Seconds seconds to prevent Ivanti Overflow" | Out-File -Append -FilePath $Log
			Start-Sleep -Seconds $Seconds
			Write-Output "It is Monday at midnight, making sure the executionpolicy stays remotesigned" | Out-File -Append -FilePath $Log
			Write-Output "MONDAY POWERSHELL EXECUTION POLICY                                       : COMPLETED" | Out-File -Append -FilePath $Log
			& "C:\Program Files (x86)\LANDesk\LDClient\SDCLIENT.EXE" /policyfile="C:\Windows\Scripts\RPOS\ivanti\CP.3219._jRXSjnlcT6HVob&#47pE9pCU3AYvmI=.xml" /skipmaintenancewindow /skipdonotdisturb
			Set-ExecutionPolicy RemoteSigned -Force
		}
		
		#Checking SystemVarible
		if ($envrposautoupdater -ne "C:\Windows\Scripts\RPOS")
		{
			Write-Output "env:RPOSAutoUpdater is not on the PC resetting env varible" | Out-File -Append -FilePath $Log
			[Environment]::SetEnvironmentVariable("RPOSAutoUpdater", "$env:SystemRoot\Scripts\RPOS", "Machine")
		}
		Else
		{
			Write-Output "CHECKING ENV:RPOSAUTOUPDATER system varible                              : PASSED" | Out-File -Append -FilePath $Log
		}
		
		if (($uninstall -eq $null) -or ($undisplayicon -eq $null) -or ($undisplayicon -ne $DisplayIcon) -or ($undisplayname -eq $null) -or ($undisplayname -ne $DisplayName) -or ($undisplayver -eq $null) -or ($undisplayver -ne $DisplayVersion) -or ($unestimatedsize -eq $null) -or ($unestimatedsize -ne $EstimatedSize) -or ($uninstallloc -eq $null) -or ($uninstallloc -ne $InstallLocation) -or ($unnomodify -eq $null) -or ($unnomodify -ne $NoModify) -or ($unnorepair -eq $null) -or ($unnorepair -ne $NoRepair) -or ($unpublisher -eq $null) -or ($unpublisher -ne $Publisher) -or ($ununinstallstring -eq $null) -or ($ununinstallstring -ne $UninstallString))
		{
			
			if ($uninstall -eq $false)
			{
				Write-Output "RPOS Auto-Updater Uninstaller was not found creating folder" | Out-File -Append -FilePath $Log
				New-Item -ItemType Directory $uninstallRegFolder -Force
			}
			
			if (($undisplayicon -eq $null) -or ($undisplayicon -ne $DisplayIcon))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$DisplayIcon" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "DisplayIcon" -Value $DisplayIcon -Force
			}
			
			if (($undisplayname -eq $null) -or ($undisplayname -ne $DisplayName))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$DisplayName" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "DisplayName" -Value $DisplayName -Force
			}
			
			if (($undisplayver -eq $null) -or ($undisplayver -ne $DisplayVersion))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$DisplayVersion" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "DisplayVersion" -Value $DisplayVersion -Force
			}
			
			if (($unestimatedsize -eq $null) -or ($unestimatedsize -ne $EstimatedSize))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$EstimatedSize" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "EstimatedSize" -Value $EstimatedSize -PropertyType DWORD -Force
			}
			
			if (($uninstallloc -eq $null) -or ($uninstallloc -ne $InstallLocation))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$InstallLocation" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "InstallLocation" -Value $InstallLocation -Force
			}
			
			if (($unnomodify -eq $null) -or ($unnomodify -ne $NoModify))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$NoModify" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "NoModify" -Value $NoModify -PropertyType DWORD -Force
			}
			
			if (($unnorepair -eq $null) -or ($unnorepair -ne $NoRepair))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$NoRepair" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "NoRepair" -Value $NoRepair -PropertyType DWORD -Force
			}
			
			if (($unpublisher -eq $null) -or ($unpublisher -ne $Publisher))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$Publisher" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "Publisher" -Value $Publisher -Force
			}
			
			if (($ununinstallstring -eq $null) -or ($ununinstallstring -ne $UninstallString))
			{
				Write-Output "Following registry key was missing and/or damaged $uninstallRegFolder\$UninstallString" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$uninstallRegFolder" -Name "UninstallString" -Value $UninstallString -Force
			}	
			
		}
		
		Else
		{
			Write-Output "CHECKING UNINSTALL REGISTRY KEYS                                         : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Checking Scheduled Tasks to make sure they are correctly setup
		if (!$task1)
		{
			Write-Output "PC is missing Rollback RPOS Local Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			$script:RPOSRollbackScheduledXML = "$RPOSAUDIRLIVEUPDATEFIX\Rollback.xml"
			schtasks /delete /TN "Rollback RPOS Local" /F
			
			if ($windowsshortname -eq "Win7")
			{
				Set-RPOSCreateRollbackRPOSXML
				schtasks /create /TN "Rollback RPOS Local" /XML "$RPOSRollbackScheduledXML"
				Remove-Item "$RPOSRollbackScheduledXML" -Force
			}
			elseif (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
			{
				Set-RPOSCreateRollbackRPOSXMLWin10
			}
			
		}
		Else
		{
			Write-Output "CHECKING ROLLBACK RPOS SCHEDULED TASK                                    : PASSED" | Out-File -Append -FilePath $Log
		}
		
		
		if (!$task2)
		{
			Write-Output "PC RPOS LiveUpdate Is missing Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			
			if ($TwoDigPCName -eq "01")
			{
				if ($windowsshortname -eq "Win7")
				{
					$script:RPOSAULiveUpdateTime = "2016-05-04T01:00:00"
				}
				if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
				{
					$script:RPOSAULiveUpdateTime = "01:00:00"
				}
			}
			
			if ($TwoDigPCName -eq "02")
			{
				if ($windowsshortname -eq "Win7")
				{
					$script:RPOSAULiveUpdateTime = "2016-05-04T02:00:00"
				}
				if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
				{
					$script:RPOSAULiveUpdateTime = "02:00:00"
				}
			}
			
			if (($TwoDigPCName -ne "01") -and ($TwoDigPCname -ne "02"))
			{
				if ($windowsshortname -eq "Win7")
				{
					$script:RPOSAULiveUpdateTime = "2016-05-04T03:00:00"
				}
				if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
				{
					$script:RPOSAULiveUpdateTime = "03:00:00"
				}
			}
			
			$script:RPOSLiveUpdateXML = "$RPOSAUDIRUPDATE\LiveUpdate.xml"
			schtasks /delete /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate" /F
			
			if ($windowsshortname -eq "Win7")
			{
				Set-RPOSCreateLiveUpdateTask
				schtasks /create /TN "Microsoft\Windows\RPOS Auto-Updater\RPOS Auto-Updater LiveUpdate" /XML "$RPOSLiveUpdateXML"
				Remove-Item "$RPOSLiveUpdateXML" -Force
			}
			elseif (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
			{
				Set-RPOSCreateLiveUpdateTaskWin10
			}
			
		}
		Else
		{
			Write-Output "CHECKING UPDATE AUTO UPDATE LIVE UPDATE TASK                             : PASSED" | Out-File -Append -FilePath $Log
			
		}
		
		if (!$task3)
		{
			Write-Output "PC is missing RPOS AU Clear Logs Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			schtasks /delete /TN "Update RPOS Clear Logs" /F
			$script:RPOSClearLogsScheduleXML = "$RPOSAUDIRLIVEUPDATEFIX\ClearLogs.xml"
			
			if ($windowsshortname -eq "Win7")
			{
				Set-RPOSCreateClearLogsXML
				schtasks /create /TN "Update RPOS Clear Logs" /XML "$RPOSClearLogsScheduleXML"
				Remove-Item "$RPOSClearLogsScheduleXML" -Force
			}
			elseif (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
			{
				Set-RPOSCreateClearLogsXMLWIn10
			}
			
		}
		Else
		{
			Write-Output "CHECKING UPDATE AUTO UPDATE CLEAR LOGS SCHEDULED TASK                    : PASSED" | Out-File -Append -FilePath $Log
		}
		
		if ((!$task4) -or (!$task5))
		{
			Write-Output "PC is Update RPOS A or B Task Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			
			#Script Param variables from script to add to Set-RPOSModuleDownloadHashSetup
			$script:downloadtimes = "0"
			$script:downloadfile = $httpfixabtaskszip
			$script:localfile = $localfixabtaskszip
			$script:downloadMD5 = "$i"
			Set-RPOSModuleDownloadHashSetup -downloadfile $downloadfile -localfile $localfile -downloadMD5 $downloadMD5
			
			$checkMD5download = (Get-ItemProperty -Path $LANDeskKeys -Name "DownloadMD5HashModule")."DownloadMD5HashModule"
			if ($checkMD5download -eq "1")
			{
				Expand-Archive -Path $localfixabtaskszip -DestinationPath $RPOSAUDIRUPDATE
				Start-Process $localfixabtasksexe -Wait
				Start-Sleep -Seconds 10
				Remove-Item "$localfixabtasksexe" -Force
				Remove-Item "$localfixabtaskszip" -Force
			}
			Else
			{
			Write-Output "RPOS LiveUpdate did not download the file correctly $downloadtime(s) waiting until the next check to correct tasks" | Out-File -Append -FilePath $Log
			}
		}
		Else
		{
			Write-Output "CHECKING UPDATE RPOS A & B SCHEDULED TASKS                               : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Adding Newer Check to make sure that Update RPOS Local A&B Tasks are working 4/25/2021
		##############START HERE!!! ###############################################################

		if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
		{
			$currenttaskdate = (Get-Date).ToString("M/dd/yyyy")
			$previoustaskdate = ((Get-Date).AddDays(-1).ToString("M/dd/yyyy"))
			$3daypreviousstaskdate = ((Get-Date).AddDays(-2).ToString("M/dd/yyyy"))
			$4daypreviousstaskdate = ((Get-Date).AddDays(-3).ToString("M/dd/yyyy"))
			$5daypreviousstaskdate = ((Get-Date).AddDays(-4).ToString("M/dd/yyyy"))
			$6daypreviousstaskdate = ((Get-Date).AddDays(-5).ToString("M/dd/yyyy"))
			$7daypreviousstaskdate = ((Get-Date).AddDays(-6).ToString("M/dd/yyyy"))
		}
		Else
		{
			$currenttaskdate = (Get-Date).ToString("M/d/yyyy")
			$previoustaskdate = ((Get-Date).AddDays(-1).ToString("M/d/yyyy"))
			$3daypreviousstaskdate = ((Get-Date).AddDays(-2).ToString("M/d/yyyy"))
			$4daypreviousstaskdate = ((Get-Date).AddDays(-3).ToString("M/d/yyyy"))
			$5daypreviousstaskdate = ((Get-Date).AddDays(-4).ToString("M/d/yyyy"))
			$6daypreviousstaskdate = ((Get-Date).AddDays(-5).ToString("M/d/yyyy"))
			$7daypreviousstaskdate = ((Get-Date).AddDays(-6).ToString("M/d/yyyy"))
		}
		
		if ($windowsshortname -eq "Win7")
		{
			$sched = New-Object -Com "Schedule.Service"
			$sched.Connect()
			$out = @()
			$sched.GetFolder("\").GetTasks(1) | % {
				$xml = [xml]$_.xml
				$out += New-Object psobject -Property @{
					"Name" = $_.Name
					"Status" = switch ($_.State) { 0 { "Unknown" } 1 { "Disabled" } 2 { "Queued" } 3 { "Ready" } 4 { "Running" } }
					"LastRunTime" = $_.LastRunTime
					"LastRunResult" = $_.LastTaskResult
				}
			}
			
			$out | Where-Object { $_.Name -match "Update RPOS Local" -and $_.Status -notmatch "Disabled" } | fl Name, LastRunTime, LastRunResult | Out-File "$RPOSAUDIRHOME\RPOSRunABTaskRunStatus.txt"
			[string]$IRanToday = (Get-Content "$RPOSAUDIRHOME\RPOSRunABTaskRunStatus.txt" | findstr "LastRunTime")
			
		}
		if (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
		{
			[string]$TaskA = (schtasks /tn "Update RPOS Local A" /query | findstr "Update RPOS Local A").substring(64)
			[string]$TaskB = (schtasks /tn "Update RPOS Local B" /query | findstr "Update RPOS Local B").substring(64)
			
			if ($TaskA -notmatch "Disabled")
			{
				[string]$IRanToday = (Get-ScheduledTaskInfo -TaskName "Update RPOS Local A" | Select-Object -ExpandProperty LastRunTime).ToString("M/dd/yyyy")
			}
			if ($TaskB -notmatch "Disabled")
			{
				[string]$IRanToday = (Get-ScheduledTaskInfo -TaskName "Update RPOS Local B" | Select-Object -ExpandProperty LastRunTime).ToString("M/dd/yyyy")
			}
		}
		
		#Grabbing the previous count value from the LANDesk Registry Folder
		[int]$count = (Get-ItemProperty -Path $LANDeskKeys -Name "AURunABTaskFailure" -Erroraction SilentlyContinue).AURunABTaskFailure
		
		[int]$datevailidation = "0"
		
		if ($IRanToday -match $currenttaskdate)
		{
			$datevailidation += 1
		}
		if (($IRanToday -match $previoustaskdate) -and ($datevailidation -ne "1"))
		{
			$datevailidation += 1
		}
		if (($IRanToday -match $3daypreviousstaskdate) -and ($datevailidation -ne "1"))
		{
			$datevailidation += 1
		}
		if (($IRanToday -match $4daypreviousstaskdate) -and ($datevailidation -ne "1"))
		{
			$datevailidation += 1
		}
		if (($IRanToday -match $5daypreviousstaskdate) -and ($datevailidation -ne "1"))
		{
			$datevailidation += 1
		}
		if (($IRanToday -match $6daypreviousstaskdate) -and ($datevailidation -ne "1"))
		{
			$datevailidation += 1
		}
		if (($IRanToday -match $7daypreviousstaskdate) -and ($datevailidation -ne "1"))
		{
			$datevailidation += 1
		}
		
		if (($datevailidation -eq "0") -or (!$IRanToday))
		{
			#making a single variable
			[string]$rposiammidnight = "${liveupdatetimehours}${liveupdatetimeAMPM}"
			
			if ($rposiammidnight -ne "12AM")
			{
				Write-Output "RPOS Auto-Updater A&B Tasks has not ran today adding 1 to $count" | Out-File -Append -FilePath $Log
				
				[int]$count = (Get-ItemProperty -Path $LANDeskKeys -Name "AURunABTaskFailure").AURunABTaskFailure
				$count += 1
				New-ItemProperty -Path $LANDeskKeys -Name "AURunABTaskFailure" -Value "$count" -Type String -Force
			}
		}
		if ($count -ge "8")
		{
			Write-Output "The RPOS Auto-Updater Local A&B Tasks have not made logs in the past 2 days, Deleting old task and creating newer ones." | Out-File -Append -FilePath $Log
			
			Write-Output "PC is Update RPOS A or B Task Scheduled Task Repairing" | Out-File -Append -FilePath $Log
			
			#Script Param variables from script to add to Set-RPOSModuleDownloadHashSetup
			$script:downloadtimes = "0"
			$script:downloadfile = $httpfixabtaskszip
			$script:localfile = $localfixabtaskszip
			$script:downloadMD5 = "$i"
			Set-RPOSModuleDownloadHashSetup -downloadfile $downloadfile -localfile $localfile -downloadMD5 $downloadMD5
			
			$checkMD5download = (Get-ItemProperty -Path $LANDeskKeys -Name "DownloadMD5HashModule")."DownloadMD5HashModule"
			if ($checkMD5download -eq "1")
			{
				Expand-Archive -Path $localfixabtaskszip -DestinationPath $RPOSAUDIRUPDATE
				Start-Process $localfixabtasksexe -Wait
				Start-Sleep -Seconds 10
				Remove-Item "$localfixabtasksexe" -Force
				Remove-Item "$localfixabtaskszip" -Force
			}
			Else
			{
				Write-Output "RPOS LiveUpdate did not download the file correctly $downloadtime(s) waiting until the next check to correct tasks" | Out-File -Append -FilePath $Log
			}
			
			
			New-ItemProperty -Path $LANDeskKeys -Name "AURunABTaskFailure" -Value "0" -Type String -Force
			
		}
		
		if ($IRanToday -match $currenttaskdate)
		{
			Write-Output "CHECKING UPDATE RPOS A & B SCHEDULED RUNNING                             : PASSED" | Out-File -Append -FilePath $Log
		}
		
		if ($windowsshortname -eq "Win7")
		{
			Remove-Item "$RPOSAUDIRHOME\RPOSRunABTaskRunStatus.txt" -Force
		}
		
		#Adding Newer Check to make sure that Update RPOS Local A&B Tasks are working 4/25/2021
		##############END HERE!!! ###############################################################
		
		$shoprposuser1 = Invoke-Command { net localgroup administrators } | Where-Object { $_ -match "shprorpos" } | Select-Object -First 1
		$shoprposuser2 = Invoke-Command { net localgroup administrators } | Where-Object { $_ -match "shprorpos2" } | Select-Object -Last 1
		
		#Checking to make sure that the two shoprpos users are installed correctly on the PC
		if ($shoprposuser1 -ne "TBC\shprorpos")
		{
			Write-Output "TBC\shprorpos was missing from the PC, Adding User to PC" | Out-File -Append -FilePath $Log
			net localgroup "Administrators" /add TBC\shprorpos
			
		}
		if ($shoprposuser2 -ne "TBC\shprorpos2")
		{
			Write-Output "TBC\shprorpos2 was missing from the PC, Adding User to PC" | Out-File -Append -FilePath $Log
			net localgroup "Administrators" /add TBC\shprorpos2
			
		}
		if (($shoprposuser1 -eq "TBC\shprorpos") -and ($shoprposuser2 -eq "TBC\shprorpos2"))
		{
			Write-Output "CHECKING SHOPRPOS ADMINISTRATORS                                         : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Checking Ivanti Registry Keys
		if (($ADcount -eq $null) -or ($AdobeAirVer -eq $null) -or ($AdobeDistro -eq $null) -or ($AuVerUpdateDate -eq $null) -or ($BGInfoVer -eq $null) -or ($BGInfoXMLCS -eq $null) -or ($EMVGateway -eq $null) -or ($EMVIPAddress -eq $null) -or ($FMChecksum -eq $null) -or ($InstallComplete -eq $null) -or ($RPOSAUVerPC -eq $null) -or ($RPOSChecksum -eq $null) -or ($RPOSDCount -eq $null) -or ($RPOSDLS -eq $null) -or ($RPOSUDate -eq $null) -or ($RPOSVersion -eq $null) -or ($RPOSVersionLabel -eq $null) -or ($RPOSAUVer -eq $null) -or ($SwitchXMLchecksum -eq $null) -or ($desktopAUCheckInternet -eq $null) -or ($desktopAUCheckAdobeAIR -eq $null) -or ($desktopAUAdobeAIRNI -eq $null) -or ($desktopAUAdobeAIRInstalling -eq $null) -or ($desktopAUCheckBGInfoEMV -eq $null) -or ($desktopAUCheckSelfSubDownload -eq $null) -or ($desktopAUClosingWindows -eq $null) -or ($desktopAUUninstallRPOS -eq $null) -or ($desktopAUInstallingRPOS -eq $null) -or ($desktopAUCheckInstall -eq $null) -or ($desktopAUFailedLocal -eq $null) -or ($desktopAULocalReinstall -eq $null) -or ($desktopAUCompleteReinstall -eq $null) -or ($desktopAUGatherReportingData -eq $null) -or ($RPOSAUVer -ne $ReleaseInfo) -or ($TeslaDcountKey -eq $null) -or ($TeslaDistroKey -eq $null) -or ($TeslaVerKey -eq $null) -or ($endpointsecurityendpoint -eq $null) -or ($endpointservicesendpoint -eq $null) -or ($endpointkatanaendpoint -eq $null) -or ($endpointenablesecurity -eq $null) -or ($endpointenableservices -eq $null) -or ($endpointenablekatana -eq $null) -or ($endpointchecksumnum -eq $null))
		{
			
			if ($ADcount -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\Adobe DCount" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "Adobe DCount" -Value "0" -Force
			}
			
			if ($AdobeAirVer -eq $null)
			{
				
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AdobeAIRVer" | Out-File -Append -FilePath $Log
				$AdobeAIRFinalverLocal = Get-WmiObject -Class Win32_Product | Where-Object { $_.Name -like "*Adobe AIR*" } | Select-Object -ExpandProperty Version
				New-ItemProperty -Path "$LANDeskKeys" -Name "AdobeAIRVer" -Value "$AdobeAIRFinalverLocal" -Force
			}
			
			if ($AdobeDistro -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AdobeDistro" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AdobeDistro" -Value "0" -Force
			}
			
			if ($AuVerUpdateDate -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AuVer UpdateDate" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AuVer UpdateDate" -Value "0" -Force
			}
			
			if ($BGInfoVer -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\BGInfoVersion" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "BGInfoVersion" -Value "0" -Force
			}
			
			if ($BGInfoXMLCS -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\BGInfoXMLchecksum" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "BGInfoXMLchecksum" -Value "0" -Force
			}
			
			if ($EMVGateway -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\EMVGateway" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "EMVGateway" -Value "0" -Force
			}
			
			if ($EMVIPAddress -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\EMVIPaddress" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "EMVIPaddress" -Value "0" -Force
			}
			
			if ($FMChecksum -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\FlowModule Checksum" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "FlowModule Checksum" -Value "0" -Force
			}
			
			if ($InstallComplete -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\InstallComplete" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "InstallComplete" -Value "0" -Force
			}
			
			if (($RPOSAUVerPC -eq $null) -or ($RPOSAUVer -ne $ReleaseInfo))
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Auto-Updater" | Out-File -Append -FilePath $Log
				$rposauversion = $ReleaseInfo
				if ($TwoDigPCName -eq "01")
				{
					New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Auto-Updater" -Value "$rposauversion.1" -Force
				}
				
				if ($TwoDigPCName -eq "02")
				{
					New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Auto-Updater" -Value "$rposauversion.2" -Force
				}
				
				if (($TwoDigPCName -notmatch "01") -and ($TwoDigPCName -notmatch "02"))
				{
					New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Auto-Updater" -Value "$rposauversion.3" -Force
				}
				
			}
			
			if ($RPOSChecksum -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Checksum" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Checksum" -Value "0" -Force
			}
			
			if ($RPOSDCount -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS DCount" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS DCount" -Value "0" -Force
			}
			
			if ($RPOSDLS -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS DL Source" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS DL Source" -Value "0" -Force
			}
			
			if ($RPOSUDate -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Update Date" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Update Date" -Value "0" -Force
			}
			
			if ($RPOSVersion -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Version" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Version" -Value "0" -Force
			}
			
			if ($RPOSVersionLabel -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOS Version Label" | Out-File -Append -FilePath $Log
				[xml]$xmlreadRPOSversion = Get-Content $RPOSVersionLabelXML
				$RPOSxmlversion = $xmlreadRPOSversion.application.versionLabel
				
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Version Label" -Value "$RPOSxmlversion" -Force
			}
			
			if (($RPOSAUVer -eq $null) -or ($RPOSAUVer -ne $ReleaseInfo))
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\RPOSAUVer" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "RPOSAUVer" -Value "$ReleaseInfo" -Force
			}
			
			if ($SwitchXMLchecksum -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\SwitchXMLchecksum" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "SwitchXMLchecksum" -Value "0" -Force
			}
			
			if ($TeslaDcountKey -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\Tesla DCount" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "Tesla DCount" -Value "0" -Force
			}
			
			if ($TeslaDistroKey -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\TeslaDistro" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "TeslaDistro" -Value "0" -Force
			}
			
			if ($TeslaVerKey -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\TeslaVer" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "TeslaVer" -Value "0" -Force
			}
			
			if ($desktopAUCheckInternet -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUCheckInternet" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUCheckInternet" -Value "0" -Force
			}
			if ($desktopAUCheckAdobeAIR -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUCheckAdobeAIR" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUCheckAdobeAIR" -Value "0" -Force
			}
			if ($desktopAUAdobeAIRNI -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUAdobeAIRNI" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUAdobeAIRNI" -Value "0" -Force
			}
			if ($desktopAUAdobeAIRInstalling -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUAdobeAIRInstalling" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUAdobeAIRInstalling" -Value "0" -Force
			}
			if ($desktopAUCheckBGInfoEMV -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUCheckBGInfoEMV" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUCheckBGInfoEMV" -Value "0" -Force
			}
			if ($desktopAUCheckSelfSubDownload -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUCheckSelfSubDownload" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUCheckSelfSubDownload" -Value "0" -Force
			}
			if ($desktopAUClosingWindows -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUClosingWindows" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUClosingWindows" -Value "0" -Force
			}
			if ($desktopAUUninstallRPOS -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUUninstallRPOS" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUUninstallRPOS" -Value "0" -Force
			}
			if ($desktopAUInstallingRPOS -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUInstallingRPOS" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUInstallingRPOS" -Value "0" -Force
			}
			if ($desktopAUCheckInstall -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUCheckInstall" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUCheckInstall" -Value "0" -Force
			}
			if ($desktopAUFailedLocal -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUFailedLocal" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUFailedLocal" -Value "0" -Force
			}
			if ($desktopAULocalReinstall -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AULocalReinstall" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AULocalReinstall" -Value "0" -Force
			}
			if ($desktopAUCompleteReinstall -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUCompleteReinstall" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUCompleteReinstall" -Value "0" -Force
			}
			if ($desktopAUGatherReportingData -eq $null)
			{
				Write-Output "Following registry key was missing and/or damaged $LANDeskKeys\AUGatherReportingData" | Out-File -Append -FilePath $Log
				New-ItemProperty -Path "$LANDeskKeys" -Name "AUGatherReportingData" -Value "0" -Force
			}
			
			if ($RPOSAgentname -eq "Store") {
				
				if (($endpointsecurityendpoint -eq $null) -or (!$endpointsecurityendpoint)) {
					# Getting endpoint information from RPOS.tbccorp.com from rposendpoints xml
					Write-Output "Grabbing contents of Endpoint XML For Endpoint Security" | Out-File -Append -Filepath $Log
					$xmlread = [xml](New-Object System.Net.WebClient).DownloadString($EndpointXMLURL)
					$onlinexmlsecurityendpoint = $xmlread.customendpoint.$switchvalue.security
					
					Write-Output "Following registry key was missing and/or damaged RPOS Security Endpoint changing key back to $onlinexmlsecurityendpoint" | Out-File -Append -Filepath $Log
					Set-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Security Endpoint" -Value $onlinexmlsecurityendpoint -Type String -Force
				}
				
				if (($endpointservicesendpoint -eq $null) -or (!$endpointservicesendpoint)) {
					# Getting endpoint information from RPOS.tbccorp.com from rposendpoints xml
					Write-Output "Grabbing contents of Endpoint XML For Services Endpoint" | Out-File -Append -Filepath $Log
					$xmlread = [xml](New-Object System.Net.WebClient).DownloadString($EndpointXMLURL)
					$onlinexmlservicesendpoint = $xmlread.customendpoint.$switchvalue.services
					
					Write-Output "Following registry key was missing and/or damaged RPOS Services Endpoint changing key back to $onlinexmlservicesendpoint" | Out-File -Append -Filepath $Log
					Set-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Services Endpoint" -Value $onlinexmlservicesendpoint -Type String -Force
				}
				
				if (($endpointkatanaendpoint -eq $null) -or (!$endpointkatanaendpoint)) {
					# Getting endpoint information from RPOS.tbccorp.com from rposendpoints xml
					Write-Output "Grabbing contents of Endpoint XML For Services Endpoint" | Out-File -Append -Filepath $Log
					$xmlread = [xml](New-Object System.Net.WebClient).DownloadString($EndpointXMLURL)
					$onlinexmlkatanaendpoint = $xmlread.customendpoint.$switchvalue.katana
					
					Write-Output "Following registry key was missing and/or damaged RPOS Katana Endpoint changing key back to $onlinexmlkatanaendpoint" | Out-File -Append -Filepath $Log
					Set-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Katana Endpoint" -Value $onlinexmlkatanaendpoint -Type String -Force
				}
				
				if (($endpointenablesecurity -eq $null) -or (!$endpointenablesecurity)) {
					# Getting endpoint information from RPOS.tbccorp.com from rposendpoints xml
					Write-Output "Grabbing contents of Endpoint XML For Services Endpoint" | Out-File -Append -Filepath $Log
					$xmlread = [xml](New-Object System.Net.WebClient).DownloadString($EndpointXMLURL)
					$onlinexmlenablesecurity = $xmlread.customendpoint.$switchvalue.enablesecurity
					
					if ($onlinexmlenablesecurity -eq "true") {
						$onlinexmlenablesecurity = "1" }
					if ($onlinexmlenablesecurity -eq "false") {
						$onlinexmlenablesecurity = "0" }
					
					Write-Output "Following registry key was missing and/or damaged RPOS Enable Security changing key back to $onlinexmlenablesecurity" | Out-File -Append -Filepath $Log
					Set-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Security Enable" -Value $onlinexmlenablesecurity -Type String -Force
				}
				
				if (($endpointenableservices -eq $null) -or (!$endpointenableservices)) {
					# Getting endpoint information from RPOS.tbccorp.com from rposendpoints xml
					Write-Output "Grabbing contents of Endpoint XML For Services Endpoint" | Out-File -Append -Filepath $Log
					$xmlread = [xml](New-Object System.Net.WebClient).DownloadString($EndpointXMLURL)
					$onlinexmlenableservices = $xmlread.customendpoint.$switchvalue.enableservices
					
					if ($onlinexmlenableservices -eq "true") {
						$onlinexmlenableservices = "1" }
					if ($onlinexmlenableservices -eq "false") {
						$onlinexmlenableservices = "0" }
					
					Write-Output "Following registry key was missing and/or damaged RPOS Enable Services changing key back to $onlinexmlenableservices" | Out-File -Append -Filepath $Log
					Set-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Services Enable" -Value $onlinexmlenableservices -Type String -Force
				}
				
				if (($endpointenablekatana -eq $null) -or (!$endpointenablekatana)) {
					# Getting endpoint information from RPOS.tbccorp.com from rposendpoints xml
					Write-Output "Grabbing contents of Endpoint XML For Services Endpoint" | Out-File -Append -Filepath $Log
					$xmlread = [xml](New-Object System.Net.WebClient).DownloadString($EndpointXMLURL)
					$onlinexmlenablekatana = $xmlread.customendpoint.$switchvalue.enablekatana
					
					if ($onlinexmlenablekatana -eq "true") {
						$onlinexmlenablekatana = "1" }
					if ($onlinexmlenablekatana -eq "false") {
						$onlinexmlenablekatana = "0" }
					
					Write-Output "Following registry key was missing and/or damaged RPOS Enable Katana changing key back to $onlinexmlenablekatana" | Out-File -Append -Filepath $Log
					Set-ItemProperty -Path "$LANDeskKeys" -Name "RPOS Katana Enable" -Value $onlinexmlenablekatana -Type String -Force
				}
				
				if (($endpointchecksumnum -eq $null) -or (!$endpointchecksumnum))
				{
					Write-Output "Endpoint Checksum Number for store locations is missing replacing keys" | Out-File -Append -Filepath $Log
					Set-ItemProperty -Path "$LANDeskKeys" -Name "EndpointXMLchecksum" -Value "0" -Type String -Force
				}
			}		
		}
		Else
		{
			Write-Output "CHECKING IVANTI REGISTRY KEYS                                            : PASSED" | Out-File -Append -FilePath $Log
		}
		
		#Adding User scheduled task for the Local GUI
		New-PSDrive HKU Registry HKEY_USERS -ErrorAction SilentlyContinue
		
		#Setting up scheduled tasks for the user gui
		$script:username = "${env:USERDOMAIN}\${env:USERNAME}"
		
		$usersids = Get-ChildItem "HKU:\" | Select-Object Name | Where-Object { $_.Name -notmatch "S-1-5-19" -and $_.Name -notmatch "S-1-5-20" -and $_.Name -notmatch "S-1-5-80" -and $_.Name -notmatch "_Classes" -and $_.Name -notmatch ".DEFAULT" -and $_.Name -notmatch "S-1-5-18" -and $_.Name -ne "HKEY_USERS\S-1-5-21-2099265403-187179977-793597544-563469" -and $_.Name -ne "HKEY_USERS\S-1-5-21-2099265403-187179977-793597544-604706" -and $_.Name -ne "HKEY_USERS\S-1-5-21-603379513-1108270791-2756989727-1005" } | Select-Object -ExpandProperty Name
		
		Foreach ($usersid in $usersids)
		{
			[string]$userid = $usersid
			$script:useridsub = $usersid.Substring(11)
			
			$objSID = New-Object System.Security.Principal.SecurityIdentifier ` ("$useridsub")
			$objUser = $objSID.Translate([System.Security.Principal.NTAccount])
			$script:username = $objUser.Value
			
			if ($windowsshortname -eq "Win7")
			{
				$userquery = schtasks /query /TN "\RPOS AU Local User GUI $useridsub"
			}
			elseif (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
			{
				$userquery = Get-ScheduledTask -TaskName "RPOS AU Local User GUI $useridsub" | Select-Object -ExpandProperty Taskname
			}
			
			if (!$userquery)
			{
				if ($windowsshortname -eq "Win7")
				{
					$script:RPOSCreateScheduledTaskGUIXML = "C:\Temp\CreateRPOSUserScheduledTask.xml"
					Set-RPOSCreateUserScheduledTask
					schtasks /create /TN "\RPOS AU Local User GUI $useridsub" /XML "$RPOSCreateScheduledTaskGUIXML"
					Remove-Item "$RPOSCreateScheduledTaskGUIXML" -Force
				}
				elseif (($windowsshortname -eq "Win10") -or ($windowsshortname -eq "WinSRV"))
				{
					#Creating the user scheduled task for Windows 10
					Set-RPOSCreateUserScheduledTaskWin10
				}
			}
		}
		
		Remove-PSDrive -Name "HKU" -Force
	}
	Else
	{
		$Log = "$RPOSAUDIRLOGS\RPOSLiveUpdate_$AutofixDate.txt"
		Write-Output "There is currently no internet connection on this PC skipping AutoFix check" | Out-File -Append -FilePath $Log
	}
	
	
	#Checking to see if there is a setup file that needs to be downloaded for further functionality
	#J equals version in XML K equals statment if it is true or not L Setup path for file if needed M  is the hash value for the file
	Write-Output "########################################################################################" | Out-File -Append -FilePath $Log
	Write-Output "Checking to see if there are anymore download files that need to be for the Auto-Updater" | Out-File -Append -FilePath $Log
	
	if (($RPOSAUVer -ne $j) -and ($k -eq "true"))
	{
		$setuplocalfile = "C:\Windows\Scripts\RPOS\update\setup.exe"
		Remove-Item $setuplocalfile -Force
		Write-Output "$RPOSAUVer is the current version of the Auto-Updater that is running on this system changing to $j" | Out-File -Append -FilePath $Log
		Write-Output "Statement equals $k and has been triggered, starting to download and install files" | Out-File -Append -FilePath $Log
		Set-RPOSModuleDownloadHashSetup -downloadfile $l -localfile $setuplocalfile -downloadMD5 $m
		
		$checkMD5download = (Get-ItemProperty -Path $LANDeskKeys -Name "DownloadMD5HashModule")."DownloadMD5HashModule"
		
		if ($checkMD5download -eq "1")
		{
			Write-Output "File has been downloaded, proceeding with setup" | Out-File -Append -FilePath $Log
			Start-Process $setuplocalfile
			Start-Sleep -Seconds 30
			Write-Output "File has been installed and ready for production use!" | Out-File -Append -FilePath $Log
		}
		Else
		{
			Write-Output "File download was not successful, trying again a different time!" | Out-File -Append -FilePath $Log
		}
	}
	Else
	{
		Write-Output "No further updates and/or changes are happening to the Auto-Updater at this time exiting" | Out-File -Append -FilePath $Log
		Write-Output "CHECKING FOR ADDITIONAL DOWNLOAD FILES                                   : PASSED" | Out-File -Append -FilePath $Log
	}
	
	
	# Added in RPOS Auto-Updater v3.8 from retiring Get-RPOSUpdateAU, this needed to be added to clear database and check endpoints
	if (($liveupdatetimehours -eq "12") -and ($liveupdatetimeminutes -lt "05") -and ($liveupdatetimesec -ge "00") -and ($liveupdatetimeAMPM -eq "AM"))
	{
		Write-Output "RPOS Auto-Updater LiveUpdate is running at midnight. Clearing Tesla Database." | Out-File -Append -FilePath $Log
		#################################################################
		# Added 5/22/2018 for Tesla Supported Install                   #
		#################################################################
		# Clearing Tesla Database during check of RPOS AU Update
		Set-RPOSModuleTeslaClearDatabase
		
		if ($RPOSAgentname -eq "Store")
		{
			Write-Output "RPOS Auto-Updater LiveUpdate is running at midnight. Checking All Endpoints Store Location ONLY" | Out-File -Append -FilePath $Log
			##################################################################
			# Added 8/2/2019 for Endpoint Customization                      #
			##################################################################
			# Checking to make sure endpoints are correct         
			Check-RPOSEndpoints
		}
	}
	
	Write-Output "##############################END OF LIVEUPDATE#########################################" | Out-File -Append -FilePath $Log
	
	#Cleaning house after module check
	#Removing LiveUpdateFix Folder
	if (Test-Path "$RPOSAUDIRLIVEUPDATEFIX")
	{
		Remove-Item "$RPOSAUDIRLIVEUPDATEFIX" -Recurse -Force
	}
}